﻿/**
 * file : /sitmapdaf.js
 * id : 20200630°1623 [after 20200221°0331]
 * encoding : UTF-8-with-BOM
 * note :
 */

var DAF_PAGENODES =
[
   [ 'CPP Scripts'        ,  './../index.html'
     , [ 'Intro into C'   ,  './../cintro/index.html' ]
     , [ 'OOP in CPP'     ,  './../cppooad/index.html'
        , [ 'Demos'       ,  './../cppooad/pages/demos.html' ]
       ]
     , [ 'Advanced CPP'   ,  './../cppadv/index.html'
        , [ 'Notes'       ,  './../cppadv/pages/notes.html' ]
       ]
   ]
];
