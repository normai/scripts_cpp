<?php
/**
 * file        : 20180331°0201 daftari/php/dbs/SQLitConn.php
 * encoding    : UTF-8-without-BOM
 * note        : Remember http://www.sqlitetutorial.net/sqlite-php/connect/ [ref 20180330°0115]
 */

namespace Trekta\Databese;

//use Trekta\Daftari as TD;
use Trekta\Daftari\Globals as Glb;
include(__DIR__ . '/../core/Globals.php');

/**
 * This class shall provide the SQLite connection
 *
 * @id 20180331°0211
 * @author ncm
 */
class SQLitConn {

   /**
    * This property shall hold the PDO instance
    *
    * @id 20180331°0221
    * @note See ref 20190312°0123 'Stackoverflow : Use of PDO in classes'
    * @note 20190312°0124 : Phan unjustifiably complains the unassigned propert
    *     with PhanUndeclaredTypeProperty, so configured it to be ignored.
    * @var Type
    */
   private $pdo = NULL; // throws PhanUndeclaredTypeProperty [phan 20190312°0124'02]

   /**
    * This method returns an instance of the PDO object that connects to the SQLite database
    *
    * @id 20180331°0231
    * @return Object The wanted PDO
    */
   public function connect() {

      require (__DIR__ . '/SQLitConf.php');

      SQLitConf::init();

      // Paranoia [seq 20190129°0951]
      // note : Remember issue 20190129°0933 'pdo fails'
      // note : See todo 20190129°1011 'implement system check'
      if ( Glb::bToggle_FALSE ) {
         //$dbg3 = \PDO::getAvailableDrivers();
         // .. implement code here to evaluate the driver list and in bad case escape ..
      }

      if ($this->pdo === NULL)
      {
         //settype($this->pdo, "object"); // [line 20190312°0125] try to avoid PhanTypeMismatchProperty below -- does not help

         try
         {
            // Instantiate new PHO object from PHP [line 20180331°0233]
            $this->pdo = new \PDO("sqlite:" . SQLitConf::$sPATH_TO_SQLITE_FILE); // PhanTypeMismatchProperty [phan 20190312°0126`11]

            echo '<p style="color:magenta;">Dbg 20210213°1511 SQLite PDO created.' . '</p>';

         } catch (\PDOException $e) {
            echo '<p style="color:magenta;">Oops 20180330°0344 : ' . $e->getMessage() . '</p>'; // message e.g. 'could not find driver', check that in php.ini line 'extension=pdo_sqlite' is active (note there exists also line ';extension=sqlite3', which we do not use.
         }
      }
      return $this->pdo;
   }
}
?>
