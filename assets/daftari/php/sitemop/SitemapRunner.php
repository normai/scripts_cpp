<?php

/**
 * This file hosts class SitemapRunner
 *
 * file      : 20140621°0321
 * license   : The MIT License (http://opensource.org/licenses/MIT)
 * copyright :
 * authors   : ncm
 * encoding  : UTF-8-without-BOM
 * status    : Proof-of-concept.
 * todo      : Finally, the Sitemap.php shall be called with an automatically
 *              generated files list, not with the manual list below.
 * todo      : Call this script via Go.php command, like all other scripts.
 * usage     : This script can be run either form the (Windows) console
 *              via the helper batchfile, or from the browser, e.g. like
 *              http://localhost/daftaridev/trunk/daftari/php/sitemop/Sitemap-caller.php
 * note      :
 */

namespace Trekta\Daftari;

/**
 * This line calls the former script-level code
 *
 * @id 20190209°0531
 */
SitemapRunner::runSitemap();

/**
 * This class calls the Sitemap.php library
 *
 * @id 20190209°0511
 */
class SitemapRunner
{

   /**
    * This method calls Sitemap.php to generate sitemap XML files
    *
    * @id 20190209°0521
    * @callers Only script-level
    */
   public static function runSitemap()
   {
      include __DIR__ . '/../../phplibs/sitemap-php/Sitemap.php';

      echo("*** Hello Sitemap.php ... ***");

      $sitemap = new \SitemapPHP\Sitemap('https://downtown.trilo.de/svn/daftaridev/trunk/');
      $sitemap->setFilename('OUTPUT.sitemap');
      $sitemap->addItem('/', '1.0', 'daily', 'Today');
      $sitemap->addItem('/index.html', '0.8', 'monthly', 'Jun 25');
      $sitemap->addItem('/daftari/index.html', '0.6', 'yearly', '14-12-2009'); // chg 20150209°1341
      $sitemap->addItem('/daftari/docs/intro.html');                   // chg 20150209°1342
      $sitemap->createSitemapIndex('http://example.com/sitemap/', 'Today');

      echo("\n*** Sitemap.php done. ***");
   }
}
/* eof */
