@echo off
rem id 20140621`0331
rem Will Sitemap.php run from the console? Yes.

color f1
title This is %~n0%~x0 by user %USERNAME%
echo ***************************************************************
echo *** thisfile = %~n0%~x0 by user %USERNAME%
echo *** thisdir  = %~dp0
echo *** currdir  = %cd%
echo *** options  = %1 %2 %3
echo ***************************************************************

%~d0
cd %~dp0

@echo on
php.exe SitemapRunner.php
@echo off

echo.
echo *** Finish %~n0%~x0
pause
