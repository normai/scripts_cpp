<?php

/**
 * This module tests the PHP Simple HTML DOM Parser
 *
 * id          : file 20110816°1624
 * license     :
 * copyright   :
 * authors     :
 * encoding    : UTF-8-without-BOM
 * note        : Using PHP Simple HTML DOM Parser 1.5 from download 20110816°2121
 *              references see http://simplehtmldom.sourceforge.net/manual.htm
 * note        : The 'PHP Simple HTML DOM Parser' has limitations: [note 20110817°1641]
 *               • It destroys file formatting when restoring the file from DOM
 *                 it's all in one line ..
 *               • It's hard to get the tag including it's attributes. You have either
 *                 'outertext' including the attributes, or 'tag' without attributes,
 *                 and there is no way to ask for the attributes without knowing them.
 * callers     : Onyl • Func daftari.js::execPage07DemoPSHDP via Ajax via Go.php
 */

namespace Trekta\Daftari;

use Trekta\Daftari\Globals as Glb;

require_once(__DIR__ . '/../../phplibs/simplehtmldom_1_8_1/simple_html_dom.php');

// for now, allow edit only on localhost
if (Glb::$Lgn_bHostIsLocalhost !== TRUE)
{
   echo 'Sorry, writing back to the file is not yet implemented.';
   return;
}

// retrieve parameters from ajax request
$sTagid = $_GET[Glb::GET_KEY_AJX_tagid];                            // 'tagid', value e.g. 'mimg.P.12'
$sFilename = $_GET[Glb::GET_KEY_AJX_file];                          // 'file'

// read raw post input [seq 20190205°0534]
// note : Compare • JsiEdit.php seq 20190205°0533 and • jsi-demo-pshdp.php seq 20190205°0534
// note : Remember issue 20190205°0531 'http_raw_post_data'
$fp = fopen("php://input", 'r');
$sAjaxmessage = stream_get_contents($fp);

// refine parameters
$aTagid = explode('.', $sTagid);
$sTag = $aTagid[1];
$iIndex = $aTagid[2] - 1;

// load live file
$s = __DIR__ . '/../../manual/blogs.html';
$s = realpath($s);
$html = \file_get_html($s);

// spot target element
// note : $e->getElementsByTagName ( $name [, $index] )
//  with w3c standard camel naming conventions is also possible
$eTarget = $html->find($sTag, $iIndex);
$sInner = $eTarget->innertext;

// write target element new value
$eTarget->innertext = $sInner . $sAjaxmessage;

// build output
$sEcho = '[' . $aTagid[0] . '.' . $sTag . '.' . $iIndex . ':] ' . $eTarget->innertext;

$eTgt = $html->find('html', 0);
$sEcho2 = formattedHtml($eTgt, 0, '');

if (Glb::bToggle_FALSE) // temporarily switched off
{
   $sContent = '<html><head><title>Oha</title></head><body><p>Oha</p></body></html>';
   $i = file_put_contents('../notes.html', $sContent);
}

if (Glb::bToggle_FALSE)
{
   echo $sEcho;
}
else
{
   echo $sEcho2;
}


/**
 * This function builds the HTML fragment for the given DOM element
 *
 * @id 20110817°1541
 * @status Recursion works, but exact text building has to be finished
 * @note This function is not ready and already abandoned. Let us preserve
 *     it a while, since the recursion works fine. Perhaps we can reuse it.
 * @callers • from self • recursively from inside the function itself
 * @param &$eleme {Element} The DOM element to be printed
 * @param $iIndent {Integer} The wanted indent per level
 * @param $sText {String} ~ The so far built fragment
 * @return String The wanted HTML fragment
 */
function formattedHtml($eleme, $iIndent, $sText) : String
{
   $sCR = Glb::$sTkNL;

   $sIndent = str_repeat(' ', $iIndent);
   $els = $eleme->children();

   if (sizeof($els) > 0)
   {
      foreach ($els as $el)
      {
         if (      ($el->tag == 'a')
             or    ($el->tag == 'link')
              or   ($el->tag == 'meta')
               or  ($el->tag == 'script')
                or ($el->tag == 'title')
                 )
         {
            $sText .= $sCR . $sIndent . $el->outertext;
         }
         else if ($el->tag === 'comment')                              // comment is already implicite with '!--'
         {
            $sText .= $sCR . $sIndent . $el->innertext;
         }
         else
         {
            $sText .= $sCR . $sIndent . '<' . $el->tag . '>';
            $sText = formattedHtml($el, $iIndent+1, $sText);
            $sText .= $sCR . $sIndent . '</' . $el->tag . '>';
         }
      }
   }
   else
   {
      $sText .= $sCR . $sIndent . $eleme->innertext;
   }

   return $sText;
}

/* eof */
