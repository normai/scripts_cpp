<?php

/**
 * This module ...
 *
 * id : file 20110818°0021
 * encoding : UTF-8-without-BOM
 * ref : http://www.php.net/manual/de/class.domimplementation.php [ref 20110817°1632]
 * note : The bottomline is as follows. It works, but writing back breaks things:
 *    • in table cells, links in paragraph are no links anymore
 *    • the image is lost
 *    • '&nbsp;' is changed to mysterious character
 * note : Remember "$dom = new DOMDocument('1.0','utf-8');" utf-8 is default anyway.
 */

//use Trekta\Daftari as TD;
//use Trekta\Daftari\Globals as Glb;

$x = new DOMImplementation();
//$doc = $x->createDocument(NULL, 'html'); // 'html' is just root element name
$doc = $x->createDocument('', 'html'); // ? // 'html' is just root element name
$doc->xmlVersion = "1.0";
//$doc->xmlEncoding = "UTF-8"; // error "Cannot write property .."

$doc->substituteEntities = false; // true; // EXPERIMENTAL - does not affect the broken '&nbsp;'

$doc->loadHTMLFile('.\..\index.html');

// The test - this works
// (issue 20110817°171102) getElementsByTagName() makes problems, but here it succeeds
$eles = $doc->getElementsByTagName('p');
foreach ($eles as $el)
{
   $s = $el->nodeValue;
   $el->nodeValue = $s . 'XXX';
}

// output
$doc->formatOutput = true;

$s = $doc->saveHTML();

echo $s;

/* eof */
