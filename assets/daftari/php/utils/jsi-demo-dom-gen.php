<?php

/**
 * This module ...
 *
 * id          : file 20110817°1651
 * license     :
 * copyright   :
 * authors     :
 * encoding    : UTF-8-without-BOM
 * note        : Compare http://www.ultramegatech.com/blog/2009/07/generating-xhtml-documents-using-domdocument-in-php [ref 20110817°1631]
 * note        : Here we can experiment with tags making problems when read from
 *                existing file, e.g. 'a' (when in 'p' inside 'td') or 'img'
 * note        :
 */

//use Trekta\Daftari as TD;
use Trekta\Daftari\Globals as Glb;

// Create document
$x1 = new DOMImplementation();
$doctype = $x1->createDocumentType
                       ( 'html'
                        , '-//W3C//DTD XHTML 1.1//EN'
                         , 'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd'
                          );

$x2 = new DOMImplementation();
$document = $x2->createDocument
                       ( 'http://www.w3.org/1999/xhtml'
                        , 'html'
                         , $doctype
                          );

// Create head element
$head = $document->createElement('head');

$metahttp = $document->createElement('meta');
$metahttp->setAttribute('http-equiv', 'Content-Type');
$metahttp->setAttribute('content', 'text/html; charset=UTF-8');
$head->appendChild($metahttp);

$title = $document->createElement('title', 'DOMDocument');
$head->appendChild($title);

$css = $document->createElement('link');
$css->setAttribute('href', 'styles.css'); // is it 'daftari.css'? [line 20150209°1312]
$css->setAttribute('rel', 'stylesheet');
$css->setAttribute('type', 'text/css');
$head->appendChild($css);

// Create body element
$body = $document->createElement('body');

// Wrapper div
$wrapper = $document->createElement('div');
$wrapper->setAttribute('id', 'wrapper');

// Header div
$header = $document->createElement('div');
$header->setAttribute('id', 'header');
$wrapper->appendChild($header);

// Header img
$himg = $document->createElement('img');
$himg->setAttribute('src', 'header.gif');
$himg->setAttribute('alt', 'Header');
$himg->setAttribute('width', '400');
$himg->setAttribute('height', '100');
$header->appendChild($himg);

// Nav div
$nav = $document->createElement('div');
$nav->setAttribute('id', 'nav');
$wrapper->appendChild($nav);

// Nav ul
$navlist = $document->createElement('ul');
$nav->appendChild($navlist);

$menuArray = array();
$menuArray['index.php'] = 'Home';
$menuArray['download.php'] = 'Download';
$menuArray['generate.php'] = 'Generate';
$menuArray['about.php'] = 'About';

$currentPage = basename($_SERVER[Glb::SVR_KEY_SCRIPT_FILENAME]); // 'SCRIPT_FILENAME'

foreach($menuArray as $page => $title)
{
   $navitem = $document->createElement('li');
   if ( $page == $currentPage )
   {
      $navitem->setAttribute('class', 'active');
   }

   $navlink = $document->createElement('a', $title);
   $navlink->setAttribute('href', $page);

   $navitem->appendChild($navlink);
   $navlist->appendChild($navitem);
}

// Content div
$content = $document->createElement('div');
$content->setAttribute('id', 'content');
$wrapper->appendChild($content);

// Actual page content
$h1 = $document->createElement('h1', 'DOMDocument');
$content->appendChild($h1);

$p = $document->createElement('p');
$text = $document->createTextNode('This page was generated using PHP\'s ');
$p->appendChild($text);
$text = $document->createElement('strong', 'DOMDocument');
$p->appendChild($text);
$text = $document->createTextNode(' class!');
$p->appendChild($text);
$content->appendChild($p);

$body->appendChild($wrapper);

// Add head and body to document
$html = $document->getElementsByTagName('html')->item(0);
$html->appendChild($head);
$html->appendChild($body);

//-----------------------------------------------------
// [issue 20110817°171101] getElementsByTagName() makes problems, but here it succeeds
$eles = $document->getElementsByTagName('li');
foreach ($eles as $el)
{
   $s = $el->nodeValue;
}
//-----------------------------------------------------

// Output document
$document->formatOutput = true;

// alternative savings
if (Glb::bToggle_FALSE)
{
   $s = $document->saveXML();
}
else
{
   $s = $document->saveHTML();
}

echo $s;

/* eof */
