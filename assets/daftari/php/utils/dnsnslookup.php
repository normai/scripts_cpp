<?php

/**
 * This module retrieves a hostname from an IP address and vice versa.
 *
 * id        : file 20110525°1211
 * license   :
 * copyright :
 * authors   :
 * status    :
 * encoding  : UTF-8-without-BOM
 * ref       : Joerg Krause, 'Kochbuch PHP5' page 138 [book 20101220°2021]
 * callers   : login.html
 * note      :
 */

use Trekta\Daftari\Globals as Glb;

// [seq 20110525°1221]
$hostname = '';
$hostnameresult = '';
if (isset($_POST[Glb::POSTKEY_TOL_DNS_hostname]))                      // 'hostname'
{
    $hostname = $_POST[Glb::POSTKEY_TOL_DNS_hostname];                 // 'hostname'
    $hostnameresult = gethostbyname($hostname);
}
$ipaddress = '';
$ipaddressresult = '';
if (isset($_POST[Glb::POSTKEY_TOL_DNS_ipaddress]))                     // 'ipaddress'
{
    $ipaddress = $_POST[Glb::POSTKEY_TOL_DNS_ipaddress];               // 'ipaddress'
    try
    {
       if ($ipaddress == '')
       {
          $ipaddressresult = '';
       }
       else if (intval($ipaddress) > 0)
       {
          $ipaddressresult = gethostbyaddr($ipaddress);
       }
       else
       {
          $ipaddressresult = '(wrong)';
       }
    }
    catch (Exception $e)
    {
       $ipaddressresult = $e;
    }
}

?>

<form method="post" action="<?php $_SERVER[Glb::SVR_KEY_PHP_SELF] ?>" id="id20201128o0312">
<p class="PanelSectionTitle" id="id20201128o0313">
               DnsNsLookup &nbsp;&nbsp;&nbsp;
               <input type="submit" value="Lookup"/>
</p>
<table id="id20201128o0314">
 <tr id="id20201128o0315">
  <td id="id20201128o0316"><label>Hostname</label></td>
  <td id="id20201128o0317">&nbsp;:&nbsp;<input type="text" name="hostname" value="<?php echo($hostname) ?>" size="15" maxlength="100"/></td><!-- fix 20110525°1231 -->
  <td id="id20201128o0322">&nbsp;&gt;&nbsp;IP&#8209;Address</td>
  <td id="id20201128o0323">&nbsp;=&nbsp;<span style="font-weight:bold;"><?php echo($hostnameresult) ?></span></td>
 </tr>
 <tr id="id20201128o0324">
  <td id="id20201128o0325"><label>IP&#8209Address</label></td>
  <td id="id20201128o0326">&nbsp;:&nbsp;<input type="text" name="ipaddress" value="<?php echo($ipaddress) ?>" size="15" maxlength="100"/></td>
  <td id="id20201128o0327">&nbsp;&gt;&nbsp;Hostname&nbsp;</td>
  <td id="id200201128o0332">&nbsp;=&nbsp;<span style="font-weight:bold;"><?php echo($ipaddressresult) ?></span></td>
 </tr>
</table>
</form>

<!-- eof -->
