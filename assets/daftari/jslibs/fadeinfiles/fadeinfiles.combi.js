/*
 This script dynamically displays text files inside an HTML page

 Version : v0.4.0 — 20240428°1757 'To be scrutinized'
 License : BSD 3-Clause License
 Copyright : © 2014 - 2024 Norbert C. Maier
*/
'use strict';
var Trekta = Trekta || {};
Trekta.Fadin = Trekta.Fadin || {};
Trekta.Fadin.Vars = Trekta.Fadin.Vars || {};
Trekta.Fadin.Vars = {aFifoHili:[], aFifoShdo:[], bConsole_FadeIn:!1, bDbg_20141118o0611_Gugg:!1, bDbg_20141117o0931_Curious_Elements:!1, bDbg_20141118o0921_Target_Element:!1, bDbg_20210515o1423_Relative_Path:!0, bDebugFadin030Each:!1, bOutpuzDebug:!1, iMaxCharPerLine:127, iReadFileNo:0, s:""};
Trekta.Fadin.Vars.converter = null;
Trekta.Fadin.Vars.eOutBox = null;
Trekta.Fadin.Vars.eOutCount = 0;
Trekta.Fadin.breakLongLine = function(g, b) {
  for (var f = ""; g.length > b;) {
    var l = g.substring(0, b), n = l.lastIndexOf(" "), q = l.lastIndexOf("\n");
    -1 !== q && (n = q);
    -1 === n && (n = b);
    f += l.substring(0, n) + "\n";
    Trekta.Utils.bIs_Browser_Explorer && (f += "<br />");
    g = g.substring(n + 1);
  }
  return f + g;
};
Trekta.Fadin.breakLongLines = function(g) {
  var b = "";
  g = g.split(/\n/);
  for (var f = 0; f < g.length; f++) {
    var l = Trekta.Fadin.breakLongLine(g[f], Trekta.Fadin.Vars.iMaxCharPerLine);
    b += l + "\n";
    Trekta.Utils.bIs_Browser_Explorer && (b += "<br />");
  }
  return b;
};
Trekta.Fadin.confirmAction = function(g) {
  return confirm(g);
};
Trekta.Fadin.convertMarkdown2Html = function(g) {
  null === Trekta.Fadin.Vars.converter && (Trekta.Fadin.Vars.converter = new showdown.Converter, Trekta.Fadin.Vars.converter.setOption("tables", !0));
  return Trekta.Fadin.Vars.converter.makeHtml(g);
};
Trekta.Fadin.fadeInFiles = function() {
  "function" === typeof setFadeInFilesMaxLineLength && (Trekta.Fadin.Vars.iMaxCharPerLine = setFadeInFilesMaxLineLength());
  for (var g = document.getElementsByClassName("fadein"), b = 0; b < g.length; b++) {
    var f = g.item(b);
    f.hasAttribute("data-fadein") || ("TR" === f.tagName ? f.setAttribute("data-fadein", "pre") : f.setAttribute("data-fadein", ""));
  }
  g = document.querySelectorAll("[data-fadein]");
  b = g.length;
  Trekta.Fadin.Vars.s = "To switch this debug messages off, set Trekta.Fad.Vars.bOutpuzDebug = false;\n[Debug 20140704°0827] Function FadeInFiles is starting.\n - Fadein max line length = " + Trekta.Fadin.Vars.iMaxCharPerLine + ".\n - Fadein number of elements = " + b + ".\n[" + (new Date).toString() + "]";
  Trekta.Fadin.outputz(Trekta.Fadin.Vars.s);
  if (1 > b) {
    Trekta.Fadin.outputz("FadeInFiles has nothing to do.");
  } else {
    if (Trekta.Utils.bShow_Debug_Dialogs) {
      f = [].slice.call(g);
      var l = "[Experiment 20141117°1341] [].slice.call() : ";
      for (n in f) {
        f.hasOwnProperty(n) && (l += " " + n);
      }
      Trekta.Fadin.outputz(l);
    }
    if (Trekta.Fadin.Vars.bDbg_20141117o0931_Curious_Elements) {
      var n = "[Debug 20141117°0931] FadeInFiles found curious initial elements list.\n      List length = " + (b + ". The forEach loop yields :");
      f = 1;
      for (var q in g) {
        if (g.hasOwnProperty(q)) {
          if (2 > f || f >= b) {
            n += "\n " + f + "  " + q.toString() + " = " + q.name;
          }
          n += ".";
          f++;
        }
      }
      Trekta.Fadin.outputz(n);
    }
    for (q = 0; q < b; q++) {
      n = g.item(q);
      f = g[q].getElementsByTagName("a")[0].getAttribute("href");
      l = g[q].getAttribute("data-fadein");
      var d = Trekta.Utils.CmdlinParser.parse(l);
      if (Trekta.Fadin.Vars.bDbg_20141118o0611_Gugg) {
        var w = n.parentNode, t = "[Gugg 20141118°0611] Target and parent nodes available.";
        t += "\n - Element " + (q + 1) + " of " + b + " :";
        t += "\n - commandline = '" + l + "'";
        t += "\n - target id = '" + n.id + "'";
        t += "\n - target tag = '" + n.tagName + "'";
        t += "\n - target class = '" + n.className + "'";
        t += "\n - target innerHTML = '" + n.innerHTML + "'";
        t += "\n - parent id = '" + w.id + "'";
        t += "\n - parent tag = '" + w.tagName + "'";
        t += "\n - parent class = '" + w.className + "'";
        Trekta.Fadin.outputz(t);
      }
      "pre" in d && (n = g[q].getElementsByTagName("pre")[0]);
      d["file-to-load"] = f;
      d["target-element"] = n;
      Trekta.Fadin.fillInFile_1_loadLibs(n, f, d);
    }
  }
};
Trekta.Fadin.fillInFile_1_loadLibs = function(g, b, f) {
  var l = "";
  "highlight-type" in f || (f["highlight-type"] = "");
  if ("" === f["highlight-type"]) {
    var n = /(?:\.([^.]+))?$/;
    if (Trekta.Utils.bToggle_FALSE) {
      var q = n.exec("file.name.with.dots.txt")[1], d = n.exec("file.txt")[1], w = n.exec("file")[1], t = n.exec("")[1], v = n.exec(null)[1], x = n.exec(void 0)[1];
      alert("Dbg 20190407°0112]" + q + " " + d + " " + w + " " + t + " " + v + " " + x + " ");
    }
    n = n.exec(b)[1];
    "bas" === n && (n = "basic");
    -1 < Trekta.Fadin.Vars.aHiliTypes.indexOf(n) && (l = n, f["highlight-type"] = l);
  }
  n = !1;
  f.markdown = !1;
  -1 !== b.indexOf(".md", b.length - 3) && (n = !0, f.markdown = !0);
  if ("undefined" === typeof hljs && "" !== l) {
    Trekta.Fadin.Vars.bConsole_FadeIn && console.log('[Dbg 20190407°0132] Load highlight.min.js for "' + b + '"'), Trekta.Fadin.Vars.aFifoHili.push([b, g]), g = Trekta.Utils.retrieveDafBaseFolderAbs("/fadeinfiles.js"), Trekta.Utils.pullScriptBehind(g + "./highlight/highlight.min.js", function() {
      Trekta.Fadin.fillInFile_2_libsLoaded(Trekta.Fadin.Vars.aFifoHili, f);
    }, null, null);
  } else {
    if (n && "undefined" === typeof showdown) {
      Trekta.Fadin.Vars.bConsole_FadeIn && console.log('[Dbg 20190405°0254] Load showdown.js for "' + b + '"');
      Trekta.Fadin.Vars.aFifoShdo.push([b, g]);
      g = Trekta.Utils.retrieveDafBaseFolderAbs("/fadeinfiles.js");
      b = Trekta.Utils.retrieveDafBaseFolderAbs("/fadeinfiles.combi.js");
      var C = ("" !== g ? g : b) + "/showdown/showdown.min.js";
      Trekta.Utils.pullScriptBehind(C, function() {
        Trekta.Fadin.fillInFile_2_libsLoaded(Trekta.Fadin.Vars.aFifoShdo, f);
      }, function() {
        alert("Failed to load " + C);
      }, null);
    } else {
      Trekta.Fadin.fillInFile_3_continue(b, g, f);
    }
  }
};
Trekta.Fadin.fillInFile_2_libsLoaded = function(g, b) {
  for (var f; 0 < g.length;) {
    f = g.shift(), Trekta.Fadin.fillInFile_3_continue(f[0], f[1], b);
  }
};
Trekta.Fadin.fillInFile_3_continue = function(g, b, f) {
  var l = null;
  try {
    Trekta.Fadin.Vars.iReadFileNo += 1, l = Trekta.Fadin.Vars.iReadFileNo, Trekta.Fadin.Vars.bConsole_FadeIn && console.log("[Dbg 20190405°0255] Reading file no " + l + ' "' + g + '"'), Trekta.Utils.readTextFile2(g, function(n) {
      Trekta.Fadin.fillInFile_4_finish(b, f, l, n, g);
    });
  } catch (n) {
    Trekta.Fadin.fillInFile_4_finish(b, !1, l, n.message, g);
  }
};
Trekta.Fadin.fillInFile_4_finish = function(g, b, f, l, n) {
  Trekta.Fadin.Vars.bConsole_FadeIn && console.log("[Dbg 20190405°0256] Continue file no " + f);
  !0 === b.markdown ? (l = Trekta.Fadin.convertMarkdown2Html(l), l = Trekta.Fadin.mdFileRelativeLinks(n, l)) : (l = Trekta.Utils.htmlEscape(l), l = Trekta.Fadin.breakLongLines(l));
  if ("undefined" === typeof g) {
    Trekta.Fadin.outputz("[Error 20141118°0851] Fatal — Target element undefined.");
  } else {
    var q = g.parentNode, d = g.nextSibling;
    "undefined" === typeof q && alert("[Error 20141118°0852] Fatal — Parent element undefined.");
    var w = q.innerHTML;
    64 < w.length && (w = w.substr(0, 64) + "...");
    if (Trekta.Fadin.Vars.bDbg_20141118o0921_Target_Element) {
      var t = "Target element :\nclassName = '" + (q.className + "'");
      t += "\nid = '" + q.id + "'";
      t += "\ntagName = '" + q.tagName + "'";
      Trekta.Fadin.outputz("[Msg 20141118°0921] " + (t + ("\ninnerHTML : " + w)));
    }
    !0 === b.markdown ? (w = document.createElement("div"), w.className = "FadeinMarkDown", w.style.border = "1.0px solid LightSlateGray", w.style.borderRadius = "0.7em", w.style.padding = "0 0.7em 0 0.7em") : w = document.createElement("pre");
    2 > l.length && (l = '<span style="font-size:small;">⚡ Sorry, JavaScript cannot fade-in file <a href="' + n + '">' + n + '</a></span> <span style="font-size:xx-small;">(file ' + f + ')</span>.<br><span style="font-size:x-small;">Problem: Some browsers refuse AJAX requests via <b>file protocol</b> (e.g. <code>file:///D:/page.html</code>).<br>Solution: View this page via a server (e.g. <code>http://localhost/page.html</code>).</span> <span style="font-size:xx-small;">[Error 20141118°0853]</span>', 
    w.style.lineHeight = "1.2em", w.style.padding = "0.4em 0 0 0.4em");
    w.setAttribute("id", "oha");
    w.innerHTML = l;
    w.setAttribute("data-wafailiskip", "yes");
    f = "P TD LI H1 H2 H3 H4 H5 H6 PRE".split(" ");
    for (n = 0; n < f.length; n++) {
      t = w.getElementsByTagName(f[n]);
      for (var v = 0; v < t.length; v++) {
        t[v].setAttribute("data-wafailiskip", "yes");
      }
    }
    "PRE" === g.tagName ? (g.innerHTML = l, g.setAttribute("data-wafailiskip", "yes")) : "undefined" === typeof q ? g.appendChild(w) : (q.insertBefore(w, d), g = w);
    "" !== b["highlight-type"] && (hljs.configure({languages:[b["highlight-type"]]}), hljs.highlightElement(g));
  }
};
Trekta.Fadin.mdFileRelativeLinks = function(g, b) {
  var f = g.match(/^.\/(.*)\/.*/);
  f = null === f ? "" : f[f.length - 1];
  Trekta.Fadin.Vars.bDbg_20210515o1423_Relative_Path && Trekta.Fadin.outputz('[msg 20210515°1423`01`] Relation "' + g + '" — "' + f + '"');
  g = /<a href=".\/|<img src=".\//g;
  "" !== f && (b = b.replaceAll(g, "$&" + f + "/"));
  return b;
};
Trekta.Fadin.outputz = function(g) {
  if (Trekta.Fadin.Vars.bOutpuzDebug) {
    "undefined" === typeof g && (g = "<i>Test2 Einfügung</i>");
    if (null === Trekta.Fadin.Vars.eOutBox) {
      var b = null, f = document.getElementsByClassName("footerReferer");
      0 < f.length && (b = f[0]);
      Trekta.Fadin.Vars.eOutBox = document.createElement("div");
      Trekta.Fadin.Vars.eOutBox.style.cssText = "border:2px solid Red; display:table;";
      null === b ? document.body.appendChild(Trekta.Fadin.Vars.eOutBox) : b.parentNode.insertBefore(Trekta.Fadin.Vars.eOutBox, b.nextSibling);
    }
    b = document.createElement("pre");
    b.className = "codeTiny";
    b.style.backgroundColor = "PapayaWhip";
    b.style.margin = "0.6em";
    Trekta.Fadin.Vars.eOutCount++;
    g = document.createTextNode("(" + Trekta.Fadin.Vars.eOutCount + ") " + g);
    b.appendChild(g);
    Trekta.Fadin.Vars.eOutBox.appendChild(b);
  }
};
Trekta.Fadin.Vars.aHiliTypes = "Apache;apache;apacheconf;Bash;bash;sh;zsh;Basic;basic;C;c;h;C#;cs;csharp;C++;cpp;hpp;cc;hh;c++;h++;cxx;hxx;CSS;css;Clojure;clojure;clj;CoffeeScript;coffeescript;coffee;cson;iced;DOS;dos;bat;cmd;Diff;diff;patch;Erlang;erlang;erl;go;go;golang;HTML, XML;xml;html;xhtml;rss;atom;xjb;xsd;xsl;plist;svg;HTTP;http;https;Haskell;haskell;hs;Ini, TOML;ini;toml;JSON;json;Java;java;jsp;JavaScript;javascript;js;jsx;Kotlin;kotlin;kt;Less;less;Lisp;lisp;Lua;lua;Makefile;makefile;mk;mak;make;Nginx;nginx;nginxconf;Objective C;objectivec;mm;objc;obj-c;obj-c++;objective-c++;PHP;php;php3;php4;php5;php6;php7;php8;PHP Template;php-template;Perl;perl;pl;pm;Plaintext;plaintext;txt;text;PowerShell;powershell;ps;ps1;Properties;properties;Python;python;py;gyp;Python REPL;python-repl;pycon;R;r;Ruby;ruby;rb;gemspec;podspec;thor;irb;Rust;rust;rs;SCSS;scss;SQL;sql;Scala;scala;Scheme;scheme;Shell;shell;console;Swift;swift;TypeScript;typescript;ts;VB.Net;vbnet;vb;VBScript-HTML;vbscript;vbs;x86 Assembly;x86asm;XML;xml;YAML;yml;yaml".split(";");
Trekta = Trekta || {};
Trekta.Utils = Trekta.Utils || {};
Trekta.Utils.getCookie = function(g) {
  var b, f = "";
  if (0 < document.cookie.length) {
    var l = document.cookie.split(";");
    for (b = 0; b < l.length; b++) {
      var n = l[b].substr(0, l[b].indexOf("="));
      var q = l[b].substr(l[b].indexOf("=") + 1);
      n = n.replace(/^\s+|\s+$/g, "");
      if (n === g) {
        f = decodeURI(q);
        break;
      }
    }
  } else {
    f = localStorage.getItem(g);
  }
  return f;
};
Trekta.Utils.getCookieBool = function(g, b) {
  b = b || !1;
  g = Trekta.Utils.getCookie(g);
  return "true" === g ? !0 : "false" === g ? !1 : b;
};
Trekta.Utils.getCookieInt = function(g, b) {
  b = b || 0;
  g = Trekta.Utils.getCookie(g);
  "" !== g && (b = parseInt(g, 10));
  return b;
};
Trekta.Utils.getFileNameFull = function() {
  var g = document.location.href;
  g = g.substring(0, -1 === g.indexOf("?") ? g.length : g.indexOf("?"));
  g = g.substring(0, -1 === g.indexOf("#") ? g.length : g.indexOf("#"));
  -1 !== g.indexOf("/", g.length - 1) && (g += "index.html");
  return g;
};
Trekta.Utils.getFilenamePlain = function() {
  var g = Trekta.Utils.getFileNameFull();
  g = g.split("/");
  return g = g[g.length - 1];
};
Trekta.Utils.htmlEscape = function(g) {
  g = g.replace(/</g, "&lt;");
  return g = g.replace(/>/g, "&gt;");
};
Trekta.Utils.isScriptAlreadyLoaded = function(g) {
  g = g.replace(/\./g, "\\.");
  g = new RegExp(g + "$", "");
  var b = document.getElementsByTagName("SCRIPT");
  if (b && 0 < b.length) {
    for (var f in b) {
      var l = Number.parseInt(f, 10);
      if (b[l] && b[l].src.match(g)) {
        return !0;
      }
    }
  }
  return !1;
};
Trekta.Utils.outDbgMsg = function(g) {
  if (Trekta.Utils.getCookieBool("checkbox_yellowdebugpane", null)) {
    if ("complete" !== document.readyState) {
      Trekta.Utils.InitialMessageCache.push(g);
    } else {
      Trekta.Utils.outDbgMsg_GuaranteeParentElement();
      Date.now || (Date.now = function() {
        return (new Date).getTime();
      });
      var b = Math.floor(Date.now() / 1000);
      b = b.toString();
      if (0 < Trekta.Utils.InitialMessageCache.length) {
        for (var f = "", l = Trekta.Utils.InitialMessageCache.length - 1; 0 <= l; l--) {
          "" !== f && (f = "\n\n" + f), f = Trekta.Utils.InitialMessageCache[l] + f;
        }
        Trekta.Utils.InitialMessageCache.length = 0;
        g = '<div style="background-color:LightGreen; margin:1.1em; padding:0.7em;">' + f + "</div>\n\n" + g;
      }
      g = g.replace(/\n/g, "<br />");
      b = '<p><span style="font-size:72%;">' + (b + "</span> ") + g + "</p>";
      document.getElementById(Daf.Dspat.Config.sFurniture_OutputArea_Id).insertAdjacentHTML("beforeend", "\n" + b);
    }
  }
};
Trekta.Utils.outDbgMsg_GuaranteeParentElement = function() {
  var g = Daf.Dspat.Config.sFurniture_OutputArea_Id, b = document.getElementById(g);
  if (!b) {
    b = '<div id="' + g + '" class="dafBoxDebugOutput"><p>[Msg 20150325°0211] Loading dafmenu.js (1/x).\n<br />&nbsp; Here comes the yellow Standard Output Box. Some page values are :' + Daf.Mnu.Jsi.getJsiIntroDebugMessage(!0) + "</p></div>";
    var f = document.getElementsByTagName("body")[0], l = document.createElement("div");
    l.innerHTML = b;
    f.appendChild(l);
    b = document.getElementById(g);
  }
  return b;
};
Trekta.Utils.pullScriptBehind = function(g, b, f, l) {
  if (0 <= Trekta.Utils.aPulled.indexOf(g)) {
    b(g, l, !0);
  } else {
    var n = document.getElementsByTagName("head")[0], q = document.createElement("script");
    q.type = "text/javascript";
    q.src = g;
    if ("undefined" !== typeof b) {
      var d = function() {
        b(g, l, !1);
      };
      q.onload = function() {
        Trekta.Utils.pullScript_onload(g, d);
      };
    }
    f = f || null;
    null !== f && (q.onerror = function() {
      f(g, l);
    });
    n.appendChild(q);
    return !0;
  }
};
Trekta.Utils.pullScript_onload = function(g, b) {
  Trekta.Utils.aPulled.push(g);
  b();
};
Trekta.Utils.readTextFile2 = function(g, b, f) {
  Trekta.Utils.ajax3Send("GET", g, "", b, f);
};
Trekta.Utils.ajax3Send = function(g, b, f, l, n) {
  l = "undefined" === typeof l ? null : l;
  n = "undefined" === typeof n ? null : n;
  var q = new XMLHttpRequest;
  q.open(g, b, !0);
  q.onreadystatechange = function() {
    if (0 !== q.readyState && 1 !== q.readyState && 2 !== q.readyState && 3 !== q.readyState && 4 === q.readyState) {
      var d = !1;
      switch(q.status) {
        case 200:
          d = !0;
      }
      d ? l(q.responseText) : n(q.responseText);
    }
  };
  try {
    q.send(null);
  } catch (d) {
    g = "<b>Sorry, some feature on this page does not work.</b>\nFile <tt>" + b + "</tt> ~~could not be read.\nYour browser said: <tt>" + d.message + "</tt>.", g = Trekta.Utils.bIs_Browser_Chrome && "file:" === location.protocol ? g + "\nYour browser seems to be Chrome, and this does not ~~read files via file protocol.\nThere are two <b>solutions</b>: (1) Use a different browser, e.g. Firefox or IE\nor (2) view this page from <tt>localhost</tt> with a HTTP server." : Trekta.Utils.bIs_Browser_Firefox && 
    "file:" === location.protocol ? g + "\nYour browser seems to be <b>Firefox</b>, and this does not ~~read files\nwith a path going below the current directory via file protocol.\nThere are two <b>solutions</b>: (1) Use a different browser, e.g. Chrome or IE\nor (2)  view this page from <tt>localhost</tt> with a HTTP server." : g + ("\n [info 20160622°0131] Failed sending request " + b + "."), l(g);
  }
};
Trekta.Utils.retrieveDafBaseFolderAbs = function(g) {
  var b = g.replace(/\./g, "\\.") + "$";
  g = new RegExp(b, "");
  b = new RegExp("(.*)" + b, "");
  var f = "", l = document.getElementsByTagName("SCRIPT");
  if (l && 0 < l.length) {
    for (var n in l) {
      var q = Number.parseInt(n, 10);
      l[q] && l[q].src.match(g) && (f = l[q].src.replace(b, "$1"));
    }
  }
  return f;
};
Trekta.Utils.sConfine = function(g, b) {
  var f = "";
  g.length > b && (f = g.substring(0, b / 2) + " … " + g.substring(g.length - b / 2));
  f = f.split("\n").join("☼");
  f = f.split("<").join("&lt;");
  return f = f.split(">").join("&gt;");
};
Trekta.Utils.setCookie = function(g, b, f) {
  var l = new Date;
  l.setDate(l.getDate() + f);
  f = encodeURI(b) + (null === f ? "" : "; Expires = " + l.toUTCString());
  document.cookie = g + "=" + f + "; path=/";
  1 > document.cookie.length && localStorage.setItem(g, b);
};
Trekta.Utils.windowOnloadDaisychain = function(g) {
  if (window.onload) {
    var b = window.onload;
    window.onload = function() {
      b(null);
      g();
    };
  } else {
    window.onload = function() {
      g();
    };
  }
};
Trekta.Utils.aPulled = [];
Trekta.Utils.bIs_Browser_Chrome = navigator.userAgent.match(/Chrome/) ? !0 : !1;
Trekta.Utils.bIs_Browser_Edge = navigator.userAgent.match(/Edge/) ? !0 : !1;
Trekta.Utils.bIs_Browser_Explorer = navigator.appName.match(/Explorer/) || window.msCrypto ? !0 : !1;
Trekta.Utils.bIs_Browser_Firefox = navigator.userAgent.match(/Firefox/) ? !0 : !1;
Trekta.Utils.bIs_Browser_Opera = navigator.userAgent.match(/(Opera)|(OPR)/) ? !0 : !1;
Trekta.Utils.bShow_Debug_Dialogs = !1;
Trekta.Utils.bToggle_FALSE = !1;
Trekta.Utils.bToggle_TRUE = !0;
Trekta.Utils.bUseMinified = !1;
Trekta.Utils.InitialMessageCache = [];
Trekta.Utils.sFurniture_OutputArea_Id = "id20150321o0231_StandardOutputDiv";
Trekta.Utils.s_DaftariBaseFolderAbs = Trekta.Utils.s_DaftariBaseFolderAbs || "";
Trekta.Utils.s_DaftariBaseFolderRel = Trekta.Utils.s_DaftariBaseFolderRel || "";
Trekta.Utils.CmdlinParser = function() {
  Trekta.Utils.parse = function(g) {
    "undefined" === typeof g && (g = "");
    for (var b = [], f = "", l = "", n = 0; n < g.length; n++) {
      var q = g.charAt(n);
      " " === q && "" === f ? "" !== l && (b.push(l), l = "") : "=" === q && "" === f ? ("" !== l && (b.push(l), l = ""), b.push("=")) : "'" === q || '"' === q ? f = "" === f ? q : "" : l += q;
    }
    b.push(l);
    g = [];
    for (l = 0; l < b.length; l++) {
      "" !== b[l] && (f = b[l], g[f] = "<n/a>", "=" === b[l + 1] && (g[f] = b[l + 2], l++, l++));
    }
    return g;
  };
  return {parse:Trekta.Utils.parse};
}();
Trekta.Utils.windowOnloadDaisychain(Trekta.Fadin.fadeInFiles);
/*
 showdown v 2.1.0 - 21-04-2022 */
!function() {
  function g(a) {
    var e = {omitExtraWLInCodeBlocks:{defaultValue:!1, describe:"Omit the default extra whiteline added to code blocks", type:"boolean"}, noHeaderId:{defaultValue:!1, describe:"Turn on/off generated header id", type:"boolean"}, prefixHeaderId:{defaultValue:!1, describe:"Add a prefix to the generated header ids. Passing a string will prefix that string to the header id. Setting to true will add a generic 'section-' prefix", type:"string"}, rawPrefixHeaderId:{defaultValue:!1, describe:'Setting this option to true will prevent showdown from modifying the prefix. This might result in malformed IDs (if, for instance, the " char is used in the prefix)', 
    type:"boolean"}, ghCompatibleHeaderId:{defaultValue:!1, describe:"Generate header ids compatible with github style (spaces are replaced with dashes, a bunch of non alphanumeric chars are removed)", type:"boolean"}, rawHeaderId:{defaultValue:!1, describe:"Remove only spaces, ' and \" from generated header ids (including prefixes), replacing them with dashes (-). WARNING: This might result in malformed ids", type:"boolean"}, headerLevelStart:{defaultValue:!1, describe:"The header blocks level start", 
    type:"integer"}, parseImgDimensions:{defaultValue:!1, describe:"Turn on/off image dimension parsing", type:"boolean"}, simplifiedAutoLink:{defaultValue:!1, describe:"Turn on/off GFM autolink style", type:"boolean"}, excludeTrailingPunctuationFromURLs:{defaultValue:!1, describe:"Excludes trailing punctuation from links generated with autoLinking", type:"boolean"}, literalMidWordUnderscores:{defaultValue:!1, describe:"Parse midword underscores as literal underscores", type:"boolean"}, literalMidWordAsterisks:{defaultValue:!1, 
    describe:"Parse midword asterisks as literal asterisks", type:"boolean"}, strikethrough:{defaultValue:!1, describe:"Turn on/off strikethrough support", type:"boolean"}, tables:{defaultValue:!1, describe:"Turn on/off tables support", type:"boolean"}, tablesHeaderId:{defaultValue:!1, describe:"Add an id to table headers", type:"boolean"}, ghCodeBlocks:{defaultValue:!0, describe:"Turn on/off GFM fenced code blocks support", type:"boolean"}, tasklists:{defaultValue:!1, describe:"Turn on/off GFM tasklist support", 
    type:"boolean"}, smoothLivePreview:{defaultValue:!1, describe:"Prevents weird effects in live previews due to incomplete input", type:"boolean"}, smartIndentationFix:{defaultValue:!1, describe:"Tries to smartly fix indentation in es6 strings", type:"boolean"}, disableForced4SpacesIndentedSublists:{defaultValue:!1, describe:"Disables the requirement of indenting nested sublists by 4 spaces", type:"boolean"}, simpleLineBreaks:{defaultValue:!1, describe:"Parses simple line breaks as <br> (GFM Style)", 
    type:"boolean"}, requireSpaceBeforeHeadingText:{defaultValue:!1, describe:"Makes adding a space between `#` and the header text mandatory (GFM Style)", type:"boolean"}, ghMentions:{defaultValue:!1, describe:"Enables github @mentions", type:"boolean"}, ghMentionsLink:{defaultValue:"https://github.com/{u}", describe:"Changes the link generated by @mentions. Only applies if ghMentions option is enabled.", type:"string"}, encodeEmails:{defaultValue:!0, describe:"Encode e-mail addresses through the use of Character Entities, transforming ASCII e-mail addresses into its equivalent decimal entities", 
    type:"boolean"}, openLinksInNewWindow:{defaultValue:!1, describe:"Open all links in new windows", type:"boolean"}, backslashEscapesHTMLTags:{defaultValue:!1, describe:"Support for HTML Tag escaping. ex: <div>foo</div>", type:"boolean"}, emoji:{defaultValue:!1, describe:"Enable emoji support. Ex: `this is a :smile: emoji`", type:"boolean"}, underline:{defaultValue:!1, describe:"Enable support for underline. Syntax is double or triple underscores: `__underline word__`. With this option enabled, underscores no longer parses into `<em>` and `<strong>`", 
    type:"boolean"}, ellipsis:{defaultValue:!0, describe:"Replaces three dots with the ellipsis unicode character", type:"boolean"}, completeHTMLDocument:{defaultValue:!1, describe:"Outputs a complete html document, including `<html>`, `<head>` and `<body>` tags", type:"boolean"}, metadata:{defaultValue:!1, describe:"Enable support for document metadata (defined at the top of the document between `«««` and `»»»` or between `---` and `---`).", type:"boolean"}, splitAdjacentBlockquotes:{defaultValue:!1, 
    describe:"Split adjacent blockquote blocks", type:"boolean"}};
    if (!1 === a) {
      return JSON.parse(JSON.stringify(e));
    }
    var c;
    a = {};
    for (c in e) {
      e.hasOwnProperty(c) && (a[c] = e[c].defaultValue);
    }
    return a;
  }
  function b(a, e) {
    e = e ? "Error in " + e + " extension->" : "Error in unnamed extension";
    var c = {valid:!0, error:""};
    d.helper.isArray(a) || (a = [a]);
    for (var k = 0; k < a.length; ++k) {
      var h = e + " sub-extension " + k + ": ", r = a[k];
      if ("object" != typeof r) {
        return c.valid = !1, c.error = h + "must be an object, but " + typeof r + " given", c;
      }
      if (!d.helper.isString(r.type)) {
        return c.valid = !1, c.error = h + 'property "type" must be a string, but ' + typeof r.type + " given", c;
      }
      var m = r.type = r.type.toLowerCase();
      if ("lang" !== (m = "html" === (m = "language" === m ? r.type = "lang" : m) ? r.type = "output" : m) && "output" !== m && "listener" !== m) {
        return c.valid = !1, c.error = h + "type " + m + ' is not recognized. Valid values: "lang/language", "output/html" or "listener"', c;
      }
      if ("listener" === m) {
        if (d.helper.isUndefined(r.listeners)) {
          return c.valid = !1, c.error = h + '. Extensions of type "listener" must have a property called "listeners"', c;
        }
      } else {
        if (d.helper.isUndefined(r.filter) && d.helper.isUndefined(r.regex)) {
          return c.valid = !1, c.error = h + m + ' extensions must define either a "regex" property or a "filter" method', c;
        }
      }
      if (r.listeners) {
        if ("object" != typeof r.listeners) {
          return c.valid = !1, c.error = h + '"listeners" property must be an object but ' + typeof r.listeners + " given", c;
        }
        for (var y in r.listeners) {
          if (r.listeners.hasOwnProperty(y) && "function" != typeof r.listeners[y]) {
            return c.valid = !1, c.error = h + '"listeners" property must be an hash of [event name]: [callback]. listeners.' + y + " must be a function but " + typeof r.listeners[y] + " given", c;
          }
        }
      }
      if (r.filter) {
        if ("function" != typeof r.filter) {
          return c.valid = !1, c.error = h + '"filter" must be a function, but ' + typeof r.filter + " given", c;
        }
      } else {
        if (r.regex) {
          if (d.helper.isString(r.regex) && (r.regex = new RegExp(r.regex, "g")), !(r.regex instanceof RegExp)) {
            return c.valid = !1, c.error = h + '"regex" property must either be a string or a RegExp object, but ' + typeof r.regex + " given", c;
          }
          if (d.helper.isUndefined(r.replace)) {
            return c.valid = !1, c.error = h + '"regex" extensions must implement a replace string or function', c;
          }
        }
      }
    }
    return c;
  }
  function f(a, e) {
    return "¨E" + e.charCodeAt(0) + "E";
  }
  function l(a, e, c, k) {
    var h, r, m = -1 < (k = k || "").indexOf("g");
    c = new RegExp(e + "|" + c, "g" + k.replace(/g/g, ""));
    k = new RegExp(e, k.replace(/g/g, ""));
    var y = [];
    do {
      for (e = 0; J = c.exec(a);) {
        if (k.test(J[0])) {
          e++ || (r = (h = c.lastIndex) - J[0].length);
        } else {
          if (e && !--e) {
            var A = J.index + J[0].length, J = {left:{start:r, end:h}, match:{start:h, end:J.index}, right:{start:J.index, end:A}, wholeMatch:{start:r, end:A}};
            if (y.push(J), !m) {
              return y;
            }
          }
        }
      }
    } while (e && (c.lastIndex = h));
    return y;
  }
  function n(a) {
    return function(e, c, k, h, r, m, y) {
      e = k = k.replace(d.helper.regexes.asteriskDashAndColon, d.helper.escapeCharactersCallback);
      h = "";
      c = c || "";
      y = y || "";
      return /^www\./i.test(k) && (k = k.replace(/^www\./i, "http://www.")), a.excludeTrailingPunctuationFromURLs && m && (h = m), c + '<a href="' + k + '"' + (a.openLinksInNewWindow ? ' rel="noopener noreferrer" target="¨E95Eblank"' : "") + ">" + e + "</a>" + h + y;
    };
  }
  function q(a, e) {
    return function(c, k, h) {
      c = "mailto:";
      return k = k || "", h = d.subParser("unescapeSpecialChars")(h, a, e), a.encodeEmails ? (c = d.helper.encodeEmailAddress(c + h), h = d.helper.encodeEmailAddress(h)) : c += h, k + '<a href="' + c + '">' + h + "</a>";
    };
  }
  var d = {}, w = {}, t = {}, v = g(!0), x = "vanilla", C = {github:{omitExtraWLInCodeBlocks:!0, simplifiedAutoLink:!0, excludeTrailingPunctuationFromURLs:!0, literalMidWordUnderscores:!0, strikethrough:!0, tables:!0, tablesHeaderId:!0, ghCodeBlocks:!0, tasklists:!0, disableForced4SpacesIndentedSublists:!0, simpleLineBreaks:!0, requireSpaceBeforeHeadingText:!0, ghCompatibleHeaderId:!0, ghMentions:!0, backslashEscapesHTMLTags:!0, emoji:!0, splitAdjacentBlockquotes:!0}, original:{noHeaderId:!0, ghCodeBlocks:!1}, 
  ghost:{omitExtraWLInCodeBlocks:!0, parseImgDimensions:!0, simplifiedAutoLink:!0, excludeTrailingPunctuationFromURLs:!0, literalMidWordUnderscores:!0, strikethrough:!0, tables:!0, tablesHeaderId:!0, ghCodeBlocks:!0, tasklists:!0, smoothLivePreview:!0, simpleLineBreaks:!0, requireSpaceBeforeHeadingText:!0, ghMentions:!1, encodeEmails:!0}, vanilla:g(!0), allOn:function() {
    var a, e = g(!0), c = {};
    for (a in e) {
      e.hasOwnProperty(a) && (c[a] = !0);
    }
    return c;
  }()};
  d.helper = {};
  d.extensions = {};
  d.setOption = function(a, e) {
    return v[a] = e, this;
  };
  d.getOption = function(a) {
    return v[a];
  };
  d.getOptions = function() {
    return v;
  };
  d.resetOptions = function() {
    v = g(!0);
  };
  d.setFlavor = function(a) {
    if (!C.hasOwnProperty(a)) {
      throw Error(a + " flavor was not found");
    }
    d.resetOptions();
    var e, c = C[a];
    for (e in x = a, c) {
      c.hasOwnProperty(e) && (v[e] = c[e]);
    }
  };
  d.getFlavor = function() {
    return x;
  };
  d.getFlavorOptions = function(a) {
    if (C.hasOwnProperty(a)) {
      return C[a];
    }
  };
  d.getDefaultOptions = g;
  d.subParser = function(a, e) {
    if (d.helper.isString(a)) {
      if (void 0 === e) {
        if (w.hasOwnProperty(a)) {
          return w[a];
        }
        throw Error("SubParser named " + a + " not registered!");
      }
      w[a] = e;
    }
  };
  d.extension = function(a, e) {
    if (!d.helper.isString(a)) {
      throw Error("Extension 'name' must be a string");
    }
    if (a = d.helper.stdExtName(a), d.helper.isUndefined(e)) {
      if (t.hasOwnProperty(a)) {
        return t[a];
      }
      throw Error("Extension named " + a + " is not registered!");
    }
    "function" == typeof e && (e = e());
    var c = b(e = d.helper.isArray(e) ? e : [e], a);
    if (!c.valid) {
      throw Error(c.error);
    }
    t[a] = e;
  };
  d.getAllExtensions = function() {
    return t;
  };
  d.removeExtension = function(a) {
    delete t[a];
  };
  d.resetExtensions = function() {
    t = {};
  };
  d.validateExtension = function(a) {
    a = b(a, null);
    return !!a.valid || (console.warn(a.error), !1);
  };
  d.hasOwnProperty("helper") || (d.helper = {});
  d.helper.isString = function(a) {
    return "string" == typeof a || a instanceof String;
  };
  d.helper.isFunction = function(a) {
    return a && "[object Function]" === {}.toString.call(a);
  };
  d.helper.isArray = function(a) {
    return Array.isArray(a);
  };
  d.helper.isUndefined = function(a) {
    return void 0 === a;
  };
  d.helper.forEach = function(a, e) {
    if (d.helper.isUndefined(a)) {
      throw Error("obj param is required");
    }
    if (d.helper.isUndefined(e)) {
      throw Error("callback param is required");
    }
    if (!d.helper.isFunction(e)) {
      throw Error("callback param must be a function/closure");
    }
    if ("function" == typeof a.forEach) {
      a.forEach(e);
    } else {
      if (d.helper.isArray(a)) {
        for (var c = 0; c < a.length; c++) {
          e(a[c], c, a);
        }
      } else {
        if ("object" != typeof a) {
          throw Error("obj does not seem to be an array or an iterable object");
        }
        for (c in a) {
          a.hasOwnProperty(c) && e(a[c], c, a);
        }
      }
    }
  };
  d.helper.stdExtName = function(a) {
    return a.replace(/[_?*+\/\\.^-]/g, "").replace(/\s/g, "").toLowerCase();
  };
  d.helper.escapeCharactersCallback = f;
  d.helper.escapeCharacters = function(a, e, c) {
    e = "([" + e.replace(/([\[\]\\])/g, "\\$1") + "])";
    c && (e = "\\\\" + e);
    c = new RegExp(e, "g");
    return a.replace(c, f);
  };
  d.helper.unescapeHTMLEntities = function(a) {
    return a.replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&");
  };
  d.helper.matchRecursiveRegExp = function(a, e, c, k) {
    e = l(a, e, c, k);
    c = [];
    for (k = 0; k < e.length; ++k) {
      c.push([a.slice(e[k].wholeMatch.start, e[k].wholeMatch.end), a.slice(e[k].match.start, e[k].match.end), a.slice(e[k].left.start, e[k].left.end), a.slice(e[k].right.start, e[k].right.end)]);
    }
    return c;
  };
  d.helper.replaceRecursiveRegExp = function(a, e, c, k, h) {
    d.helper.isFunction(e) || (r = e, e = function() {
      return r;
    });
    var r;
    k = l(a, c, k, h);
    c = a;
    h = k.length;
    if (0 < h) {
      c = [];
      0 !== k[0].wholeMatch.start && c.push(a.slice(0, k[0].wholeMatch.start));
      for (var m = 0; m < h; ++m) {
        c.push(e(a.slice(k[m].wholeMatch.start, k[m].wholeMatch.end), a.slice(k[m].match.start, k[m].match.end), a.slice(k[m].left.start, k[m].left.end), a.slice(k[m].right.start, k[m].right.end))), m < h - 1 && c.push(a.slice(k[m].wholeMatch.end, k[m + 1].wholeMatch.start));
      }
      k[h - 1].wholeMatch.end < a.length && c.push(a.slice(k[h - 1].wholeMatch.end));
      c = c.join("");
    }
    return c;
  };
  d.helper.regexIndexOf = function(a, e, c) {
    if (!d.helper.isString(a)) {
      throw "InvalidArgumentError: first parameter of showdown.helper.regexIndexOf function must be a string";
    }
    if (0 == e instanceof RegExp) {
      throw "InvalidArgumentError: second parameter of showdown.helper.regexIndexOf function must be an instance of RegExp";
    }
    a = a.substring(c || 0).search(e);
    return 0 <= a ? a + (c || 0) : a;
  };
  d.helper.splitAtIndex = function(a, e) {
    if (d.helper.isString(a)) {
      return [a.substring(0, e), a.substring(e)];
    }
    throw "InvalidArgumentError: first parameter of showdown.helper.regexIndexOf function must be a string";
  };
  d.helper.encodeEmailAddress = function(a) {
    var e = [function(c) {
      return "&#" + c.charCodeAt(0) + ";";
    }, function(c) {
      return "&#x" + c.charCodeAt(0).toString(16) + ";";
    }, function(c) {
      return c;
    }];
    return a = a.replace(/./g, function(c) {
      var k;
      return "@" === c ? e[Math.floor(2 * Math.random())](c) : .9 < (k = Math.random()) ? e[2](c) : .45 < k ? e[1](c) : e[0](c);
    });
  };
  d.helper.padEnd = function(a, e, c) {
    return e >>= 0, c = String(c || " "), a.length > e ? String(a) : ((e -= a.length) > c.length && (c += c.repeat(e / c.length)), String(a) + c.slice(0, e));
  };
  "undefined" == typeof console && (console = {warn:function(a) {
    alert(a);
  }, log:function(a) {
    alert(a);
  }, error:function(a) {
    throw a;
  }});
  d.helper.regexes = {asteriskDashAndColon:/([*_:~])/g};
  d.helper.emojis = {"+1":"\ud83d\udc4d", "-1":"\ud83d\udc4e", 100:"\ud83d\udcaf", 1234:"\ud83d\udd22", "1st_place_medal":"\ud83e\udd47", "2nd_place_medal":"\ud83e\udd48", "3rd_place_medal":"\ud83e\udd49", "8ball":"\ud83c\udfb1", a:"\ud83c\udd70️", ab:"\ud83c\udd8e", abc:"\ud83d\udd24", abcd:"\ud83d\udd21", accept:"\ud83c\ude51", aerial_tramway:"\ud83d\udea1", airplane:"✈️", alarm_clock:"⏰", alembic:"⚗️", alien:"\ud83d\udc7d", ambulance:"\ud83d\ude91", amphora:"\ud83c\udffa", anchor:"⚓️", angel:"\ud83d\udc7c", 
  anger:"\ud83d\udca2", angry:"\ud83d\ude20", anguished:"\ud83d\ude27", ant:"\ud83d\udc1c", apple:"\ud83c\udf4e", aquarius:"♒️", aries:"♈️", arrow_backward:"◀️", arrow_double_down:"⏬", arrow_double_up:"⏫", arrow_down:"⬇️", arrow_down_small:"\ud83d\udd3d", arrow_forward:"▶️", arrow_heading_down:"⤵️", arrow_heading_up:"⤴️", arrow_left:"⬅️", arrow_lower_left:"↙️", arrow_lower_right:"↘️", arrow_right:"➡️", arrow_right_hook:"↪️", arrow_up:"⬆️", arrow_up_down:"↕️", arrow_up_small:"\ud83d\udd3c", arrow_upper_left:"↖️", 
  arrow_upper_right:"↗️", arrows_clockwise:"\ud83d\udd03", arrows_counterclockwise:"\ud83d\udd04", art:"\ud83c\udfa8", articulated_lorry:"\ud83d\ude9b", artificial_satellite:"\ud83d\udef0", astonished:"\ud83d\ude32", athletic_shoe:"\ud83d\udc5f", atm:"\ud83c\udfe7", atom_symbol:"⚛️", avocado:"\ud83e\udd51", b:"\ud83c\udd71️", baby:"\ud83d\udc76", baby_bottle:"\ud83c\udf7c", baby_chick:"\ud83d\udc24", baby_symbol:"\ud83d\udebc", back:"\ud83d\udd19", bacon:"\ud83e\udd53", badminton:"\ud83c\udff8", 
  baggage_claim:"\ud83d\udec4", baguette_bread:"\ud83e\udd56", balance_scale:"⚖️", balloon:"\ud83c\udf88", ballot_box:"\ud83d\uddf3", ballot_box_with_check:"☑️", bamboo:"\ud83c\udf8d", banana:"\ud83c\udf4c", bangbang:"‼️", bank:"\ud83c\udfe6", bar_chart:"\ud83d\udcca", barber:"\ud83d\udc88", baseball:"⚾️", basketball:"\ud83c\udfc0", basketball_man:"⛹️", basketball_woman:"⛹️&zwj;♀️", bat:"\ud83e\udd87", bath:"\ud83d\udec0", bathtub:"\ud83d\udec1", battery:"\ud83d\udd0b", beach_umbrella:"\ud83c\udfd6", 
  bear:"\ud83d\udc3b", bed:"\ud83d\udecf", bee:"\ud83d\udc1d", beer:"\ud83c\udf7a", beers:"\ud83c\udf7b", beetle:"\ud83d\udc1e", beginner:"\ud83d\udd30", bell:"\ud83d\udd14", bellhop_bell:"\ud83d\udece", bento:"\ud83c\udf71", biking_man:"\ud83d\udeb4", bike:"\ud83d\udeb2", biking_woman:"\ud83d\udeb4&zwj;♀️", bikini:"\ud83d\udc59", biohazard:"☣️", bird:"\ud83d\udc26", birthday:"\ud83c\udf82", black_circle:"⚫️", black_flag:"\ud83c\udff4", black_heart:"\ud83d\udda4", black_joker:"\ud83c\udccf", black_large_square:"⬛️", 
  black_medium_small_square:"◾️", black_medium_square:"◼️", black_nib:"✒️", black_small_square:"▪️", black_square_button:"\ud83d\udd32", blonde_man:"\ud83d\udc71", blonde_woman:"\ud83d\udc71&zwj;♀️", blossom:"\ud83c\udf3c", blowfish:"\ud83d\udc21", blue_book:"\ud83d\udcd8", blue_car:"\ud83d\ude99", blue_heart:"\ud83d\udc99", blush:"\ud83d\ude0a", boar:"\ud83d\udc17", boat:"⛵️", bomb:"\ud83d\udca3", book:"\ud83d\udcd6", bookmark:"\ud83d\udd16", bookmark_tabs:"\ud83d\udcd1", books:"\ud83d\udcda", boom:"\ud83d\udca5", 
  boot:"\ud83d\udc62", bouquet:"\ud83d\udc90", bowing_man:"\ud83d\ude47", bow_and_arrow:"\ud83c\udff9", bowing_woman:"\ud83d\ude47&zwj;♀️", bowling:"\ud83c\udfb3", boxing_glove:"\ud83e\udd4a", boy:"\ud83d\udc66", bread:"\ud83c\udf5e", bride_with_veil:"\ud83d\udc70", bridge_at_night:"\ud83c\udf09", briefcase:"\ud83d\udcbc", broken_heart:"\ud83d\udc94", bug:"\ud83d\udc1b", building_construction:"\ud83c\udfd7", bulb:"\ud83d\udca1", bullettrain_front:"\ud83d\ude85", bullettrain_side:"\ud83d\ude84", burrito:"\ud83c\udf2f", 
  bus:"\ud83d\ude8c", business_suit_levitating:"\ud83d\udd74", busstop:"\ud83d\ude8f", bust_in_silhouette:"\ud83d\udc64", busts_in_silhouette:"\ud83d\udc65", butterfly:"\ud83e\udd8b", cactus:"\ud83c\udf35", cake:"\ud83c\udf70", calendar:"\ud83d\udcc6", call_me_hand:"\ud83e\udd19", calling:"\ud83d\udcf2", camel:"\ud83d\udc2b", camera:"\ud83d\udcf7", camera_flash:"\ud83d\udcf8", camping:"\ud83c\udfd5", cancer:"♋️", candle:"\ud83d\udd6f", candy:"\ud83c\udf6c", canoe:"\ud83d\udef6", capital_abcd:"\ud83d\udd20", 
  capricorn:"♑️", car:"\ud83d\ude97", card_file_box:"\ud83d\uddc3", card_index:"\ud83d\udcc7", card_index_dividers:"\ud83d\uddc2", carousel_horse:"\ud83c\udfa0", carrot:"\ud83e\udd55", cat:"\ud83d\udc31", cat2:"\ud83d\udc08", cd:"\ud83d\udcbf", chains:"⛓", champagne:"\ud83c\udf7e", chart:"\ud83d\udcb9", chart_with_downwards_trend:"\ud83d\udcc9", chart_with_upwards_trend:"\ud83d\udcc8", checkered_flag:"\ud83c\udfc1", cheese:"\ud83e\uddc0", cherries:"\ud83c\udf52", cherry_blossom:"\ud83c\udf38", chestnut:"\ud83c\udf30", 
  chicken:"\ud83d\udc14", children_crossing:"\ud83d\udeb8", chipmunk:"\ud83d\udc3f", chocolate_bar:"\ud83c\udf6b", christmas_tree:"\ud83c\udf84", church:"⛪️", cinema:"\ud83c\udfa6", circus_tent:"\ud83c\udfaa", city_sunrise:"\ud83c\udf07", city_sunset:"\ud83c\udf06", cityscape:"\ud83c\udfd9", cl:"\ud83c\udd91", clamp:"\ud83d\udddc", clap:"\ud83d\udc4f", clapper:"\ud83c\udfac", classical_building:"\ud83c\udfdb", clinking_glasses:"\ud83e\udd42", clipboard:"\ud83d\udccb", clock1:"\ud83d\udd50", clock10:"\ud83d\udd59", 
  clock1030:"\ud83d\udd65", clock11:"\ud83d\udd5a", clock1130:"\ud83d\udd66", clock12:"\ud83d\udd5b", clock1230:"\ud83d\udd67", clock130:"\ud83d\udd5c", clock2:"\ud83d\udd51", clock230:"\ud83d\udd5d", clock3:"\ud83d\udd52", clock330:"\ud83d\udd5e", clock4:"\ud83d\udd53", clock430:"\ud83d\udd5f", clock5:"\ud83d\udd54", clock530:"\ud83d\udd60", clock6:"\ud83d\udd55", clock630:"\ud83d\udd61", clock7:"\ud83d\udd56", clock730:"\ud83d\udd62", clock8:"\ud83d\udd57", clock830:"\ud83d\udd63", clock9:"\ud83d\udd58", 
  clock930:"\ud83d\udd64", closed_book:"\ud83d\udcd5", closed_lock_with_key:"\ud83d\udd10", closed_umbrella:"\ud83c\udf02", cloud:"☁️", cloud_with_lightning:"\ud83c\udf29", cloud_with_lightning_and_rain:"⛈", cloud_with_rain:"\ud83c\udf27", cloud_with_snow:"\ud83c\udf28", clown_face:"\ud83e\udd21", clubs:"♣️", cocktail:"\ud83c\udf78", coffee:"☕️", coffin:"⚰️", cold_sweat:"\ud83d\ude30", comet:"☄️", computer:"\ud83d\udcbb", computer_mouse:"\ud83d\uddb1", confetti_ball:"\ud83c\udf8a", confounded:"\ud83d\ude16", 
  confused:"\ud83d\ude15", congratulations:"㊗️", construction:"\ud83d\udea7", construction_worker_man:"\ud83d\udc77", construction_worker_woman:"\ud83d\udc77&zwj;♀️", control_knobs:"\ud83c\udf9b", convenience_store:"\ud83c\udfea", cookie:"\ud83c\udf6a", cool:"\ud83c\udd92", policeman:"\ud83d\udc6e", copyright:"©️", corn:"\ud83c\udf3d", couch_and_lamp:"\ud83d\udecb", couple:"\ud83d\udc6b", couple_with_heart_woman_man:"\ud83d\udc91", couple_with_heart_man_man:"\ud83d\udc68&zwj;❤️&zwj;\ud83d\udc68", 
  couple_with_heart_woman_woman:"\ud83d\udc69&zwj;❤️&zwj;\ud83d\udc69", couplekiss_man_man:"\ud83d\udc68&zwj;❤️&zwj;\ud83d\udc8b&zwj;\ud83d\udc68", couplekiss_man_woman:"\ud83d\udc8f", couplekiss_woman_woman:"\ud83d\udc69&zwj;❤️&zwj;\ud83d\udc8b&zwj;\ud83d\udc69", cow:"\ud83d\udc2e", cow2:"\ud83d\udc04", cowboy_hat_face:"\ud83e\udd20", crab:"\ud83e\udd80", crayon:"\ud83d\udd8d", credit_card:"\ud83d\udcb3", crescent_moon:"\ud83c\udf19", cricket:"\ud83c\udfcf", crocodile:"\ud83d\udc0a", croissant:"\ud83e\udd50", 
  crossed_fingers:"\ud83e\udd1e", crossed_flags:"\ud83c\udf8c", crossed_swords:"⚔️", crown:"\ud83d\udc51", cry:"\ud83d\ude22", crying_cat_face:"\ud83d\ude3f", crystal_ball:"\ud83d\udd2e", cucumber:"\ud83e\udd52", cupid:"\ud83d\udc98", curly_loop:"➰", currency_exchange:"\ud83d\udcb1", curry:"\ud83c\udf5b", custard:"\ud83c\udf6e", customs:"\ud83d\udec3", cyclone:"\ud83c\udf00", dagger:"\ud83d\udde1", dancer:"\ud83d\udc83", dancing_women:"\ud83d\udc6f", dancing_men:"\ud83d\udc6f&zwj;♂️", dango:"\ud83c\udf61", 
  dark_sunglasses:"\ud83d\udd76", dart:"\ud83c\udfaf", dash:"\ud83d\udca8", date:"\ud83d\udcc5", deciduous_tree:"\ud83c\udf33", deer:"\ud83e\udd8c", department_store:"\ud83c\udfec", derelict_house:"\ud83c\udfda", desert:"\ud83c\udfdc", desert_island:"\ud83c\udfdd", desktop_computer:"\ud83d\udda5", male_detective:"\ud83d\udd75️", diamond_shape_with_a_dot_inside:"\ud83d\udca0", diamonds:"♦️", disappointed:"\ud83d\ude1e", disappointed_relieved:"\ud83d\ude25", dizzy:"\ud83d\udcab", dizzy_face:"\ud83d\ude35", 
  do_not_litter:"\ud83d\udeaf", dog:"\ud83d\udc36", dog2:"\ud83d\udc15", dollar:"\ud83d\udcb5", dolls:"\ud83c\udf8e", dolphin:"\ud83d\udc2c", door:"\ud83d\udeaa", doughnut:"\ud83c\udf69", dove:"\ud83d\udd4a", dragon:"\ud83d\udc09", dragon_face:"\ud83d\udc32", dress:"\ud83d\udc57", dromedary_camel:"\ud83d\udc2a", drooling_face:"\ud83e\udd24", droplet:"\ud83d\udca7", drum:"\ud83e\udd41", duck:"\ud83e\udd86", dvd:"\ud83d\udcc0", "e-mail":"\ud83d\udce7", eagle:"\ud83e\udd85", ear:"\ud83d\udc42", ear_of_rice:"\ud83c\udf3e", 
  earth_africa:"\ud83c\udf0d", earth_americas:"\ud83c\udf0e", earth_asia:"\ud83c\udf0f", egg:"\ud83e\udd5a", eggplant:"\ud83c\udf46", eight_pointed_black_star:"✴️", eight_spoked_asterisk:"✳️", electric_plug:"\ud83d\udd0c", elephant:"\ud83d\udc18", email:"✉️", end:"\ud83d\udd1a", envelope_with_arrow:"\ud83d\udce9", euro:"\ud83d\udcb6", european_castle:"\ud83c\udff0", european_post_office:"\ud83c\udfe4", evergreen_tree:"\ud83c\udf32", exclamation:"❗️", expressionless:"\ud83d\ude11", eye:"\ud83d\udc41", 
  eye_speech_bubble:"\ud83d\udc41&zwj;\ud83d\udde8", eyeglasses:"\ud83d\udc53", eyes:"\ud83d\udc40", face_with_head_bandage:"\ud83e\udd15", face_with_thermometer:"\ud83e\udd12", fist_oncoming:"\ud83d\udc4a", factory:"\ud83c\udfed", fallen_leaf:"\ud83c\udf42", family_man_woman_boy:"\ud83d\udc6a", family_man_boy:"\ud83d\udc68&zwj;\ud83d\udc66", family_man_boy_boy:"\ud83d\udc68&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_man_girl:"\ud83d\udc68&zwj;\ud83d\udc67", family_man_girl_boy:"\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc66", 
  family_man_girl_girl:"\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_man_man_boy:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc66", family_man_man_boy_boy:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_man_man_girl:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc67", family_man_man_girl_boy:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc66", family_man_man_girl_girl:"\ud83d\udc68&zwj;\ud83d\udc68&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_man_woman_boy_boy:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc66&zwj;\ud83d\udc66", 
  family_man_woman_girl:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc67", family_man_woman_girl_boy:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc66", family_man_woman_girl_girl:"\ud83d\udc68&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_woman_boy:"\ud83d\udc69&zwj;\ud83d\udc66", family_woman_boy_boy:"\ud83d\udc69&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_woman_girl:"\ud83d\udc69&zwj;\ud83d\udc67", family_woman_girl_boy:"\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc66", 
  family_woman_girl_girl:"\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc67", family_woman_woman_boy:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc66", family_woman_woman_boy_boy:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc66&zwj;\ud83d\udc66", family_woman_woman_girl:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc67", family_woman_woman_girl_boy:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc66", family_woman_woman_girl_girl:"\ud83d\udc69&zwj;\ud83d\udc69&zwj;\ud83d\udc67&zwj;\ud83d\udc67", 
  fast_forward:"⏩", fax:"\ud83d\udce0", fearful:"\ud83d\ude28", feet:"\ud83d\udc3e", female_detective:"\ud83d\udd75️&zwj;♀️", ferris_wheel:"\ud83c\udfa1", ferry:"⛴", field_hockey:"\ud83c\udfd1", file_cabinet:"\ud83d\uddc4", file_folder:"\ud83d\udcc1", film_projector:"\ud83d\udcfd", film_strip:"\ud83c\udf9e", fire:"\ud83d\udd25", fire_engine:"\ud83d\ude92", fireworks:"\ud83c\udf86", first_quarter_moon:"\ud83c\udf13", first_quarter_moon_with_face:"\ud83c\udf1b", fish:"\ud83d\udc1f", fish_cake:"\ud83c\udf65", 
  fishing_pole_and_fish:"\ud83c\udfa3", fist_raised:"✊", fist_left:"\ud83e\udd1b", fist_right:"\ud83e\udd1c", flags:"\ud83c\udf8f", flashlight:"\ud83d\udd26", fleur_de_lis:"⚜️", flight_arrival:"\ud83d\udeec", flight_departure:"\ud83d\udeeb", floppy_disk:"\ud83d\udcbe", flower_playing_cards:"\ud83c\udfb4", flushed:"\ud83d\ude33", fog:"\ud83c\udf2b", foggy:"\ud83c\udf01", football:"\ud83c\udfc8", footprints:"\ud83d\udc63", fork_and_knife:"\ud83c\udf74", fountain:"⛲️", fountain_pen:"\ud83d\udd8b", four_leaf_clover:"\ud83c\udf40", 
  fox_face:"\ud83e\udd8a", framed_picture:"\ud83d\uddbc", free:"\ud83c\udd93", fried_egg:"\ud83c\udf73", fried_shrimp:"\ud83c\udf64", fries:"\ud83c\udf5f", frog:"\ud83d\udc38", frowning:"\ud83d\ude26", frowning_face:"☹️", frowning_man:"\ud83d\ude4d&zwj;♂️", frowning_woman:"\ud83d\ude4d", middle_finger:"\ud83d\udd95", fuelpump:"⛽️", full_moon:"\ud83c\udf15", full_moon_with_face:"\ud83c\udf1d", funeral_urn:"⚱️", game_die:"\ud83c\udfb2", gear:"⚙️", gem:"\ud83d\udc8e", gemini:"♊️", ghost:"\ud83d\udc7b", 
  gift:"\ud83c\udf81", gift_heart:"\ud83d\udc9d", girl:"\ud83d\udc67", globe_with_meridians:"\ud83c\udf10", goal_net:"\ud83e\udd45", goat:"\ud83d\udc10", golf:"⛳️", golfing_man:"\ud83c\udfcc️", golfing_woman:"\ud83c\udfcc️&zwj;♀️", gorilla:"\ud83e\udd8d", grapes:"\ud83c\udf47", green_apple:"\ud83c\udf4f", green_book:"\ud83d\udcd7", green_heart:"\ud83d\udc9a", green_salad:"\ud83e\udd57", grey_exclamation:"❕", grey_question:"❔", grimacing:"\ud83d\ude2c", grin:"\ud83d\ude01", grinning:"\ud83d\ude00", 
  guardsman:"\ud83d\udc82", guardswoman:"\ud83d\udc82&zwj;♀️", guitar:"\ud83c\udfb8", gun:"\ud83d\udd2b", haircut_woman:"\ud83d\udc87", haircut_man:"\ud83d\udc87&zwj;♂️", hamburger:"\ud83c\udf54", hammer:"\ud83d\udd28", hammer_and_pick:"⚒", hammer_and_wrench:"\ud83d\udee0", hamster:"\ud83d\udc39", hand:"✋", handbag:"\ud83d\udc5c", handshake:"\ud83e\udd1d", hankey:"\ud83d\udca9", hatched_chick:"\ud83d\udc25", hatching_chick:"\ud83d\udc23", headphones:"\ud83c\udfa7", hear_no_evil:"\ud83d\ude49", heart:"❤️", 
  heart_decoration:"\ud83d\udc9f", heart_eyes:"\ud83d\ude0d", heart_eyes_cat:"\ud83d\ude3b", heartbeat:"\ud83d\udc93", heartpulse:"\ud83d\udc97", hearts:"♥️", heavy_check_mark:"✔️", heavy_division_sign:"➗", heavy_dollar_sign:"\ud83d\udcb2", heavy_heart_exclamation:"❣️", heavy_minus_sign:"➖", heavy_multiplication_x:"✖️", heavy_plus_sign:"➕", helicopter:"\ud83d\ude81", herb:"\ud83c\udf3f", hibiscus:"\ud83c\udf3a", high_brightness:"\ud83d\udd06", high_heel:"\ud83d\udc60", hocho:"\ud83d\udd2a", hole:"\ud83d\udd73", 
  honey_pot:"\ud83c\udf6f", horse:"\ud83d\udc34", horse_racing:"\ud83c\udfc7", hospital:"\ud83c\udfe5", hot_pepper:"\ud83c\udf36", hotdog:"\ud83c\udf2d", hotel:"\ud83c\udfe8", hotsprings:"♨️", hourglass:"⌛️", hourglass_flowing_sand:"⏳", house:"\ud83c\udfe0", house_with_garden:"\ud83c\udfe1", houses:"\ud83c\udfd8", hugs:"\ud83e\udd17", hushed:"\ud83d\ude2f", ice_cream:"\ud83c\udf68", ice_hockey:"\ud83c\udfd2", ice_skate:"⛸", icecream:"\ud83c\udf66", id:"\ud83c\udd94", ideograph_advantage:"\ud83c\ude50", 
  imp:"\ud83d\udc7f", inbox_tray:"\ud83d\udce5", incoming_envelope:"\ud83d\udce8", tipping_hand_woman:"\ud83d\udc81", information_source:"ℹ️", innocent:"\ud83d\ude07", interrobang:"⁉️", iphone:"\ud83d\udcf1", izakaya_lantern:"\ud83c\udfee", jack_o_lantern:"\ud83c\udf83", japan:"\ud83d\uddfe", japanese_castle:"\ud83c\udfef", japanese_goblin:"\ud83d\udc7a", japanese_ogre:"\ud83d\udc79", jeans:"\ud83d\udc56", joy:"\ud83d\ude02", joy_cat:"\ud83d\ude39", joystick:"\ud83d\udd79", kaaba:"\ud83d\udd4b", 
  key:"\ud83d\udd11", keyboard:"⌨️", keycap_ten:"\ud83d\udd1f", kick_scooter:"\ud83d\udef4", kimono:"\ud83d\udc58", kiss:"\ud83d\udc8b", kissing:"\ud83d\ude17", kissing_cat:"\ud83d\ude3d", kissing_closed_eyes:"\ud83d\ude1a", kissing_heart:"\ud83d\ude18", kissing_smiling_eyes:"\ud83d\ude19", kiwi_fruit:"\ud83e\udd5d", koala:"\ud83d\udc28", koko:"\ud83c\ude01", label:"\ud83c\udff7", large_blue_circle:"\ud83d\udd35", large_blue_diamond:"\ud83d\udd37", large_orange_diamond:"\ud83d\udd36", last_quarter_moon:"\ud83c\udf17", 
  last_quarter_moon_with_face:"\ud83c\udf1c", latin_cross:"✝️", laughing:"\ud83d\ude06", leaves:"\ud83c\udf43", ledger:"\ud83d\udcd2", left_luggage:"\ud83d\udec5", left_right_arrow:"↔️", leftwards_arrow_with_hook:"↩️", lemon:"\ud83c\udf4b", leo:"♌️", leopard:"\ud83d\udc06", level_slider:"\ud83c\udf9a", libra:"♎️", light_rail:"\ud83d\ude88", link:"\ud83d\udd17", lion:"\ud83e\udd81", lips:"\ud83d\udc44", lipstick:"\ud83d\udc84", lizard:"\ud83e\udd8e", lock:"\ud83d\udd12", lock_with_ink_pen:"\ud83d\udd0f", 
  lollipop:"\ud83c\udf6d", loop:"➿", loud_sound:"\ud83d\udd0a", loudspeaker:"\ud83d\udce2", love_hotel:"\ud83c\udfe9", love_letter:"\ud83d\udc8c", low_brightness:"\ud83d\udd05", lying_face:"\ud83e\udd25", m:"Ⓜ️", mag:"\ud83d\udd0d", mag_right:"\ud83d\udd0e", mahjong:"\ud83c\udc04️", mailbox:"\ud83d\udceb", mailbox_closed:"\ud83d\udcea", mailbox_with_mail:"\ud83d\udcec", mailbox_with_no_mail:"\ud83d\udced", man:"\ud83d\udc68", man_artist:"\ud83d\udc68&zwj;\ud83c\udfa8", man_astronaut:"\ud83d\udc68&zwj;\ud83d\ude80", 
  man_cartwheeling:"\ud83e\udd38&zwj;♂️", man_cook:"\ud83d\udc68&zwj;\ud83c\udf73", man_dancing:"\ud83d\udd7a", man_facepalming:"\ud83e\udd26&zwj;♂️", man_factory_worker:"\ud83d\udc68&zwj;\ud83c\udfed", man_farmer:"\ud83d\udc68&zwj;\ud83c\udf3e", man_firefighter:"\ud83d\udc68&zwj;\ud83d\ude92", man_health_worker:"\ud83d\udc68&zwj;⚕️", man_in_tuxedo:"\ud83e\udd35", man_judge:"\ud83d\udc68&zwj;⚖️", man_juggling:"\ud83e\udd39&zwj;♂️", man_mechanic:"\ud83d\udc68&zwj;\ud83d\udd27", man_office_worker:"\ud83d\udc68&zwj;\ud83d\udcbc", 
  man_pilot:"\ud83d\udc68&zwj;✈️", man_playing_handball:"\ud83e\udd3e&zwj;♂️", man_playing_water_polo:"\ud83e\udd3d&zwj;♂️", man_scientist:"\ud83d\udc68&zwj;\ud83d\udd2c", man_shrugging:"\ud83e\udd37&zwj;♂️", man_singer:"\ud83d\udc68&zwj;\ud83c\udfa4", man_student:"\ud83d\udc68&zwj;\ud83c\udf93", man_teacher:"\ud83d\udc68&zwj;\ud83c\udfeb", man_technologist:"\ud83d\udc68&zwj;\ud83d\udcbb", man_with_gua_pi_mao:"\ud83d\udc72", man_with_turban:"\ud83d\udc73", tangerine:"\ud83c\udf4a", mans_shoe:"\ud83d\udc5e", 
  mantelpiece_clock:"\ud83d\udd70", maple_leaf:"\ud83c\udf41", martial_arts_uniform:"\ud83e\udd4b", mask:"\ud83d\ude37", massage_woman:"\ud83d\udc86", massage_man:"\ud83d\udc86&zwj;♂️", meat_on_bone:"\ud83c\udf56", medal_military:"\ud83c\udf96", medal_sports:"\ud83c\udfc5", mega:"\ud83d\udce3", melon:"\ud83c\udf48", memo:"\ud83d\udcdd", men_wrestling:"\ud83e\udd3c&zwj;♂️", menorah:"\ud83d\udd4e", mens:"\ud83d\udeb9", metal:"\ud83e\udd18", metro:"\ud83d\ude87", microphone:"\ud83c\udfa4", microscope:"\ud83d\udd2c", 
  milk_glass:"\ud83e\udd5b", milky_way:"\ud83c\udf0c", minibus:"\ud83d\ude90", minidisc:"\ud83d\udcbd", mobile_phone_off:"\ud83d\udcf4", money_mouth_face:"\ud83e\udd11", money_with_wings:"\ud83d\udcb8", moneybag:"\ud83d\udcb0", monkey:"\ud83d\udc12", monkey_face:"\ud83d\udc35", monorail:"\ud83d\ude9d", moon:"\ud83c\udf14", mortar_board:"\ud83c\udf93", mosque:"\ud83d\udd4c", motor_boat:"\ud83d\udee5", motor_scooter:"\ud83d\udef5", motorcycle:"\ud83c\udfcd", motorway:"\ud83d\udee3", mount_fuji:"\ud83d\uddfb", 
  mountain:"⛰", mountain_biking_man:"\ud83d\udeb5", mountain_biking_woman:"\ud83d\udeb5&zwj;♀️", mountain_cableway:"\ud83d\udea0", mountain_railway:"\ud83d\ude9e", mountain_snow:"\ud83c\udfd4", mouse:"\ud83d\udc2d", mouse2:"\ud83d\udc01", movie_camera:"\ud83c\udfa5", moyai:"\ud83d\uddff", mrs_claus:"\ud83e\udd36", muscle:"\ud83d\udcaa", mushroom:"\ud83c\udf44", musical_keyboard:"\ud83c\udfb9", musical_note:"\ud83c\udfb5", musical_score:"\ud83c\udfbc", mute:"\ud83d\udd07", nail_care:"\ud83d\udc85", 
  name_badge:"\ud83d\udcdb", national_park:"\ud83c\udfde", nauseated_face:"\ud83e\udd22", necktie:"\ud83d\udc54", negative_squared_cross_mark:"❎", nerd_face:"\ud83e\udd13", neutral_face:"\ud83d\ude10", new:"\ud83c\udd95", new_moon:"\ud83c\udf11", new_moon_with_face:"\ud83c\udf1a", newspaper:"\ud83d\udcf0", newspaper_roll:"\ud83d\uddde", next_track_button:"⏭", ng:"\ud83c\udd96", no_good_man:"\ud83d\ude45&zwj;♂️", no_good_woman:"\ud83d\ude45", night_with_stars:"\ud83c\udf03", no_bell:"\ud83d\udd15", 
  no_bicycles:"\ud83d\udeb3", no_entry:"⛔️", no_entry_sign:"\ud83d\udeab", no_mobile_phones:"\ud83d\udcf5", no_mouth:"\ud83d\ude36", no_pedestrians:"\ud83d\udeb7", no_smoking:"\ud83d\udead", "non-potable_water":"\ud83d\udeb1", nose:"\ud83d\udc43", notebook:"\ud83d\udcd3", notebook_with_decorative_cover:"\ud83d\udcd4", notes:"\ud83c\udfb6", nut_and_bolt:"\ud83d\udd29", o:"⭕️", o2:"\ud83c\udd7e️", ocean:"\ud83c\udf0a", octopus:"\ud83d\udc19", oden:"\ud83c\udf62", office:"\ud83c\udfe2", oil_drum:"\ud83d\udee2", 
  ok:"\ud83c\udd97", ok_hand:"\ud83d\udc4c", ok_man:"\ud83d\ude46&zwj;♂️", ok_woman:"\ud83d\ude46", old_key:"\ud83d\udddd", older_man:"\ud83d\udc74", older_woman:"\ud83d\udc75", om:"\ud83d\udd49", on:"\ud83d\udd1b", oncoming_automobile:"\ud83d\ude98", oncoming_bus:"\ud83d\ude8d", oncoming_police_car:"\ud83d\ude94", oncoming_taxi:"\ud83d\ude96", open_file_folder:"\ud83d\udcc2", open_hands:"\ud83d\udc50", open_mouth:"\ud83d\ude2e", open_umbrella:"☂️", ophiuchus:"⛎", orange_book:"\ud83d\udcd9", orthodox_cross:"☦️", 
  outbox_tray:"\ud83d\udce4", owl:"\ud83e\udd89", ox:"\ud83d\udc02", package:"\ud83d\udce6", page_facing_up:"\ud83d\udcc4", page_with_curl:"\ud83d\udcc3", pager:"\ud83d\udcdf", paintbrush:"\ud83d\udd8c", palm_tree:"\ud83c\udf34", pancakes:"\ud83e\udd5e", panda_face:"\ud83d\udc3c", paperclip:"\ud83d\udcce", paperclips:"\ud83d\udd87", parasol_on_ground:"⛱", parking:"\ud83c\udd7f️", part_alternation_mark:"〽️", partly_sunny:"⛅️", passenger_ship:"\ud83d\udef3", passport_control:"\ud83d\udec2", pause_button:"⏸", 
  peace_symbol:"☮️", peach:"\ud83c\udf51", peanuts:"\ud83e\udd5c", pear:"\ud83c\udf50", pen:"\ud83d\udd8a", pencil2:"✏️", penguin:"\ud83d\udc27", pensive:"\ud83d\ude14", performing_arts:"\ud83c\udfad", persevere:"\ud83d\ude23", person_fencing:"\ud83e\udd3a", pouting_woman:"\ud83d\ude4e", phone:"☎️", pick:"⛏", pig:"\ud83d\udc37", pig2:"\ud83d\udc16", pig_nose:"\ud83d\udc3d", pill:"\ud83d\udc8a", pineapple:"\ud83c\udf4d", ping_pong:"\ud83c\udfd3", pisces:"♓️", pizza:"\ud83c\udf55", place_of_worship:"\ud83d\uded0", 
  plate_with_cutlery:"\ud83c\udf7d", play_or_pause_button:"⏯", point_down:"\ud83d\udc47", point_left:"\ud83d\udc48", point_right:"\ud83d\udc49", point_up:"☝️", point_up_2:"\ud83d\udc46", police_car:"\ud83d\ude93", policewoman:"\ud83d\udc6e&zwj;♀️", poodle:"\ud83d\udc29", popcorn:"\ud83c\udf7f", post_office:"\ud83c\udfe3", postal_horn:"\ud83d\udcef", postbox:"\ud83d\udcee", potable_water:"\ud83d\udeb0", potato:"\ud83e\udd54", pouch:"\ud83d\udc5d", poultry_leg:"\ud83c\udf57", pound:"\ud83d\udcb7", 
  rage:"\ud83d\ude21", pouting_cat:"\ud83d\ude3e", pouting_man:"\ud83d\ude4e&zwj;♂️", pray:"\ud83d\ude4f", prayer_beads:"\ud83d\udcff", pregnant_woman:"\ud83e\udd30", previous_track_button:"⏮", prince:"\ud83e\udd34", princess:"\ud83d\udc78", printer:"\ud83d\udda8", purple_heart:"\ud83d\udc9c", purse:"\ud83d\udc5b", pushpin:"\ud83d\udccc", put_litter_in_its_place:"\ud83d\udeae", question:"❓", rabbit:"\ud83d\udc30", rabbit2:"\ud83d\udc07", racehorse:"\ud83d\udc0e", racing_car:"\ud83c\udfce", radio:"\ud83d\udcfb", 
  radio_button:"\ud83d\udd18", radioactive:"☢️", railway_car:"\ud83d\ude83", railway_track:"\ud83d\udee4", rainbow:"\ud83c\udf08", rainbow_flag:"\ud83c\udff3️&zwj;\ud83c\udf08", raised_back_of_hand:"\ud83e\udd1a", raised_hand_with_fingers_splayed:"\ud83d\udd90", raised_hands:"\ud83d\ude4c", raising_hand_woman:"\ud83d\ude4b", raising_hand_man:"\ud83d\ude4b&zwj;♂️", ram:"\ud83d\udc0f", ramen:"\ud83c\udf5c", rat:"\ud83d\udc00", record_button:"⏺", recycle:"♻️", red_circle:"\ud83d\udd34", registered:"®️", 
  relaxed:"☺️", relieved:"\ud83d\ude0c", reminder_ribbon:"\ud83c\udf97", repeat:"\ud83d\udd01", repeat_one:"\ud83d\udd02", rescue_worker_helmet:"⛑", restroom:"\ud83d\udebb", revolving_hearts:"\ud83d\udc9e", rewind:"⏪", rhinoceros:"\ud83e\udd8f", ribbon:"\ud83c\udf80", rice:"\ud83c\udf5a", rice_ball:"\ud83c\udf59", rice_cracker:"\ud83c\udf58", rice_scene:"\ud83c\udf91", right_anger_bubble:"\ud83d\uddef", ring:"\ud83d\udc8d", robot:"\ud83e\udd16", rocket:"\ud83d\ude80", rofl:"\ud83e\udd23", roll_eyes:"\ud83d\ude44", 
  roller_coaster:"\ud83c\udfa2", rooster:"\ud83d\udc13", rose:"\ud83c\udf39", rosette:"\ud83c\udff5", rotating_light:"\ud83d\udea8", round_pushpin:"\ud83d\udccd", rowing_man:"\ud83d\udea3", rowing_woman:"\ud83d\udea3&zwj;♀️", rugby_football:"\ud83c\udfc9", running_man:"\ud83c\udfc3", running_shirt_with_sash:"\ud83c\udfbd", running_woman:"\ud83c\udfc3&zwj;♀️", sa:"\ud83c\ude02️", sagittarius:"♐️", sake:"\ud83c\udf76", sandal:"\ud83d\udc61", santa:"\ud83c\udf85", satellite:"\ud83d\udce1", saxophone:"\ud83c\udfb7", 
  school:"\ud83c\udfeb", school_satchel:"\ud83c\udf92", scissors:"✂️", scorpion:"\ud83e\udd82", scorpius:"♏️", scream:"\ud83d\ude31", scream_cat:"\ud83d\ude40", scroll:"\ud83d\udcdc", seat:"\ud83d\udcba", secret:"㊙️", see_no_evil:"\ud83d\ude48", seedling:"\ud83c\udf31", selfie:"\ud83e\udd33", shallow_pan_of_food:"\ud83e\udd58", shamrock:"☘️", shark:"\ud83e\udd88", shaved_ice:"\ud83c\udf67", sheep:"\ud83d\udc11", shell:"\ud83d\udc1a", shield:"\ud83d\udee1", shinto_shrine:"⛩", ship:"\ud83d\udea2", 
  shirt:"\ud83d\udc55", shopping:"\ud83d\udecd", shopping_cart:"\ud83d\uded2", shower:"\ud83d\udebf", shrimp:"\ud83e\udd90", signal_strength:"\ud83d\udcf6", six_pointed_star:"\ud83d\udd2f", ski:"\ud83c\udfbf", skier:"⛷", skull:"\ud83d\udc80", skull_and_crossbones:"☠️", sleeping:"\ud83d\ude34", sleeping_bed:"\ud83d\udecc", sleepy:"\ud83d\ude2a", slightly_frowning_face:"\ud83d\ude41", slightly_smiling_face:"\ud83d\ude42", slot_machine:"\ud83c\udfb0", small_airplane:"\ud83d\udee9", small_blue_diamond:"\ud83d\udd39", 
  small_orange_diamond:"\ud83d\udd38", small_red_triangle:"\ud83d\udd3a", small_red_triangle_down:"\ud83d\udd3b", smile:"\ud83d\ude04", smile_cat:"\ud83d\ude38", smiley:"\ud83d\ude03", smiley_cat:"\ud83d\ude3a", smiling_imp:"\ud83d\ude08", smirk:"\ud83d\ude0f", smirk_cat:"\ud83d\ude3c", smoking:"\ud83d\udeac", snail:"\ud83d\udc0c", snake:"\ud83d\udc0d", sneezing_face:"\ud83e\udd27", snowboarder:"\ud83c\udfc2", snowflake:"❄️", snowman:"⛄️", snowman_with_snow:"☃️", sob:"\ud83d\ude2d", soccer:"⚽️", 
  soon:"\ud83d\udd1c", sos:"\ud83c\udd98", sound:"\ud83d\udd09", space_invader:"\ud83d\udc7e", spades:"♠️", spaghetti:"\ud83c\udf5d", sparkle:"❇️", sparkler:"\ud83c\udf87", sparkles:"✨", sparkling_heart:"\ud83d\udc96", speak_no_evil:"\ud83d\ude4a", speaker:"\ud83d\udd08", speaking_head:"\ud83d\udde3", speech_balloon:"\ud83d\udcac", speedboat:"\ud83d\udea4", spider:"\ud83d\udd77", spider_web:"\ud83d\udd78", spiral_calendar:"\ud83d\uddd3", spiral_notepad:"\ud83d\uddd2", spoon:"\ud83e\udd44", squid:"\ud83e\udd91", 
  stadium:"\ud83c\udfdf", star:"⭐️", star2:"\ud83c\udf1f", star_and_crescent:"☪️", star_of_david:"✡️", stars:"\ud83c\udf20", station:"\ud83d\ude89", statue_of_liberty:"\ud83d\uddfd", steam_locomotive:"\ud83d\ude82", stew:"\ud83c\udf72", stop_button:"⏹", stop_sign:"\ud83d\uded1", stopwatch:"⏱", straight_ruler:"\ud83d\udccf", strawberry:"\ud83c\udf53", stuck_out_tongue:"\ud83d\ude1b", stuck_out_tongue_closed_eyes:"\ud83d\ude1d", stuck_out_tongue_winking_eye:"\ud83d\ude1c", studio_microphone:"\ud83c\udf99", 
  stuffed_flatbread:"\ud83e\udd59", sun_behind_large_cloud:"\ud83c\udf25", sun_behind_rain_cloud:"\ud83c\udf26", sun_behind_small_cloud:"\ud83c\udf24", sun_with_face:"\ud83c\udf1e", sunflower:"\ud83c\udf3b", sunglasses:"\ud83d\ude0e", sunny:"☀️", sunrise:"\ud83c\udf05", sunrise_over_mountains:"\ud83c\udf04", surfing_man:"\ud83c\udfc4", surfing_woman:"\ud83c\udfc4&zwj;♀️", sushi:"\ud83c\udf63", suspension_railway:"\ud83d\ude9f", sweat:"\ud83d\ude13", sweat_drops:"\ud83d\udca6", sweat_smile:"\ud83d\ude05", 
  sweet_potato:"\ud83c\udf60", swimming_man:"\ud83c\udfca", swimming_woman:"\ud83c\udfca&zwj;♀️", symbols:"\ud83d\udd23", synagogue:"\ud83d\udd4d", syringe:"\ud83d\udc89", taco:"\ud83c\udf2e", tada:"\ud83c\udf89", tanabata_tree:"\ud83c\udf8b", taurus:"♉️", taxi:"\ud83d\ude95", tea:"\ud83c\udf75", telephone_receiver:"\ud83d\udcde", telescope:"\ud83d\udd2d", tennis:"\ud83c\udfbe", tent:"⛺️", thermometer:"\ud83c\udf21", thinking:"\ud83e\udd14", thought_balloon:"\ud83d\udcad", ticket:"\ud83c\udfab", 
  tickets:"\ud83c\udf9f", tiger:"\ud83d\udc2f", tiger2:"\ud83d\udc05", timer_clock:"⏲", tipping_hand_man:"\ud83d\udc81&zwj;♂️", tired_face:"\ud83d\ude2b", tm:"™️", toilet:"\ud83d\udebd", tokyo_tower:"\ud83d\uddfc", tomato:"\ud83c\udf45", tongue:"\ud83d\udc45", top:"\ud83d\udd1d", tophat:"\ud83c\udfa9", tornado:"\ud83c\udf2a", trackball:"\ud83d\uddb2", tractor:"\ud83d\ude9c", traffic_light:"\ud83d\udea5", train:"\ud83d\ude8b", train2:"\ud83d\ude86", tram:"\ud83d\ude8a", triangular_flag_on_post:"\ud83d\udea9", 
  triangular_ruler:"\ud83d\udcd0", trident:"\ud83d\udd31", triumph:"\ud83d\ude24", trolleybus:"\ud83d\ude8e", trophy:"\ud83c\udfc6", tropical_drink:"\ud83c\udf79", tropical_fish:"\ud83d\udc20", truck:"\ud83d\ude9a", trumpet:"\ud83c\udfba", tulip:"\ud83c\udf37", tumbler_glass:"\ud83e\udd43", turkey:"\ud83e\udd83", turtle:"\ud83d\udc22", tv:"\ud83d\udcfa", twisted_rightwards_arrows:"\ud83d\udd00", two_hearts:"\ud83d\udc95", two_men_holding_hands:"\ud83d\udc6c", two_women_holding_hands:"\ud83d\udc6d", 
  u5272:"\ud83c\ude39", u5408:"\ud83c\ude34", u55b6:"\ud83c\ude3a", u6307:"\ud83c\ude2f️", u6708:"\ud83c\ude37️", u6709:"\ud83c\ude36", u6e80:"\ud83c\ude35", u7121:"\ud83c\ude1a️", u7533:"\ud83c\ude38", u7981:"\ud83c\ude32", u7a7a:"\ud83c\ude33", umbrella:"☔️", unamused:"\ud83d\ude12", underage:"\ud83d\udd1e", unicorn:"\ud83e\udd84", unlock:"\ud83d\udd13", up:"\ud83c\udd99", upside_down_face:"\ud83d\ude43", v:"✌️", vertical_traffic_light:"\ud83d\udea6", vhs:"\ud83d\udcfc", vibration_mode:"\ud83d\udcf3", 
  video_camera:"\ud83d\udcf9", video_game:"\ud83c\udfae", violin:"\ud83c\udfbb", virgo:"♍️", volcano:"\ud83c\udf0b", volleyball:"\ud83c\udfd0", vs:"\ud83c\udd9a", vulcan_salute:"\ud83d\udd96", walking_man:"\ud83d\udeb6", walking_woman:"\ud83d\udeb6&zwj;♀️", waning_crescent_moon:"\ud83c\udf18", waning_gibbous_moon:"\ud83c\udf16", warning:"⚠️", wastebasket:"\ud83d\uddd1", watch:"⌚️", water_buffalo:"\ud83d\udc03", watermelon:"\ud83c\udf49", wave:"\ud83d\udc4b", wavy_dash:"〰️", waxing_crescent_moon:"\ud83c\udf12", 
  wc:"\ud83d\udebe", weary:"\ud83d\ude29", wedding:"\ud83d\udc92", weight_lifting_man:"\ud83c\udfcb️", weight_lifting_woman:"\ud83c\udfcb️&zwj;♀️", whale:"\ud83d\udc33", whale2:"\ud83d\udc0b", wheel_of_dharma:"☸️", wheelchair:"♿️", white_check_mark:"✅", white_circle:"⚪️", white_flag:"\ud83c\udff3️", white_flower:"\ud83d\udcae", white_large_square:"⬜️", white_medium_small_square:"◽️", white_medium_square:"◻️", white_small_square:"▫️", white_square_button:"\ud83d\udd33", wilted_flower:"\ud83e\udd40", 
  wind_chime:"\ud83c\udf90", wind_face:"\ud83c\udf2c", wine_glass:"\ud83c\udf77", wink:"\ud83d\ude09", wolf:"\ud83d\udc3a", woman:"\ud83d\udc69", woman_artist:"\ud83d\udc69&zwj;\ud83c\udfa8", woman_astronaut:"\ud83d\udc69&zwj;\ud83d\ude80", woman_cartwheeling:"\ud83e\udd38&zwj;♀️", woman_cook:"\ud83d\udc69&zwj;\ud83c\udf73", woman_facepalming:"\ud83e\udd26&zwj;♀️", woman_factory_worker:"\ud83d\udc69&zwj;\ud83c\udfed", woman_farmer:"\ud83d\udc69&zwj;\ud83c\udf3e", woman_firefighter:"\ud83d\udc69&zwj;\ud83d\ude92", 
  woman_health_worker:"\ud83d\udc69&zwj;⚕️", woman_judge:"\ud83d\udc69&zwj;⚖️", woman_juggling:"\ud83e\udd39&zwj;♀️", woman_mechanic:"\ud83d\udc69&zwj;\ud83d\udd27", woman_office_worker:"\ud83d\udc69&zwj;\ud83d\udcbc", woman_pilot:"\ud83d\udc69&zwj;✈️", woman_playing_handball:"\ud83e\udd3e&zwj;♀️", woman_playing_water_polo:"\ud83e\udd3d&zwj;♀️", woman_scientist:"\ud83d\udc69&zwj;\ud83d\udd2c", woman_shrugging:"\ud83e\udd37&zwj;♀️", woman_singer:"\ud83d\udc69&zwj;\ud83c\udfa4", woman_student:"\ud83d\udc69&zwj;\ud83c\udf93", 
  woman_teacher:"\ud83d\udc69&zwj;\ud83c\udfeb", woman_technologist:"\ud83d\udc69&zwj;\ud83d\udcbb", woman_with_turban:"\ud83d\udc73&zwj;♀️", womans_clothes:"\ud83d\udc5a", womans_hat:"\ud83d\udc52", women_wrestling:"\ud83e\udd3c&zwj;♀️", womens:"\ud83d\udeba", world_map:"\ud83d\uddfa", worried:"\ud83d\ude1f", wrench:"\ud83d\udd27", writing_hand:"✍️", x:"❌", yellow_heart:"\ud83d\udc9b", yen:"\ud83d\udcb4", yin_yang:"☯️", yum:"\ud83d\ude0b", zap:"⚡️", zipper_mouth_face:"\ud83e\udd10", zzz:"\ud83d\udca4", 
  octocat:'<img alt=":octocat:" height="20" width="20" align="absmiddle" src="https://assets-cdn.github.com/images/icons/emoji/octocat.png">', showdown:"<span style=\"font-family: 'Anonymous Pro', monospace; text-decoration: underline; text-decoration-style: dashed; text-decoration-color: #3e8b8a;text-underline-position: under;\">S</span>"};
  d.Converter = function(a) {
    function e(u, D) {
      if (D = D || null, d.helper.isString(u)) {
        if (D = u = d.helper.stdExtName(u), d.extensions[u]) {
          console.warn("DEPRECATION WARNING: " + u + " is an old extension that uses a deprecated loading method.Please inform the developer that the extension should be updated!");
          var K = d.extensions[u];
          D = u;
          if ("function" == typeof K && (K = K(new d.Converter)), d.helper.isArray(K) || (K = [K]), !(D = b(K, D)).valid) {
            throw Error(D.error);
          }
          for (u = 0; u < K.length; ++u) {
            switch(K[u].type) {
              case "lang":
                m.push(K[u]);
                break;
              case "output":
                y.push(K[u]);
                break;
              default:
                throw Error("Extension loader error: Type unrecognized!!!");
            }
          }
          return;
        }
        if (d.helper.isUndefined(t[u])) {
          throw Error('Extension "' + u + '" could not be loaded. It was either not found or is not a valid extension.');
        }
        u = t[u];
      }
      "function" == typeof u && (u = u());
      D = b(u = d.helper.isArray(u) ? u : [u], D);
      if (!D.valid) {
        throw Error(D.error);
      }
      for (D = 0; D < u.length; ++D) {
        switch(u[D].type) {
          case "lang":
            m.push(u[D]);
            break;
          case "output":
            y.push(u[D]);
        }
        if (u[D].hasOwnProperty("listeners")) {
          for (K in u[D].listeners) {
            u[D].listeners.hasOwnProperty(K) && c(K, u[D].listeners[K]);
          }
        }
      }
    }
    function c(u, D) {
      if (!d.helper.isString(u)) {
        throw Error("Invalid argument in converter.listen() method: name must be a string, but " + typeof u + " given");
      }
      if ("function" != typeof D) {
        throw Error("Invalid argument in converter.listen() method: callback must be a function, but " + typeof D + " given");
      }
      A.hasOwnProperty(u) || (A[u] = []);
      A[u].push(D);
    }
    var k, h, r = {}, m = [], y = [], A = {}, J = x, O = {parsed:{}, raw:"", format:""};
    for (k in a = a || {}, v) {
      v.hasOwnProperty(k) && (r[k] = v[k]);
    }
    if ("object" != typeof a) {
      throw Error("Converter expects the passed parameter to be an object, but " + typeof a + " was passed instead.");
    }
    for (h in a) {
      a.hasOwnProperty(h) && (r[h] = a[h]);
    }
    r.extensions && d.helper.forEach(r.extensions, e);
    this._dispatch = function(u, D, K, G) {
      if (A.hasOwnProperty(u)) {
        for (var P = 0; P < A[u].length; ++P) {
          var R = A[u][P](u, D, this, K, G);
          R && void 0 !== R && (D = R);
        }
      }
      return D;
    };
    this.listen = function(u, D) {
      return c(u, D), this;
    };
    this.makeHtml = function(u) {
      if (!u) {
        return u;
      }
      var D, K, G = {gHtmlBlocks:[], gHtmlMdBlocks:[], gHtmlSpans:[], gUrls:{}, gTitles:{}, gDimensions:{}, gListLevel:0, hashLinkCounts:{}, langExtensions:m, outputModifiers:y, converter:this, ghCodeBlocks:[], metadata:{parsed:{}, raw:"", format:""}};
      return u = (u = (u = (u = (u = u.replace(/¨/g, "¨T")).replace(/\$/g, "¨D")).replace(/\r\n/g, "\n")).replace(/\r/g, "\n")).replace(/\u00A0/g, "&nbsp;"), r.smartIndentationFix && (K = (D = u).match(/^\s*/)[0].length, K = new RegExp("^\\s{0," + K + "}", "gm"), u = D.replace(K, "")), u = "\n\n" + u + "\n\n", u = (u = d.subParser("detab")(u, r, G)).replace(/^[ \t]+$/gm, ""), d.helper.forEach(m, function(P) {
        u = d.subParser("runExtension")(P, u, r, G);
      }), u = d.subParser("metadata")(u, r, G), u = d.subParser("hashPreCodeTags")(u, r, G), u = d.subParser("githubCodeBlocks")(u, r, G), u = d.subParser("hashHTMLBlocks")(u, r, G), u = d.subParser("hashCodeTags")(u, r, G), u = d.subParser("stripLinkDefinitions")(u, r, G), u = d.subParser("blockGamut")(u, r, G), u = d.subParser("unhashHTMLSpans")(u, r, G), u = (u = (u = d.subParser("unescapeSpecialChars")(u, r, G)).replace(/¨D/g, "$$")).replace(/¨T/g, "¨"), u = d.subParser("completeHTMLDocument")(u, 
      r, G), d.helper.forEach(y, function(P) {
        u = d.subParser("runExtension")(P, u, r, G);
      }), O = G.metadata, u;
    };
    this.makeMarkdown = this.makeMd = function(u, D) {
      if (u = (u = (u = u.replace(/\r\n/g, "\n")).replace(/\r/g, "\n")).replace(/>[ \t]+</, ">¨NBSP;<"), !D) {
        if (!window || !window.document) {
          throw Error("HTMLParser is undefined. If in a webworker or nodejs environment, you need to provide a WHATWG DOM and HTML such as JSDOM");
        }
        D = window.document;
      }
      D = D.createElement("div");
      u = (D.innerHTML = u, {preList:function(P) {
        P = P.querySelectorAll("pre");
        for (var R = [], S = 0; S < P.length; ++S) {
          if (1 === P[S].childElementCount && "code" === P[S].firstChild.tagName.toLowerCase()) {
            var aa = P[S].firstChild.innerHTML.trim(), ha = P[S].firstChild.getAttribute("data-language") || "";
            if ("" === ha) {
              for (var ua = P[S].firstChild.className.split(" "), ra = 0; ra < ua.length; ++ra) {
                var sa = ua[ra].match(/^language-(.+)$/);
                if (null !== sa) {
                  ha = sa[1];
                  break;
                }
              }
            }
            aa = d.helper.unescapeHTMLEntities(aa);
            R.push(aa);
            P[S].outerHTML = '<precode language="' + ha + '" precodenum="' + S.toString() + '"></precode>';
          } else {
            R.push(P[S].innerHTML), P[S].innerHTML = "", P[S].setAttribute("prenum", S.toString());
          }
        }
        return R;
      }(D)});
      D = (!function S(R) {
        for (var aa = 0; aa < R.childNodes.length; ++aa) {
          var ha = R.childNodes[aa];
          3 === ha.nodeType ? /\S/.test(ha.nodeValue) || /^[ ]+$/.test(ha.nodeValue) ? (ha.nodeValue = ha.nodeValue.split("\n").join(" "), ha.nodeValue = ha.nodeValue.replace(/(\s)+/g, "$1")) : (R.removeChild(ha), --aa) : 1 === ha.nodeType && S(ha);
        }
      }(D), D.childNodes);
      for (var K = "", G = 0; G < D.length; G++) {
        K += d.subParser("makeMarkdown.node")(D[G], u);
      }
      return K;
    };
    this.setOption = function(u, D) {
      r[u] = D;
    };
    this.getOption = function(u) {
      return r[u];
    };
    this.getOptions = function() {
      return r;
    };
    this.addExtension = function(u, D) {
      e(u, D || null);
    };
    this.useExtension = function(u) {
      e(u);
    };
    this.setFlavor = function(u) {
      if (!C.hasOwnProperty(u)) {
        throw Error(u + " flavor was not found");
      }
      var D, K = C[u];
      for (D in J = u, K) {
        K.hasOwnProperty(D) && (r[D] = K[D]);
      }
    };
    this.getFlavor = function() {
      return J;
    };
    this.removeExtension = function(u) {
      d.helper.isArray(u) || (u = [u]);
      for (var D = 0; D < u.length; ++D) {
        for (var K = u[D], G = 0; G < m.length; ++G) {
          m[G] === K && m.splice(G, 1);
        }
        for (G = 0; G < y.length; ++G) {
          y[G] === K && y.splice(G, 1);
        }
      }
    };
    this.getAllExtensions = function() {
      return {language:m, output:y};
    };
    this.getMetadata = function(u) {
      return u ? O.raw : O.parsed;
    };
    this.getMetadataFormat = function() {
      return O.format;
    };
    this._setMetadataPair = function(u, D) {
      O.parsed[u] = D;
    };
    this._setMetadataFormat = function(u) {
      O.format = u;
    };
    this._setMetadataRaw = function(u) {
      O.raw = u;
    };
  };
  d.subParser("anchors", function(a, e, c) {
    function k(h, r, m, y, A, J, O) {
      if (d.helper.isUndefined(O) && (O = ""), m = m.toLowerCase(), -1 < h.search(/\(<?\s*>? ?(['"].*['"])?\)$/m)) {
        y = "";
      } else {
        if (!y) {
          if (m = m || r.toLowerCase().replace(/ ?\n/g, " "), d.helper.isUndefined(c.gUrls[m])) {
            return h;
          }
          y = c.gUrls[m];
          d.helper.isUndefined(c.gTitles[m]) || (O = c.gTitles[m]);
        }
      }
      return h = '<a href="' + (y = y.replace(d.helper.regexes.asteriskDashAndColon, d.helper.escapeCharactersCallback)) + '"', "" !== O && null !== O && (h += ' title="' + O.replace(/"/g, "&quot;").replace(d.helper.regexes.asteriskDashAndColon, d.helper.escapeCharactersCallback) + '"'), e.openLinksInNewWindow && !/^#/.test(y) && (h += ' rel="noopener noreferrer" target="¨E95Eblank"'), h + (">" + r + "</a>");
    }
    return a = (a = (a = (a = (a = c.converter._dispatch("anchors.before", a, e, c)).replace(/\[((?:\[[^\]]*]|[^\[\]])*)] ?(?:\n *)?\[(.*?)]()()()()/g, k)).replace(/\[((?:\[[^\]]*]|[^\[\]])*)]()[ \t]*\([ \t]?<([^>]*)>(?:[ \t]*((["'])([^"]*?)\5))?[ \t]?\)/g, k)).replace(/\[((?:\[[^\]]*]|[^\[\]])*)]()[ \t]*\([ \t]?<?([\S]+?(?:\([\S]*?\)[\S]*?)?)>?(?:[ \t]*((["'])([^"]*?)\5))?[ \t]?\)/g, k)).replace(/\[([^\[\]]+)]()()()()()/g, k), e.ghMentions && (a = a.replace(/(^|\s)(\\)?(@([a-z\d]+(?:[a-z\d.-]+?[a-z\d]+)*))/gim, 
    function(h, r, m, y, A) {
      if ("\\" === m) {
        return r + y;
      }
      if (!d.helper.isString(e.ghMentionsLink)) {
        throw Error("ghMentionsLink option must be a string");
      }
      m = "";
      return r + '<a href="' + e.ghMentionsLink.replace(/\{u}/g, A) + '"' + (e.openLinksInNewWindow ? ' rel="noopener noreferrer" target="¨E95Eblank"' : m) + ">" + y + "</a>";
    })), a = c.converter._dispatch("anchors.after", a, e, c);
  });
  var E = /([*~_]+|\b)(((https?|ftp|dict):\/\/|www\.)[^'">\s]+?\.[^'">\s]+?)()(\1)?(?=\s|$)(?!["<>])/gi, L = /([*~_]+|\b)(((https?|ftp|dict):\/\/|www\.)[^'">\s]+\.[^'">\s]+?)([.!?,()\[\]])?(\1)?(?=\s|$)(?!["<>])/gi, I = /()<(((https?|ftp|dict):\/\/|www\.)[^'">\s]+)()>()/gi, M = /(^|\s)(?:mailto:)?([A-Za-z0-9!#$%&'*+-/=?^_`{|}~.]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)(?=$|\s)/gim, T = /<()(?:mailto:)?([-.\w]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)>/gi;
  d.subParser("autoLinks", function(a, e, c) {
    return a = (a = (a = c.converter._dispatch("autoLinks.before", a, e, c)).replace(I, n(e))).replace(T, q(e, c)), c.converter._dispatch("autoLinks.after", a, e, c);
  });
  d.subParser("simplifiedAutoLinks", function(a, e, c) {
    return e.simplifiedAutoLink ? (a = c.converter._dispatch("simplifiedAutoLinks.before", a, e, c), a = (a = e.excludeTrailingPunctuationFromURLs ? a.replace(L, n(e)) : a.replace(E, n(e))).replace(M, q(e, c)), c.converter._dispatch("simplifiedAutoLinks.after", a, e, c)) : a;
  });
  d.subParser("blockGamut", function(a, e, c) {
    return a = c.converter._dispatch("blockGamut.before", a, e, c), a = d.subParser("blockQuotes")(a, e, c), a = d.subParser("headers")(a, e, c), a = d.subParser("horizontalRule")(a, e, c), a = d.subParser("lists")(a, e, c), a = d.subParser("codeBlocks")(a, e, c), a = d.subParser("tables")(a, e, c), a = d.subParser("hashHTMLBlocks")(a, e, c), a = d.subParser("paragraphs")(a, e, c), c.converter._dispatch("blockGamut.after", a, e, c);
  });
  d.subParser("blockQuotes", function(a, e, c) {
    a = c.converter._dispatch("blockQuotes.before", a, e, c);
    var k = /(^ {0,3}>[ \t]?.+\n(.+\n)*\n*)+/gm;
    return e.splitAdjacentBlockquotes && (k = /^ {0,3}>[\s\S]*?(?:\n\n)/gm), a = (a += "\n\n").replace(k, function(h) {
      return h = (h = (h = h.replace(/^[ \t]*>[ \t]?/gm, "")).replace(/¨0/g, "")).replace(/^[ \t]+$/gm, ""), h = d.subParser("githubCodeBlocks")(h, e, c), h = (h = (h = d.subParser("blockGamut")(h, e, c)).replace(/(^|\n)/g, "$1  ")).replace(/(\s*<pre>[^\r]+?<\/pre>)/gm, function(r, m) {
        return m.replace(/^  /gm, "¨0").replace(/¨0/g, "");
      }), d.subParser("hashBlock")("<blockquote>\n" + h + "\n</blockquote>", e, c);
    }), a = c.converter._dispatch("blockQuotes.after", a, e, c);
  });
  d.subParser("codeBlocks", function(a, e, c) {
    a = c.converter._dispatch("codeBlocks.before", a, e, c);
    return a = (a = (a += "¨0").replace(/(?:\n\n|^)((?:(?:[ ]{4}|\t).*\n+)+)(\n*[ ]{0,3}[^ \t\n]|(?=¨0))/g, function(k, h, r) {
      h = d.subParser("outdent")(h, e, c);
      return h = d.subParser("encodeCode")(h, e, c), h = "<pre><code>" + (h = (h = (h = d.subParser("detab")(h, e, c)).replace(/^\n+/g, "")).replace(/\n+$/g, "")) + (e.omitExtraWLInCodeBlocks ? "" : "\n") + "</code></pre>", d.subParser("hashBlock")(h, e, c) + r;
    })).replace(/¨0/, ""), a = c.converter._dispatch("codeBlocks.after", a, e, c);
  });
  d.subParser("codeSpans", function(a, e, c) {
    return a = (a = void 0 === (a = c.converter._dispatch("codeSpans.before", a, e, c)) ? "" : a).replace(/(^|[^\\])(`+)([^\r]*?[^`])\2(?!`)/gm, function(k, h, r, m) {
      return m = (m = m.replace(/^([ \t]*)/g, "")).replace(/[ \t]*$/g, ""), m = h + "<code>" + (m = d.subParser("encodeCode")(m, e, c)) + "</code>", d.subParser("hashHTMLSpans")(m, e, c);
    }), a = c.converter._dispatch("codeSpans.after", a, e, c);
  });
  d.subParser("completeHTMLDocument", function(a, e, c) {
    if (!e.completeHTMLDocument) {
      return a;
    }
    a = c.converter._dispatch("completeHTMLDocument.before", a, e, c);
    var k, h = "html", r = "<!DOCTYPE HTML>\n", m = "", y = '<meta charset="utf-8">\n', A = "", J = "";
    for (k in void 0 !== c.metadata.parsed.doctype && (r = "<!DOCTYPE " + c.metadata.parsed.doctype + ">\n", "html" !== (h = c.metadata.parsed.doctype.toString().toLowerCase()) && "html5" !== h || (y = '<meta charset="utf-8">')), c.metadata.parsed) {
      if (c.metadata.parsed.hasOwnProperty(k)) {
        switch(k.toLowerCase()) {
          case "doctype":
            break;
          case "title":
            m = "<title>" + c.metadata.parsed.title + "</title>\n";
            break;
          case "charset":
            y = "html" === h || "html5" === h ? '<meta charset="' + c.metadata.parsed.charset + '">\n' : '<meta name="charset" content="' + c.metadata.parsed.charset + '">\n';
            break;
          case "language":
          case "lang":
            A = ' lang="' + c.metadata.parsed[k] + '"';
            J += '<meta name="' + k + '" content="' + c.metadata.parsed[k] + '">\n';
            break;
          default:
            J += '<meta name="' + k + '" content="' + c.metadata.parsed[k] + '">\n';
        }
      }
    }
    return a = r + "<html" + A + ">\n<head>\n" + m + y + J + "</head>\n<body>\n" + a.trim() + "\n</body>\n</html>", c.converter._dispatch("completeHTMLDocument.after", a, e, c);
  });
  d.subParser("detab", function(a, e, c) {
    return a = (a = (a = (a = (a = (a = c.converter._dispatch("detab.before", a, e, c)).replace(/\t(?=\t)/g, "    ")).replace(/\t/g, "¨A¨B")).replace(/¨B(.+?)¨A/g, function(k, h) {
      k = h;
      h = 4 - k.length % 4;
      for (var r = 0; r < h; r++) {
        k += " ";
      }
      return k;
    })).replace(/¨A/g, "    ")).replace(/¨B/g, ""), a = c.converter._dispatch("detab.after", a, e, c);
  });
  d.subParser("ellipsis", function(a, e, c) {
    return e.ellipsis ? (a = (a = c.converter._dispatch("ellipsis.before", a, e, c)).replace(/\.\.\./g, "…"), c.converter._dispatch("ellipsis.after", a, e, c)) : a;
  });
  d.subParser("emoji", function(a, e, c) {
    return e.emoji ? (a = (a = c.converter._dispatch("emoji.before", a, e, c)).replace(/:([\S]+?):/g, function(k, h) {
      return d.helper.emojis.hasOwnProperty(h) ? d.helper.emojis[h] : k;
    }), a = c.converter._dispatch("emoji.after", a, e, c)) : a;
  });
  d.subParser("encodeAmpsAndAngles", function(a, e, c) {
    return a = (a = (a = (a = (a = c.converter._dispatch("encodeAmpsAndAngles.before", a, e, c)).replace(/&(?!#?[xX]?(?:[0-9a-fA-F]+|\w+);)/g, "&amp;")).replace(/<(?![a-z\/?$!])/gi, "&lt;")).replace(/</g, "&lt;")).replace(/>/g, "&gt;"), c.converter._dispatch("encodeAmpsAndAngles.after", a, e, c);
  });
  d.subParser("encodeBackslashEscapes", function(a, e, c) {
    return a = (a = (a = c.converter._dispatch("encodeBackslashEscapes.before", a, e, c)).replace(/\\(\\)/g, d.helper.escapeCharactersCallback)).replace(/\\([`*_{}\[\]()>#+.!~=|:-])/g, d.helper.escapeCharactersCallback), c.converter._dispatch("encodeBackslashEscapes.after", a, e, c);
  });
  d.subParser("encodeCode", function(a, e, c) {
    return a = (a = c.converter._dispatch("encodeCode.before", a, e, c)).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/([*_{}\[\]\\=~-])/g, d.helper.escapeCharactersCallback), c.converter._dispatch("encodeCode.after", a, e, c);
  });
  d.subParser("escapeSpecialCharsWithinTagAttributes", function(a, e, c) {
    return a = (a = (a = c.converter._dispatch("escapeSpecialCharsWithinTagAttributes.before", a, e, c)).replace(/<\/?[a-z\d_:-]+(?:[\s]+[\s\S]+?)?>/gi, function(k) {
      return k.replace(/(.)<\/?code>(?=.)/g, "$1`").replace(/([\\`*_~=|])/g, d.helper.escapeCharactersCallback);
    })).replace(/<!(--(?:(?:[^>-]|-[^>])(?:[^-]|-[^-])*)--)>/gi, function(k) {
      return k.replace(/([\\`*_~=|])/g, d.helper.escapeCharactersCallback);
    }), a = c.converter._dispatch("escapeSpecialCharsWithinTagAttributes.after", a, e, c);
  });
  d.subParser("githubCodeBlocks", function(a, e, c) {
    return e.ghCodeBlocks ? (a = c.converter._dispatch("githubCodeBlocks.before", a, e, c), a = (a = (a += "¨0").replace(/(?:^|\n)(?: {0,3})(```+|~~~+)(?: *)([^\s`~]*)\n([\s\S]*?)\n(?: {0,3})\1/g, function(k, h, r, m) {
      h = e.omitExtraWLInCodeBlocks ? "" : "\n";
      return m = d.subParser("encodeCode")(m, e, c), m = "<pre><code" + (r ? ' class="' + r + " language-" + r + '"' : "") + ">" + (m = (m = (m = d.subParser("detab")(m, e, c)).replace(/^\n+/g, "")).replace(/\n+$/g, "")) + h + "</code></pre>", m = d.subParser("hashBlock")(m, e, c), "\n\n¨G" + (c.ghCodeBlocks.push({text:k, codeblock:m}) - 1) + "G\n\n";
    })).replace(/¨0/, ""), c.converter._dispatch("githubCodeBlocks.after", a, e, c)) : a;
  });
  d.subParser("hashBlock", function(a, e, c) {
    return a = (a = c.converter._dispatch("hashBlock.before", a, e, c)).replace(/(^\n+|\n+$)/g, ""), a = "\n\n¨K" + (c.gHtmlBlocks.push(a) - 1) + "K\n\n", c.converter._dispatch("hashBlock.after", a, e, c);
  });
  d.subParser("hashCodeTags", function(a, e, c) {
    a = c.converter._dispatch("hashCodeTags.before", a, e, c);
    return a = d.helper.replaceRecursiveRegExp(a, function(k, h, r, m) {
      r = r + d.subParser("encodeCode")(h, e, c) + m;
      return "¨C" + (c.gHtmlSpans.push(r) - 1) + "C";
    }, "<code\\b[^>]*>", "</code>", "gim"), a = c.converter._dispatch("hashCodeTags.after", a, e, c);
  });
  d.subParser("hashElement", function(a, e, c) {
    return function(k, h) {
      return h = (h = (h = h.replace(/\n\n/g, "\n")).replace(/^\n/, "")).replace(/\n+$/g, ""), "\n\n¨K" + (c.gHtmlBlocks.push(h) - 1) + "K\n\n";
    };
  });
  d.subParser("hashHTMLBlocks", function(a, e, c) {
    function k(u, D, K, G) {
      return -1 !== K.search(/\bmarkdown\b/) && (u = K + c.converter.makeHtml(D) + G), "\n\n¨K" + (c.gHtmlBlocks.push(u) - 1) + "K\n\n";
    }
    a = c.converter._dispatch("hashHTMLBlocks.before", a, e, c);
    var h = "pre div h1 h2 h3 h4 h5 h6 blockquote table dl ol ul script noscript form fieldset iframe math style section header footer nav article aside address audio canvas figure hgroup output video p".split(" ");
    e.backslashEscapesHTMLTags && (a = a.replace(/\\<(\/?[^>]+?)>/g, function(u, D) {
      return "&lt;" + D + "&gt;";
    }));
    for (var r = 0; r < h.length; ++r) {
      for (var m = new RegExp("^ {0,3}(<" + h[r] + "\\b[^>]*>)", "im"), y = "<" + h[r] + "\\b[^>]*>", A = "</" + h[r] + ">"; -1 !== (J = d.helper.regexIndexOf(a, m));) {
        var J = d.helper.splitAtIndex(a, J), O = d.helper.replaceRecursiveRegExp(J[1], k, y, A, "im");
        if (O === J[1]) {
          break;
        }
        a = J[0].concat(O);
      }
    }
    return a = a.replace(/(\n {0,3}(<(hr)\b([^<>])*?\/?>)[ \t]*(?=\n{2,}))/g, d.subParser("hashElement")(a, e, c)), a = (a = d.helper.replaceRecursiveRegExp(a, function(u) {
      return "\n\n¨K" + (c.gHtmlBlocks.push(u) - 1) + "K\n\n";
    }, "^ {0,3}\x3c!--", "--\x3e", "gm")).replace(/(?:\n\n)( {0,3}(?:<([?%])[^\r]*?\2>)[ \t]*(?=\n{2,}))/g, d.subParser("hashElement")(a, e, c)), a = c.converter._dispatch("hashHTMLBlocks.after", a, e, c);
  });
  d.subParser("hashHTMLSpans", function(a, e, c) {
    function k(h) {
      return "¨C" + (c.gHtmlSpans.push(h) - 1) + "C";
    }
    return a = (a = (a = (a = (a = c.converter._dispatch("hashHTMLSpans.before", a, e, c)).replace(/<[^>]+?\/>/gi, k)).replace(/<([^>]+?)>[\s\S]*?<\/\1>/g, k)).replace(/<([^>]+?)\s[^>]+?>[\s\S]*?<\/\1>/g, k)).replace(/<[^>]+?>/gi, k), a = c.converter._dispatch("hashHTMLSpans.after", a, e, c);
  });
  d.subParser("unhashHTMLSpans", function(a, e, c) {
    a = c.converter._dispatch("unhashHTMLSpans.before", a, e, c);
    for (var k = 0; k < c.gHtmlSpans.length; ++k) {
      for (var h = c.gHtmlSpans[k], r = 0; /¨C(\d+)C/.test(h);) {
        var m = RegExp.$1;
        h = h.replace("¨C" + m + "C", c.gHtmlSpans[m]);
        if (10 === r) {
          console.error("maximum nesting of 10 spans reached!!!");
          break;
        }
        ++r;
      }
      a = a.replace("¨C" + k + "C", h);
    }
    return c.converter._dispatch("unhashHTMLSpans.after", a, e, c);
  });
  d.subParser("hashPreCodeTags", function(a, e, c) {
    a = c.converter._dispatch("hashPreCodeTags.before", a, e, c);
    return a = d.helper.replaceRecursiveRegExp(a, function(k, h, r, m) {
      r = r + d.subParser("encodeCode")(h, e, c) + m;
      return "\n\n¨G" + (c.ghCodeBlocks.push({text:k, codeblock:r}) - 1) + "G\n\n";
    }, "^ {0,3}<pre\\b[^>]*>\\s*<code\\b[^>]*>", "^ {0,3}</code>\\s*</pre>", "gim"), a = c.converter._dispatch("hashPreCodeTags.after", a, e, c);
  });
  d.subParser("headers", function(a, e, c) {
    function k(y) {
      var A = e.customizedHeaderId && (A = y.match(/\{([^{]+?)}\s*$/)) && A[1] ? A[1] : y;
      y = d.helper.isString(e.prefixHeaderId) ? e.prefixHeaderId : !0 === e.prefixHeaderId ? "section-" : "";
      return e.rawPrefixHeaderId || (A = y + A), A = (e.ghCompatibleHeaderId ? A.replace(/ /g, "-").replace(/&amp;/g, "").replace(/¨T/g, "").replace(/¨D/g, "").replace(/[&+$,\/:;=?@"#{}|^¨~\[\]`\\*)(%.!'<>]/g, "") : e.rawHeaderId ? A.replace(/ /g, "-").replace(/&amp;/g, "&").replace(/¨T/g, "¨").replace(/¨D/g, "$").replace(/["']/g, "-") : A.replace(/[^\w]/g, "")).toLowerCase(), e.rawPrefixHeaderId && (A = y + A), c.hashLinkCounts[A] ? A = A + "-" + c.hashLinkCounts[A]++ : c.hashLinkCounts[A] = 1, 
      A;
    }
    a = c.converter._dispatch("headers.before", a, e, c);
    var h = isNaN(parseInt(e.headerLevelStart)) ? 1 : parseInt(e.headerLevelStart), r = e.smoothLivePreview ? /^(.+)[ \t]*\n={2,}[ \t]*\n+/gm : /^(.+)[ \t]*\n=+[ \t]*\n+/gm, m = e.smoothLivePreview ? /^(.+)[ \t]*\n-{2,}[ \t]*\n+/gm : /^(.+)[ \t]*\n-+[ \t]*\n+/gm;
    r = (a = (a = a.replace(r, function(y, A) {
      y = d.subParser("spanGamut")(A, e, c);
      A = e.noHeaderId ? "" : ' id="' + k(A) + '"';
      A = "<h" + h + A + ">" + y + "</h" + h + ">";
      return d.subParser("hashBlock")(A, e, c);
    })).replace(m, function(y, A) {
      y = d.subParser("spanGamut")(A, e, c);
      A = e.noHeaderId ? "" : ' id="' + k(A) + '"';
      var J = h + 1;
      A = "<h" + J + A + ">" + y + "</h" + J + ">";
      return d.subParser("hashBlock")(A, e, c);
    }), e.requireSpaceBeforeHeadingText ? /^(#{1,6})[ \t]+(.+?)[ \t]*#*\n+/gm : /^(#{1,6})[ \t]*(.+?)[ \t]*#*\n+/gm);
    return a = a.replace(r, function(y, A, J) {
      y = J;
      y = (e.customizedHeaderId && (y = J.replace(/\s?\{([^{]+?)}\s*$/, "")), d.subParser("spanGamut")(y, e, c));
      J = e.noHeaderId ? "" : ' id="' + k(J) + '"';
      A = h - 1 + A.length;
      J = "<h" + A + J + ">" + y + "</h" + A + ">";
      return d.subParser("hashBlock")(J, e, c);
    }), a = c.converter._dispatch("headers.after", a, e, c);
  });
  d.subParser("horizontalRule", function(a, e, c) {
    a = c.converter._dispatch("horizontalRule.before", a, e, c);
    var k = d.subParser("hashBlock")("<hr />", e, c);
    return a = (a = (a = a.replace(/^ {0,2}( ?-){3,}[ \t]*$/gm, k)).replace(/^ {0,2}( ?\*){3,}[ \t]*$/gm, k)).replace(/^ {0,2}( ?_){3,}[ \t]*$/gm, k), c.converter._dispatch("horizontalRule.after", a, e, c);
  });
  d.subParser("images", function(a, e, c) {
    function k(h, r, m, y, A, J, O, u) {
      O = c.gUrls;
      var D = c.gTitles, K = c.gDimensions;
      if (m = m.toLowerCase(), u = u || "", -1 < h.search(/\(<?\s*>? ?(['"].*['"])?\)$/m)) {
        y = "";
      } else {
        if ("" === y || null === y) {
          if (m = "" !== m && null !== m ? m : r.toLowerCase().replace(/ ?\n/g, " "), d.helper.isUndefined(O[m])) {
            return h;
          }
          y = O[m];
          d.helper.isUndefined(D[m]) || (u = D[m]);
          d.helper.isUndefined(K[m]) || (A = K[m].width, J = K[m].height);
        }
      }
      r = r.replace(/"/g, "&quot;").replace(d.helper.regexes.asteriskDashAndColon, d.helper.escapeCharactersCallback);
      h = '<img src="' + y.replace(d.helper.regexes.asteriskDashAndColon, d.helper.escapeCharactersCallback) + '" alt="' + r + '"';
      return u && d.helper.isString(u) && (h += ' title="' + u.replace(/"/g, "&quot;").replace(d.helper.regexes.asteriskDashAndColon, d.helper.escapeCharactersCallback) + '"'), A && J && (h = h + (' width="' + ("*" === A ? "auto" : A)) + '" height="' + ("*" === J ? "auto" : J) + '"'), h + " />";
    }
    return a = (a = (a = (a = (a = (a = c.converter._dispatch("images.before", a, e, c)).replace(/!\[([^\]]*?)] ?(?:\n *)?\[([\s\S]*?)]()()()()()/g, k)).replace(/!\[([^\]]*?)][ \t]*()\([ \t]?<?(data:.+?\/.+?;base64,[A-Za-z0-9+/=\n]+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(["'])([^"]*?)\6)?[ \t]?\)/g, function(h, r, m, y, A, J, O, u) {
      return k(h, r, m, y.replace(/\s/g, ""), A, J, 0, u);
    })).replace(/!\[([^\]]*?)][ \t]*()\([ \t]?<([^>]*)>(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(?:(["'])([^"]*?)\6))?[ \t]?\)/g, k)).replace(/!\[([^\]]*?)][ \t]*()\([ \t]?<?([\S]+?(?:\([\S]*?\)[\S]*?)?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*(?:(["'])([^"]*?)\6)?[ \t]?\)/g, k)).replace(/!\[([^\[\]]+)]()()()()()/g, k), a = c.converter._dispatch("images.after", a, e, c);
  });
  d.subParser("italicsAndBold", function(a, e, c) {
    return a = c.converter._dispatch("italicsAndBold.before", a, e, c), a = e.literalMidWordUnderscores ? (a = (a = a.replace(/\b___(\S[\s\S]*?)___\b/g, function(k, h) {
      return "<strong><em>" + h + "</em></strong>";
    })).replace(/\b__(\S[\s\S]*?)__\b/g, function(k, h) {
      return "<strong>" + h + "</strong>";
    })).replace(/\b_(\S[\s\S]*?)_\b/g, function(k, h) {
      return "<em>" + h + "</em>";
    }) : (a = (a = a.replace(/___(\S[\s\S]*?)___/g, function(k, h) {
      return /\S$/.test(h) ? "<strong><em>" + h + "</em></strong>" : k;
    })).replace(/__(\S[\s\S]*?)__/g, function(k, h) {
      return /\S$/.test(h) ? "<strong>" + h + "</strong>" : k;
    })).replace(/_([^\s_][\s\S]*?)_/g, function(k, h) {
      return /\S$/.test(h) ? "<em>" + h + "</em>" : k;
    }), a = e.literalMidWordAsterisks ? (a = (a = a.replace(/([^*]|^)\B\*\*\*(\S[\s\S]*?)\*\*\*\B(?!\*)/g, function(k, h, r) {
      return h + "<strong><em>" + r + "</em></strong>";
    })).replace(/([^*]|^)\B\*\*(\S[\s\S]*?)\*\*\B(?!\*)/g, function(k, h, r) {
      return h + "<strong>" + r + "</strong>";
    })).replace(/([^*]|^)\B\*(\S[\s\S]*?)\*\B(?!\*)/g, function(k, h, r) {
      return h + "<em>" + r + "</em>";
    }) : (a = (a = a.replace(/\*\*\*(\S[\s\S]*?)\*\*\*/g, function(k, h) {
      return /\S$/.test(h) ? "<strong><em>" + h + "</em></strong>" : k;
    })).replace(/\*\*(\S[\s\S]*?)\*\*/g, function(k, h) {
      return /\S$/.test(h) ? "<strong>" + h + "</strong>" : k;
    })).replace(/\*([^\s*][\s\S]*?)\*/g, function(k, h) {
      return /\S$/.test(h) ? "<em>" + h + "</em>" : k;
    }), a = c.converter._dispatch("italicsAndBold.after", a, e, c);
  });
  d.subParser("lists", function(a, e, c) {
    function k(m, y) {
      c.gListLevel++;
      m = m.replace(/\n{2,}$/, "\n");
      var A = /(\n)?(^ {0,3})([*+-]|\d+[.])[ \t]+((\[(x|X| )?])?[ \t]*[^\r]+?(\n{1,2}))(?=\n*(¨0| {0,3}([*+-]|\d+[.])[ \t]+))/gm, J = /\n[ \t]*\n(?!¨0)/.test(m += "¨0");
      return e.disableForced4SpacesIndentedSublists && (A = /(\n)?(^ {0,3})([*+-]|\d+[.])[ \t]+((\[(x|X| )?])?[ \t]*[^\r]+?(\n{1,2}))(?=\n*(¨0|\2([*+-]|\d+[.])[ \t]+))/gm), m = (m = m.replace(A, function(O, u, D, K, G, P, R) {
        R = R && "" !== R.trim();
        G = d.subParser("outdent")(G, e, c);
        O = "";
        return P && e.tasklists && (O = ' class="task-list-item" style="list-style-type: none;"', G = G.replace(/^[ \t]*\[(x|X| )?]/m, function() {
          var S = '<input type="checkbox" disabled style="margin: 0px 0.35em 0.25em -1.6em; vertical-align: middle;"';
          return R && (S += " checked"), S + ">";
        })), G = G.replace(/^([-*+]|\d\.)[ \t]+[\S\n ]*/g, function(S) {
          return "¨A" + S;
        }), G = "<li" + O + ">" + (G = (G = u || -1 < G.search(/\n{2,}/) ? (G = d.subParser("githubCodeBlocks")(G, e, c), d.subParser("blockGamut")(G, e, c)) : (G = (G = d.subParser("lists")(G, e, c)).replace(/\n$/, ""), G = (G = d.subParser("hashHTMLBlocks")(G, e, c)).replace(/\n\n+/g, "\n\n"), (J ? d.subParser("paragraphs") : d.subParser("spanGamut"))(G, e, c))).replace("¨A", "")) + "</li>\n";
      })).replace(/¨0/g, ""), c.gListLevel--, m = y ? m.replace(/\s+$/, "") : m;
    }
    function h(m, y) {
      return "ol" === y && (y = m.match(/^ *(\d+)\./)) && "1" !== y[1] ? ' start="' + y[1] + '"' : "";
    }
    function r(m, y, A) {
      var J, O = e.disableForced4SpacesIndentedSublists ? /^ ?\d+\.[ \t]/gm : /^ {0,3}\d+\.[ \t]/gm, u = e.disableForced4SpacesIndentedSublists ? /^ ?[*+-][ \t]/gm : /^ {0,3}[*+-][ \t]/gm, D = "ul" === y ? O : u, K = "";
      return -1 !== m.search(D) ? function R(P) {
        var S = P.search(D), aa = h(m, y);
        -1 !== S ? (K += "\n\n<" + y + aa + ">\n" + k(P.slice(0, S), !!A) + "</" + y + ">\n", D = "ul" === (y = "ul" === y ? "ol" : "ul") ? O : u, R(P.slice(S))) : K += "\n\n<" + y + aa + ">\n" + k(P, !!A) + "</" + y + ">\n";
      }(m) : (J = h(m, y), K = "\n\n<" + y + J + ">\n" + k(m, !!A) + "</" + y + ">\n"), K;
    }
    return a = c.converter._dispatch("lists.before", a, e, c), a += "¨0", a = (a = c.gListLevel ? a.replace(/^(( {0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(¨0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm, function(m, y, A) {
      return r(y, -1 < A.search(/[*+-]/g) ? "ul" : "ol", !0);
    }) : a.replace(/(\n\n|^\n?)(( {0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(¨0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm, function(m, y, A, J) {
      return r(A, -1 < J.search(/[*+-]/g) ? "ul" : "ol", !1);
    })).replace(/¨0/, ""), a = c.converter._dispatch("lists.after", a, e, c);
  });
  d.subParser("metadata", function(a, e, c) {
    function k(h) {
      (h = (h = (c.metadata.raw = h).replace(/&/g, "&amp;").replace(/"/g, "&quot;")).replace(/\n {4}/g, " ")).replace(/^([\S ]+): +([\s\S]+?)$/gm, function(r, m, y) {
        return c.metadata.parsed[m] = y, "";
      });
    }
    return e.metadata ? (a = (a = (a = (a = c.converter._dispatch("metadata.before", a, e, c)).replace(/^\s*«««+(\S*?)\n([\s\S]+?)\n»»»+\n/, function(h, r, m) {
      return k(m), "¨M";
    })).replace(/^\s*---+(\S*?)\n([\s\S]+?)\n---+\n/, function(h, r, m) {
      return r && (c.metadata.format = r), k(m), "¨M";
    })).replace(/¨M/g, ""), c.converter._dispatch("metadata.after", a, e, c)) : a;
  });
  d.subParser("outdent", function(a, e, c) {
    return a = (a = (a = c.converter._dispatch("outdent.before", a, e, c)).replace(/^(\t|[ ]{1,4})/gm, "¨0")).replace(/¨0/g, ""), c.converter._dispatch("outdent.after", a, e, c);
  });
  d.subParser("paragraphs", function(a, e, c) {
    for (var k = (a = (a = (a = c.converter._dispatch("paragraphs.before", a, e, c)).replace(/^\n+/g, "")).replace(/\n+$/g, "")).split(/\n{2,}/g), h = [], r = k.length, m = 0; m < r; m++) {
      var y = k[m];
      0 <= y.search(/¨(K|G)(\d+)\1/g) ? h.push(y) : 0 <= y.search(/\S/) && (y = (y = d.subParser("spanGamut")(y, e, c)).replace(/^([ \t]*)/g, "<p>"), y += "</p>", h.push(y));
    }
    r = h.length;
    for (m = 0; m < r; m++) {
      k = h[m];
      for (y = !1; /¨(K|G)(\d+)\1/.test(k);) {
        var A = RegExp.$2;
        A = (A = "K" === RegExp.$1 ? c.gHtmlBlocks[A] : y ? d.subParser("encodeCode")(c.ghCodeBlocks[A].text, e, c) : c.ghCodeBlocks[A].codeblock).replace(/\$/g, "$$$$");
        k = k.replace(/(\n\n)?¨(K|G)\d+\2(\n\n)?/, A);
        /^<pre\b[^>]*>\s*<code\b[^>]*>/.test(k) && (y = !0);
      }
      h[m] = k;
    }
    return a = (a = (a = h.join("\n")).replace(/^\n+/g, "")).replace(/\n+$/g, ""), c.converter._dispatch("paragraphs.after", a, e, c);
  });
  d.subParser("runExtension", function(a, e, c, k) {
    return a.filter ? e = a.filter(e, k.converter, c) : a.regex && ((k = a.regex) instanceof RegExp || (k = new RegExp(k, "g")), e = e.replace(k, a.replace)), e;
  });
  d.subParser("spanGamut", function(a, e, c) {
    return a = c.converter._dispatch("spanGamut.before", a, e, c), a = d.subParser("codeSpans")(a, e, c), a = d.subParser("escapeSpecialCharsWithinTagAttributes")(a, e, c), a = d.subParser("encodeBackslashEscapes")(a, e, c), a = d.subParser("images")(a, e, c), a = d.subParser("anchors")(a, e, c), a = d.subParser("autoLinks")(a, e, c), a = d.subParser("simplifiedAutoLinks")(a, e, c), a = d.subParser("emoji")(a, e, c), a = d.subParser("underline")(a, e, c), a = d.subParser("italicsAndBold")(a, e, c), 
    a = d.subParser("strikethrough")(a, e, c), a = d.subParser("ellipsis")(a, e, c), a = d.subParser("hashHTMLSpans")(a, e, c), a = d.subParser("encodeAmpsAndAngles")(a, e, c), e.simpleLineBreaks ? /\n\n¨K/.test(a) || (a = a.replace(/\n+/g, "<br />\n")) : a = a.replace(/  +\n/g, "<br />\n"), c.converter._dispatch("spanGamut.after", a, e, c);
  });
  d.subParser("strikethrough", function(a, e, c) {
    return e.strikethrough && (a = (a = c.converter._dispatch("strikethrough.before", a, e, c)).replace(/(?:~){2}([\s\S]+?)(?:~){2}/g, function(k, h) {
      return "<del>" + (e.simplifiedAutoLink ? d.subParser("simplifiedAutoLinks")(h, e, c) : h) + "</del>";
    }), a = c.converter._dispatch("strikethrough.after", a, e, c)), a;
  });
  d.subParser("stripLinkDefinitions", function(a, e, c) {
    function k(h, r, m, y, A, J, O) {
      return r = r.toLowerCase(), 2 > a.toLowerCase().split(r).length - 1 ? h : (m.match(/^data:.+?\/.+?;base64,/) ? c.gUrls[r] = m.replace(/\s/g, "") : c.gUrls[r] = d.subParser("encodeAmpsAndAngles")(m, e, c), J ? J + O : (O && (c.gTitles[r] = O.replace(/"|'/g, "&quot;")), e.parseImgDimensions && y && A && (c.gDimensions[r] = {width:y, height:A}), ""));
    }
    return a = (a = (a = (a += "¨0").replace(/^ {0,3}\[([^\]]+)]:[ \t]*\n?[ \t]*<?(data:.+?\/.+?;base64,[A-Za-z0-9+/=\n]+?)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*\n?[ \t]*(?:(\n*)["|'(](.+?)["|')][ \t]*)?(?:\n\n|(?=¨0)|(?=\n\[))/gm, k)).replace(/^ {0,3}\[([^\]]+)]:[ \t]*\n?[ \t]*<?([^>\s]+)>?(?: =([*\d]+[A-Za-z%]{0,4})x([*\d]+[A-Za-z%]{0,4}))?[ \t]*\n?[ \t]*(?:(\n*)["|'(](.+?)["|')][ \t]*)?(?:\n+|(?=¨0))/gm, k)).replace(/¨0/, "");
  });
  d.subParser("tables", function(a, e, c) {
    function k(h) {
      for (var r = h.split("\n"), m = 0; m < r.length; ++m) {
        /^ {0,3}\|/.test(r[m]) && (r[m] = r[m].replace(/^ {0,3}\|/, "")), /\|[ \t]*$/.test(r[m]) && (r[m] = r[m].replace(/\|[ \t]*$/, "")), r[m] = d.subParser("codeSpans")(r[m], e, c);
      }
      var y, A, J, O, u, D = r[0].split("|").map(function(aa) {
        return aa.trim();
      }), K = r[1].split("|").map(function(aa) {
        return aa.trim();
      }), G = [], P = [], R = [], S = [];
      r.shift();
      r.shift();
      for (m = 0; m < r.length; ++m) {
        "" !== r[m].trim() && G.push(r[m].split("|").map(function(aa) {
          return aa.trim();
        }));
      }
      if (D.length < K.length) {
        return h;
      }
      for (m = 0; m < K.length; ++m) {
        R.push((y = K[m], /^:[ \t]*--*$/.test(y) ? ' style="text-align:left;"' : /^--*[ \t]*:[ \t]*$/.test(y) ? ' style="text-align:right;"' : /^:[ \t]*--*[ \t]*:$/.test(y) ? ' style="text-align:center;"' : ""));
      }
      for (m = 0; m < D.length; ++m) {
        d.helper.isUndefined(R[m]) && (R[m] = ""), P.push((A = D[m], J = R[m], void 0, O = "", A = A.trim(), "<th" + (O = e.tablesHeaderId || e.tableHeaderId ? ' id="' + A.replace(/ /g, "_").toLowerCase() + '"' : O) + J + ">" + (A = d.subParser("spanGamut")(A, e, c)) + "</th>\n"));
      }
      for (m = 0; m < G.length; ++m) {
        h = [];
        for (r = 0; r < P.length; ++r) {
          d.helper.isUndefined(G[m][r]), h.push((u = G[m][r], "<td" + R[r] + ">" + d.subParser("spanGamut")(u, e, c) + "</td>\n"));
        }
        S.push(h);
      }
      m = "<table>\n<thead>\n<tr>\n";
      u = P.length;
      for (G = 0; G < u; ++G) {
        m += P[G];
      }
      m += "</tr>\n</thead>\n<tbody>\n";
      for (G = 0; G < S.length; ++G) {
        m += "<tr>\n";
        for (P = 0; P < u; ++P) {
          m += S[G][P];
        }
        m += "</tr>\n";
      }
      return m += "</tbody>\n</table>\n";
    }
    return e.tables ? (a = (a = (a = (a = c.converter._dispatch("tables.before", a, e, c)).replace(/\\(\|)/g, d.helper.escapeCharactersCallback)).replace(/^ {0,3}\|?.+\|.+\n {0,3}\|?[ \t]*:?[ \t]*(?:[-=]){2,}[ \t]*:?[ \t]*\|[ \t]*:?[ \t]*(?:[-=]){2,}[\s\S]+?(?:\n\n|¨0)/gm, k)).replace(/^ {0,3}\|.+\|[ \t]*\n {0,3}\|[ \t]*:?[ \t]*(?:[-=]){2,}[ \t]*:?[ \t]*\|[ \t]*\n( {0,3}\|.+\|[ \t]*\n)*(?:\n|¨0)/gm, k), a = c.converter._dispatch("tables.after", a, e, c)) : a;
  });
  d.subParser("underline", function(a, e, c) {
    return e.underline ? (a = c.converter._dispatch("underline.before", a, e, c), a = (a = e.literalMidWordUnderscores ? (a = a.replace(/\b___(\S[\s\S]*?)___\b/g, function(k, h) {
      return "<u>" + h + "</u>";
    })).replace(/\b__(\S[\s\S]*?)__\b/g, function(k, h) {
      return "<u>" + h + "</u>";
    }) : (a = a.replace(/___(\S[\s\S]*?)___/g, function(k, h) {
      return /\S$/.test(h) ? "<u>" + h + "</u>" : k;
    })).replace(/__(\S[\s\S]*?)__/g, function(k, h) {
      return /\S$/.test(h) ? "<u>" + h + "</u>" : k;
    })).replace(/(_)/g, d.helper.escapeCharactersCallback), c.converter._dispatch("underline.after", a, e, c)) : a;
  });
  d.subParser("unescapeSpecialChars", function(a, e, c) {
    return a = (a = c.converter._dispatch("unescapeSpecialChars.before", a, e, c)).replace(/¨E(\d+)E/g, function(k, h) {
      h = parseInt(h);
      return String.fromCharCode(h);
    }), a = c.converter._dispatch("unescapeSpecialChars.after", a, e, c);
  });
  d.subParser("makeMarkdown.blockquote", function(a, e) {
    var c = "";
    if (a.hasChildNodes()) {
      a = a.childNodes;
      for (var k = a.length, h = 0; h < k; ++h) {
        var r = d.subParser("makeMarkdown.node")(a[h], e);
        "" !== r && (c += r);
      }
    }
    return "> " + c.trim().split("\n").join("\n> ");
  });
  d.subParser("makeMarkdown.codeBlock", function(a, e) {
    var c = a.getAttribute("language");
    a = a.getAttribute("precodenum");
    return "```" + c + "\n" + e.preList[a] + "\n```";
  });
  d.subParser("makeMarkdown.codeSpan", function(a) {
    return "`" + a.innerHTML + "`";
  });
  d.subParser("makeMarkdown.emphasis", function(a, e) {
    var c = "";
    if (a.hasChildNodes()) {
      c += "*";
      a = a.childNodes;
      for (var k = a.length, h = 0; h < k; ++h) {
        c += d.subParser("makeMarkdown.node")(a[h], e);
      }
      c += "*";
    }
    return c;
  });
  d.subParser("makeMarkdown.header", function(a, e, c) {
    c = Array(c + 1).join("#");
    var k = "";
    if (a.hasChildNodes()) {
      k = c + " ";
      a = a.childNodes;
      c = a.length;
      for (var h = 0; h < c; ++h) {
        k += d.subParser("makeMarkdown.node")(a[h], e);
      }
    }
    return k;
  });
  d.subParser("makeMarkdown.hr", function() {
    return "---";
  });
  d.subParser("makeMarkdown.image", function(a) {
    var e = "";
    return a.hasAttribute("src") && (e = (e += "![" + a.getAttribute("alt") + "](") + "<" + a.getAttribute("src") + ">", a.hasAttribute("width") && a.hasAttribute("height") && (e += " =" + a.getAttribute("width") + "x" + a.getAttribute("height")), a.hasAttribute("title") && (e += ' "' + a.getAttribute("title") + '"'), e += ")"), e;
  });
  d.subParser("makeMarkdown.links", function(a, e) {
    var c = "";
    if (a.hasChildNodes() && a.hasAttribute("href")) {
      var k = a.childNodes, h = k.length;
      c = "[";
      for (var r = 0; r < h; ++r) {
        c += d.subParser("makeMarkdown.node")(k[r], e);
      }
      c = (c += "](") + ("<" + a.getAttribute("href") + ">");
      a.hasAttribute("title") && (c += ' "' + a.getAttribute("title") + '"');
      c += ")";
    }
    return c;
  });
  d.subParser("makeMarkdown.list", function(a, e, c) {
    var k = "";
    if (!a.hasChildNodes()) {
      return "";
    }
    var h = a.childNodes, r = h.length;
    a = a.getAttribute("start") || 1;
    for (var m = 0; m < r; ++m) {
      void 0 !== h[m].tagName && "li" === h[m].tagName.toLowerCase() && (k += ("ol" === c ? a.toString() + ". " : "- ") + d.subParser("makeMarkdown.listItem")(h[m], e), ++a);
    }
    return (k + "\n\x3c!-- --\x3e\n").trim();
  });
  d.subParser("makeMarkdown.listItem", function(a, e) {
    var c = "";
    a = a.childNodes;
    for (var k = a.length, h = 0; h < k; ++h) {
      c += d.subParser("makeMarkdown.node")(a[h], e);
    }
    return /\n$/.test(c) ? c = c.split("\n").join("\n    ").replace(/^ {4}$/gm, "").replace(/\n\n+/g, "\n\n") : c += "\n", c;
  });
  d.subParser("makeMarkdown.node", function(a, e, c) {
    c = c || !1;
    var k = "";
    if (3 === a.nodeType) {
      return d.subParser("makeMarkdown.txt")(a, e);
    }
    if (8 === a.nodeType) {
      return "\x3c!--" + a.data + "--\x3e\n\n";
    }
    if (1 !== a.nodeType) {
      return "";
    }
    switch(a.tagName.toLowerCase()) {
      case "h1":
        c || (k = d.subParser("makeMarkdown.header")(a, e, 1) + "\n\n");
        break;
      case "h2":
        c || (k = d.subParser("makeMarkdown.header")(a, e, 2) + "\n\n");
        break;
      case "h3":
        c || (k = d.subParser("makeMarkdown.header")(a, e, 3) + "\n\n");
        break;
      case "h4":
        c || (k = d.subParser("makeMarkdown.header")(a, e, 4) + "\n\n");
        break;
      case "h5":
        c || (k = d.subParser("makeMarkdown.header")(a, e, 5) + "\n\n");
        break;
      case "h6":
        c || (k = d.subParser("makeMarkdown.header")(a, e, 6) + "\n\n");
        break;
      case "p":
        c || (k = d.subParser("makeMarkdown.paragraph")(a, e) + "\n\n");
        break;
      case "blockquote":
        c || (k = d.subParser("makeMarkdown.blockquote")(a, e) + "\n\n");
        break;
      case "hr":
        c || (k = d.subParser("makeMarkdown.hr")(a, e) + "\n\n");
        break;
      case "ol":
        c || (k = d.subParser("makeMarkdown.list")(a, e, "ol") + "\n\n");
        break;
      case "ul":
        c || (k = d.subParser("makeMarkdown.list")(a, e, "ul") + "\n\n");
        break;
      case "precode":
        c || (k = d.subParser("makeMarkdown.codeBlock")(a, e) + "\n\n");
        break;
      case "pre":
        c || (k = d.subParser("makeMarkdown.pre")(a, e) + "\n\n");
        break;
      case "table":
        c || (k = d.subParser("makeMarkdown.table")(a, e) + "\n\n");
        break;
      case "code":
        k = d.subParser("makeMarkdown.codeSpan")(a, e);
        break;
      case "em":
      case "i":
        k = d.subParser("makeMarkdown.emphasis")(a, e);
        break;
      case "strong":
      case "b":
        k = d.subParser("makeMarkdown.strong")(a, e);
        break;
      case "del":
        k = d.subParser("makeMarkdown.strikethrough")(a, e);
        break;
      case "a":
        k = d.subParser("makeMarkdown.links")(a, e);
        break;
      case "img":
        k = d.subParser("makeMarkdown.image")(a, e);
        break;
      default:
        k = a.outerHTML + "\n\n";
    }
    return k;
  });
  d.subParser("makeMarkdown.paragraph", function(a, e) {
    var c = "";
    if (a.hasChildNodes()) {
      a = a.childNodes;
      for (var k = a.length, h = 0; h < k; ++h) {
        c += d.subParser("makeMarkdown.node")(a[h], e);
      }
    }
    return c.trim();
  });
  d.subParser("makeMarkdown.pre", function(a, e) {
    a = a.getAttribute("prenum");
    return "<pre>" + e.preList[a] + "</pre>";
  });
  d.subParser("makeMarkdown.strikethrough", function(a, e) {
    var c = "";
    if (a.hasChildNodes()) {
      c += "~~";
      a = a.childNodes;
      for (var k = a.length, h = 0; h < k; ++h) {
        c += d.subParser("makeMarkdown.node")(a[h], e);
      }
      c += "~~";
    }
    return c;
  });
  d.subParser("makeMarkdown.strong", function(a, e) {
    var c = "";
    if (a.hasChildNodes()) {
      c += "**";
      a = a.childNodes;
      for (var k = a.length, h = 0; h < k; ++h) {
        c += d.subParser("makeMarkdown.node")(a[h], e);
      }
      c += "**";
    }
    return c;
  });
  d.subParser("makeMarkdown.table", function(a, e) {
    var c = "", k = [[], []], h = a.querySelectorAll("thead>tr>th"), r = a.querySelectorAll("tbody>tr");
    for (a = 0; a < h.length; ++a) {
      var m = d.subParser("makeMarkdown.tableCell")(h[a], e), y = "---";
      if (h[a].hasAttribute("style")) {
        switch(h[a].getAttribute("style").toLowerCase().replace(/\s/g, "")) {
          case "text-align:left;":
            y = ":---";
            break;
          case "text-align:right;":
            y = "---:";
            break;
          case "text-align:center;":
            y = ":---:";
        }
      }
      k[0][a] = m.trim();
      k[1][a] = y;
    }
    for (a = 0; a < r.length; ++a) {
      y = k.push([]) - 1;
      var A = r[a].getElementsByTagName("td");
      for (m = 0; m < h.length; ++m) {
        var J = " ";
        void 0 !== A[m] && (J = d.subParser("makeMarkdown.tableCell")(A[m], e));
        k[y].push(J);
      }
    }
    e = 3;
    for (a = 0; a < k.length; ++a) {
      for (m = 0; m < k[a].length; ++m) {
        h = k[a][m].length, e < h && (e = h);
      }
    }
    for (a = 0; a < k.length; ++a) {
      for (m = 0; m < k[a].length; ++m) {
        1 === a ? ":" === k[a][m].slice(-1) ? k[a][m] = d.helper.padEnd(k[a][m].slice(-1), e - 1, "-") + ":" : k[a][m] = d.helper.padEnd(k[a][m], e, "-") : k[a][m] = d.helper.padEnd(k[a][m], e);
      }
      c += "| " + k[a].join(" | ") + " |\n";
    }
    return c.trim();
  });
  d.subParser("makeMarkdown.tableCell", function(a, e) {
    var c = "";
    if (!a.hasChildNodes()) {
      return "";
    }
    a = a.childNodes;
    for (var k = a.length, h = 0; h < k; ++h) {
      c += d.subParser("makeMarkdown.node")(a[h], e, !0);
    }
    return c.trim();
  });
  d.subParser("makeMarkdown.txt", function(a) {
    a = a.nodeValue;
    return a = (a = a.replace(/ +/g, " ")).replace(/¨NBSP;/g, " "), d.helper.unescapeHTMLEntities(a).replace(/([*_~|`])/g, "\\$1").replace(/^(\s*)>/g, "\\$1>").replace(/^#/gm, "\\#").replace(/^(\s*)([-=]{3,})(\s*)$/, "$1\\$2$3").replace(/^( {0,3}\d+)\./gm, "$1\\.").replace(/^( {0,3})([+-])/gm, "$1\\$2").replace(/]([\s]*)\(/g, "\\]$1\\(").replace(/^ {0,3}\[([\S \t]*?)]:/gm, "\\[$1]:");
  });
  "function" == typeof define && define.amd ? define(function() {
    return d;
  }) : "undefined" != typeof module && module.exports ? module.exports = d : this.showdown = d;
}.call(this);
/*
 Highlight.js v11.9.0 (git: b7ec4bfafc)
  (c) 2006-2024 undefined and other contributors
  License: BSD-3-Clause
 `apache` grammar compiled for Highlight.js 11.9.0  `bash` grammar compiled for Highlight.js 11.9.0  `basic` grammar compiled for Highlight.js 11.9.0  `c` grammar compiled for Highlight.js 11.9.0  `clojure` grammar compiled for Highlight.js 11.9.0  `coffeescript` grammar compiled for Highlight.js 11.9.0  `cpp` grammar compiled for Highlight.js 11.9.0  `csharp` grammar compiled for Highlight.js 11.9.0  `css` grammar compiled for Highlight.js 11.9.0  `diff` grammar compiled for Highlight.js 11.9.0  `erlang` grammar compiled for Highlight.js 11.9.0  `go` grammar compiled for Highlight.js 11.9.0  `haskell` grammar compiled for Highlight.js 11.9.0  `http` grammar compiled for Highlight.js 11.9.0  `ini` grammar compiled for Highlight.js 11.9.0  `java` grammar compiled for Highlight.js 11.9.0  `javascript` grammar compiled for Highlight.js 11.9.0  `json` grammar compiled for Highlight.js 11.9.0  `kotlin` grammar compiled for Highlight.js 11.9.0  `less` grammar compiled for Highlight.js 11.9.0  `lisp` grammar compiled for Highlight.js 11.9.0  `lua` grammar compiled for Highlight.js 11.9.0  `makefile` grammar compiled for Highlight.js 11.9.0  `markdown` grammar compiled for Highlight.js 11.9.0  `nginx` grammar compiled for Highlight.js 11.9.0  `objectivec` grammar compiled for Highlight.js 11.9.0  `perl` grammar compiled for Highlight.js 11.9.0  `php` grammar compiled for Highlight.js 11.9.0  `php-template` grammar compiled for Highlight.js 11.9.0  `plaintext` grammar compiled for Highlight.js 11.9.0  `powershell` grammar compiled for Highlight.js 11.9.0  `properties` grammar compiled for Highlight.js 11.9.0  `python` grammar compiled for Highlight.js 11.9.0  `python-repl` grammar compiled for Highlight.js 11.9.0  `r` grammar compiled for Highlight.js 11.9.0  `ruby` grammar compiled for Highlight.js 11.9.0  `rust` grammar compiled for Highlight.js 11.9.0  `scala` grammar compiled for Highlight.js 11.9.0  `scheme` grammar compiled for Highlight.js 11.9.0  `scss` grammar compiled for Highlight.js 11.9.0  `shell` grammar compiled for Highlight.js 11.9.0  `sql` grammar compiled for Highlight.js 11.9.0  `swift` grammar compiled for Highlight.js 11.9.0  `typescript` grammar compiled for Highlight.js 11.9.0  `vbnet` grammar compiled for Highlight.js 11.9.0  `vbscript-html` grammar compiled for Highlight.js 11.9.0  `x86asm` grammar compiled for Highlight.js 11.9.0  `xml` grammar compiled for Highlight.js 11.9.0  `yaml` grammar compiled for Highlight.js 11.9.0 */
var hljs = function() {
  function g(p) {
    return p instanceof Map ? p.clear = p.delete = p.set = () => {
      throw Error("map is read-only");
    } : p instanceof Set && (p.add = p.clear = p.delete = () => {
      throw Error("set is read-only");
    }), Object.freeze(p), Object.getOwnPropertyNames(p).forEach(z => {
      z = p[z];
      const U = typeof z;
      "object" !== U && "function" !== U || Object.isFrozen(z) || g(z);
    }), p;
  }
  function b(p) {
    return p.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;");
  }
  function f(p, ...z) {
    const U = Object.create(null);
    for (const Z in p) {
      U[Z] = p[Z];
    }
    return z.forEach(Z => {
      for (const W in Z) {
        U[W] = Z[W];
      }
    }), U;
  }
  function l(p) {
    return p ? "string" == typeof p ? p : p.source : null;
  }
  function n(p) {
    return w("(?=", p, ")");
  }
  function q(p) {
    return w("(?:", p, ")*");
  }
  function d(p) {
    return w("(?:", p, ")?");
  }
  function w(...p) {
    return p.map(z => l(z)).join("");
  }
  function t(...p) {
    return "(" + ((z => {
      const U = z[z.length - 1];
      return "object" == typeof U && U.constructor === Object ? (z.splice(z.length - 1, 1), U) : {};
    })(p).capture ? "" : "?:") + p.map(z => l(z)).join("|") + ")";
  }
  function v(p, {joinWith:z}) {
    let U = 0;
    return p.map(Z => {
      const W = U += 1;
      Z = l(Z);
      let B = "";
      for (; 0 < Z.length;) {
        const Y = J.exec(Z);
        if (!Y) {
          B += Z;
          break;
        }
        B += Z.substring(0, Y.index);
        Z = Z.substring(Y.index + Y[0].length);
        "\\" === Y[0][0] && Y[1] ? B += "\\" + (Number(Y[1]) + W) : (B += Y[0], "(" === Y[0] && U++);
      }
      return B;
    }).map(Z => `(${Z})`).join(z);
  }
  function x(p, z) {
    "." === p.input[p.index - 1] && z.ignoreMatch();
  }
  function C(p, z) {
    void 0 !== p.className && (p.scope = p.className, delete p.className);
  }
  function E(p, z) {
    z && p.beginKeywords && (p.begin = "\\b(" + p.beginKeywords.split(" ").join("|") + ")(?!\\.)(?=\\b|\\s)", p.__beforeBegin = x, p.keywords = p.keywords || p.beginKeywords, delete p.beginKeywords, void 0 === p.relevance && (p.relevance = 0));
  }
  function L(p, z) {
    Array.isArray(p.illegal) && (p.illegal = t(...p.illegal));
  }
  function I(p, z) {
    if (p.match) {
      if (p.begin || p.end) {
        throw Error("begin & end are not supported with match");
      }
      p.begin = p.match;
      delete p.match;
    }
  }
  function M(p, z) {
    void 0 === p.relevance && (p.relevance = 1);
  }
  function T(p, z, U = "keyword") {
    function Z(B, Y) {
      z && (Y = Y.map(ka => ka.toLowerCase()));
      Y.forEach(ka => {
        var qa = ka.split("|");
        ka = qa[0];
        var ja = qa[1];
        qa = ja ? Number(ja) : ha.includes(qa[0].toLowerCase()) ? 0 : 1;
        W[ka] = [B, qa];
      });
    }
    const W = Object.create(null);
    return "string" == typeof p ? Z(U, p.split(" ")) : Array.isArray(p) ? Z(U, p) : Object.keys(p).forEach(B => {
      Object.assign(W, T(p[B], z, B));
    }), W;
  }
  function a(p, z, {key:U}) {
    let Z = 0;
    const W = p[U], B = {}, Y = {};
    for (let ka = 1; ka <= z.length; ka++) {
      Y[ka + Z] = W[ka], B[ka + Z] = !0, Z += RegExp(z[ka - 1].toString() + "|").exec("").length - 1;
    }
    p[U] = Y;
    p[U]._emit = B;
    p[U]._multi = !0;
  }
  function e(p) {
    (z => {
      z.scope && "object" == typeof z.scope && null !== z.scope && (z.beginScope = z.scope, delete z.scope);
    })(p);
    "string" == typeof p.beginScope && (p.beginScope = {_wrap:p.beginScope});
    "string" == typeof p.endScope && (p.endScope = {_wrap:p.endScope});
    (z => {
      if (Array.isArray(z.begin)) {
        if (z.skip || z.excludeBegin || z.returnBegin) {
          throw console.error("skip, excludeBegin, returnBegin not compatible with beginScope: {}"), xa;
        }
        if ("object" != typeof z.beginScope || null === z.beginScope) {
          throw console.error("beginScope must be object"), xa;
        }
        a(z, z.begin, {key:"beginScope"});
        z.begin = v(z.begin, {joinWith:""});
      }
    })(p);
    (z => {
      if (Array.isArray(z.end)) {
        if (z.skip || z.excludeEnd || z.returnEnd) {
          throw console.error("skip, excludeEnd, returnEnd not compatible with endScope: {}"), xa;
        }
        if ("object" != typeof z.endScope || null === z.endScope) {
          throw console.error("endScope must be object"), xa;
        }
        a(z, z.end, {key:"endScope"});
        z.end = v(z.end, {joinWith:""});
      }
    })(p);
  }
  function c(p) {
    function z(W, B) {
      return RegExp(l(W), "m" + (p.case_insensitive ? "i" : "") + (p.unicodeRegex ? "u" : "") + (B ? "g" : ""));
    }
    class U {
      constructor() {
        this.matchIndexes = {};
        this.regexes = [];
        this.matchAt = 1;
        this.position = 0;
      }
      addRule(W, B) {
        B.position = this.position++;
        this.matchIndexes[this.matchAt] = B;
        this.regexes.push([B, W]);
        this.matchAt += RegExp(W.toString() + "|").exec("").length - 1 + 1;
      }
      compile() {
        0 === this.regexes.length && (this.exec = () => null);
        const W = this.regexes.map(B => B[1]);
        this.matcherRe = z(v(W, {joinWith:"|"}), !0);
        this.lastIndex = 0;
      }
      exec(W) {
        this.matcherRe.lastIndex = this.lastIndex;
        W = this.matcherRe.exec(W);
        if (!W) {
          return null;
        }
        const B = W.findIndex((ka, qa) => 0 < qa && void 0 !== ka), Y = this.matchIndexes[B];
        return W.splice(0, B), Object.assign(W, Y);
      }
    }
    class Z {
      constructor() {
        this.rules = [];
        this.multiRegexes = [];
        this.regexIndex = this.lastIndex = this.count = 0;
      }
      getMatcher(W) {
        if (this.multiRegexes[W]) {
          return this.multiRegexes[W];
        }
        const B = new U;
        return this.rules.slice(W).forEach(([Y, ka]) => B.addRule(Y, ka)), B.compile(), this.multiRegexes[W] = B, B;
      }
      resumingScanAtSamePosition() {
        return 0 !== this.regexIndex;
      }
      considerAll() {
        this.regexIndex = 0;
      }
      addRule(W, B) {
        this.rules.push([W, B]);
        "begin" === B.type && this.count++;
      }
      exec(W) {
        var B = this.getMatcher(this.regexIndex);
        B.lastIndex = this.lastIndex;
        B = B.exec(W);
        !this.resumingScanAtSamePosition() || B && B.index === this.lastIndex || (B = this.getMatcher(0), B.lastIndex = this.lastIndex + 1, B = B.exec(W));
        return B && (this.regexIndex += B.position + 1, this.regexIndex === this.count && this.considerAll()), B;
      }
    }
    if (p.compilerExtensions || (p.compilerExtensions = []), p.contains && p.contains.includes("self")) {
      throw Error("ERR: contains `self` is not supported at the top-level of a language.  See documentation.");
    }
    return p.classNameAliases = f(p.classNameAliases || {}), function ka(B, Y) {
      if (B.isCompiled) {
        return B;
      }
      [C, I, e, aa].forEach(ja => ja(B, Y));
      p.compilerExtensions.forEach(ja => ja(B, Y));
      B.__beforeBegin = null;
      [E, L, M].forEach(ja => ja(B, Y));
      B.isCompiled = !0;
      let qa = null;
      return "object" == typeof B.keywords && B.keywords.$pattern && (B.keywords = Object.assign({}, B.keywords), qa = B.keywords.$pattern, delete B.keywords.$pattern), qa = qa || /\w+/, B.keywords && (B.keywords = T(B.keywords, p.case_insensitive)), B.keywordPatternRe = z(qa, !0), Y && (B.begin || (B.begin = /\B|\b/), B.beginRe = z(B.begin), B.end || B.endsWithParent || (B.end = /\B|\b/), B.end && (B.endRe = z(B.end)), B.terminatorEnd = l(B.end) || "", B.endsWithParent && Y.terminatorEnd && (B.terminatorEnd += 
      (B.end ? "|" : "") + Y.terminatorEnd)), B.illegal && (B.illegalRe = z(B.illegal)), B.contains || (B.contains = []), B.contains = [].concat(...B.contains.map(ja => (fa => (fa.variants && !fa.cachedVariants && (fa.cachedVariants = fa.variants.map(va => f(fa, {variants:null}, va))), fa.cachedVariants ? fa.cachedVariants : k(fa) ? f(fa, {starts:fa.starts ? f(fa.starts) : null}) : Object.isFrozen(fa) ? f(fa) : fa))("self" === ja ? B : ja))), B.contains.forEach(ja => {
        ka(ja, B);
      }), B.starts && ka(B.starts, Y), B.matcher = (ja => {
        const fa = new Z;
        return ja.contains.forEach(va => fa.addRule(va.begin, {rule:va, type:"begin"})), ja.terminatorEnd && fa.addRule(ja.terminatorEnd, {type:"end"}), ja.illegal && fa.addRule(ja.illegal, {type:"illegal"}), fa;
      })(B), B;
    }(p);
  }
  function k(p) {
    return !!p && (p.endsWithParent || k(p.starts));
  }
  class h {
    constructor(p) {
      void 0 === p.data && (p.data = {});
      this.data = p.data;
      this.isMatchIgnored = !1;
    }
    ignoreMatch() {
      this.isMatchIgnored = !0;
    }
  }
  class r {
    constructor(p, z) {
      this.buffer = "";
      this.classPrefix = z.classPrefix;
      p.walk(this);
    }
    addText(p) {
      this.buffer += b(p);
    }
    openNode(p) {
      p.scope && (p = ((z, {prefix:U}) => z.startsWith("language:") ? z.replace("language:", "language-") : z.includes(".") ? (z = z.split("."), [`${U}${z.shift()}`, ...z.map((Z, W) => `${Z}${"_".repeat(W + 1)}`)].join(" ")) : `${U}${z}`)(p.scope, {prefix:this.classPrefix}), this.span(p));
    }
    closeNode(p) {
      p.scope && (this.buffer += "</span>");
    }
    value() {
      return this.buffer;
    }
    span(p) {
      this.buffer += `<span class="${p}">`;
    }
  }
  const m = (p = {}) => {
    const z = {children:[]};
    return Object.assign(z, p), z;
  };
  class y {
    constructor() {
      this.rootNode = m();
      this.stack = [this.rootNode];
    }
    get top() {
      return this.stack[this.stack.length - 1];
    }
    get root() {
      return this.rootNode;
    }
    add(p) {
      this.top.children.push(p);
    }
    openNode(p) {
      p = m({scope:p});
      this.add(p);
      this.stack.push(p);
    }
    closeNode() {
      if (1 < this.stack.length) {
        return this.stack.pop();
      }
    }
    closeAllNodes() {
      for (; this.closeNode();) {
      }
    }
    toJSON() {
      return JSON.stringify(this.rootNode, null, 4);
    }
    walk(p) {
      return this.constructor._walk(p, this.rootNode);
    }
    static _walk(p, z) {
      return "string" == typeof z ? p.addText(z) : z.children && (p.openNode(z), z.children.forEach(U => this._walk(p, U)), p.closeNode(z)), p;
    }
    static _collapse(p) {
      "string" != typeof p && p.children && (p.children.every(z => "string" == typeof z) ? p.children = [p.children.join("")] : p.children.forEach(z => {
        y._collapse(z);
      }));
    }
  }
  class A extends y {
    constructor(p) {
      super();
      this.options = p;
    }
    addText(p) {
      "" !== p && this.add(p);
    }
    startScope(p) {
      this.openNode(p);
    }
    endScope() {
      this.closeNode();
    }
    __addSublanguage(p, z) {
      p = p.root;
      z && (p.scope = "language:" + z);
      this.add(p);
    }
    toHTML() {
      return (new r(this, this.options)).value();
    }
    finalize() {
      return this.closeAllNodes(), !0;
    }
  }
  const J = /\[(?:[^\\\]]|\\.)*\]|\(\??|\\([1-9][0-9]*)|\\./;
  var O = {begin:"\\\\[\\s\\S]", relevance:0};
  const u = {scope:"string", begin:"'", end:"'", illegal:"\\n", contains:[O]}, D = {scope:"string", begin:'"', end:'"', illegal:"\\n", contains:[O]}, K = (p, z, U = {}) => {
    p = f({scope:"comment", begin:p, end:z, contains:[]}, U);
    p.contains.push({scope:"doctag", begin:"[ ]*(?=(TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):)", end:/(TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):/, excludeBegin:!0, relevance:0});
    z = t("I", "a", "is", "so", "us", "to", "at", "if", "in", "it", "on", /[A-Za-z]+['](d|ve|re|ll|t|s|n)/, /[A-Za-z]+[-][a-z]+/, /[A-Za-z][a-z]{2,}/);
    return p.contains.push({begin:w(/[ ]+/, "(", z, /[.]?[:]?([.][ ]|[ ])/, "){3}")}), p;
  }, G = K("//", "$"), P = K("/\\*", "\\*/"), R = K("#", "$");
  var S = Object.freeze({__proto__:null, APOS_STRING_MODE:u, BACKSLASH_ESCAPE:O, BINARY_NUMBER_MODE:{scope:"number", begin:"\\b(0b[01]+)", relevance:0}, BINARY_NUMBER_RE:"\\b(0b[01]+)", COMMENT:K, C_BLOCK_COMMENT_MODE:P, C_LINE_COMMENT_MODE:G, C_NUMBER_MODE:{scope:"number", begin:"(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", relevance:0}, C_NUMBER_RE:"(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)", END_SAME_AS_BEGIN:p => Object.assign(p, {"on:begin":(z, 
  U) => {
    U.data._beginMatch = z[1];
  }, "on:end":(z, U) => {
    U.data._beginMatch !== z[1] && U.ignoreMatch();
  }}), HASH_COMMENT_MODE:R, IDENT_RE:"[a-zA-Z]\\w*", MATCH_NOTHING_RE:/\b\B/, METHOD_GUARD:{begin:"\\.\\s*[a-zA-Z_]\\w*", relevance:0}, NUMBER_MODE:{scope:"number", begin:"\\b\\d+(\\.\\d+)?", relevance:0}, NUMBER_RE:"\\b\\d+(\\.\\d+)?", PHRASAL_WORDS_MODE:{begin:/\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\b/}, QUOTE_STRING_MODE:D, REGEXP_MODE:{scope:"regexp", begin:/\/(?=[^/\n]*\/)/, end:/\/[gimuy]*/, contains:[O, 
  {begin:/\[/, end:/\]/, relevance:0, contains:[O]}]}, RE_STARTERS_RE:"!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~", SHEBANG:(p = {}) => {
    const z = /^#![ ]*\//;
    return p.binary && (p.begin = w(z, /.*\b/, p.binary, /\b.*/)), f({scope:"meta", begin:z, end:/$/, relevance:0, "on:begin":(U, Z) => {
      0 !== U.index && Z.ignoreMatch();
    }}, p);
  }, TITLE_MODE:{scope:"title", begin:"[a-zA-Z]\\w*", relevance:0}, UNDERSCORE_IDENT_RE:"[a-zA-Z_]\\w*", UNDERSCORE_TITLE_MODE:{scope:"title", begin:"[a-zA-Z_]\\w*", relevance:0}});
  const aa = (p, z) => {
    if (p.beforeMatch) {
      if (p.starts) {
        throw Error("beforeMatch cannot be used with starts");
      }
      z = Object.assign({}, p);
      Object.keys(p).forEach(U => {
        delete p[U];
      });
      p.keywords = z.keywords;
      p.begin = w(z.beforeMatch, n(z.begin));
      p.starts = {relevance:0, contains:[Object.assign(z, {endsParent:!0})]};
      p.relevance = 0;
      delete z.beforeMatch;
    }
  }, ha = "of and for in not or if then parent list value".split(" "), ua = {}, ra = (p, ...z) => {
    console.log("WARN: " + p, ...z);
  }, sa = (p, z) => {
    ua[`${p}/${z}`] || (console.log(`Deprecated as of ${p}. ${z}`), ua[`${p}/${z}`] = !0);
  }, xa = Error();
  class ba extends Error {
    constructor(p, z) {
      super(p);
      this.name = "HTMLInjectionError";
      this.html = z;
    }
  }
  const za = Symbol("nomatch"), Ca = p => {
    function z(H, Q, X) {
      let da = "", pa = "";
      "object" == typeof Q ? (da = H, X = Q.ignoreIllegals, pa = Q.language) : (sa("10.7.0", "highlight(lang, code, ...args) has been deprecated."), sa("10.7.0", "Please use highlight(code, options) instead.\nhttps://github.com/highlightjs/highlight.js/issues/2277"), pa = H, da = Q);
      void 0 === X && (X = !0);
      H = {code:da, language:pa};
      ja("before:highlight", H);
      X = H.result ? H.result : U(H.language, H.code, X);
      return X.code = H.code, ja("after:highlight", X), X;
    }
    function U(H, Q, X, da) {
      function pa() {
        if (!V.keywords) {
          return void oa.addText(ia);
        }
        var F = 0;
        V.keywordPatternRe.lastIndex = 0;
        let N = V.keywordPatternRe.exec(ia), ca = "";
        for (; N;) {
          ca += ia.substring(F, N.index);
          F = wa.case_insensitive ? N[0].toLowerCase() : N[0];
          const la = (na = F, V.keywords[na]);
          if (la) {
            const [ya, Qa] = la;
            (oa.addText(ca), ca = "", Ga[F] = (Ga[F] || 0) + 1, 7 >= Ga[F] && (Da += Qa), ya.startsWith("_")) ? ca += N[0] : ma(N[0], wa.classNameAliases[ya] || ya);
          } else {
            ca += N[0];
          }
          F = V.keywordPatternRe.lastIndex;
          N = V.keywordPatternRe.exec(ia);
        }
        var na;
        ca += ia.substring(F);
        oa.addText(ca);
      }
      function ea() {
        if (null != V.subLanguage) {
          a: {
            if ("" !== ia) {
              if ("string" == typeof V.subLanguage) {
                if (!fa[V.subLanguage]) {
                  oa.addText(ia);
                  break a;
                }
                var F = U(V.subLanguage, ia, !0, Ka[V.subLanguage]);
                Ka[V.subLanguage] = F._top;
              } else {
                F = Z(ia, V.subLanguage.length ? V.subLanguage : null);
              }
              0 < V.relevance && (Da += F.relevance);
              oa.__addSublanguage(F._emitter, F.language);
            }
          }
        } else {
          pa();
        }
        ia = "";
      }
      function ma(F, N) {
        "" !== F && (oa.startScope(N), oa.addText(F), oa.endScope());
      }
      function Aa(F, N) {
        let ca = 1;
        const na = N.length - 1;
        for (; ca <= na;) {
          if (!F._emit[ca]) {
            ca++;
            continue;
          }
          const la = wa.classNameAliases[F[ca]] || F[ca], ya = N[ca];
          la ? ma(ya, la) : (ia = ya, pa(), ia = "");
          ca++;
        }
      }
      function La(F, N) {
        return F.scope && "string" == typeof F.scope && oa.openNode(wa.classNameAliases[F.scope] || F.scope), F.beginScope && (F.beginScope._wrap ? (ma(ia, wa.classNameAliases[F.beginScope._wrap] || F.beginScope._wrap), ia = "") : F.beginScope._multi && (Aa(F.beginScope, N), ia = "")), V = Object.create(F, {parent:{value:V}}), V;
      }
      function Ma(F, N, ca) {
        var na = F.endRe;
        if (na = (na = na && na.exec(ca)) && 0 === na.index) {
          if (F["on:end"]) {
            const la = new h(F);
            F["on:end"](N, la);
            la.isMatchIgnored && (na = !1);
          }
          if (na) {
            for (; F.endsParent && F.parent;) {
              F = F.parent;
            }
            return F;
          }
        }
        if (F.endsWithParent) {
          return Ma(F.parent, N, ca);
        }
      }
      function Na(F, N) {
        var ca = N && N[0];
        if (ia += F, null == ca) {
          return ea(), 0;
        }
        if ("begin" === Ea.type && "end" === N.type && Ea.index === N.index && "" === ca) {
          if (ia += Q.slice(N.index, N.index + 1), !Ba) {
            throw N = Error(`0 width match regex (${H})`), N.languageName = H, N.badRule = Ea.rule, N;
          }
          return 1;
        }
        if (Ea = N, "begin" === N.type) {
          a: {
            ca = N[0];
            F = N.rule;
            var na = new h(F);
            const ya = [F.__beforeBegin, F["on:begin"]];
            for (var la of ya) {
              if (la && (la(N, na), na.isMatchIgnored)) {
                N = 0 === V.matcher.regexIndex ? (ia += ca[0], 1) : (Ha = !0, 0);
                break a;
              }
            }
            N = (F.skip ? ia += ca : (F.excludeBegin && (ia += ca), ea(), F.returnBegin || F.excludeBegin || (ia = ca)), La(F, N), F.returnBegin ? 0 : ca.length);
          }
          return N;
        }
        if ("illegal" === N.type && !X) {
          throw N = Error('Illegal lexeme "' + ca + '" for mode "' + (V.scope || "<unnamed>") + '"'), N.mode = V, N;
        }
        if ("end" === N.type) {
          la = N[0];
          F = Q.substring(N.index);
          if (na = Ma(V, N, F)) {
            F = V;
            V.endScope && V.endScope._wrap ? (ea(), ma(la, V.endScope._wrap)) : V.endScope && V.endScope._multi ? (ea(), Aa(V.endScope, N)) : F.skip ? ia += la : (F.returnEnd || F.excludeEnd || (ia += la), ea(), F.excludeEnd && (ia = la));
            do {
              V.scope && oa.closeNode(), V.skip || V.subLanguage || (Da += V.relevance), V = V.parent;
            } while (V !== na.parent);
            la = (na.starts && La(na.starts, N), F.returnEnd ? 0 : la.length);
          } else {
            la = za;
          }
          if (la !== za) {
            return la;
          }
        }
        if ("illegal" === N.type && "" === ca) {
          return 1;
        }
        if (1e5 < Ia && Ia > 3 * N.index) {
          throw Error("potential infinite loop, way more iterations than matches");
        }
        return ia += ca, ca.length;
      }
      const Ga = Object.create(null);
      let Ea = {};
      const wa = Y(H);
      if (!wa) {
        throw console.error("Could not find the language '{}', did you forget to load/include a language module?".replace("{}", H)), Error('Unknown language: "' + H + '"');
      }
      const Ra = c(wa);
      let Ja = "", V = da || Ra;
      const Ka = {}, oa = new ta.__emitter(ta);
      (() => {
        const F = [];
        for (let N = V; N !== wa; N = N.parent) {
          N.scope && F.unshift(N.scope);
        }
        F.forEach(N => oa.openNode(N));
      })();
      let ia = "", Da = 0, Ia = da = 0, Ha = !1;
      try {
        if (wa.__emitTokens) {
          wa.__emitTokens(Q, oa);
        } else {
          for (V.matcher.considerAll();;) {
            Ia++;
            Ha ? Ha = !1 : V.matcher.considerAll();
            V.matcher.lastIndex = da;
            const F = V.matcher.exec(Q);
            if (!F) {
              break;
            }
            const N = Na(Q.substring(da, F.index), F);
            da = F.index + N;
          }
          Na(Q.substring(da));
        }
        return oa.finalize(), Ja = oa.toHTML(), {language:H, value:Ja, relevance:Da, illegal:!1, _emitter:oa, _top:V};
      } catch (F) {
        if (F.message && F.message.includes("Illegal")) {
          return {language:H, value:b(Q), illegal:!0, relevance:0, _illegalBy:{message:F.message, index:da, context:Q.slice(da - 100, da + 100), mode:F.mode, resultSoFar:Ja}, _emitter:oa};
        }
        if (Ba) {
          return {language:H, value:b(Q), illegal:!1, relevance:0, errorRaised:F, _emitter:oa, _top:V};
        }
        throw F;
      }
    }
    function Z(H, Q) {
      Q = Q || ta.languages || Object.keys(fa);
      var X = (ea => {
        const ma = {value:b(ea), illegal:!1, relevance:0, _top:Oa, _emitter:new ta.__emitter(ta)};
        return ma._emitter.addText(ea), ma;
      })(H);
      Q = Q.filter(Y).filter(qa).map(ea => U(ea, H, !1));
      Q.unshift(X);
      X = Q.sort((ea, ma) => {
        if (ea.relevance !== ma.relevance) {
          return ma.relevance - ea.relevance;
        }
        if (ea.language && ma.language) {
          if (Y(ea.language).supersetOf === ma.language) {
            return 1;
          }
          if (Y(ma.language).supersetOf === ea.language) {
            return -1;
          }
        }
        return 0;
      });
      const [da, pa] = X;
      return da.secondBest = pa, da;
    }
    function W(H) {
      const Q = (pa => {
        var ea = pa.className + " ";
        ea += pa.parentNode ? pa.parentNode.className : "";
        const ma = ta.languageDetectRe.exec(ea);
        return ma ? (ea = Y(ma[1]), ea || (ra("Could not find the language '{}', did you forget to load/include a language module?".replace("{}", ma[1])), ra("Falling back to no-highlight mode for this block.", pa)), ea ? ma[1] : "no-highlight") : ea.split(/\s+/).find(Aa => ta.noHighlightRe.test(Aa) || Y(Aa));
      })(H);
      if (!ta.noHighlightRe.test(Q)) {
        if (ja("before:highlightElement", {el:H, language:Q}), H.dataset.highlighted) {
          return void console.log("Element previously highlighted. To highlight again, first unset `dataset.highlighted`.", H);
        }
        if (0 < H.children.length && (ta.ignoreUnescapedHTML || (console.warn("One of your code blocks includes unescaped HTML. This is a potentially serious security risk."), console.warn("https://github.com/highlightjs/highlight.js/wiki/security"), console.warn("The element with unescaped HTML:"), console.warn(H)), ta.throwUnescapedHTML)) {
          throw new ba("One of your code blocks includes unescaped HTML.", H.innerHTML);
        }
        var X = H.textContent, da = Q ? z(X, {language:Q, ignoreIllegals:!0}) : Z(X);
        H.innerHTML = da.value;
        H.dataset.highlighted = "yes";
        ((pa, ea, ma) => {
          ea = ea && va[ea] || ma;
          pa.classList.add("hljs");
          pa.classList.add("language-" + ea);
        })(H, Q, da.language);
        H.result = {language:da.language, re:da.relevance, relevance:da.relevance};
        da.secondBest && (H.secondBest = {language:da.secondBest.language, relevance:da.secondBest.relevance});
        ja("after:highlightElement", {el:H, result:da, text:X});
      }
    }
    function B() {
      "loading" !== document.readyState ? document.querySelectorAll(ta.cssSelector).forEach(W) : Pa = !0;
    }
    function Y(H) {
      return H = (H || "").toLowerCase(), fa[H] || fa[va[H]];
    }
    function ka(H, {languageName:Q}) {
      "string" == typeof H && (H = [H]);
      H.forEach(X => {
        va[X.toLowerCase()] = Q;
      });
    }
    function qa(H) {
      return (H = Y(H)) && !H.disableAutodetect;
    }
    function ja(H, Q) {
      Fa.forEach(X => {
        X[H] && X[H](Q);
      });
    }
    const fa = Object.create(null), va = Object.create(null), Fa = [];
    let Ba = !0;
    const Oa = {disableAutodetect:!0, name:"Plain text", contains:[]};
    let ta = {ignoreUnescapedHTML:!1, throwUnescapedHTML:!1, noHighlightRe:/^(no-?highlight)$/i, languageDetectRe:/\blang(?:uage)?-([\w-]+)\b/i, classPrefix:"hljs-", cssSelector:"pre code", languages:null, __emitter:A}, Pa = !1;
    "undefined" != typeof window && window.addEventListener && window.addEventListener("DOMContentLoaded", () => {
      Pa && B();
    }, !1);
    Object.assign(p, {highlight:z, highlightAuto:Z, highlightAll:B, highlightElement:W, highlightBlock:H => (sa("10.7.0", "highlightBlock will be removed entirely in v12.0"), sa("10.7.0", "Please use highlightElement now."), W(H)), configure:H => {
      ta = f(ta, H);
    }, initHighlighting:() => {
      B();
      sa("10.6.0", "initHighlighting() deprecated.  Use highlightAll() now.");
    }, initHighlightingOnLoad:() => {
      B();
      sa("10.6.0", "initHighlightingOnLoad() deprecated.  Use highlightAll() now.");
    }, registerLanguage:(H, Q) => {
      let X = null;
      try {
        X = Q(p);
      } catch (da) {
        console.error("Language definition for '{}' could not be registered.".replace("{}", H));
        if (!Ba) {
          throw da;
        }
        console.error(da);
        X = Oa;
      }
      X.name || (X.name = H);
      fa[H] = X;
      X.rawDefinition = Q.bind(null, p);
      X.aliases && ka(X.aliases, {languageName:H});
    }, unregisterLanguage:H => {
      delete fa[H];
      for (const Q of Object.keys(va)) {
        va[Q] === H && delete va[Q];
      }
    }, listLanguages:() => Object.keys(fa), getLanguage:Y, registerAliases:ka, autoDetection:qa, inherit:f, addPlugin:H => {
      (Q => {
        Q["before:highlightBlock"] && !Q["before:highlightElement"] && (Q["before:highlightElement"] = X => {
          Q["before:highlightBlock"](Object.assign({block:X.el}, X));
        });
        Q["after:highlightBlock"] && !Q["after:highlightElement"] && (Q["after:highlightElement"] = X => {
          Q["after:highlightBlock"](Object.assign({block:X.el}, X));
        });
      })(H);
      Fa.push(H);
    }, removePlugin:H => {
      H = Fa.indexOf(H);
      -1 !== H && Fa.splice(H, 1);
    }});
    p.debugMode = () => {
      Ba = !1;
    };
    p.safeMode = () => {
      Ba = !0;
    };
    p.versionString = "11.9.0";
    p.regex = {concat:w, lookahead:n, either:t, optional:d, anyNumberOfTimes:q};
    for (const H in S) {
      "object" == typeof S[H] && g(S[H]);
    }
    return Object.assign(p, S), p;
  };
  O = Ca({});
  return O.newInstance = () => Ca({}), O;
}();
"object" == typeof exports && "undefined" != typeof module && (module.exports = hljs);
(() => {
  var g = (() => b => {
    const f = {className:"number", begin:/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(:\d{1,5})?/};
    return {name:"Apache config", aliases:["apacheconf"], case_insensitive:!0, contains:[b.HASH_COMMENT_MODE, {className:"section", begin:/<\/?/, end:/>/, contains:[f, {className:"number", begin:/:\d{1,5}/}, b.inherit(b.QUOTE_STRING_MODE, {relevance:0})]}, {className:"attribute", begin:/\w+/, relevance:0, keywords:{_:"order deny allow setenv rewriterule rewriteengine rewritecond documentroot sethandler errordocument loadmodule options header listen serverroot servername".split(" ")}, starts:{end:/$/, 
    relevance:0, keywords:{literal:"on off all deny allow"}, contains:[{className:"meta", begin:/\s\[/, end:/\]$/}, {className:"variable", begin:/[\$%]\{/, end:/\}/, contains:["self", {className:"number", begin:/[$%]\d+/}]}, f, {className:"number", begin:/\b\d+/}, b.QUOTE_STRING_MODE]}}], illegal:/\S/};
  })();
  hljs.registerLanguage("apache", g);
})();
(() => {
  var g = (() => b => {
    const f = {};
    var l = {begin:/\$\{/, end:/\}/, contains:["self", {begin:/:-/, contains:[f]}]};
    Object.assign(f, {className:"variable", variants:[{begin:b.regex.concat(/\$[\w\d#@][\w\d_]*/, "(?![\\w\\d])(?![$])")}, l]});
    var n = {className:"subst", begin:/\$\(/, end:/\)/, contains:[b.BACKSLASH_ESCAPE]};
    l = b.inherit(b.COMMENT(), {match:[/(^|\s)/, /#.*$/], scope:{2:"comment"}});
    const q = {begin:/<<-?\s*(?=\w+)/, starts:{contains:[b.END_SAME_AS_BEGIN({begin:/(\w+)/, end:/(\w+)/, className:"string"})]}}, d = {className:"string", begin:/"/, end:/"/, contains:[b.BACKSLASH_ESCAPE, f, n]};
    n.contains.push(d);
    n = {begin:/\$?\(\(/, end:/\)\)/, contains:[{begin:/\d+#[0-9a-f]+/, className:"number"}, b.NUMBER_MODE, f]};
    const w = b.SHEBANG({binary:"(fish|bash|zsh|sh|csh|ksh|tcsh|dash|scsh)", relevance:10}), t = {className:"function", begin:/\w[\w\d_]*\s*\(\s*\)\s*\{/, returnBegin:!0, contains:[b.inherit(b.TITLE_MODE, {begin:/\w[\w\d_]*/})], relevance:0};
    return {name:"Bash", aliases:["sh"], keywords:{$pattern:/\b[a-z][a-z0-9._-]+\b/, keyword:"if then else elif fi for while until in do done case esac function select".split(" "), literal:["true", "false"], built_in:"break cd continue eval exec exit export getopts hash pwd readonly return shift test times trap umask unset alias bind builtin caller command declare echo enable help let local logout mapfile printf read readarray source type typeset ulimit unalias set shopt autoload bg bindkey bye cap chdir clone comparguments compcall compctl compdescribe compfiles compgroups compquote comptags comptry compvalues dirs disable disown echotc echoti emulate fc fg float functions getcap getln history integer jobs kill limit log noglob popd print pushd pushln rehash sched setcap setopt stat suspend ttyctl unfunction unhash unlimit unsetopt vared wait whence where which zcompile zformat zftp zle zmodload zparseopts zprof zpty zregexparse zsocket zstyle ztcp chcon chgrp chown chmod cp dd df dir dircolors ln ls mkdir mkfifo mknod mktemp mv realpath rm rmdir shred sync touch truncate vdir b2sum base32 base64 cat cksum comm csplit cut expand fmt fold head join md5sum nl numfmt od paste ptx pr sha1sum sha224sum sha256sum sha384sum sha512sum shuf sort split sum tac tail tr tsort unexpand uniq wc arch basename chroot date dirname du echo env expr factor groups hostid id link logname nice nohup nproc pathchk pinky printenv printf pwd readlink runcon seq sleep stat stdbuf stty tee test timeout tty uname unlink uptime users who whoami yes".split(" ")}, 
    contains:[w, b.SHEBANG(), t, n, l, q, {match:/(\/[a-z._-]+)+/}, d, {match:/\\"/}, {className:"string", begin:/'/, end:/'/}, {match:/\\'/}, f]};
  })();
  hljs.registerLanguage("bash", g);
})();
(() => {
  var g = (() => b => ({name:"BASIC", case_insensitive:!0, illegal:"^.", keywords:{$pattern:"[a-zA-Z][a-zA-Z0-9_$%!#]*", keyword:"ABS ASC AND ATN AUTO|0 BEEP BLOAD|10 BSAVE|10 CALL CALLS CDBL CHAIN CHDIR CHR$|10 CINT CIRCLE CLEAR CLOSE CLS COLOR COM COMMON CONT COS CSNG CSRLIN CVD CVI CVS DATA DATE$ DEFDBL DEFINT DEFSNG DEFSTR DEF|0 SEG USR DELETE DIM DRAW EDIT END ENVIRON ENVIRON$ EOF EQV ERASE ERDEV ERDEV$ ERL ERR ERROR EXP FIELD FILES FIX FOR|0 FRE GET GOSUB|10 GOTO HEX$ IF THEN ELSE|0 INKEY$ INP INPUT INPUT# INPUT$ INSTR IMP INT IOCTL IOCTL$ KEY ON OFF LIST KILL LEFT$ LEN LET LINE LLIST LOAD LOC LOCATE LOF LOG LPRINT USING LSET MERGE MID$ MKDIR MKD$ MKI$ MKS$ MOD NAME NEW NEXT NOISE NOT OCT$ ON OR PEN PLAY STRIG OPEN OPTION BASE OUT PAINT PALETTE PCOPY PEEK PMAP POINT POKE POS PRINT PRINT] PSET PRESET PUT RANDOMIZE READ REM RENUM RESET|0 RESTORE RESUME RETURN|0 RIGHT$ RMDIR RND RSET RUN SAVE SCREEN SGN SHELL SIN SOUND SPACE$ SPC SQR STEP STICK STOP STR$ STRING$ SWAP SYSTEM TAB TAN TIME$ TIMER TROFF TRON TO USR VAL VARPTR VARPTR$ VIEW WAIT WHILE WEND WIDTH WINDOW WRITE XOR".split(" ")}, 
  contains:[b.QUOTE_STRING_MODE, b.COMMENT("REM", "$", {relevance:10}), b.COMMENT("'", "$", {relevance:0}), {className:"symbol", begin:"^[0-9]+ ", relevance:10}, {className:"number", begin:"\\b\\d+(\\.\\d+)?([edED]\\d+)?[#!]?", relevance:0}, {className:"number", begin:"(&[hH][0-9a-fA-F]{1,4})"}, {className:"number", begin:"(&[oO][0-7]{1,6})"}]}))();
  hljs.registerLanguage("basic", g);
})();
(() => {
  var g = (() => b => {
    var f = b.regex, l = b.COMMENT("//", "$", {contains:[{begin:/\\\n/}]});
    const n = "(decltype\\(auto\\)|" + f.optional("[a-zA-Z_]\\w*::") + "[a-zA-Z_]\\w*" + f.optional("<[^<>]+>") + ")", q = {className:"type", variants:[{begin:"\\b[a-z\\d_]*_t\\b"}, {match:/\batomic_[a-z]{3,6}\b/}]}, d = {className:"string", variants:[{begin:'(u8?|U|L)?"', end:'"', illegal:"\\n", contains:[b.BACKSLASH_ESCAPE]}, {begin:"(u8?|U|L)?'(\\\\(x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4,8}|[0-7]{3}|\\S)|.)", end:"'", illegal:"."}, b.END_SAME_AS_BEGIN({begin:/(?:u8?|U|L)?R"([^()\\ ]{0,16})\(/, end:/\)([^()\\ ]{0,16})"/})]}, 
    w = {className:"number", variants:[{begin:"\\b(0b[01']+)"}, {begin:"(-?)\\b([\\d']+(\\.[\\d']*)?|\\.[\\d']+)((ll|LL|l|L)(u|U)?|(u|U)(ll|LL|l|L)?|f|F|b|B)"}, {begin:"(-?)(\\b0[xX][a-fA-F0-9']+|(\\b[\\d']+(\\.[\\d']*)?|\\.[\\d']+)([eE][-+]?[\\d']+)?)"}], relevance:0}, t = {className:"meta", begin:/#\s*[a-z]+\b/, end:/$/, keywords:{keyword:"if else elif endif define undef warning error line pragma _Pragma ifdef ifndef include"}, contains:[{begin:/\\\n/, relevance:0}, b.inherit(d, {className:"string"}), 
    {className:"string", begin:/<.*?>/}, l, b.C_BLOCK_COMMENT_MODE]}, v = {className:"title", begin:f.optional("[a-zA-Z_]\\w*::") + b.IDENT_RE, relevance:0}, x = f.optional("[a-zA-Z_]\\w*::") + b.IDENT_RE + "\\s*\\(";
    f = {keyword:"asm auto break case continue default do else enum extern for fortran goto if inline register restrict return sizeof struct switch typedef union volatile while _Alignas _Alignof _Atomic _Generic _Noreturn _Static_assert _Thread_local alignas alignof noreturn static_assert thread_local _Pragma".split(" "), type:"float double signed unsigned int short long char void _Bool _Complex _Imaginary _Decimal32 _Decimal64 _Decimal128 const static complex bool imaginary".split(" "), literal:"true false NULL", 
    built_in:"std string wstring cin cout cerr clog stdin stdout stderr stringstream istringstream ostringstream auto_ptr deque list queue stack vector map set pair bitset multiset multimap unordered_set unordered_map unordered_multiset unordered_multimap priority_queue make_pair array shared_ptr abort terminate abs acos asin atan2 atan calloc ceil cosh cos exit exp fabs floor fmod fprintf fputs free frexp fscanf future isalnum isalpha iscntrl isdigit isgraph islower isprint ispunct isspace isupper isxdigit tolower toupper labs ldexp log10 log malloc realloc memchr memcmp memcpy memset modf pow printf putchar puts scanf sinh sin snprintf sprintf sqrt sscanf strcat strchr strcmp strcpy strcspn strlen strncat strncmp strncpy strpbrk strrchr strspn strstr tanh tan vfprintf vprintf vsprintf endl initializer_list unique_ptr"};
    const C = [t, q, l, b.C_BLOCK_COMMENT_MODE, w, d], E = {variants:[{begin:/=/, end:/;/}, {begin:/\(/, end:/\)/}, {beginKeywords:"new throw return else", end:/;/}], keywords:f, contains:C.concat([{begin:/\(/, end:/\)/, keywords:f, contains:C.concat(["self"]), relevance:0}]), relevance:0};
    l = {begin:"(" + n + "[\\*&\\s]+)+" + x, returnBegin:!0, end:/[{;=]/, excludeEnd:!0, keywords:f, illegal:/[^\w\s\*&:<>.]/, contains:[{begin:"decltype\\(auto\\)", keywords:f, relevance:0}, {begin:x, returnBegin:!0, contains:[b.inherit(v, {className:"title.function"})], relevance:0}, {relevance:0, match:/,/}, {className:"params", begin:/\(/, end:/\)/, keywords:f, relevance:0, contains:[l, b.C_BLOCK_COMMENT_MODE, d, w, q, {begin:/\(/, end:/\)/, keywords:f, relevance:0, contains:["self", l, b.C_BLOCK_COMMENT_MODE, 
    d, w, q]}]}, q, l, b.C_BLOCK_COMMENT_MODE, t]};
    return {name:"C", aliases:["h"], keywords:f, disableAutodetect:!0, illegal:"</", contains:[].concat(E, l, C, [t, {begin:b.IDENT_RE + "::", keywords:f}, {className:"class", beginKeywords:"enum class struct union", end:/[{;:<>=]/, contains:[{beginKeywords:"final class struct"}, b.TITLE_MODE]}]), exports:{preprocessor:t, strings:d, keywords:f}};
  })();
  hljs.registerLanguage("c", g);
})();
(() => {
  var g = (() => b => {
    const f = {scope:"number", relevance:0, variants:[{match:/[-+]?0[xX][0-9a-fA-F]+N?/}, {match:/[-+]?0[0-7]+N?/}, {match:/[-+]?[1-9][0-9]?[rR][0-9a-zA-Z]+N?/}, {match:/[-+]?[0-9]+\/[0-9]+N?/}, {match:/[-+]?[0-9]+((\.[0-9]*([eE][+-]?[0-9]+)?M?)|([eE][+-]?[0-9]+M?|M))/}, {match:/[-+]?([1-9][0-9]*|0)N?/}]}, l = {scope:"character", variants:[{match:/\\o[0-3]?[0-7]{1,2}/}, {match:/\\u[0-9a-fA-F]{4}/}, {match:/\\(newline|space|tab|formfeed|backspace|return)/}, {match:/\\\S/, relevance:0}]}, n = {scope:"regex", 
    begin:/#"/, end:/"/, contains:[b.BACKSLASH_ESCAPE]}, q = b.inherit(b.QUOTE_STRING_MODE, {illegal:null}), d = {scope:"punctuation", match:/,/, relevance:0};
    b = b.COMMENT(";", "$", {relevance:0});
    const w = {className:"literal", begin:/\b(true|false|nil)\b/}, t = {begin:"\\[|(#::?[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*)?\\{", end:"[\\]\\}]", relevance:0}, v = {className:"symbol", begin:"[:]{1,2}[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*"}, x = {begin:"\\(", end:"\\)"}, C = {endsWithParent:!0, relevance:0}, E = {keywords:{$pattern:"[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*", built_in:"def defonce defprotocol defstruct defmulti defmethod defn- defn defmacro deftype defrecord cond apply if-not if-let if not not= =|0 <|0 >|0 <=|0 >=|0 ==|0 +|0 /|0 *|0 -|0 rem quot neg? pos? delay? symbol? keyword? true? false? integer? empty? coll? list? set? ifn? fn? associative? sequential? sorted? counted? reversible? number? decimal? class? distinct? isa? float? rational? reduced? ratio? odd? even? char? seq? vector? string? map? nil? contains? zero? instance? not-every? not-any? libspec? -> ->> .. . inc compare do dotimes mapcat take remove take-while drop letfn drop-last take-last drop-while while intern condp case reduced cycle split-at split-with repeat replicate iterate range merge zipmap declare line-seq sort comparator sort-by dorun doall nthnext nthrest partition eval doseq await await-for let agent atom send send-off release-pending-sends add-watch mapv filterv remove-watch agent-error restart-agent set-error-handler error-handler set-error-mode! error-mode shutdown-agents quote var fn loop recur throw try monitor-enter monitor-exit macroexpand macroexpand-1 for dosync and or when when-not when-let comp juxt partial sequence memoize constantly complement identity assert peek pop doto proxy first rest cons cast coll last butlast sigs reify second ffirst fnext nfirst nnext meta with-meta ns in-ns create-ns import refer keys select-keys vals key val rseq name namespace promise into transient persistent! conj! assoc! dissoc! pop! disj! use class type num float double short byte boolean bigint biginteger bigdec print-method print-dup throw-if printf format load compile get-in update-in pr pr-on newline flush read slurp read-line subvec with-open memfn time re-find re-groups rand-int rand mod locking assert-valid-fdecl alias resolve ref deref refset swap! reset! set-validator! compare-and-set! alter-meta! reset-meta! commute get-validator alter ref-set ref-history-count ref-min-history ref-max-history ensure sync io! new next conj set! to-array future future-call into-array aset gen-class reduce map filter find empty hash-map hash-set sorted-map sorted-map-by sorted-set sorted-set-by vec vector seq flatten reverse assoc dissoc list disj get union difference intersection extend extend-type extend-protocol int nth delay count concat chunk chunk-buffer chunk-append chunk-first chunk-rest max min dec unchecked-inc-int unchecked-inc unchecked-dec-inc unchecked-dec unchecked-negate unchecked-add-int unchecked-add unchecked-subtract-int unchecked-subtract chunk-next chunk-cons chunked-seq? prn vary-meta lazy-seq spread list* str find-keyword keyword symbol gensym force rationalize"}, 
    className:"name", begin:"[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*", relevance:0, starts:C}, L = [d, x, l, n, q, b, v, t, f, w, {begin:"[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*", relevance:0}], I = {beginKeywords:"def defonce defprotocol defstruct defmulti defmethod defn- defn defmacro deftype defrecord", keywords:{$pattern:"[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*", keyword:"def defonce defprotocol defstruct defmulti defmethod defn- defn defmacro deftype defrecord"}, 
    end:'(\\[|#|\\d|"|:|\\{|\\)|\\(|$)', contains:[{className:"title", begin:"[#]?[a-zA-Z_\\-!.?+*=<>&'][a-zA-Z_\\-!.?+*=<>&'0-9/;:$#]*", relevance:0, excludeEnd:!0, endsParent:!0}].concat(L)};
    return x.contains = [I, E, C], C.contains = L, t.contains = L, {name:"Clojure", aliases:["clj", "edn"], illegal:/\S/, contains:[d, x, l, n, q, b, v, t, f, w]};
  })();
  hljs.registerLanguage("clojure", g);
})();
(() => {
  var g = (() => {
    const b = "as in of if for while finally var new function do return void else break catch instanceof with throw case default try switch continue typeof delete let yield const class debugger async await static import from export extends".split(" "), f = "true false null undefined NaN Infinity".split(" "), l = [].concat("setInterval setTimeout clearInterval clearTimeout require exports eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape".split(" "), 
    "Object Function Boolean Symbol Math Date Number BigInt String RegExp Array Float32Array Float64Array Int8Array Uint8Array Uint8ClampedArray Int16Array Int32Array Uint16Array Uint32Array BigInt64Array BigUint64Array Set Map WeakSet WeakMap ArrayBuffer SharedArrayBuffer Atomics DataView JSON Promise Generator GeneratorFunction AsyncFunction Reflect Proxy Intl WebAssembly".split(" "), "Error EvalError InternalError RangeError ReferenceError SyntaxError TypeError URIError".split(" "));
    return n => {
      const q = {keyword:b.concat("then unless until loop by when and or is isnt not".split(" ")).filter((d = ["var", "const", "let", "function", "static"], C => !d.includes(C))), literal:f.concat(["yes", "no", "on", "off"]), built_in:l.concat(["npm", "print"])};
      var d, w = {className:"subst", begin:/#\{/, end:/\}/, keywords:q};
      const t = [n.BINARY_NUMBER_MODE, n.inherit(n.C_NUMBER_MODE, {starts:{end:"(\\s*/)?", relevance:0}}), {className:"string", variants:[{begin:/'''/, end:/'''/, contains:[n.BACKSLASH_ESCAPE]}, {begin:/'/, end:/'/, contains:[n.BACKSLASH_ESCAPE]}, {begin:/"""/, end:/"""/, contains:[n.BACKSLASH_ESCAPE, w]}, {begin:/"/, end:/"/, contains:[n.BACKSLASH_ESCAPE, w]}]}, {className:"regexp", variants:[{begin:"///", end:"///", contains:[w, n.HASH_COMMENT_MODE]}, {begin:"//[gim]{0,3}(?=\\W)", relevance:0}, 
      {begin:/\/(?![ *]).*?(?![\\]).\/[gim]{0,3}(?=\W)/}]}, {begin:"@[A-Za-z$_][0-9A-Za-z$_]*"}, {subLanguage:"javascript", excludeBegin:!0, excludeEnd:!0, variants:[{begin:"```", end:"```"}, {begin:"`", end:"`"}]}];
      w.contains = t;
      w = n.inherit(n.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*"});
      const v = {className:"params", begin:"\\([^\\(]", returnBegin:!0, contains:[{begin:/\(/, end:/\)/, keywords:q, contains:["self"].concat(t)}]}, x = {variants:[{match:[/class\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /\s+extends\s+/, "[A-Za-z$_][0-9A-Za-z$_]*"]}, {match:[/class\s+/, "[A-Za-z$_][0-9A-Za-z$_]*"]}], scope:{2:"title.class", 4:"title.class.inherited"}, keywords:q};
      return {name:"CoffeeScript", aliases:["coffee", "cson", "iced"], keywords:q, illegal:/\/\*/, contains:[...t, n.COMMENT("###", "###"), n.HASH_COMMENT_MODE, {className:"function", begin:"^\\s*[A-Za-z$_][0-9A-Za-z$_]*\\s*=\\s*(\\(.*\\)\\s*)?\\B[-=]>", end:"[-=]>", returnBegin:!0, contains:[w, v]}, {begin:/[:\(,=]\s*/, relevance:0, contains:[{className:"function", begin:"(\\(.*\\)\\s*)?\\B[-=]>", end:"[-=]>", returnBegin:!0, contains:[v]}]}, x, {begin:"[A-Za-z$_][0-9A-Za-z$_]*:", end:":", returnBegin:!0, 
      returnEnd:!0, relevance:0}]};
    };
  })();
  hljs.registerLanguage("coffeescript", g);
})();
(() => {
  var g = (() => b => {
    var f = b.regex;
    const l = b.COMMENT("//", "$", {contains:[{begin:/\\\n/}]}), n = "(?!struct)(decltype\\(auto\\)|" + f.optional("[a-zA-Z_]\\w*::") + "[a-zA-Z_]\\w*" + f.optional("<[^<>]+>") + ")", q = {className:"type", begin:"\\b[a-z\\d_]*_t\\b"}, d = {className:"string", variants:[{begin:'(u8?|U|L)?"', end:'"', illegal:"\\n", contains:[b.BACKSLASH_ESCAPE]}, {begin:"(u8?|U|L)?'(\\\\(x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4,8}|[0-7]{3}|\\S)|.)", end:"'", illegal:"."}, b.END_SAME_AS_BEGIN({begin:/(?:u8?|U|L)?R"([^()\\ ]{0,16})\(/, 
    end:/\)([^()\\ ]{0,16})"/})]}, w = {className:"number", variants:[{begin:"[+-]?(?:(?:[0-9](?:'?[0-9])*\\.(?:[0-9](?:'?[0-9])*)?|\\.[0-9](?:'?[0-9])*)(?:[Ee][+-]?[0-9](?:'?[0-9])*)?|[0-9](?:'?[0-9])*[Ee][+-]?[0-9](?:'?[0-9])*|0[Xx](?:[0-9A-Fa-f](?:'?[0-9A-Fa-f])*(?:\\.(?:[0-9A-Fa-f](?:'?[0-9A-Fa-f])*)?)?|\\.[0-9A-Fa-f](?:'?[0-9A-Fa-f])*)[Pp][+-]?[0-9](?:'?[0-9])*)(?:[Ff](?:16|32|64|128)?|(BF|bf)16|[Ll]|)"}, {begin:"[+-]?\\b(?:0[Bb][01](?:'?[01])*|0[Xx][0-9A-Fa-f](?:'?[0-9A-Fa-f])*|0(?:'?[0-7])*|[1-9](?:'?[0-9])*)(?:[Uu](?:LL?|ll?)|[Uu][Zz]?|(?:LL?|ll?)[Uu]?|[Zz][Uu]|)"}], 
    relevance:0}, t = {className:"meta", begin:/#\s*[a-z]+\b/, end:/$/, keywords:{keyword:"if else elif endif define undef warning error line pragma _Pragma ifdef ifndef include"}, contains:[{begin:/\\\n/, relevance:0}, b.inherit(d, {className:"string"}), {className:"string", begin:/<.*?>/}, l, b.C_BLOCK_COMMENT_MODE]}, v = {className:"title", begin:f.optional("[a-zA-Z_]\\w*::") + b.IDENT_RE, relevance:0}, x = f.optional("[a-zA-Z_]\\w*::") + b.IDENT_RE + "\\s*\\(", C = {type:"bool char char16_t char32_t char8_t double float int long short void wchar_t unsigned signed const static".split(" "), 
    keyword:"alignas alignof and and_eq asm atomic_cancel atomic_commit atomic_noexcept auto bitand bitor break case catch class co_await co_return co_yield compl concept const_cast|10 consteval constexpr constinit continue decltype default delete do dynamic_cast|10 else enum explicit export extern false final for friend goto if import inline module mutable namespace new noexcept not not_eq nullptr operator or or_eq override private protected public reflexpr register reinterpret_cast|10 requires return sizeof static_assert static_cast|10 struct switch synchronized template this thread_local throw transaction_safe transaction_safe_dynamic true try typedef typeid typename union using virtual volatile while xor xor_eq".split(" "), 
    literal:["NULL", "false", "nullopt", "nullptr", "true"], built_in:["_Pragma"], _type_hints:"any auto_ptr barrier binary_semaphore bitset complex condition_variable condition_variable_any counting_semaphore deque false_type future imaginary initializer_list istringstream jthread latch lock_guard multimap multiset mutex optional ostringstream packaged_task pair promise priority_queue queue recursive_mutex recursive_timed_mutex scoped_lock set shared_future shared_lock shared_mutex shared_timed_mutex shared_ptr stack string_view stringstream timed_mutex thread true_type tuple unique_lock unique_ptr unordered_map unordered_multimap unordered_multiset unordered_set variant vector weak_ptr wstring wstring_view".split(" ")};
    f = {className:"function.dispatch", relevance:0, keywords:{_hint:"abort abs acos apply as_const asin atan atan2 calloc ceil cerr cin clog cos cosh cout declval endl exchange exit exp fabs floor fmod forward fprintf fputs free frexp fscanf future invoke isalnum isalpha iscntrl isdigit isgraph islower isprint ispunct isspace isupper isxdigit labs launder ldexp log log10 make_pair make_shared make_shared_for_overwrite make_tuple make_unique malloc memchr memcmp memcpy memset modf move pow printf putchar puts realloc scanf sin sinh snprintf sprintf sqrt sscanf std stderr stdin stdout strcat strchr strcmp strcpy strcspn strlen strncat strncmp strncpy strpbrk strrchr strspn strstr swap tan tanh terminate to_underlying tolower toupper vfprintf visit vprintf vsprintf".split(" ")}, 
    begin:f.concat(/\b/, /(?!decltype)/, /(?!if)/, /(?!for)/, /(?!switch)/, /(?!while)/, b.IDENT_RE, f.lookahead(/(<[^<>]+>|)\s*\(/))};
    const E = [f, t, q, l, b.C_BLOCK_COMMENT_MODE, w, d], L = {variants:[{begin:/=/, end:/;/}, {begin:/\(/, end:/\)/}, {beginKeywords:"new throw return else", end:/;/}], keywords:C, contains:E.concat([{begin:/\(/, end:/\)/, keywords:C, contains:E.concat(["self"]), relevance:0}]), relevance:0};
    return {name:"C++", aliases:"cc c++ h++ hpp hh hxx cxx".split(" "), keywords:C, illegal:"</", classNameAliases:{"function.dispatch":"built_in"}, contains:[].concat(L, {className:"function", begin:"(" + n + "[\\*&\\s]+)+" + x, returnBegin:!0, end:/[{;=]/, excludeEnd:!0, keywords:C, illegal:/[^\w\s\*&:<>.]/, contains:[{begin:"decltype\\(auto\\)", keywords:C, relevance:0}, {begin:x, returnBegin:!0, contains:[v], relevance:0}, {begin:/::/, relevance:0}, {begin:/:/, endsWithParent:!0, contains:[d, 
    w]}, {relevance:0, match:/,/}, {className:"params", begin:/\(/, end:/\)/, keywords:C, relevance:0, contains:[l, b.C_BLOCK_COMMENT_MODE, d, w, q, {begin:/\(/, end:/\)/, keywords:C, relevance:0, contains:["self", l, b.C_BLOCK_COMMENT_MODE, d, w, q]}]}, q, l, b.C_BLOCK_COMMENT_MODE, t]}, f, E, [t, {begin:"\\b(deque|list|queue|priority_queue|pair|stack|vector|map|set|bitset|multiset|multimap|unordered_map|unordered_set|unordered_multiset|unordered_multimap|array|tuple|optional|variant|function)\\s*<(?!<)", 
    end:">", keywords:C, contains:["self", q]}, {begin:b.IDENT_RE + "::", keywords:C}, {match:[/\b(?:enum(?:\s+(?:class|struct))?|class|struct|union)/, /\s+/, /\w+/], className:{1:"keyword", 3:"title.class"}}])};
  })();
  hljs.registerLanguage("cpp", g);
})();
(() => {
  var g = (() => b => {
    const f = {keyword:"abstract as base break case catch class const continue do else event explicit extern finally fixed for foreach goto if implicit in interface internal is lock namespace new operator out override params private protected public readonly record ref return scoped sealed sizeof stackalloc static struct switch this throw try typeof unchecked unsafe using virtual void volatile while".split(" ").concat("add alias and ascending async await by descending equals from get global group init into join let nameof not notnull on or orderby partial remove select set unmanaged value|0 var when where with yield".split(" ")), 
    built_in:"bool byte char decimal delegate double dynamic enum float int long nint nuint object sbyte short string ulong uint ushort".split(" "), literal:["default", "false", "null", "true"]}, l = b.inherit(b.TITLE_MODE, {begin:"[a-zA-Z](\\.?\\w)*"}), n = {className:"number", variants:[{begin:"\\b(0b[01']+)"}, {begin:"(-?)\\b([\\d']+(\\.[\\d']*)?|\\.[\\d']+)(u|U|l|L|ul|UL|f|F|b|B)"}, {begin:"(-?)(\\b0[xX][a-fA-F0-9']+|(\\b[\\d']+(\\.[\\d']*)?|\\.[\\d']+)([eE][-+]?[\\d']+)?)"}], relevance:0};
    var q = {className:"string", begin:'@"', end:'"', contains:[{begin:'""'}]}, d = b.inherit(q, {illegal:/\n/}), w = {className:"subst", begin:/\{/, end:/\}/, keywords:f}, t = b.inherit(w, {illegal:/\n/});
    const v = {className:"string", begin:/\$"/, end:'"', illegal:/\n/, contains:[{begin:/\{\{/}, {begin:/\}\}/}, b.BACKSLASH_ESCAPE, t]}, x = {className:"string", begin:/\$@"/, end:'"', contains:[{begin:/\{\{/}, {begin:/\}\}/}, {begin:'""'}, w]}, C = b.inherit(x, {illegal:/\n/, contains:[{begin:/\{\{/}, {begin:/\}\}/}, {begin:'""'}, t]});
    w.contains = [x, v, q, b.APOS_STRING_MODE, b.QUOTE_STRING_MODE, n, b.C_BLOCK_COMMENT_MODE];
    t.contains = [C, v, d, b.APOS_STRING_MODE, b.QUOTE_STRING_MODE, n, b.inherit(b.C_BLOCK_COMMENT_MODE, {illegal:/\n/})];
    q = {variants:[x, v, q, b.APOS_STRING_MODE, b.QUOTE_STRING_MODE]};
    d = {begin:"<", end:">", contains:[{beginKeywords:"in out"}, l]};
    w = b.IDENT_RE + "(<" + b.IDENT_RE + "(\\s*,\\s*" + b.IDENT_RE + ")*>)?(\\[\\])?";
    t = {begin:"@" + b.IDENT_RE, relevance:0};
    return {name:"C#", aliases:["cs", "c#"], keywords:f, illegal:/::/, contains:[b.COMMENT("///", "$", {returnBegin:!0, contains:[{className:"doctag", variants:[{begin:"///", relevance:0}, {begin:"\x3c!--|--\x3e"}, {begin:"</?", end:">"}]}]}), b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE, {className:"meta", begin:"#", end:"$", keywords:{keyword:"if else elif endif define undef warning error line region endregion pragma checksum"}}, q, n, {beginKeywords:"class interface", relevance:0, end:/[{;=]/, 
    illegal:/[^\s:,]/, contains:[{beginKeywords:"where class"}, l, d, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, {beginKeywords:"namespace", relevance:0, end:/[{;=]/, illegal:/[^\s:]/, contains:[l, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, {beginKeywords:"record", relevance:0, end:/[{;=]/, illegal:/[^\s:]/, contains:[l, d, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, {className:"meta", begin:"^\\s*\\[(?=[\\w])", excludeBegin:!0, end:"\\]", excludeEnd:!0, contains:[{className:"string", 
    begin:/"/, end:/"/}]}, {beginKeywords:"new return throw await else", relevance:0}, {className:"function", begin:"(" + w + "\\s+)+" + b.IDENT_RE + "\\s*(<[^=]+>\\s*)?\\(", returnBegin:!0, end:/\s*[{;=]/, excludeEnd:!0, keywords:f, contains:[{beginKeywords:"public private protected static internal protected abstract async extern override unsafe virtual new sealed partial", relevance:0}, {begin:b.IDENT_RE + "\\s*(<[^=]+>\\s*)?\\(", returnBegin:!0, contains:[b.TITLE_MODE, d], relevance:0}, {match:/\(\)/}, 
    {className:"params", begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:f, relevance:0, contains:[q, n, b.C_BLOCK_COMMENT_MODE]}, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, t]};
  })();
  hljs.registerLanguage("csharp", g);
})();
(() => {
  var g = (() => {
    const b = "a abbr address article aside audio b blockquote body button canvas caption cite code dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 header hgroup html i iframe img input ins kbd label legend li main mark menu nav object ol p q quote samp section span strong summary sup table tbody td textarea tfoot th thead time tr ul var video defs g marker mask pattern svg switch symbol feBlend feColorMatrix feComponentTransfer feComposite feConvolveMatrix feDiffuseLighting feDisplacementMap feFlood feGaussianBlur feImage feMerge feMorphology feOffset feSpecularLighting feTile feTurbulence linearGradient radialGradient stop circle ellipse image line path polygon polyline rect text use textPath tspan foreignObject clipPath".split(" "), 
    f = "any-hover any-pointer aspect-ratio color color-gamut color-index device-aspect-ratio device-height device-width display-mode forced-colors grid height hover inverted-colors monochrome orientation overflow-block overflow-inline pointer prefers-color-scheme prefers-contrast prefers-reduced-motion prefers-reduced-transparency resolution scan scripting update width min-width max-width min-height max-height".split(" ").sort().reverse(), l = "active any-link blank checked current default defined dir disabled drop empty enabled first first-child first-of-type fullscreen future focus focus-visible focus-within has host host-context hover indeterminate in-range invalid is lang last-child last-of-type left link local-link not nth-child nth-col nth-last-child nth-last-col nth-last-of-type nth-of-type only-child only-of-type optional out-of-range past placeholder-shown read-only read-write required right root scope target target-within user-invalid valid visited where".split(" ").sort().reverse(), 
    n = "after backdrop before cue cue-region first-letter first-line grammar-error marker part placeholder selection slotted spelling-error".split(" ").sort().reverse(), q = "align-content align-items align-self alignment-baseline all animation animation-delay animation-direction animation-duration animation-fill-mode animation-iteration-count animation-name animation-play-state animation-timing-function backface-visibility background background-attachment background-blend-mode background-clip background-color background-image background-origin background-position background-repeat background-size baseline-shift block-size border border-block border-block-color border-block-end border-block-end-color border-block-end-style border-block-end-width border-block-start border-block-start-color border-block-start-style border-block-start-width border-block-style border-block-width border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-inline border-inline-color border-inline-end border-inline-end-color border-inline-end-style border-inline-end-width border-inline-start border-inline-start-color border-inline-start-style border-inline-start-width border-inline-style border-inline-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside cx cy caption-side caret-color clear clip clip-path clip-rule color color-interpolation color-interpolation-filters color-profile color-rendering column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns contain content content-visibility counter-increment counter-reset cue cue-after cue-before cursor direction display dominant-baseline empty-cells enable-background fill fill-opacity fill-rule filter flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float flow flood-color flood-opacity font font-display font-family font-feature-settings font-kerning font-language-override font-size font-size-adjust font-smoothing font-stretch font-style font-synthesis font-variant font-variant-caps font-variant-east-asian font-variant-ligatures font-variant-numeric font-variant-position font-variation-settings font-weight gap glyph-orientation-horizontal glyph-orientation-vertical grid grid-area grid-auto-columns grid-auto-flow grid-auto-rows grid-column grid-column-end grid-column-start grid-gap grid-row grid-row-end grid-row-start grid-template grid-template-areas grid-template-columns grid-template-rows hanging-punctuation height hyphens icon image-orientation image-rendering image-resolution ime-mode inline-size isolation kerning justify-content left letter-spacing lighting-color line-break line-height list-style list-style-image list-style-position list-style-type marker marker-end marker-mid marker-start mask margin margin-block margin-block-end margin-block-start margin-bottom margin-inline margin-inline-end margin-inline-start margin-left margin-right margin-top marks mask mask-border mask-border-mode mask-border-outset mask-border-repeat mask-border-slice mask-border-source mask-border-width mask-clip mask-composite mask-image mask-mode mask-origin mask-position mask-repeat mask-size mask-type max-block-size max-height max-inline-size max-width min-block-size min-height min-inline-size min-width mix-blend-mode nav-down nav-index nav-left nav-right nav-up none normal object-fit object-position opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-wrap overflow-x overflow-y padding padding-block padding-block-end padding-block-start padding-bottom padding-inline padding-inline-end padding-inline-start padding-left padding-right padding-top page-break-after page-break-before page-break-inside pause pause-after pause-before perspective perspective-origin pointer-events position quotes r resize rest rest-after rest-before right row-gap scroll-margin scroll-margin-block scroll-margin-block-end scroll-margin-block-start scroll-margin-bottom scroll-margin-inline scroll-margin-inline-end scroll-margin-inline-start scroll-margin-left scroll-margin-right scroll-margin-top scroll-padding scroll-padding-block scroll-padding-block-end scroll-padding-block-start scroll-padding-bottom scroll-padding-inline scroll-padding-inline-end scroll-padding-inline-start scroll-padding-left scroll-padding-right scroll-padding-top scroll-snap-align scroll-snap-stop scroll-snap-type scrollbar-color scrollbar-gutter scrollbar-width shape-image-threshold shape-margin shape-outside shape-rendering stop-color stop-opacity stroke stroke-dasharray stroke-dashoffset stroke-linecap stroke-linejoin stroke-miterlimit stroke-opacity stroke-width speak speak-as src tab-size table-layout text-anchor text-align text-align-all text-align-last text-combine-upright text-decoration text-decoration-color text-decoration-line text-decoration-style text-emphasis text-emphasis-color text-emphasis-position text-emphasis-style text-indent text-justify text-orientation text-overflow text-rendering text-shadow text-transform text-underline-position top transform transform-box transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vector-effect vertical-align visibility voice-balance voice-duration voice-family voice-pitch voice-range voice-rate voice-stress voice-volume white-space widows width will-change word-break word-spacing word-wrap writing-mode x y z-index".split(" ").sort().reverse();
    return d => {
      const w = d.regex;
      var t = d.C_BLOCK_COMMENT_MODE, v = {scope:"number", begin:d.NUMBER_RE + "(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?", relevance:0};
      const x = [d.APOS_STRING_MODE, d.QUOTE_STRING_MODE];
      return {name:"CSS", case_insensitive:!0, illegal:/[=|'\$]/, keywords:{keyframePosition:"from to"}, classNameAliases:{keyframePosition:"selector-tag"}, contains:[t, {begin:/-(webkit|moz|ms|o)-(?=[a-z])/}, v, {className:"selector-id", begin:/#[A-Za-z0-9_-]+/, relevance:0}, {className:"selector-class", begin:"\\.[a-zA-Z-][a-zA-Z0-9_-]*", relevance:0}, {scope:"selector-attr", begin:/\[/, end:/\]/, illegal:"$", contains:[d.APOS_STRING_MODE, d.QUOTE_STRING_MODE]}, {className:"selector-pseudo", variants:[{begin:":(" + 
      l.join("|") + ")"}, {begin:":(:)?(" + n.join("|") + ")"}]}, {className:"attr", begin:/--[A-Za-z_][A-Za-z0-9_-]*/}, {className:"attribute", begin:"\\b(" + q.join("|") + ")\\b"}, {begin:/:/, end:/[;}{]/, contains:[t, {scope:"number", begin:/#(([0-9a-fA-F]{3,4})|(([0-9a-fA-F]{2}){3,4}))\b/}, {scope:"meta", begin:"!important"}, v, ...x, {begin:/(url|data-uri)\(/, end:/\)/, relevance:0, keywords:{built_in:"url data-uri"}, contains:[...x, {className:"string", begin:/[^)]/, endsWithParent:!0, excludeEnd:!0}]}, 
      {className:"built_in", begin:/[\w-]+(?=\()/}]}, {begin:w.lookahead(/@/), end:"[{;]", relevance:0, illegal:/:/, contains:[{className:"keyword", begin:/@-?\w[\w]*(-\w+)*/}, {begin:/\s/, endsWithParent:!0, excludeEnd:!0, relevance:0, keywords:{$pattern:/[a-z-]+/, keyword:"and or not only", attribute:f.join(" ")}, contains:[{begin:/[a-z-]+(?=:)/, className:"attribute"}, ...x, v]}]}, {className:"selector-tag", begin:"\\b(" + b.join("|") + ")\\b"}]};
    };
  })();
  hljs.registerLanguage("css", g);
})();
(() => {
  var g = (() => b => {
    b = b.regex;
    return {name:"Diff", aliases:["patch"], contains:[{className:"meta", relevance:10, match:b.either(/^@@ +-\d+,\d+ +\+\d+,\d+ +@@/, /^\*\*\* +\d+,\d+ +\*\*\*\*$/, /^--- +\d+,\d+ +----$/)}, {className:"comment", variants:[{begin:b.either(/Index: /, /^index/, /={3,}/, /^-{3}/, /^\*{3} /, /^\+{3}/, /^diff --git/), end:/$/}, {match:/^\*{15}$/}]}, {className:"addition", begin:/^\+/, end:/$/}, {className:"deletion", begin:/^-/, end:/$/}, {className:"addition", begin:/^!/, end:/$/}]};
  })();
  hljs.registerLanguage("diff", g);
})();
(() => {
  var g = (() => b => {
    const f = {keyword:"after and andalso|10 band begin bnot bor bsl bzr bxor case catch cond div end fun if let not of orelse|10 query receive rem try when xor", literal:"false true"}, l = b.COMMENT("%", "$"), n = {className:"number", begin:"\\b(\\d+(_\\d+)*#[a-fA-F0-9]+(_[a-fA-F0-9]+)*|\\d+(_\\d+)*(\\.\\d+(_\\d+)*)?([eE][-+]?\\d+)?)", relevance:0};
    var q = {begin:"fun\\s+[a-z'][a-zA-Z0-9_']*/\\d+"}, d = {begin:"([a-z'][a-zA-Z0-9_']*:[a-z'][a-zA-Z0-9_']*|[a-z'][a-zA-Z0-9_']*)\\(", end:"\\)", returnBegin:!0, relevance:0, contains:[{begin:"([a-z'][a-zA-Z0-9_']*:[a-z'][a-zA-Z0-9_']*|[a-z'][a-zA-Z0-9_']*)", relevance:0}, {begin:"\\(", end:"\\)", endsWithParent:!0, returnEnd:!0, relevance:0}]};
    const w = {begin:/\{/, end:/\}/, relevance:0}, t = {begin:"\\b_([A-Z][A-Za-z0-9_]*)?", relevance:0}, v = {begin:"[A-Z][a-zA-Z0-9_]*", relevance:0}, x = {begin:"#" + b.UNDERSCORE_IDENT_RE, relevance:0, returnBegin:!0, contains:[{begin:"#" + b.UNDERSCORE_IDENT_RE, relevance:0}, {begin:/\{/, end:/\}/, relevance:0}]}, C = {beginKeywords:"fun receive if try case", end:"end", keywords:f};
    C.contains = [l, q, b.inherit(b.APOS_STRING_MODE, {className:""}), C, d, b.QUOTE_STRING_MODE, n, w, t, v, x];
    q = [l, q, C, d, b.QUOTE_STRING_MODE, n, w, t, v, x];
    d.contains[1].contains = q;
    w.contains = q;
    x.contains[1].contains = q;
    d = {className:"params", begin:"\\(", end:"\\)", contains:q};
    return {name:"Erlang", aliases:["erl"], keywords:f, illegal:"(</|\\*=|\\+=|-=|/\\*|\\*/|\\(\\*|\\*\\))", contains:[{className:"function", begin:"^[a-z'][a-zA-Z0-9_']*\\s*\\(", end:"->", returnBegin:!0, illegal:"\\(|#|//|/\\*|\\\\|:|;", contains:[d, b.inherit(b.TITLE_MODE, {begin:"[a-z'][a-zA-Z0-9_']*"})], starts:{end:";|\\.", keywords:f, contains:q}}, l, {begin:"^-", end:"\\.", relevance:0, excludeEnd:!0, returnBegin:!0, keywords:{$pattern:"-" + b.IDENT_RE, keyword:"-module -record -undef -export -ifdef -ifndef -author -copyright -doc -vsn -import -include -include_lib -compile -define -else -endif -file -behaviour -behavior -spec".split(" ").map(E => 
    E + "|1.5").join(" ")}, contains:[d]}, n, b.QUOTE_STRING_MODE, x, t, v, w, {begin:/\.$/}]};
  })();
  hljs.registerLanguage("erlang", g);
})();
(() => {
  var g = (() => b => {
    const f = {keyword:"break case chan const continue default defer else fallthrough for func go goto if import interface map package range return select struct switch type var".split(" "), type:"bool byte complex64 complex128 error float32 float64 int8 int16 int32 int64 string uint8 uint16 uint32 uint64 int uint uintptr rune".split(" "), literal:["true", "false", "iota", "nil"], built_in:"append cap close complex copy imag len make new panic print println real recover delete".split(" ")};
    return {name:"Go", aliases:["golang"], keywords:f, illegal:"</", contains:[b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE, {className:"string", variants:[b.QUOTE_STRING_MODE, b.APOS_STRING_MODE, {begin:"`", end:"`"}]}, {className:"number", variants:[{begin:b.C_NUMBER_RE + "[i]", relevance:1}, b.C_NUMBER_MODE]}, {begin:/:=/}, {className:"function", beginKeywords:"func", end:"\\s*(\\{|$)", excludeEnd:!0, contains:[b.TITLE_MODE, {className:"params", begin:/\(/, end:/\)/, endsParent:!0, keywords:f, 
    illegal:/["']/}]}]};
  })();
  hljs.registerLanguage("go", g);
})();
(() => {
  var g = (() => b => {
    const f = {variants:[b.COMMENT("--+", "$"), b.COMMENT(/\{-/, /-\}/, {contains:["self"]})]}, l = {className:"meta", begin:/\{-#/, end:/#-\}/}, n = {className:"meta", begin:"^#", end:"$"}, q = {className:"type", begin:"\\b[A-Z][\\w']*", relevance:0}, d = {begin:"\\(", end:"\\)", illegal:'"', contains:[l, n, {className:"type", begin:"\\b[A-Z][\\w]*(\\((\\.\\.|,|\\w+)\\))?"}, b.inherit(b.TITLE_MODE, {begin:"[_a-z][\\w']*"}), f]};
    return {name:"Haskell", aliases:["hs"], keywords:"let in if then else case of where do module import hiding qualified type data newtype deriving class instance as default infix infixl infixr foreign export ccall stdcall cplusplus jvm dotnet safe unsafe family forall mdo proc rec", unicodeRegex:!0, contains:[{beginKeywords:"module", end:"where", keywords:"module where", contains:[d, f], illegal:"\\W\\.|;"}, {begin:"\\bimport\\b", end:"$", keywords:"import qualified as hiding", contains:[d, f], 
    illegal:"\\W\\.|;"}, {className:"class", begin:"^(\\s*)?(class|instance)\\b", end:"where", keywords:"class family instance where", contains:[q, d, f]}, {className:"class", begin:"\\b(data|(new)?type)\\b", end:"$", keywords:"data family type newtype deriving", contains:[l, q, d, {begin:/\{/, end:/\}/, contains:d.contains}, f]}, {beginKeywords:"default", end:"$", contains:[q, d, f]}, {beginKeywords:"infix infixl infixr", end:"$", contains:[b.C_NUMBER_MODE, f]}, {begin:"\\bforeign\\b", end:"$", 
    keywords:"foreign import export ccall stdcall cplusplus jvm dotnet safe unsafe", contains:[q, b.QUOTE_STRING_MODE, f]}, {className:"meta", begin:"#!\\/usr\\/bin\\/env runhaskell", end:"$"}, l, n, {scope:"string", begin:/'(?=\\?.')/, end:/'/, contains:[{scope:"char.escape", match:/\\./}]}, b.QUOTE_STRING_MODE, {className:"number", relevance:0, variants:[{match:"\\b(([0-9]_*)+)(\\.(([0-9]_*)+))?([eE][+-]?(([0-9]_*)+))?\\b"}, {match:"\\b0[xX]_*(([0-9a-fA-F]_*)+)(\\.(([0-9a-fA-F]_*)+))?([pP][+-]?(([0-9]_*)+))?\\b"}, 
    {match:"\\b0[oO](([0-7]_*)+)\\b"}, {match:"\\b0[bB](([01]_*)+)\\b"}]}, q, b.inherit(b.TITLE_MODE, {begin:"^[_a-z][\\w']*"}), {begin:"(?!-)([!#$%&*+.\\/<=>?@\\\\^~-]|(?!([(),;\\[\\]`|{}]|[_:\"']))(\\p{S}|\\p{P}))--+|--+(?!-)([!#$%&*+.\\/<=>?@\\\\^~-]|(?!([(),;\\[\\]`|{}]|[_:\"']))(\\p{S}|\\p{P}))"}, f, {begin:"->|<-"}]};
  })();
  hljs.registerLanguage("haskell", g);
})();
(() => {
  var g = (() => b => {
    const f = {className:"attribute", begin:b.regex.concat("^", /[A-Za-z][A-Za-z0-9-]*/, "(?=\\:\\s)"), starts:{contains:[{className:"punctuation", begin:/: /, relevance:0, starts:{end:"$", relevance:0}}]}}, l = [f, {begin:"\\n\\n", starts:{subLanguage:[], endsWithParent:!0}}];
    return {name:"HTTP", aliases:["https"], illegal:/\S/, contains:[{begin:"^(?=HTTP/([32]|1\\.[01]) \\d{3})", end:/$/, contains:[{className:"meta", begin:"HTTP/([32]|1\\.[01])"}, {className:"number", begin:"\\b\\d{3}\\b"}], starts:{end:/\b\B/, illegal:/\S/, contains:l}}, {begin:"(?=^[A-Z]+ (.*?) HTTP/([32]|1\\.[01])$)", end:/$/, contains:[{className:"string", begin:" ", end:" ", excludeBegin:!0, excludeEnd:!0}, {className:"meta", begin:"HTTP/([32]|1\\.[01])"}, {className:"keyword", begin:"[A-Z]+"}], 
    starts:{end:/\b\B/, illegal:/\S/, contains:l}}, b.inherit(f, {relevance:0})]};
  })();
  hljs.registerLanguage("http", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex, l = {className:"number", relevance:0, variants:[{begin:/([+-]+)?[\d]+_[\d_]+/}, {begin:b.NUMBER_RE}]}, n = b.COMMENT();
    n.variants = [{begin:/;/, end:/$/}, {begin:/#/, end:/$/}];
    const q = {className:"variable", variants:[{begin:/\$[\w\d"][\w\d_]*/}, {begin:/\$\{(.*?)\}/}]}, d = {className:"literal", begin:/\bon|off|true|false|yes|no\b/};
    b = {className:"string", contains:[b.BACKSLASH_ESCAPE], variants:[{begin:"'''", end:"'''", relevance:10}, {begin:'"""', end:'"""', relevance:10}, {begin:'"', end:'"'}, {begin:"'", end:"'"}]};
    const w = {begin:/\[/, end:/\]/, contains:[n, d, q, b, l, "self"], relevance:0}, t = f.either(/[A-Za-z0-9_-]+/, /"(\\"|[^"])*"/, /'[^']*'/);
    return {name:"TOML, also INI", aliases:["toml"], case_insensitive:!0, illegal:/\S/, contains:[n, {className:"section", begin:/\[+/, end:/\]+/}, {begin:f.concat(t, "(\\s*\\.\\s*", t, ")*", f.lookahead(/\s*=\s*[^#\s]/)), className:"attr", starts:{end:/$/, contains:[n, w, d, q, b, l]}}]};
  })();
  hljs.registerLanguage("ini", g);
})();
(() => {
  var g = (() => {
    function b(l, n, q) {
      return -1 === q ? "" : l.replace(n, d => b(l, n, q - 1));
    }
    var f = {className:"number", variants:[{begin:"(\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))|\\.)?|(\\.([0-9](_*[0-9])*)))[eE][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))[fFdD]?\\b|\\.([fFdD]\\b)?)"}, {begin:"(\\.([0-9](_*[0-9])*))[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)[fFdD]\\b"}, {begin:"\\b0[xX](([0-9a-fA-F](_*[0-9a-fA-F])*)\\.?|([0-9a-fA-F](_*[0-9a-fA-F])*)?\\.([0-9a-fA-F](_*[0-9a-fA-F])*))[pP][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b(0|[1-9](_*[0-9])*)[lL]?\\b"}, 
    {begin:"\\b0[xX]([0-9a-fA-F](_*[0-9a-fA-F])*)[lL]?\\b"}, {begin:"\\b0(_*[0-7])*[lL]?\\b"}, {begin:"\\b0[bB][01](_*[01])*[lL]?\\b"}], relevance:0};
    return l => {
      const n = l.regex, q = "[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*" + b("(?:<[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*~~~(?:\\s*,\\s*[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*~~~)*>)?", /~~~/g, 2), d = {keyword:"synchronized;abstract;private;var;static;if;const ;for;while;strictfp;finally;protected;import;native;final;void;enum;else;break;transient;catch;instanceof;volatile;case;assert;package;default;public;try;switch;continue;throws;protected;public;private;module;requires;exports;do;sealed;yield;permits".split(";"), literal:["false", 
      "true", "null"], type:"char boolean long float int byte short double".split(" "), built_in:["super", "this"]}, w = {className:"meta", begin:"@[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*", contains:[{begin:/\(/, end:/\)/, contains:["self"]}]}, t = {className:"params", begin:/\(/, end:/\)/, keywords:d, relevance:0, contains:[l.C_BLOCK_COMMENT_MODE], endsParent:!0};
      return {name:"Java", aliases:["jsp"], keywords:d, illegal:/<\/|#/, contains:[l.COMMENT("/\\*\\*", "\\*/", {relevance:0, contains:[{begin:/\w+@/, relevance:0}, {className:"doctag", begin:"@[A-Za-z]+"}]}), {begin:/import java\.[a-z]+\./, keywords:"import", relevance:2}, l.C_LINE_COMMENT_MODE, l.C_BLOCK_COMMENT_MODE, {begin:/"""/, end:/"""/, className:"string", contains:[l.BACKSLASH_ESCAPE]}, l.APOS_STRING_MODE, l.QUOTE_STRING_MODE, {match:[/\b(?:class|interface|enum|extends|implements|new)/, 
      /\s+/, "[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*"], className:{1:"keyword", 3:"title.class"}}, {match:/non-sealed/, scope:"keyword"}, {begin:[n.concat(/(?!else)/, "[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*"), /\s+/, "[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*", /\s+/, /=(?!=)/], className:{1:"type", 3:"variable", 5:"operator"}}, {begin:[/record/, /\s+/, "[À-ʸa-zA-Z_$][À-ʸa-zA-Z_$0-9]*"], className:{1:"keyword", 3:"title.class"}, contains:[t, l.C_LINE_COMMENT_MODE, l.C_BLOCK_COMMENT_MODE]}, {beginKeywords:"new throw return else", 
      relevance:0}, {begin:["(?:" + q + "\\s+)", l.UNDERSCORE_IDENT_RE, /\s*(?=\()/], className:{2:"title.function"}, keywords:d, contains:[{className:"params", begin:/\(/, end:/\)/, keywords:d, relevance:0, contains:[w, l.APOS_STRING_MODE, l.QUOTE_STRING_MODE, f, l.C_BLOCK_COMMENT_MODE]}, l.C_LINE_COMMENT_MODE, l.C_BLOCK_COMMENT_MODE]}, f, w]};
    };
  })();
  hljs.registerLanguage("java", g);
})();
(() => {
  var g = (() => {
    const b = "as in of if for while finally var new function do return void else break catch instanceof with throw case default try switch continue typeof delete let yield const class debugger async await static import from export extends".split(" "), f = "true false null undefined NaN Infinity".split(" "), l = "Object Function Boolean Symbol Math Date Number BigInt String RegExp Array Float32Array Float64Array Int8Array Uint8Array Uint8ClampedArray Int16Array Int32Array Uint16Array Uint32Array BigInt64Array BigUint64Array Set Map WeakSet WeakMap ArrayBuffer SharedArrayBuffer Atomics DataView JSON Promise Generator GeneratorFunction AsyncFunction Reflect Proxy Intl WebAssembly".split(" "), 
    n = "Error EvalError InternalError RangeError ReferenceError SyntaxError TypeError URIError".split(" "), q = "setInterval setTimeout clearInterval clearTimeout require exports eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape".split(" "), d = "arguments this super console window document localStorage sessionStorage module global".split(" "), w = [].concat(q, l, n);
    return t => {
      const v = t.regex;
      var x = /<[A-Za-z0-9\\._:-]+/, C = /\/[A-Za-z0-9\\._:-]+>|\/>/;
      const E = {$pattern:"[A-Za-z$_][0-9A-Za-z$_]*", keyword:b, literal:f, built_in:w, "variable.language":d}, L = {className:"number", variants:[{begin:"(\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)((\\.([0-9](_?[0-9])*))|\\.)?|(\\.([0-9](_?[0-9])*)))[eE][+-]?([0-9](_?[0-9])*)\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)\\b((\\.([0-9](_?[0-9])*))\\b|\\.)?|(\\.([0-9](_?[0-9])*))\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*)n\\b"}, {begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*n?\\b"}, {begin:"\\b0[bB][0-1](_?[0-1])*n?\\b"}, 
      {begin:"\\b0[oO][0-7](_?[0-7])*n?\\b"}, {begin:"\\b0[0-7]+n?\\b"}], relevance:0};
      var I = {className:"subst", begin:"\\$\\{", end:"\\}", keywords:E, contains:[]};
      const M = {begin:"html`", end:"", starts:{end:"`", returnEnd:!1, contains:[t.BACKSLASH_ESCAPE, I], subLanguage:"xml"}}, T = {begin:"css`", end:"", starts:{end:"`", returnEnd:!1, contains:[t.BACKSLASH_ESCAPE, I], subLanguage:"css"}}, a = {begin:"gql`", end:"", starts:{end:"`", returnEnd:!1, contains:[t.BACKSLASH_ESCAPE, I], subLanguage:"graphql"}}, e = {className:"string", begin:"`", end:"`", contains:[t.BACKSLASH_ESCAPE, I]}, c = {className:"comment", variants:[t.COMMENT(/\/\*\*(?!\/)/, "\\*/", 
      {relevance:0, contains:[{begin:"(?=@[A-Za-z]+)", relevance:0, contains:[{className:"doctag", begin:"@[A-Za-z]+"}, {className:"type", begin:"\\{", end:"\\}", excludeEnd:!0, excludeBegin:!0, relevance:0}, {className:"variable", begin:"[A-Za-z$_][0-9A-Za-z$_]*(?=\\s*(-)|$)", endsParent:!0, relevance:0}, {begin:/(?=[^\n])\s/, relevance:0}]}]}), t.C_BLOCK_COMMENT_MODE, t.C_LINE_COMMENT_MODE]};
      var k = [t.APOS_STRING_MODE, t.QUOTE_STRING_MODE, M, T, a, e, {match:/\$\d+/}, L];
      I.contains = k.concat({begin:/\{/, end:/\}/, keywords:E, contains:["self"].concat(k)});
      I = [].concat(c, I.contains);
      I = I.concat([{begin:/\(/, end:/\)/, keywords:E, contains:["self"].concat(I)}]);
      k = {className:"params", begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:E, contains:I};
      const h = {variants:[{match:[/class/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /\s+/, /extends/, /\s+/, v.concat("[A-Za-z$_][0-9A-Za-z$_]*", "(", v.concat(/\./, "[A-Za-z$_][0-9A-Za-z$_]*"), ")*")], scope:{1:"keyword", 3:"title.class", 5:"keyword", 7:"title.class.inherited"}}, {match:[/class/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*"], scope:{1:"keyword", 3:"title.class"}}]}, r = {relevance:0, match:v.either(/\bJSON/, /\b[A-Z][a-z]+([A-Z][a-z]*|\d)*/, /\b[A-Z]{2,}([A-Z][a-z]+|\d)+([A-Z][a-z]*)*/, /\b[A-Z]{2,}[a-z]+([A-Z][a-z]+|\d)*([A-Z][a-z]*)*/), 
      className:"title.class", keywords:{_:[...l, ...n]}}, m = {variants:[{match:[/function/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /(?=\s*\()/]}, {match:[/function/, /\s*(?=\()/]}], className:{1:"keyword", 3:"title.function"}, label:"func.def", contains:[k], illegal:/%/}, y = {match:v.concat(/\b/, (A = [...q, "super", "import"], v.concat("(?!", A.join("|"), ")")), "[A-Za-z$_][0-9A-Za-z$_]*", v.lookahead(/\(/)), className:"title.function", relevance:0};
      var A;
      A = {begin:v.concat(/\./, v.lookahead(v.concat("[A-Za-z$_][0-9A-Za-z$_]*", /(?![0-9A-Za-z$_(])/))), end:"[A-Za-z$_][0-9A-Za-z$_]*", excludeBegin:!0, keywords:"prototype", className:"property", relevance:0};
      const J = {match:[/get|set/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /(?=\()/], className:{1:"keyword", 3:"title.function"}, contains:[{begin:/\(\)/}, k]}, O = "(\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)|" + t.UNDERSCORE_IDENT_RE + ")\\s*=>", u = {match:[/const|var|let/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /\s*/, /=\s*/, /(async\s*)?/, v.lookahead(O)], keywords:"async", className:{1:"keyword", 3:"title.function"}, contains:[k]};
      return {name:"JavaScript", aliases:["js", "jsx", "mjs", "cjs"], keywords:E, exports:{PARAMS_CONTAINS:I, CLASS_REFERENCE:r}, illegal:/#(?![$_A-z])/, contains:[t.SHEBANG({label:"shebang", binary:"node", relevance:5}), {label:"use_strict", className:"meta", relevance:10, begin:/^\s*['"]use (strict|asm)['"]/}, t.APOS_STRING_MODE, t.QUOTE_STRING_MODE, M, T, a, e, c, {match:/\$\d+/}, L, r, {className:"attr", begin:"[A-Za-z$_][0-9A-Za-z$_]*" + v.lookahead(":"), relevance:0}, u, {begin:"(" + t.RE_STARTERS_RE + 
      "|\\b(case|return|throw)\\b)\\s*", keywords:"return throw case", relevance:0, contains:[c, t.REGEXP_MODE, {className:"function", begin:O, returnBegin:!0, end:"\\s*=>", contains:[{className:"params", variants:[{begin:t.UNDERSCORE_IDENT_RE, relevance:0}, {className:null, begin:/\(\s*\)/, skip:!0}, {begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:E, contains:I}]}]}, {begin:/,/, relevance:0}, {match:/\s+/, relevance:0}, {variants:[{begin:"<>", end:"</>"}, {match:/<[A-Za-z0-9\\._:-]+\s*\/>/}, 
      {begin:x, "on:begin":(D, K) => {
        const G = D[0].length + D.index, P = D.input[G];
        if ("<" === P || "," === P) {
          return void K.ignoreMatch();
        }
        let R;
        ">" === P && (((S, {after:aa}) => {
          const ha = "</" + S[0].slice(1);
          return -1 !== S.input.indexOf(ha, aa);
        })(D, {after:G}) || K.ignoreMatch());
        D = D.input.substring(G);
        ((R = D.match(/^\s*=/)) || (R = D.match(/^\s+extends\s+/)) && 0 === R.index) && K.ignoreMatch();
      }, end:C}], subLanguage:"xml", contains:[{begin:x, end:C, skip:!0, contains:["self"]}]}]}, m, {beginKeywords:"while if switch catch for"}, {begin:"\\b(?!function)" + t.UNDERSCORE_IDENT_RE + "\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)\\s*\\{", returnBegin:!0, label:"func.def", contains:[k, t.inherit(t.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*", className:"title.function"})]}, {match:/\.\.\./, relevance:0}, A, {match:"\\$[A-Za-z$_][0-9A-Za-z$_]*", relevance:0}, {match:[/\bconstructor(?=\s*\()/], 
      className:{1:"title.function"}, contains:[k]}, y, {relevance:0, match:/\b[A-Z][A-Z_0-9]+\b/, className:"variable.constant"}, h, J, {match:/\$[(.]/}]};
    };
  })();
  hljs.registerLanguage("javascript", g);
})();
(() => {
  var g = (() => b => {
    const f = ["true", "false", "null"], l = {scope:"literal", beginKeywords:f.join(" ")};
    return {name:"JSON", keywords:{literal:f}, contains:[{className:"attr", begin:/"(\\.|[^\\"\r\n])*"(?=\s*:)/, relevance:1.01}, {match:/[{}[\],:]/, className:"punctuation", relevance:0}, b.QUOTE_STRING_MODE, l, b.C_NUMBER_MODE, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE], illegal:"\\S"};
  })();
  hljs.registerLanguage("json", g);
})();
(() => {
  var g = (() => {
    var b = {className:"number", variants:[{begin:"(\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))|\\.)?|(\\.([0-9](_*[0-9])*)))[eE][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)((\\.([0-9](_*[0-9])*))[fFdD]?\\b|\\.([fFdD]\\b)?)"}, {begin:"(\\.([0-9](_*[0-9])*))[fFdD]?\\b"}, {begin:"\\b([0-9](_*[0-9])*)[fFdD]\\b"}, {begin:"\\b0[xX](([0-9a-fA-F](_*[0-9a-fA-F])*)\\.?|([0-9a-fA-F](_*[0-9a-fA-F])*)?\\.([0-9a-fA-F](_*[0-9a-fA-F])*))[pP][+-]?([0-9](_*[0-9])*)[fFdD]?\\b"}, {begin:"\\b(0|[1-9](_*[0-9])*)[lL]?\\b"}, 
    {begin:"\\b0[xX]([0-9a-fA-F](_*[0-9a-fA-F])*)[lL]?\\b"}, {begin:"\\b0(_*[0-7])*[lL]?\\b"}, {begin:"\\b0[bB][01](_*[01])*[lL]?\\b"}], relevance:0};
    return f => {
      const l = {keyword:"abstract as val var vararg get set class object open private protected public noinline crossinline dynamic final enum if else do while for when throw try catch finally import package is in fun override companion reified inline lateinit init interface annotation data sealed internal infix operator out by constructor super tailrec where const inner suspend typealias external expect actual", built_in:"Byte Short Char Int Long Boolean Float Double Void Unit Nothing", literal:"true false null"}, 
      n = {className:"symbol", begin:f.UNDERSCORE_IDENT_RE + "@"};
      var q = {className:"subst", begin:/\$\{/, end:/\}/, contains:[f.C_NUMBER_MODE]}, d = {className:"variable", begin:"\\$" + f.UNDERSCORE_IDENT_RE};
      d = {className:"string", variants:[{begin:'"""', end:'"""(?=[^"])', contains:[d, q]}, {begin:"'", end:"'", illegal:/\n/, contains:[f.BACKSLASH_ESCAPE]}, {begin:'"', end:'"', illegal:/\n/, contains:[f.BACKSLASH_ESCAPE, d, q]}]};
      q.contains.push(d);
      q = {className:"meta", begin:"@(?:file|property|field|get|set|receiver|param|setparam|delegate)\\s*:(?:\\s*" + f.UNDERSCORE_IDENT_RE + ")?"};
      const w = {className:"meta", begin:"@" + f.UNDERSCORE_IDENT_RE, contains:[{begin:/\(/, end:/\)/, contains:[f.inherit(d, {className:"string"}), "self"]}]}, t = f.COMMENT("/\\*", "\\*/", {contains:[f.C_BLOCK_COMMENT_MODE]}), v = {variants:[{className:"type", begin:f.UNDERSCORE_IDENT_RE}, {begin:/\(/, end:/\)/, contains:[]}]};
      return v.variants[1].contains = [v], v.variants[1].contains = [v], {name:"Kotlin", aliases:["kt", "kts"], keywords:l, contains:[f.COMMENT("/\\*\\*", "\\*/", {relevance:0, contains:[{className:"doctag", begin:"@[A-Za-z]+"}]}), f.C_LINE_COMMENT_MODE, t, {className:"keyword", begin:/\b(break|continue|return|this)\b/, starts:{contains:[{className:"symbol", begin:/@\w+/}]}}, n, q, w, {className:"function", beginKeywords:"fun", end:"[(]|$", returnBegin:!0, excludeEnd:!0, keywords:l, relevance:5, 
      contains:[{begin:f.UNDERSCORE_IDENT_RE + "\\s*\\(", returnBegin:!0, relevance:0, contains:[f.UNDERSCORE_TITLE_MODE]}, {className:"type", begin:/</, end:/>/, keywords:"reified", relevance:0}, {className:"params", begin:/\(/, end:/\)/, endsParent:!0, keywords:l, relevance:0, contains:[{begin:/:/, end:/[=,\/]/, endsWithParent:!0, contains:[v, f.C_LINE_COMMENT_MODE, t], relevance:0}, f.C_LINE_COMMENT_MODE, t, q, w, d, f.C_NUMBER_MODE]}, t]}, {begin:[/class|interface|trait/, /\s+/, f.UNDERSCORE_IDENT_RE], 
      beginScope:{3:"title.class"}, keywords:"class interface trait", end:/[:\{(]|$/, excludeEnd:!0, illegal:"extends implements", contains:[{beginKeywords:"public protected internal private constructor"}, f.UNDERSCORE_TITLE_MODE, {className:"type", begin:/</, end:/>/, excludeBegin:!0, excludeEnd:!0, relevance:0}, {className:"type", begin:/[,:]\s*/, end:/[<\(,){\s]|$/, excludeBegin:!0, returnEnd:!0}, q, w]}, d, {className:"meta", begin:"^#!/usr/bin/env", end:"$", illegal:"\n"}, b]};
    };
  })();
  hljs.registerLanguage("kotlin", g);
})();
(() => {
  var g = (() => {
    const b = "a abbr address article aside audio b blockquote body button canvas caption cite code dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 header hgroup html i iframe img input ins kbd label legend li main mark menu nav object ol p q quote samp section span strong summary sup table tbody td textarea tfoot th thead time tr ul var video defs g marker mask pattern svg switch symbol feBlend feColorMatrix feComponentTransfer feComposite feConvolveMatrix feDiffuseLighting feDisplacementMap feFlood feGaussianBlur feImage feMerge feMorphology feOffset feSpecularLighting feTile feTurbulence linearGradient radialGradient stop circle ellipse image line path polygon polyline rect text use textPath tspan foreignObject clipPath".split(" "), 
    f = "any-hover any-pointer aspect-ratio color color-gamut color-index device-aspect-ratio device-height device-width display-mode forced-colors grid height hover inverted-colors monochrome orientation overflow-block overflow-inline pointer prefers-color-scheme prefers-contrast prefers-reduced-motion prefers-reduced-transparency resolution scan scripting update width min-width max-width min-height max-height".split(" ").sort().reverse(), l = "active any-link blank checked current default defined dir disabled drop empty enabled first first-child first-of-type fullscreen future focus focus-visible focus-within has host host-context hover indeterminate in-range invalid is lang last-child last-of-type left link local-link not nth-child nth-col nth-last-child nth-last-col nth-last-of-type nth-of-type only-child only-of-type optional out-of-range past placeholder-shown read-only read-write required right root scope target target-within user-invalid valid visited where".split(" ").sort().reverse(), 
    n = "after backdrop before cue cue-region first-letter first-line grammar-error marker part placeholder selection slotted spelling-error".split(" ").sort().reverse(), q = "align-content align-items align-self alignment-baseline all animation animation-delay animation-direction animation-duration animation-fill-mode animation-iteration-count animation-name animation-play-state animation-timing-function backface-visibility background background-attachment background-blend-mode background-clip background-color background-image background-origin background-position background-repeat background-size baseline-shift block-size border border-block border-block-color border-block-end border-block-end-color border-block-end-style border-block-end-width border-block-start border-block-start-color border-block-start-style border-block-start-width border-block-style border-block-width border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-inline border-inline-color border-inline-end border-inline-end-color border-inline-end-style border-inline-end-width border-inline-start border-inline-start-color border-inline-start-style border-inline-start-width border-inline-style border-inline-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside cx cy caption-side caret-color clear clip clip-path clip-rule color color-interpolation color-interpolation-filters color-profile color-rendering column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns contain content content-visibility counter-increment counter-reset cue cue-after cue-before cursor direction display dominant-baseline empty-cells enable-background fill fill-opacity fill-rule filter flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float flow flood-color flood-opacity font font-display font-family font-feature-settings font-kerning font-language-override font-size font-size-adjust font-smoothing font-stretch font-style font-synthesis font-variant font-variant-caps font-variant-east-asian font-variant-ligatures font-variant-numeric font-variant-position font-variation-settings font-weight gap glyph-orientation-horizontal glyph-orientation-vertical grid grid-area grid-auto-columns grid-auto-flow grid-auto-rows grid-column grid-column-end grid-column-start grid-gap grid-row grid-row-end grid-row-start grid-template grid-template-areas grid-template-columns grid-template-rows hanging-punctuation height hyphens icon image-orientation image-rendering image-resolution ime-mode inline-size isolation kerning justify-content left letter-spacing lighting-color line-break line-height list-style list-style-image list-style-position list-style-type marker marker-end marker-mid marker-start mask margin margin-block margin-block-end margin-block-start margin-bottom margin-inline margin-inline-end margin-inline-start margin-left margin-right margin-top marks mask mask-border mask-border-mode mask-border-outset mask-border-repeat mask-border-slice mask-border-source mask-border-width mask-clip mask-composite mask-image mask-mode mask-origin mask-position mask-repeat mask-size mask-type max-block-size max-height max-inline-size max-width min-block-size min-height min-inline-size min-width mix-blend-mode nav-down nav-index nav-left nav-right nav-up none normal object-fit object-position opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-wrap overflow-x overflow-y padding padding-block padding-block-end padding-block-start padding-bottom padding-inline padding-inline-end padding-inline-start padding-left padding-right padding-top page-break-after page-break-before page-break-inside pause pause-after pause-before perspective perspective-origin pointer-events position quotes r resize rest rest-after rest-before right row-gap scroll-margin scroll-margin-block scroll-margin-block-end scroll-margin-block-start scroll-margin-bottom scroll-margin-inline scroll-margin-inline-end scroll-margin-inline-start scroll-margin-left scroll-margin-right scroll-margin-top scroll-padding scroll-padding-block scroll-padding-block-end scroll-padding-block-start scroll-padding-bottom scroll-padding-inline scroll-padding-inline-end scroll-padding-inline-start scroll-padding-left scroll-padding-right scroll-padding-top scroll-snap-align scroll-snap-stop scroll-snap-type scrollbar-color scrollbar-gutter scrollbar-width shape-image-threshold shape-margin shape-outside shape-rendering stop-color stop-opacity stroke stroke-dasharray stroke-dashoffset stroke-linecap stroke-linejoin stroke-miterlimit stroke-opacity stroke-width speak speak-as src tab-size table-layout text-anchor text-align text-align-all text-align-last text-combine-upright text-decoration text-decoration-color text-decoration-line text-decoration-style text-emphasis text-emphasis-color text-emphasis-position text-emphasis-style text-indent text-justify text-orientation text-overflow text-rendering text-shadow text-transform text-underline-position top transform transform-box transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vector-effect vertical-align visibility voice-balance voice-duration voice-family voice-pitch voice-range voice-rate voice-stress voice-volume white-space widows width will-change word-break word-spacing word-wrap writing-mode x y z-index".split(" ").sort().reverse(), 
    d = l.concat(n).sort().reverse();
    return w => {
      var t = {className:"built_in", begin:/[\w-]+(?=\()/}, v = {scope:"selector-attr", begin:/\[/, end:/\]/, illegal:"$", contains:[w.APOS_STRING_MODE, w.QUOTE_STRING_MODE]}, x = {scope:"number", begin:w.NUMBER_RE + "(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?", relevance:0};
      const C = [];
      var E = [];
      const L = (e, c, k) => ({className:e, begin:c, relevance:k});
      var I = {$pattern:/[a-z-]+/, keyword:"and or not only", attribute:f.join(" ")};
      E.push(w.C_LINE_COMMENT_MODE, w.C_BLOCK_COMMENT_MODE, {className:"string", begin:"~?'.*?'"}, {className:"string", begin:'~?".*?"'}, x, {begin:"(url|data-uri)\\(", starts:{className:"string", end:"[\\)\\n]", excludeEnd:!0}}, {scope:"number", begin:/#(([0-9a-fA-F]{3,4})|(([0-9a-fA-F]{2}){3,4}))\b/}, {begin:"\\(", end:"\\)", contains:E, keywords:I, relevance:0}, L("variable", "@@?[\\w-]+", 10), L("variable", "@\\{[\\w-]+\\}"), L("built_in", "~?`[^`]*?`"), {className:"attribute", begin:"[\\w-]+\\s*:", 
      end:":", returnBegin:!0, excludeEnd:!0}, {scope:"meta", begin:"!important"}, {beginKeywords:"and not"}, t);
      const M = E.concat({begin:/\{/, end:/\}/, contains:C}), T = {beginKeywords:"when", endsWithParent:!0, contains:[{beginKeywords:"and not"}].concat(E)}, a = {begin:"([\\w-]+|@\\{[\\w-]+\\})\\s*:", returnBegin:!0, end:/[;}]/, relevance:0, contains:[{begin:/-(webkit|moz|ms|o)-/}, {className:"attr", begin:/--[A-Za-z_][A-Za-z0-9_-]*/}, {className:"attribute", begin:"\\b(" + q.join("|") + ")\\b", end:/(?=:)/, starts:{endsWithParent:!0, illegal:"[<=$]", relevance:0, contains:E}}]};
      E = {className:"keyword", begin:"@(import|media|charset|font-face|(-[a-z]+-)?keyframes|supports|document|namespace|page|viewport|host)\\b", starts:{end:"[;{}]", keywords:I, returnEnd:!0, contains:E, relevance:0}};
      I = {className:"variable", variants:[{begin:"@[\\w-]+\\s*:", relevance:15}, {begin:"@[\\w-]+"}], starts:{end:"[;}]", returnEnd:!0, contains:M}};
      v = {variants:[{begin:"[\\.#:&\\[>]", end:"[;{}]"}, {begin:"([\\w-]+|@\\{[\\w-]+\\})", end:/\{/}], returnBegin:!0, returnEnd:!0, illegal:"[<='$\"]", relevance:0, contains:[w.C_LINE_COMMENT_MODE, w.C_BLOCK_COMMENT_MODE, T, L("keyword", "all\\b"), L("variable", "@\\{[\\w-]+\\}"), {begin:"\\b(" + b.join("|") + ")\\b", className:"selector-tag"}, x, L("selector-tag", "([\\w-]+|@\\{[\\w-]+\\})", 0), L("selector-id", "#([\\w-]+|@\\{[\\w-]+\\})"), L("selector-class", "\\.([\\w-]+|@\\{[\\w-]+\\})", 
      0), L("selector-tag", "&", 0), v, {className:"selector-pseudo", begin:":(" + l.join("|") + ")"}, {className:"selector-pseudo", begin:":(:)?(" + n.join("|") + ")"}, {begin:/\(/, end:/\)/, relevance:0, contains:M}, {begin:"!important"}, t]};
      x = {begin:"[\\w-]+:(:)?" + `(${d.join("|")})`, returnBegin:!0, contains:[v]};
      return C.push(w.C_LINE_COMMENT_MODE, w.C_BLOCK_COMMENT_MODE, E, I, x, a, v, T, t), {name:"Less", case_insensitive:!0, illegal:"[=>'/<($\"]", contains:C};
    };
  })();
  hljs.registerLanguage("less", g);
})();
(() => {
  var g = (() => b => {
    const f = {className:"literal", begin:"\\b(t{1}|nil)\\b"}, l = {className:"number", variants:[{begin:"(-|\\+)?\\d+(\\.\\d+|\\/\\d+)?((d|e|f|l|s|D|E|F|L|S)(\\+|-)?\\d+)?", relevance:0}, {begin:"#(b|B)[0-1]+(/[0-1]+)?"}, {begin:"#(o|O)[0-7]+(/[0-7]+)?"}, {begin:"#(x|X)[0-9a-fA-F]+(/[0-9a-fA-F]+)?"}, {begin:"#(c|C)\\((-|\\+)?\\d+(\\.\\d+|\\/\\d+)?((d|e|f|l|s|D|E|F|L|S)(\\+|-)?\\d+)? +(-|\\+)?\\d+(\\.\\d+|\\/\\d+)?((d|e|f|l|s|D|E|F|L|S)(\\+|-)?\\d+)?", end:"\\)"}]}, n = b.inherit(b.QUOTE_STRING_MODE, 
    {illegal:null}), q = b.COMMENT(";", "$", {relevance:0}), d = {begin:"\\*", end:"\\*"}, w = {className:"symbol", begin:"[:&][a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*"}, t = {begin:"[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*", relevance:0}, v = {contains:[l, n, d, w, {begin:"\\(", end:"\\)", contains:["self", f, n, l, t]}, t], variants:[{begin:"['`]\\(", end:"\\)"}, {begin:"\\(quote ", end:"\\)", keywords:{name:"quote"}}, {begin:"'\\|[^]*?\\|"}]}, x = {variants:[{begin:"'[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*"}, 
    {begin:"#'[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*(::[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*)*"}]}, C = {begin:"\\(\\s*", end:"\\)"}, E = {endsWithParent:!0, relevance:0};
    return C.contains = [{className:"name", variants:[{begin:"[a-zA-Z_\\-+\\*\\/<=>&#][a-zA-Z0-9_\\-+*\\/<=>&#!]*", relevance:0}, {begin:"\\|[^]*?\\|"}]}, E], E.contains = [v, x, C, f, l, n, q, d, w, {begin:"\\|[^]*?\\|"}, t], {name:"Lisp", illegal:/\S/, contains:[l, b.SHEBANG(), f, n, q, v, x, C, t]};
  })();
  hljs.registerLanguage("lisp", g);
})();
(() => {
  var g = (() => b => {
    const f = {begin:"\\[=*\\[", end:"\\]=*\\]", contains:["self"]}, l = [b.COMMENT("--(?!\\[=*\\[)", "$"), b.COMMENT("--\\[=*\\[", "\\]=*\\]", {contains:[f], relevance:10})];
    return {name:"Lua", keywords:{$pattern:b.UNDERSCORE_IDENT_RE, literal:"true false nil", keyword:"and break do else elseif end for goto if in local not or repeat return then until while", built_in:"_G _ENV _VERSION __index __newindex __mode __call __metatable __tostring __len __gc __add __sub __mul __div __mod __pow __concat __unm __eq __lt __le assert collectgarbage dofile error getfenv getmetatable ipairs load loadfile loadstring module next pairs pcall print rawequal rawget rawset require select setfenv setmetatable tonumber tostring type unpack xpcall arg self coroutine resume yield status wrap create running debug getupvalue debug sethook getmetatable gethook setmetatable setlocal traceback setfenv getinfo setupvalue getlocal getregistry getfenv io lines write close flush open output type read stderr stdin input stdout popen tmpfile math log max acos huge ldexp pi cos tanh pow deg tan cosh sinh random randomseed frexp ceil floor rad abs sqrt modf asin min mod fmod log10 atan2 exp sin atan os exit setlocale date getenv difftime remove time clock tmpname rename execute package preload loadlib loaded loaders cpath config path seeall string sub upper len gfind rep find match char dump gmatch reverse byte format gsub lower table setn insert getn foreachi maxn foreach concat sort remove"}, 
    contains:l.concat([{className:"function", beginKeywords:"function", end:"\\)", contains:[b.inherit(b.TITLE_MODE, {begin:"([_a-zA-Z]\\w*\\.)*([_a-zA-Z]\\w*:)?[_a-zA-Z]\\w*"}), {className:"params", begin:"\\(", endsWithParent:!0, contains:l}].concat(l)}, b.C_NUMBER_MODE, b.APOS_STRING_MODE, b.QUOTE_STRING_MODE, {className:"string", begin:"\\[=*\\[", end:"\\]=*\\]", contains:[f], relevance:5}])};
  })();
  hljs.registerLanguage("lua", g);
})();
(() => {
  var g = (() => b => {
    const f = {className:"variable", variants:[{begin:"\\$\\(" + b.UNDERSCORE_IDENT_RE + "\\)", contains:[b.BACKSLASH_ESCAPE]}, {begin:/\$[@%<?\^\+\*]/}]};
    return {name:"Makefile", aliases:["mk", "mak", "make"], keywords:{$pattern:/[\w-]+/, keyword:"define endef undefine ifdef ifndef ifeq ifneq else endif include -include sinclude override export unexport private vpath"}, contains:[b.HASH_COMMENT_MODE, f, {className:"string", begin:/"/, end:/"/, contains:[b.BACKSLASH_ESCAPE, f]}, {className:"variable", begin:/\$\([\w-]+\s/, end:/\)/, keywords:{built_in:"subst patsubst strip findstring filter filter-out sort word wordlist firstword lastword dir notdir suffix basename addsuffix addprefix join wildcard realpath abspath error warning shell origin flavor foreach if or and call eval file value"}, 
    contains:[f]}, {begin:"^" + b.UNDERSCORE_IDENT_RE + "\\s*(?=[:+?]?=)"}, {className:"meta", begin:/^\.PHONY:/, end:/$/, keywords:{$pattern:/[\.\w]+/, keyword:".PHONY"}}, {className:"section", begin:/^[^\s]+:/, end:/$/, contains:[f]}]};
  })();
  hljs.registerLanguage("makefile", g);
})();
(() => {
  var g = (() => b => {
    const f = {begin:/<\/?[A-Za-z_]/, end:">", subLanguage:"xml", relevance:0}, l = {variants:[{begin:/\[.+?\]\[.*?\]/, relevance:0}, {begin:/\[.+?\]\(((data|javascript|mailto):|(?:http|ftp)s?:\/\/).*?\)/, relevance:2}, {begin:b.regex.concat(/\[.+?\]\(/, /[A-Za-z][A-Za-z0-9+.-]*/, /:\/\/.*?\)/), relevance:2}, {begin:/\[.+?\]\([./?&#].*?\)/, relevance:1}, {begin:/\[.*?\]\(.*?\)/, relevance:0}], returnBegin:!0, contains:[{match:/\[(?=\])/}, {className:"string", relevance:0, begin:"\\[", end:"\\]", 
    excludeBegin:!0, returnEnd:!0}, {className:"link", relevance:0, begin:"\\]\\(", end:"\\)", excludeBegin:!0, excludeEnd:!0}, {className:"symbol", relevance:0, begin:"\\]\\[", end:"\\]", excludeBegin:!0, excludeEnd:!0}]}, n = {className:"strong", contains:[], variants:[{begin:/_{2}(?!\s)/, end:/_{2}/}, {begin:/\*{2}(?!\s)/, end:/\*{2}/}]}, q = {className:"emphasis", contains:[], variants:[{begin:/\*(?![*\s])/, end:/\*/}, {begin:/_(?![_\s])/, end:/_/, relevance:0}]}, d = b.inherit(n, {contains:[]});
    b = b.inherit(q, {contains:[]});
    n.contains.push(b);
    q.contains.push(d);
    let w = [f, l];
    return [n, q, d, b].forEach(t => {
      t.contains = t.contains.concat(w);
    }), w = w.concat(n, q), {name:"Markdown", aliases:["md", "mkdown", "mkd"], contains:[{className:"section", variants:[{begin:"^#{1,6}", end:"$", contains:w}, {begin:"(?=^.+?\\n[=-]{2,}$)", contains:[{begin:"^[=-]*$"}, {begin:"^", end:"\\n", contains:w}]}]}, f, {className:"bullet", begin:"^[ \t]*([*+-]|(\\d+\\.))(?=\\s+)", end:"\\s+", excludeEnd:!0}, n, q, {className:"quote", begin:"^>\\s+", contains:w, end:"$"}, {className:"code", variants:[{begin:"(`{3,})[^`](.|\\n)*?\\1`*[ ]*"}, {begin:"(~{3,})[^~](.|\\n)*?\\1~*[ ]*"}, 
    {begin:"```", end:"```+[ ]*$"}, {begin:"~~~", end:"~~~+[ ]*$"}, {begin:"`.+?`"}, {begin:"(?=^( {4}|\\t))", contains:[{begin:"^( {4}|\\t)", end:"(\\n)$"}], relevance:0}]}, {begin:"^[-\\*]{3,}", end:"$"}, l, {begin:/^\[[^\n]+\]:/, returnBegin:!0, contains:[{className:"symbol", begin:/\[/, end:/\]/, excludeBegin:!0, excludeEnd:!0}, {className:"link", begin:/:\s*/, end:/$/, excludeBegin:!0}]}]};
  })();
  hljs.registerLanguage("markdown", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex;
    var l = {className:"variable", variants:[{begin:/\$\d+/}, {begin:/\$\{\w+\}/}, {begin:f.concat(/[$@]/, b.UNDERSCORE_IDENT_RE)}]};
    l = {endsWithParent:!0, keywords:{$pattern:/[a-z_]{2,}|\/dev\/poll/, literal:"on off yes no true false none blocked debug info notice warn error crit select break last permanent redirect kqueue rtsig epoll poll /dev/poll".split(" ")}, relevance:0, illegal:"=>", contains:[b.HASH_COMMENT_MODE, {className:"string", contains:[b.BACKSLASH_ESCAPE, l], variants:[{begin:/"/, end:/"/}, {begin:/'/, end:/'/}]}, {begin:"([a-z]+):/", end:"\\s", endsWithParent:!0, excludeEnd:!0, contains:[l]}, {className:"regexp", 
    contains:[b.BACKSLASH_ESCAPE, l], variants:[{begin:"\\s\\^", end:"\\s|\\{|;", returnEnd:!0}, {begin:"~\\*?\\s+", end:"\\s|\\{|;", returnEnd:!0}, {begin:"\\*(\\.[a-z\\-]+)+"}, {begin:"([a-z\\-]+\\.)+\\*"}]}, {className:"number", begin:"\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}(:\\d{1,5})?\\b"}, {className:"number", begin:"\\b\\d+[kKmMgGdshdwy]?\\b", relevance:0}, l]};
    return {name:"Nginx config", aliases:["nginxconf"], contains:[b.HASH_COMMENT_MODE, {beginKeywords:"upstream location", end:/;|\{/, contains:l.contains, keywords:{section:"upstream location"}}, {className:"section", begin:f.concat(b.UNDERSCORE_IDENT_RE + f.lookahead(/\s+\{/)), relevance:0}, {begin:f.lookahead(b.UNDERSCORE_IDENT_RE + "\\s"), end:";|\\{", contains:[{className:"attribute", begin:b.UNDERSCORE_IDENT_RE, starts:l}], relevance:0}], illegal:"[^\\s\\}\\{]"};
  })();
  hljs.registerLanguage("nginx", g);
})();
(() => {
  var g = (() => b => {
    const f = /[a-zA-Z@][a-zA-Z0-9_]*/, l = {$pattern:f, keyword:["@interface", "@class", "@protocol", "@implementation"]};
    return {name:"Objective-C", aliases:["mm", "objc", "obj-c", "obj-c++", "objective-c++"], keywords:{"variable.language":["this", "super"], $pattern:f, keyword:"while export sizeof typedef const struct for union volatile static mutable if do return goto enum else break extern asm case default register explicit typename switch continue inline readonly assign readwrite self @synchronized id typeof nonatomic IBOutlet IBAction strong weak copy in out inout bycopy byref oneway __strong __weak __block __autoreleasing @private @protected @public @try @property @end @throw @catch @finally @autoreleasepool @synthesize @dynamic @selector @optional @required @encode @package @import @defs @compatibility_alias __bridge __bridge_transfer __bridge_retained __bridge_retain __covariant __contravariant __kindof _Nonnull _Nullable _Null_unspecified __FUNCTION__ __PRETTY_FUNCTION__ __attribute__ getter setter retain unsafe_unretained nonnull nullable null_unspecified null_resettable class instancetype NS_DESIGNATED_INITIALIZER NS_UNAVAILABLE NS_REQUIRES_SUPER NS_RETURNS_INNER_POINTER NS_INLINE NS_AVAILABLE NS_DEPRECATED NS_ENUM NS_OPTIONS NS_SWIFT_UNAVAILABLE NS_ASSUME_NONNULL_BEGIN NS_ASSUME_NONNULL_END NS_REFINED_FOR_SWIFT NS_SWIFT_NAME NS_SWIFT_NOTHROW NS_DURING NS_HANDLER NS_ENDHANDLER NS_VALUERETURN NS_VOIDRETURN".split(" "), 
    literal:"false true FALSE TRUE nil YES NO NULL".split(" "), built_in:["dispatch_once_t", "dispatch_queue_t", "dispatch_sync", "dispatch_async", "dispatch_once"], type:"int float char unsigned signed short long double wchar_t unichar void bool BOOL id|0 _Bool".split(" ")}, illegal:"</", contains:[{className:"built_in", begin:"\\b(AV|CA|CF|CG|CI|CL|CM|CN|CT|MK|MP|MTK|MTL|NS|SCN|SK|UI|WK|XC)\\w+"}, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE, b.C_NUMBER_MODE, b.QUOTE_STRING_MODE, b.APOS_STRING_MODE, 
    {className:"string", variants:[{begin:'@"', end:'"', illegal:"\\n", contains:[b.BACKSLASH_ESCAPE]}]}, {className:"meta", begin:/#\s*[a-z]+\b/, end:/$/, keywords:{keyword:"if else elif endif define undef warning error line pragma ifdef ifndef include"}, contains:[{begin:/\\\n/, relevance:0}, b.inherit(b.QUOTE_STRING_MODE, {className:"string"}), {className:"string", begin:/<.*?>/, end:/$/, illegal:"\\n"}, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, {className:"class", begin:"(" + l.keyword.join("|") + 
    ")\\b", end:/(\{|$)/, excludeEnd:!0, keywords:l, contains:[b.UNDERSCORE_TITLE_MODE]}, {begin:"\\." + b.UNDERSCORE_IDENT_RE, relevance:0}]};
  })();
  hljs.registerLanguage("objectivec", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex, l = /[dualxmsipngr]{0,12}/, n = {$pattern:/[\w.]+/, keyword:"abs accept alarm and atan2 bind binmode bless break caller chdir chmod chomp chop chown chr chroot class close closedir connect continue cos crypt dbmclose dbmopen defined delete die do dump each else elsif endgrent endhostent endnetent endprotoent endpwent endservent eof eval exec exists exit exp fcntl field fileno flock for foreach fork format formline getc getgrent getgrgid getgrnam gethostbyaddr gethostbyname gethostent getlogin getnetbyaddr getnetbyname getnetent getpeername getpgrp getpriority getprotobyname getprotobynumber getprotoent getpwent getpwnam getpwuid getservbyname getservbyport getservent getsockname getsockopt given glob gmtime goto grep gt hex if index int ioctl join keys kill last lc lcfirst length link listen local localtime log lstat lt ma map method mkdir msgctl msgget msgrcv msgsnd my ne next no not oct open opendir or ord our pack package pipe pop pos print printf prototype push q|0 qq quotemeta qw qx rand read readdir readline readlink readpipe recv redo ref rename require reset return reverse rewinddir rindex rmdir say scalar seek seekdir select semctl semget semop send setgrent sethostent setnetent setpgrp setpriority setprotoent setpwent setservent setsockopt shift shmctl shmget shmread shmwrite shutdown sin sleep socket socketpair sort splice split sprintf sqrt srand stat state study sub substr symlink syscall sysopen sysread sysseek system syswrite tell telldir tie tied time times tr truncate uc ucfirst umask undef unless unlink unpack unshift untie until use utime values vec wait waitpid wantarray warn when while write x|0 xor y|0"}, 
    q = {className:"subst", begin:"[$@]\\{", end:"\\}", keywords:n}, d = {begin:/->\{/, end:/\}/}, w = {scope:"attr", match:/\s+:\s*\w+(\s*\(.*?\))?/}, t = {scope:"variable", variants:[{begin:/\$\d/}, {begin:f.concat(/[$%@](\^\w\b|#\w+(::\w+)*|\{\w+\}|\w+(::\w*)*)/, "(?![A-Za-z])(?![@$%])")}, {begin:/[$%@][^\s\w{=]|\$=/, relevance:0}], contains:[w]}, v = {className:"number", variants:[{match:/0?\.[0-9][0-9_]+\b/}, {match:/\bv?(0|[1-9][0-9_]*(\.[0-9_]+)?|[1-9][0-9_]*)\b/}, {match:/\b0[0-7][0-7_]*\b/}, 
    {match:/\b0x[0-9a-fA-F][0-9a-fA-F_]*\b/}, {match:/\b0b[0-1][0-1_]*\b/}], relevance:0}, x = [b.BACKSLASH_ESCAPE, q, t], C = [/!/, /\//, /\|/, /\?/, /'/, /"/, /#/], E = (I, M, T = "\\1") => {
      const a = "\\1" === T ? T : f.concat(T, M);
      return f.concat(f.concat("(?:", I, ")"), M, /(?:\\.|[^\\\/])*?/, a, /(?:\\.|[^\\\/])*?/, T, l);
    }, L = (I, M, T) => f.concat(f.concat("(?:", I, ")"), M, /(?:\\.|[^\\\/])*?/, T, l);
    b = [t, b.HASH_COMMENT_MODE, b.COMMENT(/^=\w/, /=cut/, {endsWithParent:!0}), d, {className:"string", contains:x, variants:[{begin:"q[qwxr]?\\s*\\(", end:"\\)", relevance:5}, {begin:"q[qwxr]?\\s*\\[", end:"\\]", relevance:5}, {begin:"q[qwxr]?\\s*\\{", end:"\\}", relevance:5}, {begin:"q[qwxr]?\\s*\\|", end:"\\|", relevance:5}, {begin:"q[qwxr]?\\s*<", end:">", relevance:5}, {begin:"qw\\s+q", end:"q", relevance:5}, {begin:"'", end:"'", contains:[b.BACKSLASH_ESCAPE]}, {begin:'"', end:'"'}, {begin:"`", 
    end:"`", contains:[b.BACKSLASH_ESCAPE]}, {begin:/\{\w+\}/, relevance:0}, {begin:"-?\\w+\\s*=>", relevance:0}]}, v, {begin:"(\\/\\/|" + b.RE_STARTERS_RE + "|\\b(split|return|print|reverse|grep)\\b)\\s*", keywords:"split return print reverse grep", relevance:0, contains:[b.HASH_COMMENT_MODE, {className:"regexp", variants:[{begin:E("s|tr|y", f.either(...C, {capture:!0}))}, {begin:E("s|tr|y", "\\(", "\\)")}, {begin:E("s|tr|y", "\\[", "\\]")}, {begin:E("s|tr|y", "\\{", "\\}")}], relevance:2}, {className:"regexp", 
    variants:[{begin:/(m|qr)\/\//, relevance:0}, {begin:L("(?:m|qr)?", /\//, /\//)}, {begin:L("m|qr", f.either(...C, {capture:!0}), /\1/)}, {begin:L("m|qr", /\(/, /\)/)}, {begin:L("m|qr", /\[/, /\]/)}, {begin:L("m|qr", /\{/, /\}/)}]}]}, {className:"function", beginKeywords:"sub method", end:"(\\s*\\(.*?\\))?[;{]", excludeEnd:!0, relevance:5, contains:[b.TITLE_MODE, w]}, {className:"class", beginKeywords:"class", end:"[;{]", excludeEnd:!0, relevance:5, contains:[b.TITLE_MODE, w, v]}, {begin:"-\\w\\b", 
    relevance:0}, {begin:"^__DATA__$", end:"^__END__$", subLanguage:"mojolicious", contains:[{begin:"^@@.*", end:"$", className:"comment"}]}];
    return q.contains = b, d.contains = b, {name:"Perl", aliases:["pl", "pm"], keywords:n, contains:b};
  })();
  hljs.registerLanguage("perl", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex;
    var l = /(?![A-Za-z0-9])(?![$])/;
    const n = f.concat(/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/, l);
    l = f.concat(/(\\?[A-Z][a-z0-9_\x7f-\xff]+|\\?[A-Z]+(?=[A-Z][a-z0-9_\x7f-\xff])){1,}/, l);
    const q = {scope:"variable", match:"\\$+" + n};
    var d = {scope:"subst", variants:[{begin:/\$\w+/}, {begin:/\{\$/, end:/\}/}]}, w = b.inherit(b.APOS_STRING_MODE, {illegal:null});
    d = {scope:"string", variants:[b.inherit(b.QUOTE_STRING_MODE, {illegal:null, contains:b.QUOTE_STRING_MODE.contains.concat(d)}), w, {begin:/<<<[ \t]*(?:(\w+)|"(\w+)")\n/, end:/[ \t]*(\w+)\b/, contains:b.QUOTE_STRING_MODE.contains.concat(d), "on:begin":(a, e) => {
      e.data._beginMatch = a[1] || a[2];
    }, "on:end":(a, e) => {
      e.data._beginMatch !== a[1] && e.ignoreMatch();
    }}, b.END_SAME_AS_BEGIN({begin:/<<<[ \t]*'(\w+)'\n/, end:/[ \t]*(\w+)\b/})]};
    w = {scope:"number", variants:[{begin:"\\b0[bB][01]+(?:_[01]+)*\\b"}, {begin:"\\b0[oO][0-7]+(?:_[0-7]+)*\\b"}, {begin:"\\b0[xX][\\da-fA-F]+(?:_[\\da-fA-F]+)*\\b"}, {begin:"(?:\\b\\d+(?:_\\d+)*(\\.(?:\\d+(?:_\\d+)*))?|\\B\\.\\d+)(?:[eE][+-]?\\d+)?"}], relevance:0};
    const t = ["false", "null", "true"];
    var v = "__CLASS__ __DIR__ __FILE__ __FUNCTION__ __COMPILER_HALT_OFFSET__ __LINE__ __METHOD__ __NAMESPACE__ __TRAIT__ die echo exit include include_once print require require_once array abstract and as binary bool boolean break callable case catch class clone const continue declare default do double else elseif empty enddeclare endfor endforeach endif endswitch endwhile enum eval extends final finally float for foreach from global goto if implements instanceof insteadof int integer interface isset iterable list match|0 mixed new never object or private protected public readonly real return string switch throw trait try unset use var void while xor yield".split(" "), 
    x = "Error|0 AppendIterator ArgumentCountError ArithmeticError ArrayIterator ArrayObject AssertionError BadFunctionCallException BadMethodCallException CachingIterator CallbackFilterIterator CompileError Countable DirectoryIterator DivisionByZeroError DomainException EmptyIterator ErrorException Exception FilesystemIterator FilterIterator GlobIterator InfiniteIterator InvalidArgumentException IteratorIterator LengthException LimitIterator LogicException MultipleIterator NoRewindIterator OutOfBoundsException OutOfRangeException OuterIterator OverflowException ParentIterator ParseError RangeException RecursiveArrayIterator RecursiveCachingIterator RecursiveCallbackFilterIterator RecursiveDirectoryIterator RecursiveFilterIterator RecursiveIterator RecursiveIteratorIterator RecursiveRegexIterator RecursiveTreeIterator RegexIterator RuntimeException SeekableIterator SplDoublyLinkedList SplFileInfo SplFileObject SplFixedArray SplHeap SplMaxHeap SplMinHeap SplObjectStorage SplObserver SplPriorityQueue SplQueue SplStack SplSubject SplTempFileObject TypeError UnderflowException UnexpectedValueException UnhandledMatchError ArrayAccess BackedEnum Closure Fiber Generator Iterator IteratorAggregate Serializable Stringable Throwable Traversable UnitEnum WeakReference WeakMap Directory __PHP_Incomplete_Class parent php_user_filter self static stdClass".split(" ");
    const C = {keyword:v, literal:(a => {
      const e = [];
      return a.forEach(c => {
        e.push(c);
        c.toLowerCase() === c ? e.push(c.toUpperCase()) : e.push(c.toLowerCase());
      }), e;
    })(t), built_in:x}, E = a => a.map(e => e.replace(/\|\d+$/, "")), L = {variants:[{match:[/new/, f.concat("[ \t\n]", "+"), f.concat("(?!", E(x).join("\\b|"), "\\b)"), l], scope:{1:"keyword", 4:"title.class"}}]};
    var I = f.concat(n, "\\b(?!\\()");
    I = {variants:[{match:[f.concat(/::/, f.lookahead(/(?!class\b)/)), I], scope:{2:"variable.constant"}}, {match:[/::/, /class/], scope:{2:"variable.language"}}, {match:[l, f.concat(/::/, f.lookahead(/(?!class\b)/)), I], scope:{1:"title.class", 3:"variable.constant"}}, {match:[l, f.concat("::", f.lookahead(/(?!class\b)/))], scope:{1:"title.class"}}, {match:[l, /::/, /class/], scope:{1:"title.class", 3:"variable.language"}}]};
    const M = {scope:"attr", match:f.concat(n, f.lookahead(":"), f.lookahead(/(?!::)/))}, T = {relevance:0, begin:/\(/, end:/\)/, keywords:C, contains:[M, q, I, b.C_BLOCK_COMMENT_MODE, d, w, L]};
    v = {relevance:0, match:[/\b/, f.concat("(?!fn\\b|function\\b|", E(v).join("\\b|"), "|", E(x).join("\\b|"), "\\b)"), n, f.concat("[ \t\n]", "*"), f.lookahead(/(?=\()/)], scope:{3:"title.function.invoke"}, contains:[T]};
    T.contains.push(v);
    x = [M, I, b.C_BLOCK_COMMENT_MODE, d, w, L];
    return {case_insensitive:!1, keywords:C, contains:[{begin:f.concat(/#\[\s*/, l), beginScope:"meta", end:/]/, endScope:"meta", keywords:{literal:t, keyword:["new", "array"]}, contains:[{begin:/\[/, end:/]/, keywords:{literal:t, keyword:["new", "array"]}, contains:["self", ...x]}, ...x, {scope:"meta", match:l}]}, b.HASH_COMMENT_MODE, b.COMMENT("//", "$"), b.COMMENT("/\\*", "\\*/", {contains:[{scope:"doctag", match:"@[A-Za-z]+"}]}), {match:/__halt_compiler\(\);/, keywords:"__halt_compiler", starts:{scope:"comment", 
    end:b.MATCH_NOTHING_RE, contains:[{match:/\?>/, scope:"meta", endsParent:!0}]}}, {scope:"meta", variants:[{begin:/<\?php/, relevance:10}, {begin:/<\?=/}, {begin:/<\?/, relevance:.1}, {begin:/\?>/}]}, {scope:"variable.language", match:/\$this\b/}, q, v, I, {match:[/const/, /\s/, n], scope:{1:"keyword", 3:"variable.constant"}}, L, {scope:"function", relevance:0, beginKeywords:"fn function", end:/[;{]/, excludeEnd:!0, illegal:"[$%\\[]", contains:[{beginKeywords:"use"}, b.UNDERSCORE_TITLE_MODE, {begin:"=>", 
    endsParent:!0}, {scope:"params", begin:"\\(", end:"\\)", excludeBegin:!0, excludeEnd:!0, keywords:C, contains:["self", q, I, b.C_BLOCK_COMMENT_MODE, d, w]}]}, {scope:"class", variants:[{beginKeywords:"enum", illegal:/[($"]/}, {beginKeywords:"class interface trait", illegal:/[:($"]/}], relevance:0, end:/\{/, excludeEnd:!0, contains:[{beginKeywords:"extends implements"}, b.UNDERSCORE_TITLE_MODE]}, {beginKeywords:"namespace", relevance:0, end:";", illegal:/[.']/, contains:[b.inherit(b.UNDERSCORE_TITLE_MODE, 
    {scope:"title.class"})]}, {beginKeywords:"use", relevance:0, end:";", contains:[{match:/\b(as|const|function)\b/, scope:"keyword"}, b.UNDERSCORE_TITLE_MODE]}, d, w]};
  })();
  hljs.registerLanguage("php", g);
})();
(() => {
  var g = (() => b => ({name:"PHP template", subLanguage:"xml", contains:[{begin:/<\?(php|=)?/, end:/\?>/, subLanguage:"php", contains:[{begin:"/\\*", end:"\\*/", skip:!0}, {begin:'b"', end:'"', skip:!0}, {begin:"b'", end:"'", skip:!0}, b.inherit(b.APOS_STRING_MODE, {illegal:null, className:null, contains:null, skip:!0}), b.inherit(b.QUOTE_STRING_MODE, {illegal:null, className:null, contains:null, skip:!0})]}]}))();
  hljs.registerLanguage("php-template", g);
})();
(() => {
  var g = (() => b => ({name:"Plain text", aliases:["text", "txt"], disableAutodetect:!0}))();
  hljs.registerLanguage("plaintext", g);
})();
(() => {
  var g = (() => b => {
    const f = {$pattern:/-?[A-z\.\-]+\b/, keyword:"if else foreach return do while until elseif begin for trap data dynamicparam end break throw param continue finally in switch exit filter try process catch hidden static parameter", built_in:"ac asnp cat cd CFS chdir clc clear clhy cli clp cls clv cnsn compare copy cp cpi cpp curl cvpa dbp del diff dir dnsn ebp echo|0 epal epcsv epsn erase etsn exsn fc fhx fl ft fw gal gbp gc gcb gci gcm gcs gdr gerr ghy gi gin gjb gl gm gmo gp gps gpv group gsn gsnp gsv gtz gu gv gwmi h history icm iex ihy ii ipal ipcsv ipmo ipsn irm ise iwmi iwr kill lp ls man md measure mi mount move mp mv nal ndr ni nmo npssc nsn nv ogv oh popd ps pushd pwd r rbp rcjb rcsn rd rdr ren ri rjb rm rmdir rmo rni rnp rp rsn rsnp rujb rv rvpa rwmi sajb sal saps sasv sbp sc scb select set shcm si sl sleep sls sort sp spjb spps spsv start stz sujb sv swmi tee trcm type wget where wjb write"};
    var l = {begin:"`[\\s\\S]", relevance:0};
    const n = {className:"variable", variants:[{begin:/\$\B/}, {className:"keyword", begin:/\$this/}, {begin:/\$[\w\d][\w\d_:]*/}]}, q = {className:"string", variants:[{begin:/"/, end:/"/}, {begin:/@"/, end:/^"@/}], contains:[l, n, {className:"variable", begin:/\$[A-z]/, end:/[^A-z]/}]}, d = {className:"string", variants:[{begin:/'/, end:/'/}, {begin:/@'/, end:/^'@/}]}, w = b.inherit(b.COMMENT(null, null), {variants:[{begin:/#/, end:/$/}, {begin:/<#/, end:/#>/}], contains:[{className:"doctag", variants:[{begin:/\.(synopsis|description|example|inputs|outputs|notes|link|component|role|functionality)/}, 
    {begin:/\.(parameter|forwardhelptargetname|forwardhelpcategory|remotehelprunspace|externalhelp)\s+\S+/}]}]}), t = {className:"class", beginKeywords:"class enum", end:/\s*[{]/, excludeEnd:!0, relevance:0, contains:[b.TITLE_MODE]}, v = {className:"function", begin:/function\s+/, end:/\s*\{|$/, excludeEnd:!0, returnBegin:!0, relevance:0, contains:[{begin:"function", relevance:0, className:"keyword"}, {className:"title", begin:/\w[\w\d]*((-)[\w\d]+)*/, relevance:0}, {begin:/\(/, end:/\)/, className:"params", 
    relevance:0, contains:[n]}]}, x = {begin:/using\s/, end:/$/, returnBegin:!0, contains:[q, d, {className:"keyword", begin:/(using|assembly|command|module|namespace|type)/}]}, C = {className:"function", begin:/\[.*\]\s*[\w]+[ ]??\(/, end:/$/, returnBegin:!0, relevance:0, contains:[{className:"keyword", begin:"(".concat(f.keyword.toString().replace(/\s/g, "|"), ")\\b"), endsParent:!0, relevance:0}, b.inherit(b.TITLE_MODE, {endsParent:!0})]};
    b = [C, w, l, b.NUMBER_MODE, q, d, {className:"built_in", variants:[{begin:"(Add|Clear|Close|Copy|Enter|Exit|Find|Format|Get|Hide|Join|Lock|Move|New|Open|Optimize|Pop|Push|Redo|Remove|Rename|Reset|Resize|Search|Select|Set|Show|Skip|Split|Step|Switch|Undo|Unlock|Watch|Backup|Checkpoint|Compare|Compress|Convert|ConvertFrom|ConvertTo|Dismount|Edit|Expand|Export|Group|Import|Initialize|Limit|Merge|Mount|Out|Publish|Restore|Save|Sync|Unpublish|Update|Approve|Assert|Build|Complete|Confirm|Deny|Deploy|Disable|Enable|Install|Invoke|Register|Request|Restart|Resume|Start|Stop|Submit|Suspend|Uninstall|Unregister|Wait|Debug|Measure|Ping|Repair|Resolve|Test|Trace|Connect|Disconnect|Read|Receive|Send|Write|Block|Grant|Protect|Revoke|Unblock|Unprotect|Use|ForEach|Sort|Tee|Where)+(-)[\\w\\d]+"}]}, 
    n, {className:"literal", begin:/\$(null|true|false)\b/}, {className:"selector-tag", begin:/@\B/, relevance:0}];
    l = {begin:/\[/, end:/\]/, excludeBegin:!0, excludeEnd:!0, relevance:0, contains:[].concat("self", b, {begin:"(string|char|byte|int|long|bool|decimal|single|double|DateTime|xml|array|hashtable|void)", className:"built_in", relevance:0}, {className:"type", begin:/[\.\w\d]+/, relevance:0})};
    return C.contains.unshift(l), {name:"PowerShell", aliases:["pwsh", "ps", "ps1"], case_insensitive:!0, keywords:f, contains:b.concat(t, v, x, {variants:[{className:"operator", begin:"(-and|-as|-band|-bnot|-bor|-bxor|-casesensitive|-ccontains|-ceq|-cge|-cgt|-cle|-clike|-clt|-cmatch|-cne|-cnotcontains|-cnotlike|-cnotmatch|-contains|-creplace|-csplit|-eq|-exact|-f|-file|-ge|-gt|-icontains|-ieq|-ige|-igt|-ile|-ilike|-ilt|-imatch|-in|-ine|-inotcontains|-inotlike|-inotmatch|-ireplace|-is|-isnot|-isplit|-join|-le|-like|-lt|-match|-ne|-not|-notcontains|-notin|-notlike|-notmatch|-or|-regex|-replace|-shl|-shr|-split|-wildcard|-xor)\\b"}, 
    {className:"literal", begin:/(-){1,2}[\w\d-]+/, relevance:0}]}, l)};
  })();
  hljs.registerLanguage("powershell", g);
})();
(() => {
  var g = (() => b => ({name:".properties", disableAutodetect:!0, case_insensitive:!0, illegal:/\S/, contains:[b.COMMENT("^\\s*[!#]", "$"), {returnBegin:!0, variants:[{begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+[ \\t\\f]*[:=][ \\t\\f]*"}, {begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+[ \\t\\f]+"}], contains:[{className:"attr", begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+", endsParent:!0}], starts:{end:"([ \\t\\f]*[:=][ \\t\\f]*|[ \\t\\f]+)", relevance:0, starts:{className:"string", end:/$/, relevance:0, contains:[{begin:"\\\\\\\\"}, 
  {begin:"\\\\\\n"}]}}}, {className:"attr", begin:"([^\\\\:= \\t\\f\\n]|\\\\.)+[ \\t\\f]*$"}]}))();
  hljs.registerLanguage("properties", g);
})();
(() => {
  var g = (() => b => {
    var f = b.regex;
    const l = /[\p{XID_Start}_]\p{XID_Continue}*/u;
    var n = "and as assert async await break case class continue def del elif else except finally for from global if import in is lambda match nonlocal|10 not or pass raise return try while with yield".split(" ");
    const q = {$pattern:/[A-Za-z]\w+|__\w+__/, keyword:n, built_in:"__import__ abs all any ascii bin bool breakpoint bytearray bytes callable chr classmethod compile complex delattr dict dir divmod enumerate eval exec filter float format frozenset getattr globals hasattr hash help hex id input int isinstance issubclass iter len list locals map max memoryview min next object oct open ord pow print property range repr reversed round set setattr slice sorted staticmethod str sum super tuple type vars zip".split(" "), 
    literal:"__debug__ Ellipsis False None NotImplemented True".split(" "), type:"Any Callable Coroutine Dict List Literal Generic Optional Sequence Set Tuple Type Union".split(" ")}, d = {className:"meta", begin:/^(>>>|\.\.\.) /}, w = {className:"subst", begin:/\{/, end:/\}/, keywords:q, illegal:/#/};
    var t = {begin:/\{\{/, relevance:0};
    t = {className:"string", contains:[b.BACKSLASH_ESCAPE], variants:[{begin:/([uU]|[bB]|[rR]|[bB][rR]|[rR][bB])?'''/, end:/'''/, contains:[b.BACKSLASH_ESCAPE, d], relevance:10}, {begin:/([uU]|[bB]|[rR]|[bB][rR]|[rR][bB])?"""/, end:/"""/, contains:[b.BACKSLASH_ESCAPE, d], relevance:10}, {begin:/([fF][rR]|[rR][fF]|[fF])'''/, end:/'''/, contains:[b.BACKSLASH_ESCAPE, d, t, w]}, {begin:/([fF][rR]|[rR][fF]|[fF])"""/, end:/"""/, contains:[b.BACKSLASH_ESCAPE, d, t, w]}, {begin:/([uU]|[rR])'/, end:/'/, relevance:10}, 
    {begin:/([uU]|[rR])"/, end:/"/, relevance:10}, {begin:/([bB]|[bB][rR]|[rR][bB])'/, end:/'/}, {begin:/([bB]|[bB][rR]|[rR][bB])"/, end:/"/}, {begin:/([fF][rR]|[rR][fF]|[fF])'/, end:/'/, contains:[b.BACKSLASH_ESCAPE, t, w]}, {begin:/([fF][rR]|[rR][fF]|[fF])"/, end:/"/, contains:[b.BACKSLASH_ESCAPE, t, w]}, b.APOS_STRING_MODE, b.QUOTE_STRING_MODE]};
    n = "\\b|" + n.join("|");
    n = {className:"number", relevance:0, variants:[{begin:`(\\b(${"[0-9](_?[0-9])*"})|(${"(\\b([0-9](_?[0-9])*))?\\.([0-9](_?[0-9])*)|\\b([0-9](_?[0-9])*)\\."}))[eE][+-]?(${"[0-9](_?[0-9])*"})[jJ]?(?=${n})`}, {begin:"((\\b([0-9](_?[0-9])*))?\\.([0-9](_?[0-9])*)|\\b([0-9](_?[0-9])*)\\.)[jJ]?"}, {begin:`\\b([1-9](_?[0-9])*|0+(_?0)*)[lLjJ]?(?=${n})`}, {begin:`\\b0[bB](_?[01])+[lL]?(?=${n})`}, {begin:`\\b0[oO](_?[0-7])+[lL]?(?=${n})`}, {begin:`\\b0[xX](_?[0-9a-fA-F])+[lL]?(?=${n})`}, {begin:`\\b(${"[0-9](_?[0-9])*"})[jJ](?=${n})`}]};
    f = {className:"comment", begin:f.lookahead(/# type:/), end:/$/, keywords:q, contains:[{begin:/# type:/}, {begin:/#/, end:/\b\B/, endsWithParent:!0}]};
    const v = {className:"params", variants:[{className:"", begin:/\(\s*\)/, skip:!0}, {begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:q, contains:["self", d, n, t, b.HASH_COMMENT_MODE]}]};
    return w.contains = [t, n, d], {name:"Python", aliases:["py", "gyp", "ipython"], unicodeRegex:!0, keywords:q, illegal:/(<\/|\?)|=>/, contains:[d, n, {begin:/\bself\b/}, {beginKeywords:"if", relevance:0}, {match:/\bor\b/, scope:"keyword"}, t, f, b.HASH_COMMENT_MODE, {match:[/\bdef/, /\s+/, l], scope:{1:"keyword", 3:"title.function"}, contains:[v]}, {variants:[{match:[/\bclass/, /\s+/, l, /\s*/, /\(\s*/, l, /\s*\)/]}, {match:[/\bclass/, /\s+/, l]}], scope:{1:"keyword", 3:"title.class", 6:"title.class.inherited"}}, 
    {className:"meta", begin:/^[\t ]*@/, end:/(?=#)|$/, contains:[n, v, t]}]};
  })();
  hljs.registerLanguage("python", g);
})();
(() => {
  var g = (() => b => ({aliases:["pycon"], contains:[{className:"meta.prompt", starts:{end:/ |$/, starts:{end:"$", subLanguage:"python"}}, variants:[{begin:/^>>>(?=[ ]|$)/}, {begin:/^\.\.\.(?=[ ]|$)/}]}]}))();
  hljs.registerLanguage("python-repl", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex, l = /(?:(?:[a-zA-Z]|\.[._a-zA-Z])[._a-zA-Z0-9]*)|\.(?!\d)/, n = f.either(/0[xX][0-9a-fA-F]+\.[0-9a-fA-F]*[pP][+-]?\d+i?/, /0[xX][0-9a-fA-F]+(?:[pP][+-]?\d+)?[Li]?/, /(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?[Li]?/), q = /[=!<>:]=|\|\||&&|:::?|<-|<<-|->>|->|\|>|[-+*\/?!$&|:<=>@^~]|\*\*/, d = f.either(/[()]/, /[{}]/, /\[\[/, /[[\]]/, /\\/, /,/);
    return {name:"R", keywords:{$pattern:l, keyword:"function if in break next repeat else for while", literal:"NULL NA TRUE FALSE Inf NaN NA_integer_|10 NA_real_|10 NA_character_|10 NA_complex_|10", built_in:"LETTERS letters month.abb month.name pi T F abs acos acosh all any anyNA Arg as.call as.character as.complex as.double as.environment as.integer as.logical as.null.default as.numeric as.raw asin asinh atan atanh attr attributes baseenv browser c call ceiling class Conj cos cosh cospi cummax cummin cumprod cumsum digamma dim dimnames emptyenv exp expression floor forceAndCall gamma gc.time globalenv Im interactive invisible is.array is.atomic is.call is.character is.complex is.double is.environment is.expression is.finite is.function is.infinite is.integer is.language is.list is.logical is.matrix is.na is.name is.nan is.null is.numeric is.object is.pairlist is.raw is.recursive is.single is.symbol lazyLoadDBfetch length lgamma list log max min missing Mod names nargs nzchar oldClass on.exit pos.to.env proc.time prod quote range Re rep retracemem return round seq_along seq_len seq.int sign signif sin sinh sinpi sqrt standardGeneric substitute sum switch tan tanh tanpi tracemem trigamma trunc unclass untracemem UseMethod xtfrm"}, 
    contains:[b.COMMENT(/#'/, /$/, {contains:[{scope:"doctag", match:/@examples/, starts:{end:f.lookahead(f.either(/\n^#'\s*(?=@[a-zA-Z]+)/, /\n^(?!#')/)), endsParent:!0}}, {scope:"doctag", begin:"@param", end:/$/, contains:[{scope:"variable", variants:[{match:l}, {match:/`(?:\\.|[^`\\])+`/}], endsParent:!0}]}, {scope:"doctag", match:/@[a-zA-Z]+/}, {scope:"keyword", match:/\\[a-zA-Z]+/}]}), b.HASH_COMMENT_MODE, {scope:"string", contains:[b.BACKSLASH_ESCAPE], variants:[b.END_SAME_AS_BEGIN({begin:/[rR]"(-*)\(/, 
    end:/\)(-*)"/}), b.END_SAME_AS_BEGIN({begin:/[rR]"(-*)\{/, end:/\}(-*)"/}), b.END_SAME_AS_BEGIN({begin:/[rR]"(-*)\[/, end:/\](-*)"/}), b.END_SAME_AS_BEGIN({begin:/[rR]'(-*)\(/, end:/\)(-*)'/}), b.END_SAME_AS_BEGIN({begin:/[rR]'(-*)\{/, end:/\}(-*)'/}), b.END_SAME_AS_BEGIN({begin:/[rR]'(-*)\[/, end:/\](-*)'/}), {begin:'"', end:'"', relevance:0}, {begin:"'", end:"'", relevance:0}]}, {relevance:0, variants:[{scope:{1:"operator", 2:"number"}, match:[q, n]}, {scope:{1:"operator", 2:"number"}, match:[/%[^%]*%/, 
    n]}, {scope:{1:"punctuation", 2:"number"}, match:[d, n]}, {scope:{2:"number"}, match:[/[^a-zA-Z0-9._]|^/, n]}]}, {scope:{3:"operator"}, match:[l, /\s+/, /<-/, /\s+/]}, {scope:"operator", relevance:0, variants:[{match:q}, {match:/%[^%]*%/}]}, {scope:"punctuation", relevance:0, match:d}, {begin:"`", end:"`", contains:[{begin:/\\./}]}]};
  })();
  hljs.registerLanguage("r", g);
})();
(() => {
  var g = (() => b => {
    var f = b.regex, l = f.either(/\b([A-Z]+[a-z0-9]+)+/, /\b([A-Z]+[a-z0-9]+)+[A-Z]+/), n = f.concat(l, /(::\w+)*/);
    const q = {"variable.constant":["__FILE__", "__LINE__", "__ENCODING__"], "variable.language":["self", "super"], keyword:"alias and begin BEGIN break case class defined do else elsif end END ensure for if in module next not or redo require rescue retry return then undef unless until when while yield include extend prepend public private protected raise throw".split(" "), built_in:"proc lambda attr_accessor attr_reader attr_writer define_method private_constant module_function".split(" "), literal:["true", 
    "false", "nil"]};
    var d = {className:"doctag", begin:"@[A-Za-z]+"};
    const w = {begin:"#<", end:">"};
    d = [b.COMMENT("#", "$", {contains:[d]}), b.COMMENT("^=begin", "^=end", {contains:[d], relevance:10}), b.COMMENT("^__END__", b.MATCH_NOTHING_RE)];
    const t = {className:"subst", begin:/#\{/, end:/\}/, keywords:q}, v = {className:"string", contains:[b.BACKSLASH_ESCAPE, t], variants:[{begin:/'/, end:/'/}, {begin:/"/, end:/"/}, {begin:/`/, end:/`/}, {begin:/%[qQwWx]?\(/, end:/\)/}, {begin:/%[qQwWx]?\[/, end:/\]/}, {begin:/%[qQwWx]?\{/, end:/\}/}, {begin:/%[qQwWx]?</, end:/>/}, {begin:/%[qQwWx]?\//, end:/\//}, {begin:/%[qQwWx]?%/, end:/%/}, {begin:/%[qQwWx]?-/, end:/-/}, {begin:/%[qQwWx]?\|/, end:/\|/}, {begin:/\B\?(\\\d{1,3})/}, {begin:/\B\?(\\x[A-Fa-f0-9]{1,2})/}, 
    {begin:/\B\?(\\u\{?[A-Fa-f0-9]{1,6}\}?)/}, {begin:/\B\?(\\M-\\C-|\\M-\\c|\\c\\M-|\\M-|\\C-\\M-)[\x20-\x7e]/}, {begin:/\B\?\\(c|C-)[\x20-\x7e]/}, {begin:/\B\?\\?\S/}, {begin:f.concat(/<<[-~]?'?/, f.lookahead(/(\w+)(?=\W)[^\n]*\n(?:[^\n]*\n)*?\s*\1\b/)), contains:[b.END_SAME_AS_BEGIN({begin:/(\w+)/, end:/(\w+)/, contains:[b.BACKSLASH_ESCAPE, t]})]}]};
    f = {variants:[{match:/\(\)/}, {className:"params", begin:/\(/, end:/(?=\))/, excludeBegin:!0, endsParent:!0, keywords:q}]};
    l = [v, {variants:[{match:[/class\s+/, n, /\s+<\s+/, n]}, {match:[/\b(class|module)\s+/, n]}], scope:{2:"title.class", 4:"title.class.inherited"}, keywords:q}, {match:[/(include|extend)\s+/, n], scope:{2:"title.class"}, keywords:q}, {relevance:0, match:[n, /\.new[. (]/], scope:{1:"title.class"}}, {relevance:0, match:/\b[A-Z][A-Z_0-9]+\b/, className:"variable.constant"}, {relevance:0, match:l, scope:"title.class"}, {match:[/def/, /\s+/, "([a-zA-Z_]\\w*[!?=]?|[-+~]@|<<|>>|=~|===?|<=>|[<>]=?|\\*\\*|[-/+%^&*~`|]|\\[\\]=?)"], 
    scope:{1:"keyword", 3:"title.function"}, contains:[f]}, {begin:b.IDENT_RE + "::"}, {className:"symbol", begin:b.UNDERSCORE_IDENT_RE + "(!|\\?)?:", relevance:0}, {className:"symbol", begin:":(?!\\s)", contains:[v, {begin:"([a-zA-Z_]\\w*[!?=]?|[-+~]@|<<|>>|=~|===?|<=>|[<>]=?|\\*\\*|[-/+%^&*~`|]|\\[\\]=?)"}], relevance:0}, {className:"number", relevance:0, variants:[{begin:"\\b([1-9](_?[0-9])*|0)(\\.([0-9](_?[0-9])*))?([eE][+-]?([0-9](_?[0-9])*)|r)?i?\\b"}, {begin:"\\b0[dD][0-9](_?[0-9])*r?i?\\b"}, 
    {begin:"\\b0[bB][0-1](_?[0-1])*r?i?\\b"}, {begin:"\\b0[oO][0-7](_?[0-7])*r?i?\\b"}, {begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*r?i?\\b"}, {begin:"\\b0(_?[0-7])+r?i?\\b"}]}, {className:"variable", begin:"(\\$\\W)|((\\$|@@?)(\\w+))(?=[^@$?])(?![A-Za-z])(?![@$?'])"}, {className:"params", begin:/\|/, end:/\|/, excludeBegin:!0, excludeEnd:!0, relevance:0, keywords:q}, {begin:"(" + b.RE_STARTERS_RE + "|unless)\\s*", keywords:"unless", contains:[{className:"regexp", contains:[b.BACKSLASH_ESCAPE, t], 
    illegal:/\n/, variants:[{begin:"/", end:"/[a-z]*"}, {begin:/%r\{/, end:/\}[a-z]*/}, {begin:"%r\\(", end:"\\)[a-z]*"}, {begin:"%r!", end:"![a-z]*"}, {begin:"%r\\[", end:"\\][a-z]*"}]}].concat(w, d), relevance:0}].concat(w, d);
    t.contains = l;
    f.contains = l;
    n = [{begin:/^\s*=>/, starts:{end:"$", contains:l}}, {className:"meta.prompt", begin:"^([>?]>|[\\w#]+\\(\\w+\\):\\d+:\\d+[>*]|(\\w+-)?\\d+\\.\\d+\\.\\d+(p\\d+)?[^\\d][^>]+>)(?=[ ])", starts:{end:"$", keywords:q, contains:l}}];
    return d.unshift(w), {name:"Ruby", aliases:["rb", "gemspec", "podspec", "thor", "irb"], keywords:q, illegal:/\/\*/, contains:[b.SHEBANG({binary:"ruby"})].concat(n).concat(d).concat(l)};
  })();
  hljs.registerLanguage("ruby", g);
})();
(() => {
  var g = (() => b => {
    var f = b.regex;
    f = {className:"title.function.invoke", relevance:0, begin:f.concat(/\b/, /(?!let|for|while|if|else|match\b)/, b.IDENT_RE, f.lookahead(/\s*\(/))};
    const l = "drop ;Copy;Send;Sized;Sync;Drop;Fn;FnMut;FnOnce;ToOwned;Clone;Debug;PartialEq;PartialOrd;Eq;Ord;AsRef;AsMut;Into;From;Default;Iterator;Extend;IntoIterator;DoubleEndedIterator;ExactSizeIterator;SliceConcatExt;ToString;assert!;assert_eq!;bitflags!;bytes!;cfg!;col!;concat!;concat_idents!;debug_assert!;debug_assert_eq!;env!;eprintln!;panic!;file!;format!;format_args!;include_bytes!;include_str!;line!;local_data_key!;module_path!;option_env!;print!;println!;select!;stringify!;try!;unimplemented!;unreachable!;vec!;write!;writeln!;macro_rules!;assert_ne!;debug_assert_ne!".split(";"), 
    n = "i8 i16 i32 i64 i128 isize u8 u16 u32 u64 u128 usize f32 f64 str char bool Box Option Result String Vec".split(" ");
    return {name:"Rust", aliases:["rs"], keywords:{$pattern:b.IDENT_RE + "!?", type:n, keyword:"abstract as async await become box break const continue crate do dyn else enum extern false final fn for if impl in let loop macro match mod move mut override priv pub ref return self Self static struct super trait true try type typeof unsafe unsized use virtual where while yield".split(" "), literal:"true false Some None Ok Err".split(" "), built_in:l}, illegal:"</", contains:[b.C_LINE_COMMENT_MODE, b.COMMENT("/\\*", 
    "\\*/", {contains:["self"]}), b.inherit(b.QUOTE_STRING_MODE, {begin:/b?"/, illegal:null}), {className:"string", variants:[{begin:/b?r(#*)"(.|\n)*?"\1(?!#)/}, {begin:/b?'\\?(x\w{2}|u\w{4}|U\w{8}|.)'/}]}, {className:"symbol", begin:/'[a-zA-Z_][a-zA-Z0-9_]*/}, {className:"number", variants:[{begin:"\\b0b([01_]+)([ui](8|16|32|64|128|size)|f(32|64))?"}, {begin:"\\b0o([0-7_]+)([ui](8|16|32|64|128|size)|f(32|64))?"}, {begin:"\\b0x([A-Fa-f0-9_]+)([ui](8|16|32|64|128|size)|f(32|64))?"}, {begin:"\\b(\\d[\\d_]*(\\.[0-9_]+)?([eE][+-]?[0-9_]+)?)([ui](8|16|32|64|128|size)|f(32|64))?"}], 
    relevance:0}, {begin:[/fn/, /\s+/, b.UNDERSCORE_IDENT_RE], className:{1:"keyword", 3:"title.function"}}, {className:"meta", begin:"#!?\\[", end:"\\]", contains:[{className:"string", begin:/"/, end:/"/, contains:[b.BACKSLASH_ESCAPE]}]}, {begin:[/let/, /\s+/, /(?:mut\s+)?/, b.UNDERSCORE_IDENT_RE], className:{1:"keyword", 3:"keyword", 4:"variable"}}, {begin:[/for/, /\s+/, b.UNDERSCORE_IDENT_RE, /\s+/, /in/], className:{1:"keyword", 3:"variable", 5:"keyword"}}, {begin:[/type/, /\s+/, b.UNDERSCORE_IDENT_RE], 
    className:{1:"keyword", 3:"title.class"}}, {begin:[/(?:trait|enum|struct|union|impl|for)/, /\s+/, b.UNDERSCORE_IDENT_RE], className:{1:"keyword", 3:"title.class"}}, {begin:b.IDENT_RE + "::", keywords:{keyword:"Self", built_in:l, type:n}}, {className:"punctuation", begin:"->"}, f]};
  })();
  hljs.registerLanguage("rust", g);
})();
(() => {
  var g = (() => b => {
    var f = {className:"subst", variants:[{begin:"\\$[A-Za-z0-9_]+"}, {begin:/\$\{/, end:/\}/}]};
    f = {className:"string", variants:[{begin:'"""', end:'"""'}, {begin:'"', end:'"', illegal:"\\n", contains:[b.BACKSLASH_ESCAPE]}, {begin:'[a-z]+"', end:'"', illegal:"\\n", contains:[b.BACKSLASH_ESCAPE, f]}, {className:"string", begin:'[a-z]+"""', end:'"""', contains:[f], relevance:10}]};
    const l = {className:"type", begin:"\\b[A-Z][A-Za-z0-9_]*", relevance:0};
    var n = {className:"title", begin:/[^0-9\n\t "'(),.`{}\[\]:;][^\n\t "'(),.`{}\[\]:;]+|[^0-9\n\t "'(),.`{}\[\]:;=]/, relevance:0};
    const q = {className:"class", beginKeywords:"class object trait type", end:/[:={\[\n;]/, excludeEnd:!0, contains:[b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE, {beginKeywords:"extends with", relevance:10}, {begin:/\[/, end:/\]/, excludeBegin:!0, excludeEnd:!0, relevance:0, contains:[l, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, {className:"params", begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, relevance:0, contains:[l, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE]}, n]};
    n = {className:"function", beginKeywords:"def", end:b.regex.lookahead(/[:={\[(\n;]/), contains:[n]};
    return {name:"Scala", keywords:{literal:"true false null", keyword:"type yield lazy override def with val var sealed abstract private trait object if then forSome for while do throw finally protected extends import final return else break new catch super class case package default try this match continue throws implicit export enum given transparent"}, contains:[{begin:["//>", /\s+/, /using/, /\s+/, /\S+/], beginScope:{1:"comment", 3:"keyword", 5:"type"}, end:/$/, contains:[{className:"string", 
    begin:/\S+/}]}, b.C_LINE_COMMENT_MODE, b.C_BLOCK_COMMENT_MODE, f, l, n, q, b.C_NUMBER_MODE, {begin:[/^\s*/, "extension", /\s+(?=[[(])/], beginScope:{2:"keyword"}}, {begin:[/^\s*/, /end/, /\s+/, /(extension\b)?/], beginScope:{2:"keyword", 4:"keyword"}}, {match:/\.inline\b/}, {begin:/\binline(?=\s)/, keywords:"inline"}, {begin:[/\(\s*/, /using/, /\s+(?!\))/], beginScope:{2:"keyword"}}, {className:"meta", begin:"@[A-Za-z]+"}]};
  })();
  hljs.registerLanguage("scala", g);
})();
(() => {
  var g = (() => b => {
    const f = {className:"literal", begin:"(#t|#f|#\\\\[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+|#\\\\.)"}, l = {className:"number", variants:[{begin:"(-|\\+)?\\d+([./]\\d+)?", relevance:0}, {begin:"(-|\\+)?\\d+([./]\\d+)?[+\\-](-|\\+)?\\d+([./]\\d+)?i", relevance:0}, {begin:"#b[0-1]+(/[0-1]+)?"}, {begin:"#o[0-7]+(/[0-7]+)?"}, {begin:"#x[0-9a-f]+(/[0-9a-f]+)?"}]}, n = b.QUOTE_STRING_MODE, q = [b.COMMENT(";", "$", {relevance:0}), b.COMMENT("#\\|", "\\|#")], d = {begin:"[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+", 
    relevance:0}, w = {className:"symbol", begin:"'[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+"}, t = {endsWithParent:!0, relevance:0}, v = {variants:[{begin:/'/}, {begin:"`"}], contains:[{begin:"\\(", end:"\\)", contains:["self", f, n, l, d, w]}]};
    var x = {className:"name", relevance:0, begin:"[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+", keywords:{$pattern:"[^\\(\\)\\[\\]\\{\\}\",'`;#|\\\\\\s]+", built_in:"case-lambda call/cc class define-class exit-handler field import inherit init-field interface let*-values let-values let/ec mixin opt-lambda override protect provide public rename require require-for-syntax syntax syntax-case syntax-error unit/sig unless when with-syntax and begin call-with-current-continuation call-with-input-file call-with-output-file case cond define define-syntax delay do dynamic-wind else for-each if lambda let let* let-syntax letrec letrec-syntax map or syntax-rules ' * + , ,@ - ... / ; < <= = => > >= ` abs acos angle append apply asin assoc assq assv atan boolean? caar cadr call-with-input-file call-with-output-file call-with-values car cdddar cddddr cdr ceiling char->integer char-alphabetic? char-ci<=? char-ci<? char-ci=? char-ci>=? char-ci>? char-downcase char-lower-case? char-numeric? char-ready? char-upcase char-upper-case? char-whitespace? char<=? char<? char=? char>=? char>? char? close-input-port close-output-port complex? cons cos current-input-port current-output-port denominator display eof-object? eq? equal? eqv? eval even? exact->inexact exact? exp expt floor force gcd imag-part inexact->exact inexact? input-port? integer->char integer? interaction-environment lcm length list list->string list->vector list-ref list-tail list? load log magnitude make-polar make-rectangular make-string make-vector max member memq memv min modulo negative? newline not null-environment null? number->string number? numerator odd? open-input-file open-output-file output-port? pair? peek-char port? positive? procedure? quasiquote quote quotient rational? rationalize read read-char real-part real? remainder reverse round scheme-report-environment set! set-car! set-cdr! sin sqrt string string->list string->number string->symbol string-append string-ci<=? string-ci<? string-ci=? string-ci>=? string-ci>? string-copy string-fill! string-length string-ref string-set! string<=? string<? string=? string>=? string>? string? substring symbol->string symbol? tan transcript-off transcript-on truncate values vector vector->list vector-fill! vector-length vector-ref vector-set! with-input-from-file with-output-to-file write write-char zero?"}};
    x = {variants:[{begin:"\\(", end:"\\)"}, {begin:"\\[", end:"\\]"}], contains:[{begin:/lambda/, endsWithParent:!0, returnBegin:!0, contains:[x, {endsParent:!0, variants:[{begin:/\(/, end:/\)/}, {begin:/\[/, end:/\]/}], contains:[d]}]}, x, t]};
    return t.contains = [f, l, n, d, w, v, x].concat(q), {name:"Scheme", aliases:["scm"], illegal:/\S/, contains:[b.SHEBANG(), l, n, w, v, x].concat(q)};
  })();
  hljs.registerLanguage("scheme", g);
})();
(() => {
  var g = (() => {
    const b = "a abbr address article aside audio b blockquote body button canvas caption cite code dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 header hgroup html i iframe img input ins kbd label legend li main mark menu nav object ol p q quote samp section span strong summary sup table tbody td textarea tfoot th thead time tr ul var video defs g marker mask pattern svg switch symbol feBlend feColorMatrix feComponentTransfer feComposite feConvolveMatrix feDiffuseLighting feDisplacementMap feFlood feGaussianBlur feImage feMerge feMorphology feOffset feSpecularLighting feTile feTurbulence linearGradient radialGradient stop circle ellipse image line path polygon polyline rect text use textPath tspan foreignObject clipPath".split(" "), 
    f = "any-hover any-pointer aspect-ratio color color-gamut color-index device-aspect-ratio device-height device-width display-mode forced-colors grid height hover inverted-colors monochrome orientation overflow-block overflow-inline pointer prefers-color-scheme prefers-contrast prefers-reduced-motion prefers-reduced-transparency resolution scan scripting update width min-width max-width min-height max-height".split(" ").sort().reverse(), l = "active any-link blank checked current default defined dir disabled drop empty enabled first first-child first-of-type fullscreen future focus focus-visible focus-within has host host-context hover indeterminate in-range invalid is lang last-child last-of-type left link local-link not nth-child nth-col nth-last-child nth-last-col nth-last-of-type nth-of-type only-child only-of-type optional out-of-range past placeholder-shown read-only read-write required right root scope target target-within user-invalid valid visited where".split(" ").sort().reverse(), 
    n = "after backdrop before cue cue-region first-letter first-line grammar-error marker part placeholder selection slotted spelling-error".split(" ").sort().reverse(), q = "align-content align-items align-self alignment-baseline all animation animation-delay animation-direction animation-duration animation-fill-mode animation-iteration-count animation-name animation-play-state animation-timing-function backface-visibility background background-attachment background-blend-mode background-clip background-color background-image background-origin background-position background-repeat background-size baseline-shift block-size border border-block border-block-color border-block-end border-block-end-color border-block-end-style border-block-end-width border-block-start border-block-start-color border-block-start-style border-block-start-width border-block-style border-block-width border-bottom border-bottom-color border-bottom-left-radius border-bottom-right-radius border-bottom-style border-bottom-width border-collapse border-color border-image border-image-outset border-image-repeat border-image-slice border-image-source border-image-width border-inline border-inline-color border-inline-end border-inline-end-color border-inline-end-style border-inline-end-width border-inline-start border-inline-start-color border-inline-start-style border-inline-start-width border-inline-style border-inline-width border-left border-left-color border-left-style border-left-width border-radius border-right border-right-color border-right-style border-right-width border-spacing border-style border-top border-top-color border-top-left-radius border-top-right-radius border-top-style border-top-width border-width bottom box-decoration-break box-shadow box-sizing break-after break-before break-inside cx cy caption-side caret-color clear clip clip-path clip-rule color color-interpolation color-interpolation-filters color-profile color-rendering column-count column-fill column-gap column-rule column-rule-color column-rule-style column-rule-width column-span column-width columns contain content content-visibility counter-increment counter-reset cue cue-after cue-before cursor direction display dominant-baseline empty-cells enable-background fill fill-opacity fill-rule filter flex flex-basis flex-direction flex-flow flex-grow flex-shrink flex-wrap float flow flood-color flood-opacity font font-display font-family font-feature-settings font-kerning font-language-override font-size font-size-adjust font-smoothing font-stretch font-style font-synthesis font-variant font-variant-caps font-variant-east-asian font-variant-ligatures font-variant-numeric font-variant-position font-variation-settings font-weight gap glyph-orientation-horizontal glyph-orientation-vertical grid grid-area grid-auto-columns grid-auto-flow grid-auto-rows grid-column grid-column-end grid-column-start grid-gap grid-row grid-row-end grid-row-start grid-template grid-template-areas grid-template-columns grid-template-rows hanging-punctuation height hyphens icon image-orientation image-rendering image-resolution ime-mode inline-size isolation kerning justify-content left letter-spacing lighting-color line-break line-height list-style list-style-image list-style-position list-style-type marker marker-end marker-mid marker-start mask margin margin-block margin-block-end margin-block-start margin-bottom margin-inline margin-inline-end margin-inline-start margin-left margin-right margin-top marks mask mask-border mask-border-mode mask-border-outset mask-border-repeat mask-border-slice mask-border-source mask-border-width mask-clip mask-composite mask-image mask-mode mask-origin mask-position mask-repeat mask-size mask-type max-block-size max-height max-inline-size max-width min-block-size min-height min-inline-size min-width mix-blend-mode nav-down nav-index nav-left nav-right nav-up none normal object-fit object-position opacity order orphans outline outline-color outline-offset outline-style outline-width overflow overflow-wrap overflow-x overflow-y padding padding-block padding-block-end padding-block-start padding-bottom padding-inline padding-inline-end padding-inline-start padding-left padding-right padding-top page-break-after page-break-before page-break-inside pause pause-after pause-before perspective perspective-origin pointer-events position quotes r resize rest rest-after rest-before right row-gap scroll-margin scroll-margin-block scroll-margin-block-end scroll-margin-block-start scroll-margin-bottom scroll-margin-inline scroll-margin-inline-end scroll-margin-inline-start scroll-margin-left scroll-margin-right scroll-margin-top scroll-padding scroll-padding-block scroll-padding-block-end scroll-padding-block-start scroll-padding-bottom scroll-padding-inline scroll-padding-inline-end scroll-padding-inline-start scroll-padding-left scroll-padding-right scroll-padding-top scroll-snap-align scroll-snap-stop scroll-snap-type scrollbar-color scrollbar-gutter scrollbar-width shape-image-threshold shape-margin shape-outside shape-rendering stop-color stop-opacity stroke stroke-dasharray stroke-dashoffset stroke-linecap stroke-linejoin stroke-miterlimit stroke-opacity stroke-width speak speak-as src tab-size table-layout text-anchor text-align text-align-all text-align-last text-combine-upright text-decoration text-decoration-color text-decoration-line text-decoration-style text-emphasis text-emphasis-color text-emphasis-position text-emphasis-style text-indent text-justify text-orientation text-overflow text-rendering text-shadow text-transform text-underline-position top transform transform-box transform-origin transform-style transition transition-delay transition-duration transition-property transition-timing-function unicode-bidi vector-effect vertical-align visibility voice-balance voice-duration voice-family voice-pitch voice-range voice-rate voice-stress voice-volume white-space widows width will-change word-break word-spacing word-wrap writing-mode x y z-index".split(" ").sort().reverse();
    return d => {
      var w = d.C_BLOCK_COMMENT_MODE, t = {scope:"number", begin:/#(([0-9a-fA-F]{3,4})|(([0-9a-fA-F]{2}){3,4}))\b/}, v = {className:"built_in", begin:/[\w-]+(?=\()/}, x = {scope:"number", begin:d.NUMBER_RE + "(%|em|ex|ch|rem|vw|vh|vmin|vmax|cm|mm|in|pt|pc|px|deg|grad|rad|turn|s|ms|Hz|kHz|dpi|dpcm|dppx)?", relevance:0};
      const C = {className:"variable", begin:"(\\$[a-zA-Z-][a-zA-Z0-9_-]*)\\b", relevance:0};
      return {name:"SCSS", case_insensitive:!0, illegal:"[=/|']", contains:[d.C_LINE_COMMENT_MODE, d.C_BLOCK_COMMENT_MODE, x, {className:"selector-id", begin:"#[A-Za-z0-9_-]+", relevance:0}, {className:"selector-class", begin:"\\.[A-Za-z0-9_-]+", relevance:0}, {scope:"selector-attr", begin:/\[/, end:/\]/, illegal:"$", contains:[d.APOS_STRING_MODE, d.QUOTE_STRING_MODE]}, {className:"selector-tag", begin:"\\b(" + b.join("|") + ")\\b", relevance:0}, {className:"selector-pseudo", begin:":(" + l.join("|") + 
      ")"}, {className:"selector-pseudo", begin:":(:)?(" + n.join("|") + ")"}, C, {begin:/\(/, end:/\)/, contains:[x]}, {className:"attr", begin:/--[A-Za-z_][A-Za-z0-9_-]*/}, {className:"attribute", begin:"\\b(" + q.join("|") + ")\\b"}, {begin:"\\b(whitespace|wait|w-resize|visible|vertical-text|vertical-ideographic|uppercase|upper-roman|upper-alpha|underline|transparent|top|thin|thick|text|text-top|text-bottom|tb-rl|table-header-group|table-footer-group|sw-resize|super|strict|static|square|solid|small-caps|separate|se-resize|scroll|s-resize|rtl|row-resize|ridge|right|repeat|repeat-y|repeat-x|relative|progress|pointer|overline|outside|outset|oblique|nowrap|not-allowed|normal|none|nw-resize|no-repeat|no-drop|newspaper|ne-resize|n-resize|move|middle|medium|ltr|lr-tb|lowercase|lower-roman|lower-alpha|loose|list-item|line|line-through|line-edge|lighter|left|keep-all|justify|italic|inter-word|inter-ideograph|inside|inset|inline|inline-block|inherit|inactive|ideograph-space|ideograph-parenthesis|ideograph-numeric|ideograph-alpha|horizontal|hidden|help|hand|groove|fixed|ellipsis|e-resize|double|dotted|distribute|distribute-space|distribute-letter|distribute-all-lines|disc|disabled|default|decimal|dashed|crosshair|collapse|col-resize|circle|char|center|capitalize|break-word|break-all|bottom|both|bolder|bold|block|bidi-override|below|baseline|auto|always|all-scroll|absolute|table|table-cell)\\b"}, 
      {begin:/:/, end:/[;}{]/, relevance:0, contains:[w, C, t, x, d.QUOTE_STRING_MODE, d.APOS_STRING_MODE, {scope:"meta", begin:"!important"}, v]}, {begin:"@(page|font-face)", keywords:{$pattern:"@[a-z-]+", keyword:"@page @font-face"}}, {begin:"@", end:"[{;]", returnBegin:!0, keywords:{$pattern:/[a-z-]+/, keyword:"and or not only", attribute:f.join(" ")}, contains:[{begin:"@[a-z-]+", className:"keyword"}, {begin:/[a-z-]+(?=:)/, className:"attribute"}, C, d.QUOTE_STRING_MODE, d.APOS_STRING_MODE, t, 
      x]}, v]};
    };
  })();
  hljs.registerLanguage("scss", g);
})();
(() => {
  var g = (() => b => ({name:"Shell Session", aliases:["console", "shellsession"], contains:[{className:"meta.prompt", begin:/^\s{0,3}[/~\w\d[\]()@-]*[>%$#][ ]?/, starts:{end:/[^\\](?=\s*$)/, subLanguage:"bash"}}]}))();
  hljs.registerLanguage("shell", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex, l = b.COMMENT("--", "$"), n = ["true", "false", "unknown"], q = "bigint binary blob boolean char character clob date dec decfloat decimal float int integer interval nchar nclob national numeric real row smallint time timestamp varchar varying varbinary".split(" "), d = "abs acos array_agg asin atan avg cast ceil ceiling coalesce corr cos cosh count covar_pop covar_samp cume_dist dense_rank deref element exp extract first_value floor json_array json_arrayagg json_exists json_object json_objectagg json_query json_table json_table_primitive json_value lag last_value lead listagg ln log log10 lower max min mod nth_value ntile nullif percent_rank percentile_cont percentile_disc position position_regex power rank regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy regr_syy row_number sin sinh sqrt stddev_pop stddev_samp substring substring_regex sum tan tanh translate translate_regex treat trim trim_array unnest upper value_of var_pop var_samp width_bucket".split(" "), 
    w = "create table;insert into;primary key;foreign key;not null;alter table;add constraint;grouping sets;on overflow;character set;respect nulls;ignore nulls;nulls first;nulls last;depth first;breadth first".split(";"), t = "abs acos all allocate alter and any are array array_agg array_max_cardinality as asensitive asin asymmetric at atan atomic authorization avg begin begin_frame begin_partition between bigint binary blob boolean both by call called cardinality cascaded case cast ceil ceiling char char_length character character_length check classifier clob close coalesce collate collect column commit condition connect constraint contains convert copy corr corresponding cos cosh count covar_pop covar_samp create cross cube cume_dist current current_catalog current_date current_default_transform_group current_path current_role current_row current_schema current_time current_timestamp current_path current_role current_transform_group_for_type current_user cursor cycle date day deallocate dec decimal decfloat declare default define delete dense_rank deref describe deterministic disconnect distinct double drop dynamic each element else empty end end_frame end_partition end-exec equals escape every except exec execute exists exp external extract false fetch filter first_value float floor for foreign frame_row free from full function fusion get global grant group grouping groups having hold hour identity in indicator initial inner inout insensitive insert int integer intersect intersection interval into is join json_array json_arrayagg json_exists json_object json_objectagg json_query json_table json_table_primitive json_value lag language large last_value lateral lead leading left like like_regex listagg ln local localtime localtimestamp log log10 lower match match_number match_recognize matches max member merge method min minute mod modifies module month multiset national natural nchar nclob new no none normalize not nth_value ntile null nullif numeric octet_length occurrences_regex of offset old omit on one only open or order out outer over overlaps overlay parameter partition pattern per percent percent_rank percentile_cont percentile_disc period portion position position_regex power precedes precision prepare primary procedure ptf range rank reads real recursive ref references referencing regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy regr_syy release result return returns revoke right rollback rollup row row_number rows running savepoint scope scroll search second seek select sensitive session_user set show similar sin sinh skip smallint some specific specifictype sql sqlexception sqlstate sqlwarning sqrt start static stddev_pop stddev_samp submultiset subset substring substring_regex succeeds sum symmetric system system_time system_user table tablesample tan tanh then time timestamp timezone_hour timezone_minute to trailing translate translate_regex translation treat trigger trim trim_array true truncate uescape union unique unknown unnest update upper user using value values value_of var_pop var_samp varbinary varchar varying versioning when whenever where width_bucket window with within without year add asc collation desc final first last view".split(" ").filter(x => 
    !d.includes(x)), v = {begin:f.concat(/\b/, f.either(...d), /\s*\(/), relevance:0, keywords:{built_in:d}};
    return {name:"SQL", case_insensitive:!0, illegal:/[{}]|<\//, keywords:{$pattern:/\b[\w\.]+/, keyword:((x, {exceptions:C, when:E} = {}) => (C = C || [], x.map(L => L.match(/\|\d+$/) || C.includes(L) ? L : E(L) ? L + "|0" : L)))(t, {when:x => 3 > x.length}), literal:n, type:q, built_in:"current_catalog current_date current_default_transform_group current_path current_role current_schema current_transform_group_for_type current_user session_user system_time system_user current_time localtime current_timestamp localtimestamp".split(" ")}, 
    contains:[{begin:f.either(...w), relevance:0, keywords:{$pattern:/[\w\.]+/, keyword:t.concat(w), literal:n, type:q}}, {className:"type", begin:f.either("double precision", "large object", "with timezone", "without timezone")}, v, {className:"variable", begin:/@[a-z0-9][a-z0-9_]*/}, {className:"string", variants:[{begin:/'/, end:/'/, contains:[{begin:/''/}]}]}, {begin:/"/, end:/"/, contains:[{begin:/""/}]}, b.C_NUMBER_MODE, b.C_BLOCK_COMMENT_MODE, l, {className:"operator", begin:/[-+*/=%^~]|&&?|\|\|?|!=?|<(?:=>?|<|>)?|>[>=]?/, 
    relevance:0}]};
  })();
  hljs.registerLanguage("sql", g);
})();
(() => {
  var g = (() => {
    function b(...k) {
      return k.map(h => h ? "string" == typeof h ? h : h.source : null).join("");
    }
    function f(...k) {
      return "(" + ((h => {
        const r = h[h.length - 1];
        return "object" == typeof r && r.constructor === Object ? (h.splice(h.length - 1, 1), r) : {};
      })(k).capture ? "" : "?:") + k.map(h => h ? "string" == typeof h ? h : h.source : null).join("|") + ")";
    }
    const l = k => b(/\b/, k, /\w$/.test(k) ? /\b/ : /\B/), n = ["Protocol", "Type"].map(l), q = ["init", "self"].map(l), d = ["Any", "Self"], w = ["actor", "any", "associatedtype", "async", "await", /as\?/, /as!/, "as", "borrowing", "break", "case", "catch", "class", "consume", "consuming", "continue", "convenience", "copy", "default", "defer", "deinit", "didSet", "distributed", "do", "dynamic", "each", "else", "enum", "extension", "fallthrough", /fileprivate\(set\)/, "fileprivate", "final", "for", 
    "func", "get", "guard", "if", "import", "indirect", "infix", /init\?/, /init!/, "inout", /internal\(set\)/, "internal", "in", "is", "isolated", "nonisolated", "lazy", "let", "macro", "mutating", "nonmutating", /open\(set\)/, "open", "operator", "optional", "override", "postfix", "precedencegroup", "prefix", /private\(set\)/, "private", "protocol", /public\(set\)/, "public", "repeat", "required", "rethrows", "return", "set", "some", "static", "struct", "subscript", "super", "switch", "throws", 
    "throw", /try\?/, /try!/, "try", "typealias", /unowned\(safe\)/, /unowned\(unsafe\)/, "unowned", "var", "weak", "where", "while", "willSet"], t = ["false", "nil", "true"], v = "assignment associativity higherThan left lowerThan none right".split(" "), x = "#colorLiteral #column #dsohandle #else #elseif #endif #error #file #fileID #fileLiteral #filePath #function #if #imageLiteral #keyPath #line #selector #sourceLocation #warning".split(" "), C = "abs all any assert assertionFailure debugPrint dump fatalError getVaList isKnownUniquelyReferenced max min numericCast pointwiseMax pointwiseMin precondition preconditionFailure print readLine repeatElement sequence stride swap swift_unboxFromSwiftValueWithType transcode type unsafeBitCast unsafeDowncast withExtendedLifetime withUnsafeMutablePointer withUnsafePointer withVaList withoutActuallyEscaping zip".split(" ");
    var E = f(/[/=\-+!*%<>&|^~?]/, /[\u00A1-\u00A7]/, /[\u00A9\u00AB]/, /[\u00AC\u00AE]/, /[\u00B0\u00B1]/, /[\u00B6\u00BB\u00BF\u00D7\u00F7]/, /[\u2016-\u2017]/, /[\u2020-\u2027]/, /[\u2030-\u203E]/, /[\u2041-\u2053]/, /[\u2055-\u205E]/, /[\u2190-\u23FF]/, /[\u2500-\u2775]/, /[\u2794-\u2BFF]/, /[\u2E00-\u2E7F]/, /[\u3001-\u3003]/, /[\u3008-\u3020]/, /[\u3030]/);
    const L = f(E, /[\u0300-\u036F]/, /[\u1DC0-\u1DFF]/, /[\u20D0-\u20FF]/, /[\uFE00-\uFE0F]/, /[\uFE20-\uFE2F]/), I = b(E, L, "*");
    E = f(/[a-zA-Z_]/, /[\u00A8\u00AA\u00AD\u00AF\u00B2-\u00B5\u00B7-\u00BA]/, /[\u00BC-\u00BE\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF]/, /[\u0100-\u02FF\u0370-\u167F\u1681-\u180D\u180F-\u1DBF]/, /[\u1E00-\u1FFF]/, /[\u200B-\u200D\u202A-\u202E\u203F-\u2040\u2054\u2060-\u206F]/, /[\u2070-\u20CF\u2100-\u218F\u2460-\u24FF\u2776-\u2793]/, /[\u2C00-\u2DFF\u2E80-\u2FFF]/, /[\u3004-\u3007\u3021-\u302F\u3031-\u303F\u3040-\uD7FF]/, /[\uF900-\uFD3D\uFD40-\uFDCF\uFDF0-\uFE1F\uFE30-\uFE44]/, /[\uFE47-\uFEFE\uFF00-\uFFFD]/);
    const M = f(E, /\d/, /[\u0300-\u036F\u1DC0-\u1DFF\u20D0-\u20FF\uFE20-\uFE2F]/), T = b(E, M, "*"), a = b(/[A-Z]/, M, "*"), e = ["attached", "autoclosure", b(/convention\(/, f("swift", "block", "c"), /\)/), "discardableResult", "dynamicCallable", "dynamicMemberLookup", "escaping", "freestanding", "frozen", "GKInspectable", "IBAction", "IBDesignable", "IBInspectable", "IBOutlet", "IBSegueAction", "inlinable", "main", "nonobjc", "NSApplicationMain", "NSCopying", "NSManaged", b(/objc\(/, T, /\)/), 
    "objc", "objcMembers", "propertyWrapper", "requires_stored_property_inits", "resultBuilder", "Sendable", "testable", "UIApplicationMain", "unchecked", "unknown", "usableFromInline", "warn_unqualified_access"], c = "iOS iOSApplicationExtension macOS macOSApplicationExtension macCatalyst macCatalystApplicationExtension watchOS watchOSApplicationExtension tvOS tvOSApplicationExtension swift".split(" ");
    return k => {
      var h = {match:/\s+/, relevance:0}, r = k.COMMENT("/\\*", "\\*/", {contains:["self"]});
      r = [k.C_LINE_COMMENT_MODE, r];
      var m = {match:[/\./, f(...n, ...q)], className:{2:"keyword"}}, y = {match:b(/\./, f(...w)), relevance:0}, A = w.filter(ba => "string" == typeof ba).concat(["_|0"]), J = {variants:[{className:"keyword", match:f(...w.filter(ba => "string" != typeof ba).concat(d).map(l), ...q)}]};
      A = {$pattern:f(/\b\w+/, /#\w+/), keyword:A.concat(x), literal:t};
      m = [m, y, J];
      y = [{match:b(/\./, f(...C)), relevance:0}, {className:"built_in", match:b(/\b/, f(...C), /(?=\()/)}];
      var O = {match:/->/, relevance:0};
      J = [O, {className:"operator", relevance:0, variants:[{match:I}, {match:`\\.(\\.|${L})+`}]}];
      const u = {className:"number", relevance:0, variants:[{match:"\\b(([0-9]_*)+)(\\.(([0-9]_*)+))?([eE][+-]?(([0-9]_*)+))?\\b"}, {match:"\\b0x(([0-9a-fA-F]_*)+)(\\.(([0-9a-fA-F]_*)+))?([pP][+-]?(([0-9]_*)+))?\\b"}, {match:/\b0o([0-7]_*)+\b/}, {match:/\b0b([01]_*)+\b/}]}, D = (ba = "") => ({className:"subst", variants:[{match:b(/\\/, ba, /[0\\tnr"']/)}, {match:b(/\\/, ba, /u\{[0-9a-fA-F]{1,8}\}/)}]}), K = (ba = "") => ({className:"subst", match:b(/\\/, ba, /[\t ]*(?:[\r\n]|\r\n)/)}), G = (ba = 
      "") => ({className:"subst", label:"interpol", begin:b(/\\/, ba, /\(/), end:/\)/});
      var P = (ba = "") => ({begin:b(ba, /"""/), end:b(/"""/, ba), contains:[D(ba), K(ba), G(ba)]}), R = (ba = "") => ({begin:b(ba, /"/), end:b(/"/, ba), contains:[D(ba), G(ba)]});
      P = {className:"string", variants:[P(), P("#"), P("##"), P("###"), R(), R("#"), R("##"), R("###")]};
      const S = [k.BACKSLASH_ESCAPE, {begin:/\[/, end:/\]/, relevance:0, contains:[k.BACKSLASH_ESCAPE]}];
      R = {begin:/\/[^\s](?=[^/\n]*\/)/, end:/\//, contains:S};
      var aa = ba => {
        const za = b(ba, /\//);
        ba = b(/\//, ba);
        return {begin:za, end:ba, contains:[...S, {scope:"comment", begin:`#(?!.*${ba})`, end:/$/}]};
      };
      R = {scope:"regexp", variants:[aa("###"), aa("##"), aa("#"), R]};
      var ha = {match:b(/`/, T, /`/)};
      aa = [ha, {className:"variable", match:/\$\d+/}, {className:"variable", match:`\\$${M}+`}];
      const ua = [{match:/(@|#(un)?)available/, scope:"keyword", starts:{contains:[{begin:/\(/, end:/\)/, keywords:c, contains:[...J, u, P]}]}}, {scope:"keyword", match:b(/@/, f(...e))}, {scope:"meta", match:b(/@/, T)}], ra = {match:b("(?=", /\b[A-Z]/, ")"), relevance:0, contains:[{className:"type", match:b(/(AV|CA|CF|CG|CI|CL|CM|CN|CT|MK|MP|MTK|MTL|NS|SCN|SK|UI|WK|XC)/, M, "+")}, {className:"type", match:a, relevance:0}, {match:/[?!]+/, relevance:0}, {match:/\.\.\./, relevance:0}, {match:b(/\s+&\s+/, 
      b("(?=", a, ")")), relevance:0}]};
      O = {begin:/</, end:/>/, keywords:A, contains:[...r, ...m, ...ua, O, ra]};
      ra.contains.push(O);
      O = {begin:/\(/, end:/\)/, relevance:0, keywords:A, contains:["self", {match:b(T, /\s*:/), keywords:"_|0", relevance:0}, ...r, R, ...m, ...y, ...J, u, P, ...aa, ...ua, ra]};
      var sa = {begin:/</, end:/>/, keywords:"repeat each", contains:[...r, ra]}, xa = {begin:/\(/, end:/\)/, keywords:A, contains:[{begin:f(b("(?=", b(T, /\s*:/), ")"), b("(?=", b(T, /\s+/, T, /\s*:/), ")")), end:/:/, relevance:0, contains:[{className:"keyword", match:/\b_\b/}, {className:"params", match:T}]}, ...r, ...m, ...J, u, P, ...ua, ra, O], endsParent:!0, illegal:/["']/};
      ha = {match:[/(func|macro)/, /\s+/, f(ha.match, T, I)], className:{1:"keyword", 3:"title.function"}, contains:[sa, xa, h], illegal:[/\[/, /%/]};
      h = {match:[/\b(?:subscript|init[?!]?)/, /\s*(?=[<(])/], className:{1:"keyword"}, contains:[sa, xa, h], illegal:/\[|%/};
      sa = {match:[/operator/, /\s+/, I], className:{1:"keyword", 3:"title"}};
      xa = {begin:[/precedencegroup/, /\s+/, a], className:{1:"keyword", 3:"title"}, contains:[ra], keywords:[...v, ...t], end:/}/};
      for (const ba of P.variants) {
        const za = ba.contains.find(p => "interpol" === p.label);
        za.keywords = A;
        const Ca = [...m, ...y, ...J, u, P, ...aa];
        za.contains = [...Ca, {begin:/\(/, end:/\)/, contains:["self", ...Ca]}];
      }
      return {name:"Swift", keywords:A, contains:[...r, ha, h, {beginKeywords:"struct protocol class extension enum actor", end:"\\{", excludeEnd:!0, keywords:A, contains:[k.inherit(k.TITLE_MODE, {className:"title.class", begin:/[A-Za-z$_][\u00C0-\u02B80-9A-Za-z$_]*/}), ...m]}, sa, xa, {beginKeywords:"import", end:/$/, contains:[...r], relevance:0}, R, ...m, ...y, ...J, u, P, ...aa, ...ua, ra, O]};
    };
  })();
  hljs.registerLanguage("swift", g);
})();
(() => {
  var g = (() => {
    function b(v) {
      const x = v.regex;
      var C = /<[A-Za-z0-9\\._:-]+/, E = /\/[A-Za-z0-9\\._:-]+>|\/>/;
      const L = {$pattern:"[A-Za-z$_][0-9A-Za-z$_]*", keyword:f, literal:l, built_in:t, "variable.language":w}, I = {className:"number", variants:[{begin:"(\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)((\\.([0-9](_?[0-9])*))|\\.)?|(\\.([0-9](_?[0-9])*)))[eE][+-]?([0-9](_?[0-9])*)\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*|0[0-7]*[89][0-9]*)\\b((\\.([0-9](_?[0-9])*))\\b|\\.)?|(\\.([0-9](_?[0-9])*))\\b"}, {begin:"\\b(0|[1-9](_?[0-9])*)n\\b"}, {begin:"\\b0[xX][0-9a-fA-F](_?[0-9a-fA-F])*n?\\b"}, {begin:"\\b0[bB][0-1](_?[0-1])*n?\\b"}, 
      {begin:"\\b0[oO][0-7](_?[0-7])*n?\\b"}, {begin:"\\b0[0-7]+n?\\b"}], relevance:0};
      var M = {className:"subst", begin:"\\$\\{", end:"\\}", keywords:L, contains:[]};
      const T = {begin:"html`", end:"", starts:{end:"`", returnEnd:!1, contains:[v.BACKSLASH_ESCAPE, M], subLanguage:"xml"}}, a = {begin:"css`", end:"", starts:{end:"`", returnEnd:!1, contains:[v.BACKSLASH_ESCAPE, M], subLanguage:"css"}}, e = {begin:"gql`", end:"", starts:{end:"`", returnEnd:!1, contains:[v.BACKSLASH_ESCAPE, M], subLanguage:"graphql"}}, c = {className:"string", begin:"`", end:"`", contains:[v.BACKSLASH_ESCAPE, M]}, k = {className:"comment", variants:[v.COMMENT(/\/\*\*(?!\/)/, "\\*/", 
      {relevance:0, contains:[{begin:"(?=@[A-Za-z]+)", relevance:0, contains:[{className:"doctag", begin:"@[A-Za-z]+"}, {className:"type", begin:"\\{", end:"\\}", excludeEnd:!0, excludeBegin:!0, relevance:0}, {className:"variable", begin:"[A-Za-z$_][0-9A-Za-z$_]*(?=\\s*(-)|$)", endsParent:!0, relevance:0}, {begin:/(?=[^\n])\s/, relevance:0}]}]}), v.C_BLOCK_COMMENT_MODE, v.C_LINE_COMMENT_MODE]};
      var h = [v.APOS_STRING_MODE, v.QUOTE_STRING_MODE, T, a, e, c, {match:/\$\d+/}, I];
      M.contains = h.concat({begin:/\{/, end:/\}/, keywords:L, contains:["self"].concat(h)});
      M = [].concat(k, M.contains);
      M = M.concat([{begin:/\(/, end:/\)/, keywords:L, contains:["self"].concat(M)}]);
      h = {className:"params", begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:L, contains:M};
      const r = {variants:[{match:[/class/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /\s+/, /extends/, /\s+/, x.concat("[A-Za-z$_][0-9A-Za-z$_]*", "(", x.concat(/\./, "[A-Za-z$_][0-9A-Za-z$_]*"), ")*")], scope:{1:"keyword", 3:"title.class", 5:"keyword", 7:"title.class.inherited"}}, {match:[/class/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*"], scope:{1:"keyword", 3:"title.class"}}]}, m = {relevance:0, match:x.either(/\bJSON/, /\b[A-Z][a-z]+([A-Z][a-z]*|\d)*/, /\b[A-Z]{2,}([A-Z][a-z]+|\d)+([A-Z][a-z]*)*/, /\b[A-Z]{2,}[a-z]+([A-Z][a-z]+|\d)*([A-Z][a-z]*)*/), 
      className:"title.class", keywords:{_:[...n, ...q]}}, y = {variants:[{match:[/function/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /(?=\s*\()/]}, {match:[/function/, /\s*(?=\()/]}], className:{1:"keyword", 3:"title.function"}, label:"func.def", contains:[h], illegal:/%/}, A = {match:x.concat(/\b/, (J = [...d, "super", "import"], x.concat("(?!", J.join("|"), ")")), "[A-Za-z$_][0-9A-Za-z$_]*", x.lookahead(/\(/)), className:"title.function", relevance:0};
      var J;
      J = {begin:x.concat(/\./, x.lookahead(x.concat("[A-Za-z$_][0-9A-Za-z$_]*", /(?![0-9A-Za-z$_(])/))), end:"[A-Za-z$_][0-9A-Za-z$_]*", excludeBegin:!0, keywords:"prototype", className:"property", relevance:0};
      const O = {match:[/get|set/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /(?=\()/], className:{1:"keyword", 3:"title.function"}, contains:[{begin:/\(\)/}, h]}, u = "(\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)|" + v.UNDERSCORE_IDENT_RE + ")\\s*=>", D = {match:[/const|var|let/, /\s+/, "[A-Za-z$_][0-9A-Za-z$_]*", /\s*/, /=\s*/, /(async\s*)?/, x.lookahead(u)], keywords:"async", className:{1:"keyword", 3:"title.function"}, contains:[h]};
      return {name:"JavaScript", aliases:["js", "jsx", "mjs", "cjs"], keywords:L, exports:{PARAMS_CONTAINS:M, CLASS_REFERENCE:m}, illegal:/#(?![$_A-z])/, contains:[v.SHEBANG({label:"shebang", binary:"node", relevance:5}), {label:"use_strict", className:"meta", relevance:10, begin:/^\s*['"]use (strict|asm)['"]/}, v.APOS_STRING_MODE, v.QUOTE_STRING_MODE, T, a, e, c, k, {match:/\$\d+/}, I, m, {className:"attr", begin:"[A-Za-z$_][0-9A-Za-z$_]*" + x.lookahead(":"), relevance:0}, D, {begin:"(" + v.RE_STARTERS_RE + 
      "|\\b(case|return|throw)\\b)\\s*", keywords:"return throw case", relevance:0, contains:[k, v.REGEXP_MODE, {className:"function", begin:u, returnBegin:!0, end:"\\s*=>", contains:[{className:"params", variants:[{begin:v.UNDERSCORE_IDENT_RE, relevance:0}, {className:null, begin:/\(\s*\)/, skip:!0}, {begin:/\(/, end:/\)/, excludeBegin:!0, excludeEnd:!0, keywords:L, contains:M}]}]}, {begin:/,/, relevance:0}, {match:/\s+/, relevance:0}, {variants:[{begin:"<>", end:"</>"}, {match:/<[A-Za-z0-9\\._:-]+\s*\/>/}, 
      {begin:C, "on:begin":(K, G) => {
        const P = K[0].length + K.index, R = K.input[P];
        if ("<" === R || "," === R) {
          return void G.ignoreMatch();
        }
        let S;
        ">" === R && (((aa, {after:ha}) => {
          const ua = "</" + aa[0].slice(1);
          return -1 !== aa.input.indexOf(ua, ha);
        })(K, {after:P}) || G.ignoreMatch());
        K = K.input.substring(P);
        ((S = K.match(/^\s*=/)) || (S = K.match(/^\s+extends\s+/)) && 0 === S.index) && G.ignoreMatch();
      }, end:E}], subLanguage:"xml", contains:[{begin:C, end:E, skip:!0, contains:["self"]}]}]}, y, {beginKeywords:"while if switch catch for"}, {begin:"\\b(?!function)" + v.UNDERSCORE_IDENT_RE + "\\([^()]*(\\([^()]*(\\([^()]*\\)[^()]*)*\\)[^()]*)*\\)\\s*\\{", returnBegin:!0, label:"func.def", contains:[h, v.inherit(v.TITLE_MODE, {begin:"[A-Za-z$_][0-9A-Za-z$_]*", className:"title.function"})]}, {match:/\.\.\./, relevance:0}, J, {match:"\\$[A-Za-z$_][0-9A-Za-z$_]*", relevance:0}, {match:[/\bconstructor(?=\s*\()/], 
      className:{1:"title.function"}, contains:[h]}, A, {relevance:0, match:/\b[A-Z][A-Z_0-9]+\b/, className:"variable.constant"}, r, O, {match:/\$[(.]/}]};
    }
    const f = "as in of if for while finally var new function do return void else break catch instanceof with throw case default try switch continue typeof delete let yield const class debugger async await static import from export extends".split(" "), l = "true false null undefined NaN Infinity".split(" "), n = "Object Function Boolean Symbol Math Date Number BigInt String RegExp Array Float32Array Float64Array Int8Array Uint8Array Uint8ClampedArray Int16Array Int32Array Uint16Array Uint32Array BigInt64Array BigUint64Array Set Map WeakSet WeakMap ArrayBuffer SharedArrayBuffer Atomics DataView JSON Promise Generator GeneratorFunction AsyncFunction Reflect Proxy Intl WebAssembly".split(" "), 
    q = "Error EvalError InternalError RangeError ReferenceError SyntaxError TypeError URIError".split(" "), d = "setInterval setTimeout clearInterval clearTimeout require exports eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent encodeURI encodeURIComponent escape unescape".split(" "), w = "arguments this super console window document localStorage sessionStorage module global".split(" "), t = [].concat(d, n, q);
    return v => {
      const x = b(v);
      var C = "any void number boolean string object never symbol bigint unknown".split(" ");
      const E = {beginKeywords:"namespace", end:/\{/, excludeEnd:!0, contains:[x.exports.CLASS_REFERENCE]}, L = {beginKeywords:"interface", end:/\{/, excludeEnd:!0, keywords:{keyword:"interface extends", built_in:C}, contains:[x.exports.CLASS_REFERENCE]};
      C = {$pattern:"[A-Za-z$_][0-9A-Za-z$_]*", keyword:f.concat("type namespace interface public private protected implements declare abstract readonly enum override".split(" ")), literal:l, built_in:t.concat(C), "variable.language":w};
      const I = {className:"meta", begin:"@[A-Za-z$_][0-9A-Za-z$_]*"}, M = (T, a, e) => {
        const c = T.contains.findIndex(k => k.label === a);
        if (-1 === c) {
          throw Error("can not find mode to replace");
        }
        T.contains.splice(c, 1, e);
      };
      return Object.assign(x.keywords, C), x.exports.PARAMS_CONTAINS.push(I), x.contains = x.contains.concat([I, E, L]), M(x, "shebang", v.SHEBANG()), M(x, "use_strict", {className:"meta", relevance:10, begin:/^\s*['"]use strict['"]/}), x.contains.find(T => "func.def" === T.label).relevance = 0, Object.assign(x, {name:"TypeScript", aliases:["ts", "tsx", "mts", "cts"]}), x;
    };
  })();
  hljs.registerLanguage("typescript", g);
})();
(() => {
  var g = (() => b => {
    var f = b.regex, l = /\d{1,2}\/\d{1,2}\/\d{4}/;
    const n = /\d{4}-\d{1,2}-\d{1,2}/, q = /(\d|1[012])(:\d+){0,2} *(AM|PM)/, d = /\d{1,2}(:\d{1,2}){1,2}/;
    f = {className:"literal", variants:[{begin:f.concat(/# */, f.either(n, l), / *#/)}, {begin:f.concat(/# */, d, / *#/)}, {begin:f.concat(/# */, q, / *#/)}, {begin:f.concat(/# */, f.either(n, l), / +/, f.either(q, d), / *#/)}]};
    l = b.COMMENT(/'''/, /$/, {contains:[{className:"doctag", begin:/<\/?/, end:/>/}]});
    b = b.COMMENT(null, /$/, {variants:[{begin:/'/}, {begin:/([\t ]|^)REM(?=\s)/}]});
    return {name:"Visual Basic .NET", aliases:["vb"], case_insensitive:!0, classNameAliases:{label:"symbol"}, keywords:{keyword:"addhandler alias aggregate ansi as async assembly auto binary by byref byval call case catch class compare const continue custom declare default delegate dim distinct do each equals else elseif end enum erase error event exit explicit finally for friend from function get global goto group handles if implements imports in inherits interface into iterator join key let lib loop me mid module mustinherit mustoverride mybase myclass namespace narrowing new next notinheritable notoverridable of off on operator option optional order overloads overridable overrides paramarray partial preserve private property protected public raiseevent readonly redim removehandler resume return select set shadows shared skip static step stop structure strict sub synclock take text then throw to try unicode until using when where while widening with withevents writeonly yield", 
    built_in:"addressof and andalso await directcast gettype getxmlnamespace is isfalse isnot istrue like mod nameof new not or orelse trycast typeof xor cbool cbyte cchar cdate cdbl cdec cint clng cobj csbyte cshort csng cstr cuint culng cushort", type:"boolean byte char date decimal double integer long object sbyte short single string uinteger ulong ushort", literal:"true false nothing"}, illegal:"//|\\{|\\}|endif|gosub|variant|wend|^\\$ ", contains:[{className:"string", begin:/"(""|[^/n])"C\b/}, 
    {className:"string", begin:/"/, end:/"/, illegal:/\n/, contains:[{begin:/""/}]}, f, {className:"number", relevance:0, variants:[{begin:/\b\d[\d_]*((\.[\d_]+(E[+-]?[\d_]+)?)|(E[+-]?[\d_]+))[RFD@!#]?/}, {begin:/\b\d[\d_]*((U?[SIL])|[%&])?/}, {begin:/&H[\dA-F_]+((U?[SIL])|[%&])?/}, {begin:/&O[0-7_]+((U?[SIL])|[%&])?/}, {begin:/&B[01_]+((U?[SIL])|[%&])?/}]}, {className:"label", begin:/^\w+:/}, l, b, {className:"meta", begin:/[\t ]*#(const|disable|else|elseif|enable|end|externalsource|if|region)\b/, 
    end:/$/, keywords:{keyword:"const disable else elseif enable end externalsource if region then"}, contains:[b]}]};
  })();
  hljs.registerLanguage("vbnet", g);
})();
(() => {
  var g = (() => b => ({name:"VBScript in HTML", subLanguage:"xml", contains:[{begin:"<%", end:"%>", subLanguage:"vbscript"}]}))();
  hljs.registerLanguage("vbscript-html", g);
})();
(() => {
  var g = (() => b => ({name:"Intel x86 Assembly", case_insensitive:!0, keywords:{$pattern:"[.%]?" + b.IDENT_RE, keyword:"lock rep repe repz repne repnz xaquire xrelease bnd nobnd aaa aad aam aas adc add and arpl bb0_reset bb1_reset bound bsf bsr bswap bt btc btr bts call cbw cdq cdqe clc cld cli clts cmc cmp cmpsb cmpsd cmpsq cmpsw cmpxchg cmpxchg486 cmpxchg8b cmpxchg16b cpuid cpu_read cpu_write cqo cwd cwde daa das dec div dmint emms enter equ f2xm1 fabs fadd faddp fbld fbstp fchs fclex fcmovb fcmovbe fcmove fcmovnb fcmovnbe fcmovne fcmovnu fcmovu fcom fcomi fcomip fcomp fcompp fcos fdecstp fdisi fdiv fdivp fdivr fdivrp femms feni ffree ffreep fiadd ficom ficomp fidiv fidivr fild fimul fincstp finit fist fistp fisttp fisub fisubr fld fld1 fldcw fldenv fldl2e fldl2t fldlg2 fldln2 fldpi fldz fmul fmulp fnclex fndisi fneni fninit fnop fnsave fnstcw fnstenv fnstsw fpatan fprem fprem1 fptan frndint frstor fsave fscale fsetpm fsin fsincos fsqrt fst fstcw fstenv fstp fstsw fsub fsubp fsubr fsubrp ftst fucom fucomi fucomip fucomp fucompp fxam fxch fxtract fyl2x fyl2xp1 hlt ibts icebp idiv imul in inc incbin insb insd insw int int01 int1 int03 int3 into invd invpcid invlpg invlpga iret iretd iretq iretw jcxz jecxz jrcxz jmp jmpe lahf lar lds lea leave les lfence lfs lgdt lgs lidt lldt lmsw loadall loadall286 lodsb lodsd lodsq lodsw loop loope loopne loopnz loopz lsl lss ltr mfence monitor mov movd movq movsb movsd movsq movsw movsx movsxd movzx mul mwait neg nop not or out outsb outsd outsw packssdw packsswb packuswb paddb paddd paddsb paddsiw paddsw paddusb paddusw paddw pand pandn pause paveb pavgusb pcmpeqb pcmpeqd pcmpeqw pcmpgtb pcmpgtd pcmpgtw pdistib pf2id pfacc pfadd pfcmpeq pfcmpge pfcmpgt pfmax pfmin pfmul pfrcp pfrcpit1 pfrcpit2 pfrsqit1 pfrsqrt pfsub pfsubr pi2fd pmachriw pmaddwd pmagw pmulhriw pmulhrwa pmulhrwc pmulhw pmullw pmvgezb pmvlzb pmvnzb pmvzb pop popa popad popaw popf popfd popfq popfw por prefetch prefetchw pslld psllq psllw psrad psraw psrld psrlq psrlw psubb psubd psubsb psubsiw psubsw psubusb psubusw psubw punpckhbw punpckhdq punpckhwd punpcklbw punpckldq punpcklwd push pusha pushad pushaw pushf pushfd pushfq pushfw pxor rcl rcr rdshr rdmsr rdpmc rdtsc rdtscp ret retf retn rol ror rdm rsdc rsldt rsm rsts sahf sal salc sar sbb scasb scasd scasq scasw sfence sgdt shl shld shr shrd sidt sldt skinit smi smint smintold smsw stc std sti stosb stosd stosq stosw str sub svdc svldt svts swapgs syscall sysenter sysexit sysret test ud0 ud1 ud2b ud2 ud2a umov verr verw fwait wbinvd wrshr wrmsr xadd xbts xchg xlatb xlat xor cmove cmovz cmovne cmovnz cmova cmovnbe cmovae cmovnb cmovb cmovnae cmovbe cmovna cmovg cmovnle cmovge cmovnl cmovl cmovnge cmovle cmovng cmovc cmovnc cmovo cmovno cmovs cmovns cmovp cmovpe cmovnp cmovpo je jz jne jnz ja jnbe jae jnb jb jnae jbe jna jg jnle jge jnl jl jnge jle jng jc jnc jo jno js jns jpo jnp jpe jp sete setz setne setnz seta setnbe setae setnb setnc setb setnae setcset setbe setna setg setnle setge setnl setl setnge setle setng sets setns seto setno setpe setp setpo setnp addps addss andnps andps cmpeqps cmpeqss cmpleps cmpless cmpltps cmpltss cmpneqps cmpneqss cmpnleps cmpnless cmpnltps cmpnltss cmpordps cmpordss cmpunordps cmpunordss cmpps cmpss comiss cvtpi2ps cvtps2pi cvtsi2ss cvtss2si cvttps2pi cvttss2si divps divss ldmxcsr maxps maxss minps minss movaps movhps movlhps movlps movhlps movmskps movntps movss movups mulps mulss orps rcpps rcpss rsqrtps rsqrtss shufps sqrtps sqrtss stmxcsr subps subss ucomiss unpckhps unpcklps xorps fxrstor fxrstor64 fxsave fxsave64 xgetbv xsetbv xsave xsave64 xsaveopt xsaveopt64 xrstor xrstor64 prefetchnta prefetcht0 prefetcht1 prefetcht2 maskmovq movntq pavgb pavgw pextrw pinsrw pmaxsw pmaxub pminsw pminub pmovmskb pmulhuw psadbw pshufw pf2iw pfnacc pfpnacc pi2fw pswapd maskmovdqu clflush movntdq movnti movntpd movdqa movdqu movdq2q movq2dq paddq pmuludq pshufd pshufhw pshuflw pslldq psrldq psubq punpckhqdq punpcklqdq addpd addsd andnpd andpd cmpeqpd cmpeqsd cmplepd cmplesd cmpltpd cmpltsd cmpneqpd cmpneqsd cmpnlepd cmpnlesd cmpnltpd cmpnltsd cmpordpd cmpordsd cmpunordpd cmpunordsd cmppd comisd cvtdq2pd cvtdq2ps cvtpd2dq cvtpd2pi cvtpd2ps cvtpi2pd cvtps2dq cvtps2pd cvtsd2si cvtsd2ss cvtsi2sd cvtss2sd cvttpd2pi cvttpd2dq cvttps2dq cvttsd2si divpd divsd maxpd maxsd minpd minsd movapd movhpd movlpd movmskpd movupd mulpd mulsd orpd shufpd sqrtpd sqrtsd subpd subsd ucomisd unpckhpd unpcklpd xorpd addsubpd addsubps haddpd haddps hsubpd hsubps lddqu movddup movshdup movsldup clgi stgi vmcall vmclear vmfunc vmlaunch vmload vmmcall vmptrld vmptrst vmread vmresume vmrun vmsave vmwrite vmxoff vmxon invept invvpid pabsb pabsw pabsd palignr phaddw phaddd phaddsw phsubw phsubd phsubsw pmaddubsw pmulhrsw pshufb psignb psignw psignd extrq insertq movntsd movntss lzcnt blendpd blendps blendvpd blendvps dppd dpps extractps insertps movntdqa mpsadbw packusdw pblendvb pblendw pcmpeqq pextrb pextrd pextrq phminposuw pinsrb pinsrd pinsrq pmaxsb pmaxsd pmaxud pmaxuw pminsb pminsd pminud pminuw pmovsxbw pmovsxbd pmovsxbq pmovsxwd pmovsxwq pmovsxdq pmovzxbw pmovzxbd pmovzxbq pmovzxwd pmovzxwq pmovzxdq pmuldq pmulld ptest roundpd roundps roundsd roundss crc32 pcmpestri pcmpestrm pcmpistri pcmpistrm pcmpgtq popcnt getsec pfrcpv pfrsqrtv movbe aesenc aesenclast aesdec aesdeclast aesimc aeskeygenassist vaesenc vaesenclast vaesdec vaesdeclast vaesimc vaeskeygenassist vaddpd vaddps vaddsd vaddss vaddsubpd vaddsubps vandpd vandps vandnpd vandnps vblendpd vblendps vblendvpd vblendvps vbroadcastss vbroadcastsd vbroadcastf128 vcmpeq_ospd vcmpeqpd vcmplt_ospd vcmpltpd vcmple_ospd vcmplepd vcmpunord_qpd vcmpunordpd vcmpneq_uqpd vcmpneqpd vcmpnlt_uspd vcmpnltpd vcmpnle_uspd vcmpnlepd vcmpord_qpd vcmpordpd vcmpeq_uqpd vcmpnge_uspd vcmpngepd vcmpngt_uspd vcmpngtpd vcmpfalse_oqpd vcmpfalsepd vcmpneq_oqpd vcmpge_ospd vcmpgepd vcmpgt_ospd vcmpgtpd vcmptrue_uqpd vcmptruepd vcmplt_oqpd vcmple_oqpd vcmpunord_spd vcmpneq_uspd vcmpnlt_uqpd vcmpnle_uqpd vcmpord_spd vcmpeq_uspd vcmpnge_uqpd vcmpngt_uqpd vcmpfalse_ospd vcmpneq_ospd vcmpge_oqpd vcmpgt_oqpd vcmptrue_uspd vcmppd vcmpeq_osps vcmpeqps vcmplt_osps vcmpltps vcmple_osps vcmpleps vcmpunord_qps vcmpunordps vcmpneq_uqps vcmpneqps vcmpnlt_usps vcmpnltps vcmpnle_usps vcmpnleps vcmpord_qps vcmpordps vcmpeq_uqps vcmpnge_usps vcmpngeps vcmpngt_usps vcmpngtps vcmpfalse_oqps vcmpfalseps vcmpneq_oqps vcmpge_osps vcmpgeps vcmpgt_osps vcmpgtps vcmptrue_uqps vcmptrueps vcmplt_oqps vcmple_oqps vcmpunord_sps vcmpneq_usps vcmpnlt_uqps vcmpnle_uqps vcmpord_sps vcmpeq_usps vcmpnge_uqps vcmpngt_uqps vcmpfalse_osps vcmpneq_osps vcmpge_oqps vcmpgt_oqps vcmptrue_usps vcmpps vcmpeq_ossd vcmpeqsd vcmplt_ossd vcmpltsd vcmple_ossd vcmplesd vcmpunord_qsd vcmpunordsd vcmpneq_uqsd vcmpneqsd vcmpnlt_ussd vcmpnltsd vcmpnle_ussd vcmpnlesd vcmpord_qsd vcmpordsd vcmpeq_uqsd vcmpnge_ussd vcmpngesd vcmpngt_ussd vcmpngtsd vcmpfalse_oqsd vcmpfalsesd vcmpneq_oqsd vcmpge_ossd vcmpgesd vcmpgt_ossd vcmpgtsd vcmptrue_uqsd vcmptruesd vcmplt_oqsd vcmple_oqsd vcmpunord_ssd vcmpneq_ussd vcmpnlt_uqsd vcmpnle_uqsd vcmpord_ssd vcmpeq_ussd vcmpnge_uqsd vcmpngt_uqsd vcmpfalse_ossd vcmpneq_ossd vcmpge_oqsd vcmpgt_oqsd vcmptrue_ussd vcmpsd vcmpeq_osss vcmpeqss vcmplt_osss vcmpltss vcmple_osss vcmpless vcmpunord_qss vcmpunordss vcmpneq_uqss vcmpneqss vcmpnlt_usss vcmpnltss vcmpnle_usss vcmpnless vcmpord_qss vcmpordss vcmpeq_uqss vcmpnge_usss vcmpngess vcmpngt_usss vcmpngtss vcmpfalse_oqss vcmpfalsess vcmpneq_oqss vcmpge_osss vcmpgess vcmpgt_osss vcmpgtss vcmptrue_uqss vcmptruess vcmplt_oqss vcmple_oqss vcmpunord_sss vcmpneq_usss vcmpnlt_uqss vcmpnle_uqss vcmpord_sss vcmpeq_usss vcmpnge_uqss vcmpngt_uqss vcmpfalse_osss vcmpneq_osss vcmpge_oqss vcmpgt_oqss vcmptrue_usss vcmpss vcomisd vcomiss vcvtdq2pd vcvtdq2ps vcvtpd2dq vcvtpd2ps vcvtps2dq vcvtps2pd vcvtsd2si vcvtsd2ss vcvtsi2sd vcvtsi2ss vcvtss2sd vcvtss2si vcvttpd2dq vcvttps2dq vcvttsd2si vcvttss2si vdivpd vdivps vdivsd vdivss vdppd vdpps vextractf128 vextractps vhaddpd vhaddps vhsubpd vhsubps vinsertf128 vinsertps vlddqu vldqqu vldmxcsr vmaskmovdqu vmaskmovps vmaskmovpd vmaxpd vmaxps vmaxsd vmaxss vminpd vminps vminsd vminss vmovapd vmovaps vmovd vmovq vmovddup vmovdqa vmovqqa vmovdqu vmovqqu vmovhlps vmovhpd vmovhps vmovlhps vmovlpd vmovlps vmovmskpd vmovmskps vmovntdq vmovntqq vmovntdqa vmovntpd vmovntps vmovsd vmovshdup vmovsldup vmovss vmovupd vmovups vmpsadbw vmulpd vmulps vmulsd vmulss vorpd vorps vpabsb vpabsw vpabsd vpacksswb vpackssdw vpackuswb vpackusdw vpaddb vpaddw vpaddd vpaddq vpaddsb vpaddsw vpaddusb vpaddusw vpalignr vpand vpandn vpavgb vpavgw vpblendvb vpblendw vpcmpestri vpcmpestrm vpcmpistri vpcmpistrm vpcmpeqb vpcmpeqw vpcmpeqd vpcmpeqq vpcmpgtb vpcmpgtw vpcmpgtd vpcmpgtq vpermilpd vpermilps vperm2f128 vpextrb vpextrw vpextrd vpextrq vphaddw vphaddd vphaddsw vphminposuw vphsubw vphsubd vphsubsw vpinsrb vpinsrw vpinsrd vpinsrq vpmaddwd vpmaddubsw vpmaxsb vpmaxsw vpmaxsd vpmaxub vpmaxuw vpmaxud vpminsb vpminsw vpminsd vpminub vpminuw vpminud vpmovmskb vpmovsxbw vpmovsxbd vpmovsxbq vpmovsxwd vpmovsxwq vpmovsxdq vpmovzxbw vpmovzxbd vpmovzxbq vpmovzxwd vpmovzxwq vpmovzxdq vpmulhuw vpmulhrsw vpmulhw vpmullw vpmulld vpmuludq vpmuldq vpor vpsadbw vpshufb vpshufd vpshufhw vpshuflw vpsignb vpsignw vpsignd vpslldq vpsrldq vpsllw vpslld vpsllq vpsraw vpsrad vpsrlw vpsrld vpsrlq vptest vpsubb vpsubw vpsubd vpsubq vpsubsb vpsubsw vpsubusb vpsubusw vpunpckhbw vpunpckhwd vpunpckhdq vpunpckhqdq vpunpcklbw vpunpcklwd vpunpckldq vpunpcklqdq vpxor vrcpps vrcpss vrsqrtps vrsqrtss vroundpd vroundps vroundsd vroundss vshufpd vshufps vsqrtpd vsqrtps vsqrtsd vsqrtss vstmxcsr vsubpd vsubps vsubsd vsubss vtestps vtestpd vucomisd vucomiss vunpckhpd vunpckhps vunpcklpd vunpcklps vxorpd vxorps vzeroall vzeroupper pclmullqlqdq pclmulhqlqdq pclmullqhqdq pclmulhqhqdq pclmulqdq vpclmullqlqdq vpclmulhqlqdq vpclmullqhqdq vpclmulhqhqdq vpclmulqdq vfmadd132ps vfmadd132pd vfmadd312ps vfmadd312pd vfmadd213ps vfmadd213pd vfmadd123ps vfmadd123pd vfmadd231ps vfmadd231pd vfmadd321ps vfmadd321pd vfmaddsub132ps vfmaddsub132pd vfmaddsub312ps vfmaddsub312pd vfmaddsub213ps vfmaddsub213pd vfmaddsub123ps vfmaddsub123pd vfmaddsub231ps vfmaddsub231pd vfmaddsub321ps vfmaddsub321pd vfmsub132ps vfmsub132pd vfmsub312ps vfmsub312pd vfmsub213ps vfmsub213pd vfmsub123ps vfmsub123pd vfmsub231ps vfmsub231pd vfmsub321ps vfmsub321pd vfmsubadd132ps vfmsubadd132pd vfmsubadd312ps vfmsubadd312pd vfmsubadd213ps vfmsubadd213pd vfmsubadd123ps vfmsubadd123pd vfmsubadd231ps vfmsubadd231pd vfmsubadd321ps vfmsubadd321pd vfnmadd132ps vfnmadd132pd vfnmadd312ps vfnmadd312pd vfnmadd213ps vfnmadd213pd vfnmadd123ps vfnmadd123pd vfnmadd231ps vfnmadd231pd vfnmadd321ps vfnmadd321pd vfnmsub132ps vfnmsub132pd vfnmsub312ps vfnmsub312pd vfnmsub213ps vfnmsub213pd vfnmsub123ps vfnmsub123pd vfnmsub231ps vfnmsub231pd vfnmsub321ps vfnmsub321pd vfmadd132ss vfmadd132sd vfmadd312ss vfmadd312sd vfmadd213ss vfmadd213sd vfmadd123ss vfmadd123sd vfmadd231ss vfmadd231sd vfmadd321ss vfmadd321sd vfmsub132ss vfmsub132sd vfmsub312ss vfmsub312sd vfmsub213ss vfmsub213sd vfmsub123ss vfmsub123sd vfmsub231ss vfmsub231sd vfmsub321ss vfmsub321sd vfnmadd132ss vfnmadd132sd vfnmadd312ss vfnmadd312sd vfnmadd213ss vfnmadd213sd vfnmadd123ss vfnmadd123sd vfnmadd231ss vfnmadd231sd vfnmadd321ss vfnmadd321sd vfnmsub132ss vfnmsub132sd vfnmsub312ss vfnmsub312sd vfnmsub213ss vfnmsub213sd vfnmsub123ss vfnmsub123sd vfnmsub231ss vfnmsub231sd vfnmsub321ss vfnmsub321sd rdfsbase rdgsbase rdrand wrfsbase wrgsbase vcvtph2ps vcvtps2ph adcx adox rdseed clac stac xstore xcryptecb xcryptcbc xcryptctr xcryptcfb xcryptofb montmul xsha1 xsha256 llwpcb slwpcb lwpval lwpins vfmaddpd vfmaddps vfmaddsd vfmaddss vfmaddsubpd vfmaddsubps vfmsubaddpd vfmsubaddps vfmsubpd vfmsubps vfmsubsd vfmsubss vfnmaddpd vfnmaddps vfnmaddsd vfnmaddss vfnmsubpd vfnmsubps vfnmsubsd vfnmsubss vfrczpd vfrczps vfrczsd vfrczss vpcmov vpcomb vpcomd vpcomq vpcomub vpcomud vpcomuq vpcomuw vpcomw vphaddbd vphaddbq vphaddbw vphadddq vphaddubd vphaddubq vphaddubw vphaddudq vphadduwd vphadduwq vphaddwd vphaddwq vphsubbw vphsubdq vphsubwd vpmacsdd vpmacsdqh vpmacsdql vpmacssdd vpmacssdqh vpmacssdql vpmacsswd vpmacssww vpmacswd vpmacsww vpmadcsswd vpmadcswd vpperm vprotb vprotd vprotq vprotw vpshab vpshad vpshaq vpshaw vpshlb vpshld vpshlq vpshlw vbroadcasti128 vpblendd vpbroadcastb vpbroadcastw vpbroadcastd vpbroadcastq vpermd vpermpd vpermps vpermq vperm2i128 vextracti128 vinserti128 vpmaskmovd vpmaskmovq vpsllvd vpsllvq vpsravd vpsrlvd vpsrlvq vgatherdpd vgatherqpd vgatherdps vgatherqps vpgatherdd vpgatherqd vpgatherdq vpgatherqq xabort xbegin xend xtest andn bextr blci blcic blsi blsic blcfill blsfill blcmsk blsmsk blsr blcs bzhi mulx pdep pext rorx sarx shlx shrx tzcnt tzmsk t1mskc valignd valignq vblendmpd vblendmps vbroadcastf32x4 vbroadcastf64x4 vbroadcasti32x4 vbroadcasti64x4 vcompresspd vcompressps vcvtpd2udq vcvtps2udq vcvtsd2usi vcvtss2usi vcvttpd2udq vcvttps2udq vcvttsd2usi vcvttss2usi vcvtudq2pd vcvtudq2ps vcvtusi2sd vcvtusi2ss vexpandpd vexpandps vextractf32x4 vextractf64x4 vextracti32x4 vextracti64x4 vfixupimmpd vfixupimmps vfixupimmsd vfixupimmss vgetexppd vgetexpps vgetexpsd vgetexpss vgetmantpd vgetmantps vgetmantsd vgetmantss vinsertf32x4 vinsertf64x4 vinserti32x4 vinserti64x4 vmovdqa32 vmovdqa64 vmovdqu32 vmovdqu64 vpabsq vpandd vpandnd vpandnq vpandq vpblendmd vpblendmq vpcmpltd vpcmpled vpcmpneqd vpcmpnltd vpcmpnled vpcmpd vpcmpltq vpcmpleq vpcmpneqq vpcmpnltq vpcmpnleq vpcmpq vpcmpequd vpcmpltud vpcmpleud vpcmpnequd vpcmpnltud vpcmpnleud vpcmpud vpcmpequq vpcmpltuq vpcmpleuq vpcmpnequq vpcmpnltuq vpcmpnleuq vpcmpuq vpcompressd vpcompressq vpermi2d vpermi2pd vpermi2ps vpermi2q vpermt2d vpermt2pd vpermt2ps vpermt2q vpexpandd vpexpandq vpmaxsq vpmaxuq vpminsq vpminuq vpmovdb vpmovdw vpmovqb vpmovqd vpmovqw vpmovsdb vpmovsdw vpmovsqb vpmovsqd vpmovsqw vpmovusdb vpmovusdw vpmovusqb vpmovusqd vpmovusqw vpord vporq vprold vprolq vprolvd vprolvq vprord vprorq vprorvd vprorvq vpscatterdd vpscatterdq vpscatterqd vpscatterqq vpsraq vpsravq vpternlogd vpternlogq vptestmd vptestmq vptestnmd vptestnmq vpxord vpxorq vrcp14pd vrcp14ps vrcp14sd vrcp14ss vrndscalepd vrndscaleps vrndscalesd vrndscaless vrsqrt14pd vrsqrt14ps vrsqrt14sd vrsqrt14ss vscalefpd vscalefps vscalefsd vscalefss vscatterdpd vscatterdps vscatterqpd vscatterqps vshuff32x4 vshuff64x2 vshufi32x4 vshufi64x2 kandnw kandw kmovw knotw kortestw korw kshiftlw kshiftrw kunpckbw kxnorw kxorw vpbroadcastmb2q vpbroadcastmw2d vpconflictd vpconflictq vplzcntd vplzcntq vexp2pd vexp2ps vrcp28pd vrcp28ps vrcp28sd vrcp28ss vrsqrt28pd vrsqrt28ps vrsqrt28sd vrsqrt28ss vgatherpf0dpd vgatherpf0dps vgatherpf0qpd vgatherpf0qps vgatherpf1dpd vgatherpf1dps vgatherpf1qpd vgatherpf1qps vscatterpf0dpd vscatterpf0dps vscatterpf0qpd vscatterpf0qps vscatterpf1dpd vscatterpf1dps vscatterpf1qpd vscatterpf1qps prefetchwt1 bndmk bndcl bndcu bndcn bndmov bndldx bndstx sha1rnds4 sha1nexte sha1msg1 sha1msg2 sha256rnds2 sha256msg1 sha256msg2 hint_nop0 hint_nop1 hint_nop2 hint_nop3 hint_nop4 hint_nop5 hint_nop6 hint_nop7 hint_nop8 hint_nop9 hint_nop10 hint_nop11 hint_nop12 hint_nop13 hint_nop14 hint_nop15 hint_nop16 hint_nop17 hint_nop18 hint_nop19 hint_nop20 hint_nop21 hint_nop22 hint_nop23 hint_nop24 hint_nop25 hint_nop26 hint_nop27 hint_nop28 hint_nop29 hint_nop30 hint_nop31 hint_nop32 hint_nop33 hint_nop34 hint_nop35 hint_nop36 hint_nop37 hint_nop38 hint_nop39 hint_nop40 hint_nop41 hint_nop42 hint_nop43 hint_nop44 hint_nop45 hint_nop46 hint_nop47 hint_nop48 hint_nop49 hint_nop50 hint_nop51 hint_nop52 hint_nop53 hint_nop54 hint_nop55 hint_nop56 hint_nop57 hint_nop58 hint_nop59 hint_nop60 hint_nop61 hint_nop62 hint_nop63", 
  built_in:"ip eip rip al ah bl bh cl ch dl dh sil dil bpl spl r8b r9b r10b r11b r12b r13b r14b r15b ax bx cx dx si di bp sp r8w r9w r10w r11w r12w r13w r14w r15w eax ebx ecx edx esi edi ebp esp eip r8d r9d r10d r11d r12d r13d r14d r15d rax rbx rcx rdx rsi rdi rbp rsp r8 r9 r10 r11 r12 r13 r14 r15 cs ds es fs gs ss st st0 st1 st2 st3 st4 st5 st6 st7 mm0 mm1 mm2 mm3 mm4 mm5 mm6 mm7 xmm0  xmm1  xmm2  xmm3  xmm4  xmm5  xmm6  xmm7  xmm8  xmm9 xmm10  xmm11 xmm12 xmm13 xmm14 xmm15 xmm16 xmm17 xmm18 xmm19 xmm20 xmm21 xmm22 xmm23 xmm24 xmm25 xmm26 xmm27 xmm28 xmm29 xmm30 xmm31 ymm0  ymm1  ymm2  ymm3  ymm4  ymm5  ymm6  ymm7  ymm8  ymm9 ymm10  ymm11 ymm12 ymm13 ymm14 ymm15 ymm16 ymm17 ymm18 ymm19 ymm20 ymm21 ymm22 ymm23 ymm24 ymm25 ymm26 ymm27 ymm28 ymm29 ymm30 ymm31 zmm0  zmm1  zmm2  zmm3  zmm4  zmm5  zmm6  zmm7  zmm8  zmm9 zmm10  zmm11 zmm12 zmm13 zmm14 zmm15 zmm16 zmm17 zmm18 zmm19 zmm20 zmm21 zmm22 zmm23 zmm24 zmm25 zmm26 zmm27 zmm28 zmm29 zmm30 zmm31 k0 k1 k2 k3 k4 k5 k6 k7 bnd0 bnd1 bnd2 bnd3 cr0 cr1 cr2 cr3 cr4 cr8 dr0 dr1 dr2 dr3 dr8 tr3 tr4 tr5 tr6 tr7 r0 r1 r2 r3 r4 r5 r6 r7 r0b r1b r2b r3b r4b r5b r6b r7b r0w r1w r2w r3w r4w r5w r6w r7w r0d r1d r2d r3d r4d r5d r6d r7d r0h r1h r2h r3h r0l r1l r2l r3l r4l r5l r6l r7l r8l r9l r10l r11l r12l r13l r14l r15l db dw dd dq dt ddq do dy dz resb resw resd resq rest resdq reso resy resz incbin equ times byte word dword qword nosplit rel abs seg wrt strict near far a32 ptr", 
  meta:"%define %xdefine %+ %undef %defstr %deftok %assign %strcat %strlen %substr %rotate %elif %else %endif %if %ifmacro %ifctx %ifidn %ifidni %ifid %ifnum %ifstr %iftoken %ifempty %ifenv %error %warning %fatal %rep %endrep %include %push %pop %repl %pathsearch %depend %use %arg %stacksize %local %line %comment %endcomment .nolist __FILE__ __LINE__ __SECT__  __BITS__ __OUTPUT_FORMAT__ __DATE__ __TIME__ __DATE_NUM__ __TIME_NUM__ __UTC_DATE__ __UTC_TIME__ __UTC_DATE_NUM__ __UTC_TIME_NUM__  __PASS__ struc endstruc istruc at iend align alignb sectalign daz nodaz up down zero default option assume public bits use16 use32 use64 default section segment absolute extern global common cpu float __utf16__ __utf16le__ __utf16be__ __utf32__ __utf32le__ __utf32be__ __float8__ __float16__ __float32__ __float64__ __float80m__ __float80e__ __float128l__ __float128h__ __Infinity__ __QNaN__ __SNaN__ Inf NaN QNaN SNaN float8 float16 float32 float64 float80m float80e float128l float128h __FLOAT_DAZ__ __FLOAT_ROUND__ __FLOAT__"}, 
  contains:[b.COMMENT(";", "$", {relevance:0}), {className:"number", variants:[{begin:"\\b(?:([0-9][0-9_]*)?\\.[0-9_]*(?:[eE][+-]?[0-9_]+)?|(0[Xx])?[0-9][0-9_]*(\\.[0-9_]*)?(?:[pP](?:[+-]?[0-9_]+)?)?)\\b", relevance:0}, {begin:"\\$[0-9][0-9A-Fa-f]*", relevance:0}, {begin:"\\b(?:[0-9A-Fa-f][0-9A-Fa-f_]*[Hh]|[0-9][0-9_]*[DdTt]?|[0-7][0-7_]*[QqOo]|[0-1][0-1_]*[BbYy])\\b"}, {begin:"\\b(?:0[Xx][0-9A-Fa-f_]+|0[DdTt][0-9_]+|0[QqOo][0-7_]+|0[BbYy][0-1_]+)\\b"}]}, b.QUOTE_STRING_MODE, {className:"string", 
  variants:[{begin:"'", end:"[^\\\\]'"}, {begin:"`", end:"[^\\\\]`"}], relevance:0}, {className:"symbol", variants:[{begin:"^\\s*[A-Za-z._?][A-Za-z0-9_$#@~.?]*(:|\\s+label)"}, {begin:"^\\s*%%[A-Za-z0-9_$#@~.?]*:"}], relevance:0}, {className:"subst", begin:"%[0-9]+", relevance:0}, {className:"subst", begin:"%!S+", relevance:0}, {className:"meta", begin:/^\s*\.[\w_-]+/}]}))();
  hljs.registerLanguage("x86asm", g);
})();
(() => {
  var g = (() => b => {
    const f = b.regex, l = f.concat(/[\p{L}_]/u, f.optional(/[\p{L}0-9_.-]*:/u), /[\p{L}0-9_.-]*/u), n = {className:"symbol", begin:/&[a-z]+;|&#[0-9]+;|&#x[a-f0-9]+;/}, q = {begin:/\s/, contains:[{className:"keyword", begin:/#?[a-z_][a-z1-9_-]+/, illegal:/\n/}]}, d = b.inherit(q, {begin:/\(/, end:/\)/}), w = b.inherit(b.APOS_STRING_MODE, {className:"string"}), t = b.inherit(b.QUOTE_STRING_MODE, {className:"string"}), v = {endsWithParent:!0, illegal:/</, relevance:0, contains:[{className:"attr", begin:/[\p{L}0-9._:-]+/u, 
    relevance:0}, {begin:/=\s*/, relevance:0, contains:[{className:"string", endsParent:!0, variants:[{begin:/"/, end:/"/, contains:[n]}, {begin:/'/, end:/'/, contains:[n]}, {begin:/[^\s"'=<>`]+/}]}]}]};
    return {name:"HTML, XML", aliases:"html xhtml rss atom xjb xsd xsl plist wsf svg".split(" "), case_insensitive:!0, unicodeRegex:!0, contains:[{className:"meta", begin:/<![a-z]/, end:/>/, relevance:10, contains:[q, t, w, d, {begin:/\[/, end:/\]/, contains:[{className:"meta", begin:/<![a-z]/, end:/>/, contains:[q, d, t, w]}]}]}, b.COMMENT(/\x3c!--/, /--\x3e/, {relevance:10}), {begin:/<!\[CDATA\[/, end:/\]\]>/, relevance:10}, n, {className:"meta", end:/\?>/, variants:[{begin:/<\?xml/, relevance:10, 
    contains:[t]}, {begin:/<\?[a-z][a-z0-9]+/}]}, {className:"tag", begin:/<style(?=\s|>)/, end:/>/, keywords:{name:"style"}, contains:[v], starts:{end:/<\/style>/, returnEnd:!0, subLanguage:["css", "xml"]}}, {className:"tag", begin:/<script(?=\s|>)/, end:/>/, keywords:{name:"script"}, contains:[v], starts:{end:/<\/script>/, returnEnd:!0, subLanguage:["javascript", "handlebars", "xml"]}}, {className:"tag", begin:/<>|<\/>/}, {className:"tag", begin:f.concat(/</, f.lookahead(f.concat(l, f.either(/\/>/, 
    />/, /\s/)))), end:/\/?>/, contains:[{className:"name", begin:l, relevance:0, starts:v}]}, {className:"tag", begin:f.concat(/<\//, f.lookahead(f.concat(l, />/))), contains:[{className:"name", begin:l, relevance:0}, {begin:/>/, relevance:0, endsParent:!0}]}]};
  })();
  hljs.registerLanguage("xml", g);
})();
(() => {
  var g = (() => b => {
    var f = {className:"string", relevance:0, variants:[{begin:/'/, end:/'/}, {begin:/"/, end:/"/}, {begin:/\S+/}], contains:[b.BACKSLASH_ESCAPE, {className:"template-variable", variants:[{begin:/\{\{/, end:/\}\}/}, {begin:/%\{/, end:/\}/}]}]};
    const l = b.inherit(f, {variants:[{begin:/'/, end:/'/}, {begin:/"/, end:/"/}, {begin:/[^\s,{}[\]]+/}]}), n = {end:",", endsWithParent:!0, excludeEnd:!0, keywords:"true false yes no null", relevance:0};
    b = [{className:"attr", variants:[{begin:/\w[\w :()\./-]*:(?=[ \t]|$)/}, {begin:/"\w[\w :()\./-]*":(?=[ \t]|$)/}, {begin:/'\w[\w :()\./-]*':(?=[ \t]|$)/}]}, {className:"meta", begin:"^---\\s*$", relevance:10}, {className:"string", begin:"[\\|>]([1-9]?[+-])?[ ]*\\n( +)[^ ][^\\n]*\\n(\\2[^\\n]+\\n?)*"}, {begin:"<%[%=-]?", end:"[%-]?%>", subLanguage:"ruby", excludeBegin:!0, excludeEnd:!0, relevance:0}, {className:"type", begin:"!\\w+![\\w#;/?:@&=+$,.~*'()[\\]]+"}, {className:"type", begin:"!<[\\w#;/?:@&=+$,.~*'()[\\]]+>"}, 
    {className:"type", begin:"![\\w#;/?:@&=+$,.~*'()[\\]]+"}, {className:"type", begin:"!![\\w#;/?:@&=+$,.~*'()[\\]]+"}, {className:"meta", begin:"&" + b.UNDERSCORE_IDENT_RE + "$"}, {className:"meta", begin:"\\*" + b.UNDERSCORE_IDENT_RE + "$"}, {className:"bullet", begin:"-(?=[ ]|$)", relevance:0}, b.HASH_COMMENT_MODE, {beginKeywords:"true false yes no null", keywords:{literal:"true false yes no null"}}, {className:"number", begin:"\\b[0-9]{4}(-[0-9][0-9]){0,2}([Tt \\t][0-9][0-9]?(:[0-9][0-9]){2})?(\\.[0-9]*)?([ \\t])*(Z|[-+][0-9][0-9]?(:[0-9][0-9])?)?\\b"}, 
    {className:"number", begin:b.C_NUMBER_RE + "\\b", relevance:0}, {begin:/\{/, end:/\}/, contains:[n], illegal:"\\n", relevance:0}, {begin:"\\[", end:"\\]", contains:[n], illegal:"\\n", relevance:0}, f];
    f = [...b];
    return f.pop(), f.push(l), n.contains = f, {name:"YAML", case_insensitive:!0, aliases:["yml"], contains:b};
  })();
  hljs.registerLanguage("yaml", g);
})();
!function(g, b) {
  function f(t) {
    try {
      var v = b.querySelectorAll("code.hljs,code.nohighlight"), x;
      for (x in v) {
        v.hasOwnProperty(x) && (v[x].classList.contains("nohljsln") || l(v[x], t));
      }
    } catch (C) {
      g.console.error("LineNumbers error: ", C);
    }
  }
  function l(t, v) {
    "object" == typeof t && g.setTimeout(function() {
      t.innerHTML = n(t, v);
    }, 0);
  }
  function n(t, v) {
    var x;
    v = {singleLine:function(C) {
      return !!C.singleLine && C.singleLine;
    }(x = (x = v) || {}), startFrom:function(C, E) {
      var L = 1;
      isFinite(E.startFrom) && (L = E.startFrom);
      C = function(I, M) {
        return I.hasAttribute(M) ? I.getAttribute(M) : null;
      }(C, "data-ln-start-from");
      return null !== C && (L = function(I, M) {
        if (!I) {
          return M;
        }
        I = Number(I);
        return isFinite(I) ? I : M;
      }(C, 1)), L;
    }(t, x)};
    return function L(E) {
      E = E.childNodes;
      for (var I in E) {
        if (E.hasOwnProperty(I)) {
          var M = E[I];
          if (0 < (M.textContent.trim().match(w) || []).length) {
            if (0 < M.childNodes.length) {
              L(M);
            } else {
              M = M.parentNode;
              var T = M.className;
              if (/hljs-/.test(T)) {
                var a = M.innerHTML;
                a = 0 === a.length ? [] : a.split(w);
                for (var e = 0, c = ""; e < a.length; e++) {
                  c += q('<span class="{0}">{1}</span>\n', [T, 0 < a[e].length ? a[e] : " "]);
                }
                M.innerHTML = c.trim();
              }
            }
          }
        }
      }
    }(t), function(E, L) {
      var I = 0 === E.length ? [] : E.split(w);
      "" === I[I.length - 1].trim() && I.pop();
      if (1 < I.length || L.singleLine) {
        E = "";
        for (var M = 0, T = I.length; M < T; M++) {
          E += q('<tr><td class="{0} {1}" {3}="{5}"><div class="{2}" {3}="{5}"></div></td><td class="{0} {4}" {3}="{5}">{6}</td></tr>', ["hljs-ln-line", "hljs-ln-numbers", "hljs-ln-n", "data-line-number", "hljs-ln-code", M + L.startFrom, 0 < I[M].length ? I[M] : " "]);
        }
        return q('<table class="{0}">{1}</table>', ["hljs-ln", E]);
      }
      return E;
    }(t.innerHTML, v);
  }
  function q(t, v) {
    return t.replace(/\{(\d+)\}/g, function(x, C) {
      return void 0 !== v[C] ? v[C] : x;
    });
  }
  var d, w = /\r\n|\r|\n/g;
  g.hljs ? (g.hljs.initLineNumbersOnLoad = function(t) {
    "interactive" === b.readyState || "complete" === b.readyState ? f(t) : g.addEventListener("DOMContentLoaded", function() {
      f(t);
    });
  }, g.hljs.lineNumbersBlock = l, g.hljs.lineNumbersValue = function(t, v) {
    if ("string" == typeof t) {
      var x = document.createElement("code");
      return x.innerHTML = t, n(x, v);
    }
  }, (d = b.createElement("style")).type = "text/css", d.innerHTML = q(".{0}{border-collapse:collapse}.{0} td{padding:0}.{1}:before{content:attr({2})}", ["hljs-ln", "hljs-ln-n", "data-line-number"]), b.getElementsByTagName("head")[0].appendChild(d)) : g.console.error("highlight.js not detected!");
  document.addEventListener("copy", function(t) {
    var v = window.getSelection(), x;
    a: {
      for (x = v.anchorNode; x;) {
        if (x.className && -1 !== x.className.indexOf("hljs-ln-code")) {
          x = 1;
          break a;
        }
        x = x.parentNode;
      }
      x = void 0;
    }
    if (x) {
      if (-1 !== window.navigator.userAgent.indexOf("Edge")) {
        for (var C = v.toString(), E = v.anchorNode; "TD" !== E.nodeName;) {
          E = E.parentNode;
        }
        for (x = v.focusNode; "TD" !== x.nodeName;) {
          x = x.parentNode;
        }
        var L = parseInt(E.dataset.lineNumber);
        v = parseInt(x.dataset.lineNumber);
        if (L == v) {
          v = C;
        } else {
          var I, M = E.textContent;
          x = x.textContent;
          for (v < L && (I = L, L = v, v = I, I = M, M = x, x = I); 0 !== C.indexOf(M);) {
            M = M.slice(1);
          }
          for (; -1 === C.lastIndexOf(x);) {
            x = x.slice(0, -1);
          }
          for (I = M; "TABLE" !== E.nodeName;) {
            E = E.parentNode;
          }
          for (L += 1; L < v; ++L) {
            C = q('.{0}[{1}="{2}"]', ["hljs-ln-code", "data-line-number", L]), I += "\n" + E.querySelector(C).textContent;
          }
          v = I + ("\n" + x);
        }
      } else {
        v = v.toString();
      }
      t.clipboardData.setData("text/plain", v);
      t.preventDefault();
    }
  });
}(window, document);

