﻿#
# file         : 20210427°1511 dropdown/build.py
# encoding     : UTF-8-with-BOM
# interpeter   : Python 3.7.8
# requirements : • Closure Compiler on drive • Java path set • Python path set

"""
   This minifies DropDown
"""

import os, sys

# Provide setting(2)
sBinGoCloCom = 'G:/work/gipsydrive/app.composer/trunk/bin/goclocom/closure-compiler-v20210406.jar'

# Provide constant(s)
sSp = ' '

# Prologue
print('* DropDown build *')
os.chdir(os.path.dirname(__file__))

# Build yes or no?
bBuild = True
if os.path.isfile('./dropdown2.min.js') :
   if os.path.getmtime('./dropdown2.js') < os.path.getmtime('./dropdown2.min.js') :
      bBuild = False

# Assemble command
sCmd = 'java.exe -jar' + sSp + sBinGoCloCom \
      + sSp + './dropdown2.js' \
      + sSp + '--js_output_file' + sSp + './dropdown2.min.js' \
      + sSp + '--create_source_map' + sSp + './dropdown2.min.js.map' \
      + sSp + '--formatting' + sSp + 'PRETTY_PRINT' \
      + sSp + '--charset UTF-8'

# Perform task
if bBuild == True :
   print(' - building ...')
   os.system(sCmd)
else :
   print(' - is up-to-date')

print(' - DropDown done.')
