﻿
   About folder daftari/phplibs/sitemap-php [folder ~20140620.1651]

   with file Sitemap.php [file ~20140620.1652]

   The current file version [version 20190311°1026] :
   • extracted from file sitemap-php-master.zip [file 20190311°1023]
   • downloaded from https://github.com/evert/sitemap-php [dld 20190311°1011]

   About sitemap files specifications see
   • https://www.sitemaps.org/protocol.html [ref 20130504°1331]

   ———————————————————————
   Project log :

   downloads 20190311°1011 three forks
   See https://downtown.trilo.de/svn/dow20190106/trunk/20190311o1011.sitemap-php

   downlad 20161010°1211 three different forks
   Fork https://github.com/evert/sitemap-php with newer version

   note 20161008°0731
   Page http://github.com/osmanungur/sitemap-php is no more available.
   It moved to https://github.com/o/sitemap-php. Use ersion from there.

   note 20140620°1643
   See file daftari/../Sitemap.20140620o1643.php

   note 20140620.1643
   File https://github.com/o/sitemap-php/blob/master/Sitemap.php
   downloaded for possible later use.
   See also the fork on https://github.com/evert/sitemap-php/.

   ref 20140620°1642
   url : https://github.com/o/sitemap-php

   ———————————————————————
   [file 20140620°1647] ◦Ω
