﻿
   img 20040101°1826 [[20040101°182403]]
   objecttype  : img.digitalphoto
   title       : Weihnachtslandschaft
   location    : Österreich
   license     : CC BY-SA 3.0 http://creativecommons.org/licenses/by-sa/3.0 [ref 20110922°2121]
   copyright   : © Dr. Gudrun Mueller
   author      : Dr. Gudrun Mueller
   albums      : nature
   camera      :
   processing  : v1 GIMP square; brightness +20; contrast +50; colorbalance blue -30 midtones preserve-luminosity;
   quality     :
   note        :
   ⬞Ω
