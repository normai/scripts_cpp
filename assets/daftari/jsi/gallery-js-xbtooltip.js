/*!
 *  id          : 20110512°1341 daftari/jsi/gallery-js-xbtooltip.js
 *  summary     : This module provides tooltips
 *  license     : Unspecified
 *  copyright   : © 2010 webmatze.de
 *  authors     : Mathias Karstädt, modified by Norbert C. Maier
 *  reference   : webmatze.de → Ein einfacher Cross-Browser Tooltip mit Javascript und CSS [ref 20201213°0922, 20110511°0122]
 *     web.archive.org/web/20180815164838/http://webmatze.de/ein-einfacher-cross-browser-tooltip-mit-javascript-und-css/
 *  callers     :
 */

// Options for JSHint [seq 20190423°0421`13]
/* jshint laxbreak : true, laxcomma : true */
/* globals Daf */

// [chg 20190404°0755] Shift XBT wrapper
// Function 20110511°1321 'the XBT wrapper' experimentally shifted to daftari.js 

/**
 *  This function constitutes the XBTooltip itself
 *
 *  @id 20110511°1322
 *  @do Process todo 20190404°0753 'Format tooltip when called with text parameter'
 *  @callers
 *  @param {Object} window — ..
 *  @param {Object} document — ..
 *  @return {undefined} —
 */
( function ( window, document ) {                                       // [marker 20210416°1633`55 GoCloCom]

    'use strict';

    /**
     *  Announce the Daftari root namespace
     *
     *  @id 20170923°0421`12
     *  @const — Namespace
     */
    Daf = window.Daf || {};

    var XBTooltip = function( element, userConf, tooltip)
    {
        var config =
        {
            id : userConf.id || undefined,
            className : userConf.className || undefined,
            x : userConf.x || 20,
            y : userConf.y || 20,
            text : userConf.text || undefined
        };

        // What shall the parameter mean? Seems superfluous! [note 20210416°1732`01]
        var over = function(event)
        {
            tooltip.style.display = "block";
        };

        // What shall the parameter mean? Seems superfluous! [note 20210416°1732`02]
        var out = function(event)
        {
            tooltip.style.display = "none";
        };

        // ()
        var move = function(event)
        {
            event = event ? event : window.event;
            if ( event.pageX === null && event.clientX !== null )
            {
                var doc = document.documentElement, body = document.body;
                event.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                event.pageY = event.clientY + (doc && doc.scrollTop  || body && body.scrollTop  || 0) - (doc && doc.clientTop  || body && body.clientTop  || 0);
            }
            // //tooltip.style.top = (event.pageY + config.y) + "px";   // Original line, replaced in below sequence
            // //tooltip.style.left = (event.pageX + config.x) + "px";  // Original line, replaced in below sequence

            //--------------------
            // Calculate position after quadrant [seq 20150313°2011]
            // summary : This calculates the position of the tooltip after the viewport
            //     quadrant. If upper left, go lower right. If lower right, go upper left.
            // note : This sequence is partially redundand with 20150313°1841.
            var vp = Daf.Utlis.getViewPortSize();
            if ( event.pageX > (vp.iWidth / 2 )) {
                tooltip.style.left = (event.pageX - config.x - tooltip.offsetWidth) + "px";
            }
            else {
                tooltip.style.left = (event.pageX + config.x) + "px";
            }
            if ( event.pageY > (vp.iHeight / 2)) {
                tooltip.style.top = (event.pageY - config.y - tooltip.offsetHeight) + "px";
            }
            else {
                tooltip.style.top = (event.pageY + config.y) + "px";
            }
            //--------------------
        };

        // ()
        if ((typeof tooltip === 'undefined') && config.id) {
            tooltip = document.getElementById(config.id);
            if (tooltip) tooltip = tooltip.parentNode.removeChild(tooltip);
        }
        if ((typeof tooltip === 'undefined') && config.text) {
            tooltip = document.createElement("div");
            if (config.id) tooltip.id= config.id;
            tooltip.innerHTML = config.text;
        }
        if (config.className) {
            tooltip.className = config.className;
        }

        // () Catch IE quirk [seq 20120901°1211]
        // IE throws error here if something is not very correct
        try {
            tooltip = document.body.appendChild(tooltip);
        }
        catch (exc) {
            // Provisory - just skip the tooltip
            return;
        }

        // ()
        tooltip.style.position = "absolute";
        element.onmouseover = over;
        element.onmouseout = out;
        element.onmousemove = move;

        // () Why the parameter? [note 20210416°1732`03]
        over(null);
    };
    window.XBTooltip = window.XBT = XBTooltip;
})(this, this.document);

// eof
