<?php //session_start();

   // file 20231009°1111
   // summary : Debug issue 20231008°1331 'Session cannot be started'
   // chain   : file 20120906°1241 Go.php


   // SHIFTED HERE 20231008°1321 -- Does not help against quirk
   // ref : https://stackoverflow.com/questions/49024013/php-warning-session-start-cannot-start-session-when-headers-already-sent [ref 20231008°1336]
   // ref : https://stackoverflow.com/questions/8028957/how-to-fix-headers-already-sent-error-in-php [ref 20231008°1338]
   ////$x = session_save_path();
   if (session_status() === PHP_SESSION_NONE) {
      if (session_start() !== true) {
         // Fatal error, using css class 'error' [seq 20110508°1841]
         echo('<p class="PhpError">Quirk 20110508°1852`13 was seen.</p>');
         exit();                                                    // [line 20120916°1701]
      }
      else {
         echo('<p>Session now started.</p>');
      }
   }
   else {
      echo('<p>Session was already started.</p>');
   }


   echo('<p>Session vari XTRA = "' . (isset($_SESSION['XTRA']) ? $_SESSION['XTRA'] : 'N/A') . '"</p>');
   $_SESSION['XTRA'] = date("Y-m-d`H:i:s");

   ////echo('<p>(1) Session folder = "' . $x . '"</p>');
   echo('<p>() Session folder = "' . session_save_path() . '"</p>');
   echo('Aloha');

   phpinfo();

   #die();
