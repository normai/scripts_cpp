﻿<img src="./assets/imgs/20180504o0103.graymouse.v1.x0128y0067.png" align="right" width="128" height="67" alt="Logo 20180504o0103">

# C/C++-Scripts &nbsp; <sup><sub><sup>*v0.6.1*</sup></sub></sup>

For the pages go

## &nbsp; &nbsp; &nbsp; [**normai.gitlab.io/scripts_cpp/index.html**](https://normai.gitlab.io/scripts_cpp/index.html)

Subject: Some scripts on C and C++

Status: Possibly messy

The scripts are:

- **Introduction into C** — Just some little paragraphs

- **C++ Basics** — Planned

- **Object Oriented Application Development with C++** — Pretty complete ..

- **C++ Advanced** — Pretty complete ..

&nbsp;

&nbsp;

---

<sup><sub>*Project 20200630°1237, file 20230510°1713, repo 20230510°1711]* ⬞Ω</sub></sup>
