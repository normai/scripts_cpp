/**
 * file : 20200913`0643 cp252nontypes.cpp, project 20200913`0641
 * summary : This demonstrates non-type template parameters
 * author : cppreference.com
 * license : CC BY-SA 3.0
 * see : en.cppreference.com/w/cpp/language/template_parameters [ref 20200911`1022]
 * note :
 */

#include <iostream>
#include <string>
using namespace std;

// (1) simple non-type template parameter
template<int N> struct S1 { int as[N]; };

// (2)
template<const char*> struct S2 {};

// (3) complicated non-type example
template
<
   char c,                     // integral type
   int(&ra)[5],                // lvalue reference to object (of array type)
   int(*pf)(int),              // pointer to function
   int(S1<10>::*a)[10]         // pointer to member object (of type int[10])
> struct Complicated
{
   // calls the function selected at compile time and stores
   //  the result in the array selected at compile time
   void foo(char base)
   {
      ra[4] = pf(c - base);
   }
};

//S2<"fail"> s2;               // error: string literal cannot be used
char okay[] = "okay";          // static object with linkage
//S2< &okay[0] > s2;           // error: array element has no linkage
S2<okay> s2;                   // works

int ag[5];
int f(int n) { return n; }

int main()
{
   cout << "This demonstrates non-type template parameters .." << endl;

   S1<10> s;                   // s.a is an array of 10 int
   s.as[9] = 4;

   Complicated < '2', ag, f, &S1<10>::as> c;
   c.foo('0');

   cout << s.as[9] << " " << ag[4] << '\n';

   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
