/**
 * file : 20200909`2113 cp23vartplats.cpp, project 20200909`2111
 * summary : This demonstrates a variadic template class
 * author : Rainer Grimm, modified by ncm
 * license : GNU General Public License v3.0
 * see : www.modernescpp.com/index.php/c-insights-variadic-templates [ref 20200913`0552]
 * todo : Explain, what it is exactly about that 'first argument'? [todo 20200921`1711]
 */

#include <iostream>
using namespace std;

// single argument overload required to terminate the recursion
template<class T> T add1(const T& arg)
{
   return arg;
}

// the first argument determines the return type
// MS compiler warning "C26451: Arithmetic overflow: Using operator '+' on a 4 byte value
// and then casting the result to a 8 byte value. Cast the value to the wider type before
// calling operator '+' to avoid overflow (io.2)"
template<class T, class... ARX> T add1(const T& arg, const ARX&... args)
{
   return arg + add1(args...); // MS compiler warning C26451
}

// single argument overload required to terminate the recursion
template<class T> T add2(const T& arg)
{
   return arg;
}

// like above, but this causes no warning C26451 because '<T>' makes the conversions explicit
template<class T, class... ARX> T add2(const T& arg, const ARX&... args)
{
   return arg + add2<T>(args...);
}

int main()
{
   cout << "This demonstrates a variadic template class .." << endl;
   //--------------------------------------------------

   auto x1 = add1(1, 2u, 3ull, 4.5f);
   cout << "(1) " << x1 << " " << typeid(x1).name() << endl;   // 10 int

   auto x2 = add1(1.2, 2u, 3ull, 4.5f);
   cout << "(2) " << x2 << " " << typeid(x2).name() << endl;   // 10.2 double

   auto x3 = add2(1, 2u, 3ull, 4.5f);
   cout << "(3) " << x3 << " " << typeid(x3).name() << endl;   // 10 int

   auto x4 = add2(1.2, 2u, 3ull, 4.5f);
   cout << "(4) " << x4 << " " << typeid(x4).name() << endl;   // 10.7 double

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
