/**
 * file : 20200909`1913 cp212functplats.cpp, project 20200909`1911 Cp212FuncTplats
 * summary : This demonstrates a simple function template
 * authors : cplusplus.com, ncm
 * license : unspecified
 * see : Code after www.cplusplus.com/doc/oldtutorial/templates/ [ref 20200911`0926] subchapter 1 'Function templates'
 * ref : stackoverflow.com/questions/2023977/difference-of-keywords-typename-and-class-in-templates [ref 20201015`1452]
 * note :
 */

#include <iostream>
#include <string>
using namespace std;

class A {
public:
   A() {}
   A(string s) : text(s) {}
   string text {"Allo"};
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// for (1)
template <class T> T GetMax1(T a, T b) {
   T result;
   result = (a > b) ? a : b;
   return (result);
}

// experiment
template <class T, class R> R GetMax1(T a, T b) {
   R result;
   result = (a > b) ? a : b;
   cout << "datentyp 1 = " << typeid(result).name() << endl;
   return (result);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// for (2) function template II

template <class T> T GetMax2(T a, T b) {
   return (a > b ? a : b);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int main()
{
   cout << "This is cp21fumctplats, demonstrating a function template .." << endl;
   //--------------------------------------------------
   // (1) Example 1

   int i = 5, j = 6, k;
   long l = 10, m = 5, n;
   bool b, b1 = true, b2 = false;
   string s, s1 = "Aloha", s2 = "Hello";
   A a1, a2("Holla");

   k = GetMax1<int>(i, j);
   n = GetMax1<long>(l, m);
   b = GetMax1<bool>(b1, b2);
   s = GetMax1<string>(s1, s2);
   //A a = GetMax1<A>(a1, a2); // error in template function 'T does not define this operator ..'

   cout << "(1.1) " << k << endl;
   cout << "(1.2) " << n << endl;
   cout << "(1.3) " << b << endl;
   cout << "(1.4) " << s << endl;
   //cout << "(1.5) " << a.text << endl;

   // experiments
   cout << "(1.6) " << GetMax1<int>(2.1, 3.2) << endl;
   auto x = GetMax1<double, int>(2.1, 3.2); // a call which needs the type parameters, and cannot be done with automatic type deduction
   cout << "(1.7) " << x << endl;
   cout << "datentyp 2 = " << typeid(x).name() << endl;

   //--------------------------------------------------
   // (2) Example 2 -- automatic type deduction

   int i2 = 5, j2 = 6, k2;
   long l2 = 10, m2 = 5, n2;
   k2 = GetMax2(i2, j2);
   n2 = GetMax2(l2, m2);

#if 0
   // experiment
   A s101 = A();
   A s102 = A();
   A s103 = GetMax2(s101, s102);
#endif

   cout << "(2.1) " << k2 << endl;
   cout << "(2.1) " << n2 << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
