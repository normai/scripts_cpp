/**
 * file : 20200909`2313 cp253nontypes.cpp, project 20200909`2311
 * summary : This demonstrates non-type template parameters
 * authors : contributed by Xeo
 * license : CC BY-SA 4.0
 * see : stackoverflow.com/questions/5687540/non-type-template-parameters [ref 20200911`1023]
 * note :
 */

#include <iostream>
#include <string>
using namespace std;

// pointer to object
template <string * temp> void f()
{
   cout << "f() : " << *temp << endl;
   *temp += "_postfix1_";
}

// reference to object
template <string & temp> void g()
{
   cout << "g() : " << temp << endl;
   temp += "...appended some string";
}

string s; // must not be local as it must have external linkage!

int main()
{
   cout << "This demonstrates non-type template parameters .." << endl;
   //--------------------------------------------------

   s = "can assign values locally";
   f<&s>();
   g<s>();
   cout << "(1) : " << s << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
