/**
 * file : 20200921`1113 varargstest.cpp, project 20200921`1111
 * summary : Demonstrate how to find the number of varargs or parameter pack, resp.
 * authors : Norbert C. Maier
 * license : MIT
 * see : en.cppreference.com/w/cpp/language/sizeof... [ref 20201014`0232]
 * todo : Provide demo 'simplest non-template vararg function' [todo 20201004`1111]
 * note :
 */

#include <iostream>
using namespace std;

template<class T> T f(const T& arg)
{
   return arg;
}

template<class T, class... ARGS> T f(const T& arg, const ARGS&... args)
{
   cout << "(1) sizeof : " <<  sizeof...(args) << endl;
   return arg + (long) f(args...);                     // the long cast prevents MS warning "Arithmetic overflow .. Cast the value to the wider type before calling '+' .."
}

int main()
{
   cout << "This varargstest.cpp .." << endl;
   //--------------------------------------------------

   auto x = f(1, 2u, 3ull, 4.5f);

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
