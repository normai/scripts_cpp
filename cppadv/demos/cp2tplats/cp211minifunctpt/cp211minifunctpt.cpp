/**
 *  file : 20200920`1733 mini_functemplate project 20200920`1731
 *  summary : Demonstrate how to use MS compiler from batchfile
 *  authors : cplusplus.com, ncm
 *  license : MIT/X11
 *  note : Snippet taken from file 20200909`1913 cp212functplats.cpp
 *  compile : VS2019 batch
 */

#include <iostream>
using namespace std;

template <class T> T GetMax2(T a, T b) {
   return (a > b ? a : b);
}

int main()
{
   int i, i1 = 5, i2 = 6;
   i = GetMax2(i1, i2);
   cout << i << endl;

   long l, l1 = 10, l2 = 5;
   l = GetMax2(l1, l2);
   cout << l << endl;

   return 0;
}
