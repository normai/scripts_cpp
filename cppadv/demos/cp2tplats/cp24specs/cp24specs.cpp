/**
 * file : 20200909`2213 cp24specs.cpp, project 20200909`2211
 * summary : This demonstrates template specialisation
 * author : Rainer Grimm, modified by ncm
 * license : GNU General Public License v3.0
 * original : github.com/RainerGrimm/ModernesCppSource/blob/master/source/functionTemplateSpecialisation.cpp [ref 2020xxxx`xxxx]
 * see : github.com/RainerGrimm/ModernesCppSource [ref 20200913`0553]
 * todo : What is specialzation good for, if I can replace the template
 *   by some ordinary function? We need an example which uses the both,
 *   a type parameter plus a non-type parameter. [todo 20200921`1715]
 */

#include <iostream>
#include <string>
using namespace std;

// (1.1)
template <typename T> string getTypeName1(T) {
   return "unknown type";
}

// (1.2) using keyword 'class' is counterintuitive but correct anyway
template <class T> string getTypeName2(T) {
   return "unknown type";
}

// (2.1) Why use a template when a fixed function would suffice? ////
template <> string getTypeName1<int> (int) {
   return "int";
}

// (2.2) Experiment //// take multiple parameters
template <typename T> string getTypeName3(int i, T x) {
   return "int" + string(typeid(x).name());
}

// (3)
string getTypeName1(double) {
   return "double";
}

int main()
{
   cout << "Cp24Specs .." << endl;
   //--------------------------------------------------

   cout << "(1) getTypeName1(true)         : " << getTypeName1(true) << endl;
   cout << "(2) getTypeName2(true)         : " << getTypeName2(true) << endl;
   cout << "(3) getTypeName1(4711)         : " << getTypeName1(4711) << endl;
   cout << "(4) getTypeName1(3.14)         : " << getTypeName1(3.14) << endl;
   cout << "(5) getTypeName1('Nochwas')    : " << getTypeName1("Nochwas") << endl;
   cout << "(6) getTypeName3(3.14, \"Oha\")  : " << getTypeName3(314, "Oha") << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
