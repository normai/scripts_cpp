/**
 * file        : 20200912`0113 cp72tplclass.cpp, project 20200912`0111
 * summary     :
 * authors     :
 * license     :
 * copyright   :
 * reference   :
 * note        :
 */

#include <iostream>
using namespace std;

int main()
{
   cout << "This is Cp72TplClass .." << endl;
   //--------------------------------------------------

   cout << "See chapter 2.2 'Class Templates'" << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
