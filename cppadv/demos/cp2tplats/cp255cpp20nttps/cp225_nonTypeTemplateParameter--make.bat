@echo off
goto skip
rem file 20200913`0637
color 1f
echo **************************************************
echo *** thisfile = %~n0%~x0
echo *** thisdrive = %~d0
echo *** thisdir = %~dp0
echo *** currdir = %cd%
echo **************************************************
rem Remember ref 20201104�1417 https://stackoverflow.com/questions/8756828/visual-studio-command-prompt-gives-common-was-unexpected-at-this-time
:skip

%~d0
cd %~dp0

call .\..\..\~!vcvarsall.bat

rem cl.exe /std:c++17 cp255_nonTypeTemplateParameter.cpp
rem cl.exe /std:c++latest cp255_nonTypeTemplateParameter.cpp
@echo on

cl.exe /std:c++latest cp255_nonTypeTemplateParameter.cpp

@echo off

echo.
echo ****** Finished %~n0%~x0 ******
pause
