/**
 *  file : 20200913`0633 nonTypeTemplateParameter.cpp
 *  summary : This demonstrates non-type template parameters
 *  author : Rainer Grimm, modified by ncm
 *  license : GNU General Public License v3.0
 *  see : github.com/RainerGrimm/ModernesCppSource/blob/master/source/nonTypeTemplateParameter.cpp [ref 20201015`1516]
 *  see : www.modernescpp.com/index.php/template-improvements-with-c-20 [ref 20201015`1512]
 *  see : www.modernescpp.com/index.php/component/jaggyblog/template-improvements-with-c-20 [ref 20201015`1514]
 *  compile : C++20 does not (yet) compile with VS2019
 *  note : Code identical with file 20200913`0633 cp254nontypes.cpp
 */

// nonTypeTemplateParameter.cpp

#if 0

struct ClassType {
    constexpr ClassType(int) {}
};

template <ClassType cl>
auto getClassType() {
    return cl;
}

template <double d>
auto getDouble() {
    return d;
}

#endif

int main() {

#if 0
    auto c1 = getClassType<ClassType(2020)>();

    auto d1 = getDouble<5.5>();
    auto d2 = getDouble<6.5>();

#endif
}
