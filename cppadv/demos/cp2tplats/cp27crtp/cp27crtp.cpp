/**
 * file : 20200910`0213 cp27crtp.cpp, project 20200910`0211
 * summary : This demonstrates CRTP
 * authors : wikipedia.org, modified by ncm
 * license : CC BY-SA 3.0 Unported
 * see : en.wikipedia.org/wiki/Curiously_recurring_template_pattern [ref 20200911`0934]
 * note :
 */

#include <iostream>
using namespace std;

template <typename T> struct counter
{
   static int objects_created;
   static int objects_alive;

   counter() {
      ++objects_created;
      ++objects_alive;
   }

   /*
   counter(const counter& x) {                 //// Copy constructor? Seems to be some unused relic.
      ++objects_created;
      ++objects_alive;
   }
   */

protected:
   // objects should never be removed through pointers of this type //// rather delete this remark?
   ~counter() {
      --objects_alive;
   }
};

template <typename T> int counter<T>::objects_created{0};
template <typename T> int counter<T>::objects_alive{0};

class X : counter<X>
{
   // ...
};

class Y : counter<Y>
{
   // ...
};

int main()
{
   cout << "This demonstrates CRTP .." << endl;

   X* x1 = new X();
   X* x2 = new X();
   X* x3 = new X();
   Y* y1 = new Y();

   // experiment
   ////X* x4 = new X(x1);
   X* x5 = x1;

   delete x3;

   counter<X>* cx;
   counter<Y>* cy;
   int ixCre = cx->objects_created;
   int ixAli = cx->objects_alive;
   int iyCre = cy->objects_created;
   int iyAli = cy->objects_alive;

   cout << "X created/alive : " << ixCre << " / " << ixAli << endl;
   cout << "Y created/alive : " << iyCre << " / " << iyAli << endl;

   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
