/**
 * file : 20200910`0113 cp24functors.cpp, project 20200910`0111
 * summary : Demonstrate function objects
 * authors : Code after Wikipedia, modified by ncm
 * license : CC BY-SA 3.0 Unported
 * see : en.wikipedia.org/wiki/Function_object#In_C_and_C++ [ref 20200911`0957]
 * todo : Supplement link to compare project with sort function feeded by a lambda expression /////  [todo 20200921`1721]
 * todo : Which advance has struct IntComparator as opposed to supplemented function Compare?  [todo 20200921`1725]
 */

#include <algorithm> // sort
#include <iostream>
#include <typeinfo> // typeid
#include <string>
#include <vector>
using namespace std;

// (1) comparator predicate: returns true if a < b, false otherwise
struct ComparatorInt
{
   bool operator()(const int &a, const int &b) const
   {
      return a < b;
   }
};

// (2) same function as above, just more flexible
template <class T> struct ComparatorAny
{
   bool operator()(const T &a, const T &b) const
   {
      return a < b;
   }
};

// (3) supplemented ////
bool ComparatorAnyAsFunc(const int& a, const int& b) {
   return a < b;
}

int main()
{
   cout << "This demonstrates some function objects .." << endl;

   //--------------------------------------------------
   // (1) simplest use case

   vector<int> items1{ 4, 3, 1, 2 };
   sort(items1.begin(), items1.end(), ComparatorInt());

   cout << "(1)     : " << typeid(ComparatorInt).name() << endl; // just for fun
   cout << "(1)     : " << typeid(ComparatorInt()).name() << endl; // just for fun

   cout << "(1)     :";
   for (int i : items1) { cout << " " << i; }
   cout << endl;

   //--------------------------------------------------
   // (2) put comparator function into variable
   vector<int> items2{ 4, 3, 1, 2 };
   auto x1 = ComparatorInt();
   ////auto x1 = string(); //// ComparatorInt();
   sort(items2.begin(), items2.end(), x1);

   cout << "(2)     : " << typeid(x1).name() << endl; // just for fun
   cout << "(2)     :";
   for (int i : items2) { cout << " " << i; }
   cout << endl;

   //--------------------------------------------------
   // (3) use the pure function
   vector<int> items3{ 4, 3, 1, 2 };
   auto x3 = ComparatorAnyAsFunc;
   sort(items3.begin(), items3.end(), x3);

   cout << "(3)     : " << typeid(x3).name() << endl; // just for fun
   cout << "        :";
   for (int i : items3) { cout << " " << i; }
   cout << endl;

   //--------------------------------------------------
   // (4) see the template function object as variable

   vector<string> v2{ "Oha", "Hopla", "Allo", "Yup", "Jo" };

   auto f = ComparatorAny<string>();
   sort(v2.begin(), v2.end(), f);

   cout << "(4)     : " << typeid(f).name() << endl; // just for fun
   cout << "        :";
   for (string s : v2) {
      cout << " " << s;
   }
   cout << endl;

   //--------------------------------------------------
   cout << "Press Enter to exit.";
   (void) getchar();
   return 0;
}
