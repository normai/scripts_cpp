/**
 *  file : 20200913`0633 cp254nontypes.cpp project 20200913`0631
 *  summary : This demonstrates non-type template parameters
 *  author : Rainer Grimm (modified by ncm)
 *  license : GNU General Public License v3.0
 *  see : github.com/RainerGrimm/ModernesCppSource/blob/master/source/nonTypeTemplateParameter.cpp [ref 20201015`1516]
 *  see : www.modernescpp.com/index.php/template-improvements-with-c-20 [ref 20201015`1512]
 *  see : www.modernescpp.com/index.php/component/jaggyblog/template-improvements-with-c-20 [ref 20201015`1514]
 *  note : Does not (yet) compile with MS compiler with setting "C++ Language Standard = Preview
 *         Features from the Latest C++ Working Draft (std:c++latest)" (with VS2019 in Oct 2020)
 */

#include <iostream>
#include <string>
using namespace std;

#if 0
struct ClassType {
   constexpr ClassType(int) {}                         // (1)
};

template <ClassType cl>                                // (2)
auto getClassType() {
   return cl;
}

template <double d>                                    // (3)
auto getDouble() {
   return d;
}
#endif

int main()
{
   cout << "Here comes nonTypeTemplateParameter.cpp .. NOT YET!" << endl;
   //--------------------------------------------------

#if 0

   auto c1 = getClassType<ClassType(2020)>();

   auto d1 = getDouble<5.5>();                         // (4)
   auto d2 = getDouble<6.5>();                         // (4)

#endif

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
