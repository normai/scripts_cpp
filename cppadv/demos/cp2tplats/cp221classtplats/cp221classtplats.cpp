/**
 * file: 20200909`2013 cp221classtplats.cpp, project 20200909`2011
 * summary : This demonstrates a class template with a member template
 * authors : cplusplus.com, modified by ncm
 * license : unspecified
 * see : Code after www.cplusplus.com/doc/oldtutorial/templates/ [ref 20200911`0926]
 */

#include <iostream>
using namespace std;

template <class T> class mypair {
   T a, b;
 public:
   mypair(T first, T second)
   {
      a = first; b = second;
   }
   T getmax();
};

template <class X> X mypair<X>::getmax()
{
   X retval;
   retval = a > b ? a : b;
   return retval;
}

int main()
{
   cout << "This demonstrates a simple class template with a member template" << endl;
   //--------------------------------------------------

   mypair <int> myobject(100, 75);
   cout << myobject.getmax() << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
