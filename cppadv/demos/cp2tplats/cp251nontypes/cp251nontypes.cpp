/**
 * file : 20200913`0653 cp251nontypes.cpp, project 20200913`0651
 * summary : This demonstrates non-type template parameters (NTTPs)
 * author : cppreference.com, modified by ncm
 * license : CC BY-SA 3.0
 * see : en.cppreference.com/w/cpp/language/template_parameters [ref 20200911`1022]
 * note : This is a reduction of the complete example in the next project
 */

#include <iostream>
#include <string>
using namespace std;

// simple non-type template parameter
template<int N> struct S1 { int a[N]; };

// the same, but with array elements initialized
template<int N> struct S2 { int a[N] = { 0 }; };

int main()
{
   cout << "This demonstrates simplest non-type template parameters .." << endl;
   //--------------------------------------------------

   S1<10> s1;                                           // s.a is an array of 10 int
   s1.a[7] = 42;

   cout << "(1) :";
   for (int i = 0; i < size(s1.a); i++) {
      cout << " " << s1.a[i];
      if (i == 3) { cout << hex; }
   }
   cout << dec << endl;

   //--------------------------------------------------

   S2<10> s2;
   s2.a[7] = 42;

   cout << "(2) :";
   for (int i = 0; i < size(s2.a); i++) {
      cout << " " << s2.a[i];
   }
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
