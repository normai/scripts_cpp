/**
 * file : 20200910`0513 cp33structbind.cpp, project 20200910`0511
 * summary : This demonstrates the std::tie function for unpacking a tuple,
 *    and how the C++17 structured binding makes the task easier
 * authors : Steve Lorimer (modified by ncm)
 * license : unspecified
 * see : skebanga.github.io/structured-bindings/ [ref 20200911`1143]
 * note :
 */

#include <iostream>
#include <tuple> // std::make_tuple, std::tie
using namespace std;

int main()
{
   cout << "This is cp33stuctbind.cpp .." << endl;
   //--------------------------------------------------
   // (1) before C++17

   auto tuple1 = make_tuple(1, 'a', 2.3, "Bla");

   // first we have to declare the variables
   int i1;
   char c1;
   double d1;
   string s1;

   // now we can unpack the tuple into its individual components
   tie(i1, c1, d1, s1) = tuple1;

   cout << "i1 = " << i1 << ", c1 = " << c1 << ", d1 = " << d1 << ", s1 = "<< s1 << endl;

   //--------------------------------------------------
   // (2) with C++17

   auto tuple2 = make_tuple(1, 'a', 2.3);

   // unpack the tuple into individual variables declared at the call site
   //auto [i2, c2] = tuple2;           //// number mismatch
   auto [i2, c2, d2] = tuple2;

   cout << "i2 = " << i2 << ", c2 = " << c2 << ", d2 = " << d2 << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void)getchar();
   return 0;
}
