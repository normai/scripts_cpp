/**
 * file: 20200921`1853 cp325auto.cpp, project 20200921`18 1
 * summary: Demonstrate how auto makes refactoring easier
 * authors: Rainer Grimm refactAuto.cpp, modified by ncm
 * license: GNU GPL v3
 * see : www.modernescpp.com/index.php/automatically-inititialized [ref 20200913`0122]
 * see : github.com/RainerGrimm/ModernesCppSource/blob/master/source/refactAuto.cpp [ref 20201015`1154]
 * note:
 */

#include <iostream>
#include <typeinfo>
using namespace std;

int main()
{
   cout << "This is cp325auto.cpp .." << endl;
   //--------------------------------------------------

   auto a = 5;
   auto b = 10;
   auto sum = a * b * 3;
   auto res1 = sum + 10;
   std::cout << "typeid(res1).name() : " << typeid(res1).name() << std::endl;

   /*
   // Have an initial sequence ..
   int a = 5;
   int b = 10;
   int sum = a * b * 3;
   int res = sum + 10;

   // .. refactored to different types ..
   int a2= 5;
   double b2= 10.5;
   double sum2= a2 * b2 * 3;
   double res2= sum2 * 10.5;
   */

   // .. is easy with auto
   auto a2 = 5;
   auto b2a = 10.5;                            //// is against the curly braces recommendation ..
   auto b2b { 10.5 };                          //// .. but the refactoring advantage of 'auto' exists anyway
   auto sum2 = a2 * b2b * 3;
   auto res2 = sum2 * 10;
   std::cout << "typeid(res2).name() : " << typeid(res2).name() << std::endl;

   auto a3 = 5;
   auto b3 = 10;
   auto sum3 = a3 * b3 * 3.1f;
   auto res3 = sum3 * 10;
   std::cout << "typeid(res3).name() : " << typeid(res3).name() << std::endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
