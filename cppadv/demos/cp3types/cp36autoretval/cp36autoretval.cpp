/**
 * file : 20200910`0813 cp36autoretval.cpp, project 20200910`0811
 * summary : Demonstrate C++14 function return type deduction
 * authors : Marius Bancila
 * license : unspecified(?)
 * see : codexpert.ro/blog/2013/12/22/function-return-type-deduction-in-cpp14/ [ref 20200911`1222]
 * note :
 */

#include <iostream>
using namespace std;

// (1.1) C++11
auto foo1() -> int
{
   return 42;
}

// (1.2)  C++11
template <typename T> auto foo2(T value) -> decltype(value.bar())
{
   return value.bar();
}

// (2.1)  C++14
auto foo3()
{
   return 42;
}

// (2.2)  C++14
template <typename T> auto foo(T value)
{
   return value.bar();                                 // value must have member bar -- curious
}

// (3) if a function has more than one return statement they all must deduce the same type
auto foo()
{
   if (1 < 2) return 1;
   return 0;
}

// (4) A recursive function can have an auto return type only if it
//  has a non-recursive return statement *before* the recursive call.
//  Otherwise error "Cannot deduce return type from function ..." ////
auto fibonacci(int n)
{
   /*
   if (n > 2) { return fibonacci(n - 1) + fibonacci(n - 2); }  // error "Cannot deduce return type from function ..."
   else { return 1; }
   */

   if (n <= 2) { return 1; }
   else { return fibonacci(n - 1) + fibonacci(n - 2); }        // fine
}

int main()
{
   cout << "This is Cp36AutoRetVal .." << endl;
   //--------------------------------------------------

   int i1 = fibonacci(1);
   cout << "fibonacci(1) = " << i1 << endl;

   int i2 = fibonacci(2);
   cout << "fibonacci(2) = " << i2 << endl;

   int i3 = fibonacci(3);
   cout << "fibonacci(3) = " << i3 << endl;

   int i4 = fibonacci(4);
   cout << "fibonacci(4) = " << i4 << endl;

   int i5 = fibonacci(5);
   cout << "fibonacci(5) = " << i5 << endl;

   int i6 = fibonacci(6);
   cout << "fibonacci(6) = " << i6 << endl;

   int i7 = fibonacci(7);
   cout << "fibonacci(7) = " << i7 << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
