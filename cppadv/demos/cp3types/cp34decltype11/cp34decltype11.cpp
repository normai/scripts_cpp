﻿/**
 * file : 20200910°0613 cp34decltype11.cpp, project 20200910°0611
 * summary : Demonstrate decltype C++11
 * authors : (1) cppreference.com (2) Arne März, Modifications by ncm
 * license : (1) CC BY-SA 4.0 (2) unspecified
 * see : (1) en.cppreference.com/w/cpp/language/decltype [ref 20200911`1152]
 *            Note the statement "Note that if the name of an object is parenthesized,
 *            it is treated as an ordinary lvalue expression, thus decltype(x) and
 *            decltype((x)) are often different types."
 * see : (2) arne-mertz.de/2017/01/decltype-declval/ [ref 20200911`1156]
 * encocding : Unicode (UTF-8-with-BOM) [screenshot 20200913°0132]
 * todo : Split example into two files [todo 20201015`1157]
 */

#include <iostream>
#include <climits>
#include <vector>
using namespace std;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// (1) after cppreference.com

struct A { double x; };
const A* a;

decltype(a->x) y;                              // type of y is double (declared type)
decltype((a->x)) z1 = y;                       // type of z is (const) double& (lvalue expression)
decltype(a->x) z2 = y;                         //// without double parenthesis: type of z is const double

template<typename T, typename U>

// return type depends on template parameters
// return type can be deduced since C++14
auto add(T t, U u) -> decltype(t + u)
{
   return t + u;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// (2) after Arne März
struct X {
   //x() { cout << " SIZEOF=" << sizeof(bar)};
   int i;
   double bar (short);                                 ////
};

double X::bar(short s){
   cout << "(2)    : BAR " << sizeof(s) << endl;
   return (double) s;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int main()
{
   cout << "This is Cp34DeclType11 .." << endl;
   //--------------------------------------------------
   // (1) after cppreference.com

   std::cout << "(y)  : " << typeid(y).name() << endl;
   std::cout << "(z1) : " << typeid(z1).name() << endl;
   std::cout << "(z2) : " << typeid(z2).name() << endl;

   int i = 33;
   decltype(i) j = i * 2;

   std::cout << "(1.1)  : i = " << i << ", " << "j = " << j << endl;

   auto f = [](int a, int b) -> int { return a * b; };

   decltype(f) g = f;  // the type of a lambda function is unique and unnamed
   i = f(2, 2);
   j = g(3, 3);

   std::cout << "(1.2)  : " << typeid(f).name() << endl; // e.g. "class <lambda_fe1db75d4c60df2c564713fee1924e8a>"

   std::cout << "(1.3)  : i = " << i << ", " << "j = " << j << endl;

   //--------------------------------------------------
   // (2) after Arne März

   X x;
   decltype(x) y;                                      // y has type X;
   std::vector<decltype(x.i)> vi;                      // vector<int>
   using memberFunctionPointer = decltype(&X::bar);    // double X::(*)(short)

   auto lam = [&]() -> decltype(y) { return y; };      // decltype(y) is const X&

   double dTest = x.bar(123);


   // (3) also test the add function
   auto ret1 = add(2.3f, 4);
   auto ret2 = add(2.3f, ULLONG_MAX);
   auto ret3 = add(2, 4ull);
   auto ret4 = add(2.3f, 4.4);
   std::cout << "(3.1)  : " << typeid(ret1).name() << endl;
   std::cout << "(3.2)  : " << typeid(ret2).name() << " " << ULLONG_MAX << endl;
   std::cout << "(3.3)  : " << typeid(ret3).name() << endl;
   std::cout << "(3.4)  : " << typeid(ret4).name() << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
