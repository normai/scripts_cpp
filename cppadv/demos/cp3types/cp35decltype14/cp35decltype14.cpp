/**
 * file : 20200910`0713 cp35decltype14.cpp, project 20200910`0711
 * summary :
 * authors :
 * license :
 * see : en.cppreference.com/w/cpp/language/auto [ref 20200911`1122], but
 *        that is already implemented in file 20200910`0413 cp32autos.cpp
 * todo : Provide small example with only 'decltype(auto)' [todo 20201015`1212]
 * note :
 */

#include <iostream>
using namespace std;

int main()
{
   cout << "This is cp35decltype14.cpp .." << endl;
   //--------------------------------------------------





   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
