/**
 * file: 20200921`1833 cp323auto.cpp, project 20200921`1831
 * summary: Demonstrate some simple use cases of auto
 * authors: Rainer Grimm
 * license: GNU GPL v3
 * see: www.modernescpp.com/index.php/automatically-inititialized [ref 20200913`0122]
 * note: Lines are from main() number 4 on that page
 */

#include <iostream>
#include <vector>
using namespace std;

struct T1 {};

struct T2 {
   int mem;                                    // Not ok: indeterminate value
public:
   T2() {}                                     // VS2019 warning "C26495: 'T2::mem' is uninitialized. Always initialize a member variable."
};

int n;                                         //  ok: initialized to 0

int main()
{
   cout << "This is cp323auto.cpp .." << endl;
   //--------------------------------------------------

   int n;                                      // Not ok: indeterminate value
   string s;                                   // ok: Invocation of the default constructor; initialized to "" 
   T1 t1;                                      // ok: Invocation of the default constructor 
   T2 t2;                                      // ok: Invocation of the default constructor

   // cout << "n  " << n << endl;              // error 'uninitialized local variable'
   cout << "::n " << ::n << endl;
   cout << "s: " << s << endl;
   cout << "T2().mem: " << T2().mem << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
