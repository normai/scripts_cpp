/**
 * file: 20200910`0413 cp32autos.cpp, project 20200910`0411
 * summary: Demonstrates the more complicated use cases of auto
 * authors: cppreference.com
 * license: CC BY-SA 4.0
 * see: en.cppreference.com/w/cpp/language/auto [ref 20200911`1122]
 * see : stackoverflow.com/questions/24109737/what-are-some-uses-of-decltypeauto (for even more confusion?) [ref 20201006`1412]
 * see : en.cppreference.com/w/cpp/language/function for the arrow operator [ref 20201006`1422]
 * note:
 */

#include <iostream>
#include <utility>
using namespace std;

// (1) the return type is the type of operator+(T, U)
template<class T, class U> auto add(T t, U u) { return t + u; }

// (2)
// Perfect forwarding of a function call must use decltype(auto)
// in case the function it calls returns by reference
template<class F, class... Args> decltype(auto) PerfectForward(F fun, Args&&... args)
{
   return fun(forward<Args>(args) ...);
}

// (3) C++17 auto parameter declaration
// (3.1) using the arrow operator
template<auto n> auto f1() -> pair<decltype(n), decltype(n)> // auto can't deduce from return brace-init-list
{
   return { n, n };
}

// (3.2) equivalent, using the traditional return type declaration
template<auto n> pair<decltype(n), decltype(n)> f2()
{
   return { n, n };
}

int main()
{
   cout << "This is cp32autos.cpp .." << endl;
   //--------------------------------------------------

   auto a = 1 + 2;                                     // type of a is int
   auto b = add(1, 1.2);                               // type of b is double
   static_assert(is_same_v<decltype(a), int>);
   static_assert(is_same_v<decltype(b), double>);
   cout << "b-type : " << typeid(b).name() << endl;

   auto c0 = a;                                        // type of c0 is int, holding a copy of a
   decltype(auto) c1 = a;                              // type of c1 is int, holding a copy of a
   decltype(auto) c2 = (a);                            // type of c2 is int&, an alias of a
   cout << "a, before modification through c2 = " << a << '\n';
   ++c2;
   cout << "a, after modification through c2 = " << a << '\n';

   auto [v1, w1] = f1<0>();                            // structured binding declaration
   auto [v2, w2] = f2<0>();                            // equivalent

   auto d = { 1, 2 };                                  // OK: type of d is std::initializer_list<int>
   auto n = { 5 };                                     // OK: type of n is std::initializer_list<int>
   //auto e{1, 2};                                     // Error as of C++17, std::initializer_list<int> before
   auto m{ 5 };                                        // OK: type of m is int as of C++17, initializer_list<int> before
   //decltype(auto) z = { 1, 2 }                       // Error: {1, 2} is not an expression

    // auto is commonly used for unnamed types such as the types of lambda expressions
   auto lambda = [](int x) { return x + 3; };
   //cout << "lambda type = " << typeid(lambda).name() << endl;

   //  auto int x;                                     // valid C++98, error as of C++11
   //  auto x;                                         // valid C, error in C++

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
