/**
 * file: 20200921`1843 cp324auto.cpp, project 20200921`1841
 * summary: Demonstrate some use cases of 'auto'
 * authors: Rainer Grimm
 * license: GNU GPL v3
 * see: www.modernescpp.com/index.php/automatically-inititialized [ref 20200913`0122]
 * note:
 */

#include <iostream>
#include <vector>
using namespace std;

struct T1 {};

struct T2 {
   int mem = 0;                                // auto mem = 0 is an error
public:
   T2() {}
};

auto n = 0;

int main()
{
   cout << "This is cp324auto.cpp .." << endl;
   //--------------------------------------------------

   using namespace std::string_literals;

   auto n = 0;
   auto s1 = ""s;                              // const char* //// see en.cppreference.com/w/cpp/string/basic_string/operator%22%22s // docs.microsoft.com/en-us/cpp/cpp/string-and-character-literals-cpp?view=vs-2019
   auto s2 = "";                               //// just for comparison
   auto t1 = T1();
   auto t2 = T2();

   std::cout << "::n      : " << ::n << std::endl;
   std::cout << "n        : " << n << std::endl;
   std::cout << "s1       : '" << s1 << "'" << std::endl;
   std::cout << "s2       : '" << s2 << "'" << std::endl;
   std::cout << "T2().mem : " << T2().mem << std::endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
