/**
 * file : 20200910`0313 cp31tempargs.cpp, project 20200910`0311
 * summary : Demonstrate templates while omitting the explicite type parameters on caller side
 * authors : Reiner Grimm templateArgumentDeduction.cpp, modified by ncm
 * license : GNU GPL v3
 * see : github.com/RainerGrimm/ModernesCppSource/blob/master/source/templateArgumentDeduction.cpp [ref 20201015`1152]
 * see : www.modernescpp.com/index.php/c-core-guidelines-template-interfaces [ref 20200913`0116]
 * note :
 */

#include <iostream>
using namespace std;

// (1)
template <typename T> void showMe1(const T& t) {
   cout << t << endl;
}

// (2)
template <typename T> struct ShowMe2 {
   ShowMe2(const T& t) {
      cout << t << endl;
   }
};

int main()
{
   cout << "This is Cp31TempArgs .." << endl;
   //--------------------------------------------------

   showMe1(5.5);                               // not showMe<double>(5.5);
   showMe1(5);                                 // not showMe<int>(5);

   ShowMe2(5.5);                               // not ShowMe<double>(5.5);
   ShowMe2(5);                                 // not ShowMe<int>(5);

   ShowMe2 x1 = ShowMe2<int>(123);             // this works anyway ////
   ShowMe2 x2 = ShowMe2("Aloha");              ////
   ShowMe2 x3 = ShowMe2<string>("He");         //// Sometimes VS2019 warning "C26444: Don't try to declare a local variable with no name"

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar(); ////
   return 0;
}
