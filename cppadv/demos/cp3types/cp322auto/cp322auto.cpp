/**
 * file: 20200921`1823 cp322auto.cpp, project 20200921`1821
 * summary: Demonstrate simple use cases of auto
 * authors: Rainer Grimm
 * license: GNU GPL v3
 * see: www.modernescpp.com/index.php/automatically-inititialized [ref 20200913`0122]
 * note: This are the second and third main methods on that page
 */

//#include "get_type.hpp"                      //// from Boost library

#include <iostream>
using namespace std;

template <typename T> class GetType;

int main()
{
   cout << "This is cp322auto.cpp .." << endl;
   //--------------------------------------------------
   // from main 1

   int i1a = 2011;
   const int i2a = 2014;
   const int& i3a = i2a;

   auto a2 = i2a;                              // int
   auto a3 = i3a;                              // int
   const auto a4 = i3a;                        ////

   //--------------------------------------------------
   // from main 2

   int i1b = 2011;
   const int i2b = 2014;                       // GetType<decltype(i2b)> myType;
   const int& i3b = i2b;                       // GetType<decltype(i3)> myType;

   const auto a2b = i2b;                       // GetType<decltype(a2)> myType;
   volatile auto a3b = i3b;                    // GetType<decltype(a3)> myType;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
