/**
 * file: 20200921`1813 cp321auto.cpp, project 20200921`1811
 * summary: Demonstrate some simple use cases of auto
 * authors: Rainer Grimm
 * license: GNU GPL v3
 * see: www.modernescpp.com/index.php/automatically-inititialized [ref 20200913`0122]
 * note:
 */

#include <iostream>
#include <vector>
using namespace std;

int myAdd(int a, int b) { return a + b; }

int main()
{
   cout << "This is cp321auto.cpp .." << endl;
   //--------------------------------------------------

   // (1) define an int-value
   int i = 5;                                          // explicit
   auto i1 = 5;                                        // auto

   // (2) define a reference to an int
   int& b1 = i;                                        // explicit
   auto & b2 = i;                                      // auto

   // (2.1) more details
   auto b11 = &i;
   auto* b12 = &i;
   cout << " typeid(i).name()    : " << typeid(i).name() << endl;
   cout << " typeid(b1).name()   : " << typeid(b1).name() << endl;
   cout << " typeid(b2).name()   : " << typeid(b2).name() << endl;
   cout << " typeid(&i).name()   : " << typeid(&i).name() << endl;
   cout << " typeid(b11).name()  : " << typeid(b11).name() << endl;
   cout << " typeid(b12).name()  : " << typeid(b12).name() << endl;

   // (3) define a pointer to a function
   int (*add1)(int, int) = myAdd;                      // explicit
   auto add2 = myAdd;                                  // auto

   // (4) iterate through a vector
   vector<int> vec;

   // (4.1)
   for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it) {} // ~~ [n:m)

   // (4.2)
   for (auto it1 = vec.begin(); it1 != vec.end(); ++it1) {}

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
