/**
 * file : 20200910`1813 cp37autoctypes.cpp, project 20200910`1811
 * summary : Some lines using class template argument deduction (CTAD)
 * authors : cppreference.com
 * license : CC BY-SA 3.0
 * see : en.cppreference.com/w/cpp/language/class_template_argument_deduction [ref 20200911`1232]
 * note :
 */

#include <algorithm> // sort
#include <iostream>
#include <vector>
using namespace std;

int main()
{
   cout << "This is Cp37AutoCTypes .. (see values in debugger)" << endl;
   //--------------------------------------------------

   // Class template argument deduction (CTAD)
   std::pair p1(2, 4.5);                       // deduces to std::pair<int, double> p(2, 4.5);
   std::tuple t1(4, 3, 2.5);                   // same as auto t = std::make_tuple(4, 3, 2.5);
   std::less l1;                               // same as std::less<void> l; //// what exacly does less?

   // chross check -- being explicit
   std::pair<int, double> p2(2, 4.5);
   std::tuple<int, int, double> t2(4, 3, 2.5);
   std::less<void> l2;

   //// // failed experiment to see what less exactly does
   //// vector v { 2, 3, 4, 5, 6};
   //// sort (v.begin(), v.end(), l1); // no

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
