
   Solution Cp3Types

   • Cp31TempArgs —

   • Cp32Autos —

   • Cp33StructBind —

   • Cp34DeclType11 —

   • Cp35DeclType14 —

   • Cp36AutoRetVal —

   • Cp37AutoCTypes —


   ——————————————————————————————————————————————

   note 20200921`1731 get rid of warning with the plain getchar
   about : Warning 'C6301: return value ignored' in default 'Level3 (/W3)'
   see : stackoverflow.com/questions/41219513/can-i-suppress-a-specific-c6031-warning-cstringloadstring [ref 20200913`0126]
   note : C trick after Detlef Cramm: Place a '(void)' cast in front of getchar().

   ——————————————————————————————————————————————
   [file 20200910°0302 solution 20200910°0301] ܀Ω
