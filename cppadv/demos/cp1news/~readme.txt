
   Solution Cp11News

   • Cp11Range1Flavours — The possible types of the loop variable

   • Cp11Range2ForEach — How it was before range based for

   • Cp11Range3Map — Loop over a somewhat trickier container

   • Cp124Forward — after Rainer Grimm

   • Cp125IntroRvalRefs — after Hinnant/Stroustrup/Kozicki 2006

   • Cp13Inits —

   • Cp14Braces —

   • Cp15Fold1Demo —

   • Cp15Fold2CppRef —

   • Cp15Fold3Bptst —

   • Cp15Fold4Wicht —

   • Cp15Fold5Coliru —

   • Cp15Fold16Stacko —


   ————————————————————————————————————————————————————

   solution 20200825°0601
   objecttype  : VS-2019 solution
   title       : Demonstrate the range based for loop
   summary     :
   status      :
   note        :
   ܀

   ————————————————————————————————————————————————————

   20200825°0601 solution
   20200825°0611 range1flavours
   20200825°0621 range2foreach
   20200825°0631 range3map

   ——————————————————————————————————————————————
   [file 20200825°0602 solution 20200825°0601] ܀Ω
