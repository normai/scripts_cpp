/**
 * file : 20200825`0622 cp11range2foreach, project 20200825`0621
 * summary : Demonstrate the same as in range1flavours, just using the for_each function
 * license : CC-BY-SA 3.0
 * authors : cppreference.com, modified by ncm
 * origin : Code after en.cppreference.com/w/cpp/algorithm/for_each [ref 20200909`1413]
 */

#include <algorithm> // for_each
#include <iostream>
#include <vector>
using namespace std;

void FunctionForLambda(const int& i)                   //// what about pure value?
{
   cout << ' ' << i;
}

int main() {
   cout << "This is Cp11Range2ForEach, demonstrating the for_each function .." << endl;
   //--------------------------------------------------

   vector<int> v = { 0, 1, 2, 3, 4, 5 };

   // (1) access by const reference
   cout << "(1)  ";
   for_each(v.begin(), v.end(), [](const int& i) { cout << ' ' << i; });
   cout << endl;

   // (1b) access by reference //// not listed on cppreference page
   cout << "(1b) ";
   for_each(v.begin(), v.end(), [](int & i) { cout << ' ' << i; }); // 'int& i' or 'int &i' or 'int & i'
   cout << endl;

   // (2.1) access by value, the type of i is int
   cout << "(2.1)";
   for_each(v.begin(), v.end(), [](int i) { cout << ' ' << i; });
   cout << endl;

   // (2.2) experiment -- use a function instead of lambda ////
   cout << "(2.2)";
   for_each(v.begin(), v.end(), FunctionForLambda);
   cout << endl;

   // (3) access by forwarding reference, the type of i is int&
   cout << "(3)  ";
   for_each(v.begin(), v.end(), [](auto&& i) { cout << ' ' << i; });
   cout << endl;

   // (4) access by f-d reference, the type of i is const int&
   cout << "(4)  ";
   const auto& cv = v;
   for_each(cv.begin(), cv.end(), [](auto&& i) { cout << ' ' << i; });
   cout << endl;

   // (5) the initializer may be a braced-init-list
   cout << "(5)  ";
   // not possible
   cout << endl;

   // (6) the initializer may be an array
   cout << "(6)  ";
   // not possible, initializer must be a class type
   cout << endl;

   // (7) the loop variable needs not be used
   cout << "(3)  ";
   for_each(v.begin(), v.end(), []([[maybe_unused]] int n) { cout << ' ' << 1; });
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
