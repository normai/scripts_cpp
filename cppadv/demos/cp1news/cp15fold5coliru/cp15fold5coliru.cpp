/**
 * file : 20200825`0113 cp15fold5coliru.cpp, project 20200825`0111
 * summary : Demonstrate variadic template
 * origin : coliru.stacked-crooked.com/a/620e0f7cacba4970 [ref 20200912`1242]
 * ref : thread www.cplusplus.com/forum/general/193855/ [ref 20200912`1241]
 */

#include <iostream> 

#define COMMA ,
#define LOGICAL_AND &&

template< typename... PACK > void print_all(PACK... pack)
{
   const auto print_line = [](auto a) { return bool(std::cout << a << ' '); };

   // unary right fold, syntax: ( pack_expression binary_operator ... )
   (print_line(pack) COMMA ...);
   //      pack_expression - an expression that contains an unexpanded parameter pack => print_line(pack)
   //      binary_operator - any one of the permissible 32 binary binary_operators => COMMA ie. ,

   std::cout << '\n';

   // unary left fold, syntax: ( ... binary_operator pack_expression  )
   (... LOGICAL_AND print_line(pack));
   //      binary_operator - any one of the permissible 32 binary binary_operators => LOGICAL_AND ie. &&
   //      pack_expression - an expression that contains an unexpanded parameter pack => print_line(pack)

   std::cout << '\n';

   // binary left fold, syntax: ( init_expression binary_operator ... binary_operator pack_expression  )
   ((std::cout << "result is: ") COMMA ... COMMA print_line(pack));
   //      init_expression - an expression that does not contain an unexpanded parameter pack => ( std::cout << "result: " )
   //      binary_operator - any one of the permissible 32 binary binary_operators => COMMA ie ,
   //      pack_expression - an expression that contains an unexpanded parameter pack => print_line(pack)

   std::cout << '\n';

   // binary right fold, syntax: ( pack_expression binary_operator ... binary_operator init_expression   )
   (print_line(pack) LOGICAL_AND ... LOGICAL_AND(std::cout << " (all items in the pack were printed)\n"));
   //      pack_expression - an expression that contains an unexpanded parameter pack => print_line(pack)
   //      binary_operator - any one of the permissible 32 binary binary_operators => LOGICAL_AND ie. &&
   //      init_expression - an expression that does not contain an unexpanded parameter pack => ( std::cout << " (all items in the pack were printed)\n" )
}

template < typename... PACK > auto left_fold(PACK... pack) { return (... / pack); }
template < typename... PACK > auto right_fold(PACK... pack) { return (pack / ...); }

int main()
{
   print_all("first", 2, "third", 4, "five");

   const int a = 1000000, b = 20, c = 10, d = 2;

   // difference between left fold and rigfht fold
   std::cout << "left fold: " << left_fold(a, b, c, d) << ' ' << ((a / b) / c) / d << '\n'
      << "right fold: " << right_fold(a, b, c, d) << ' ' << a / (b / (c / d)) << '\n';

   (void)getchar();
}
