/**
 * file        : 20200909`1813 cp14braces.cpp, project 20200909`1811
 * summary     : Demonstrate traditional initialization versus curly-braces-initialization
 * authors     : ncm
 * license     : MIT
 * reference   :
 * note        :
 */

#include <iostream>
#include <vector>
using namespace std;

int main()
{
   cout << "This is Cp14Braces, demonstrating parenthesis versus braces .." << endl;
   //--------------------------------------------------

   int i1 = 1;
   int i2 = 2.1;                               // silently let go through
   int i3 (3);
   int i4 (4.4);                               // silently let go through
   int i5 { 5 };
   //int i6 { 6.6 };                           // rejected
   int i7 { (int) 7.7 };                       // being explicit

   cout << "i1  = " << i1 << endl;
   cout << "i2  = " << i2 << endl;
   cout << "i3  = " << i3 << endl;
   cout << "i4  = " << i4 << endl;
   cout << "i5  = " << i5 << endl;
   cout << "i7  = " << i7 << endl;

   vector<int> v0(2);                          //
   vector<int> v1(2, 3);                       // different from curly braces: number of elements, value of elements
   vector<int> v2{ 2, 3 };
   //vector<int> v3( 3, 4, 5);                 // no such constructor
   vector<int> v4{ 3, 4, 5 };

   cout << "v0  :";
   for (auto a : v0) { cout << " " << a; }
   cout << endl << "v1  :";
   for (auto a : v1) { cout << " " << a; }
   cout << endl << "v2  :";
   for (auto a : v2) { cout << " " << a; }
   cout << endl << "v4  :";
   for (auto a : v4) { cout << " " << a; }
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
