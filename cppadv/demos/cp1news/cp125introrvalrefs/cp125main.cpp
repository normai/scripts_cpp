/**
 * file : 20201106`0111 cp125main.cpp, project 20200909`1611 Cp125IntroRvalRefs
 * summary : Try to follow article "A Brief Introduction to Rvalue References"
 * license : not specified
 * authors : Norbert C. Maier
 * note :
 */

int main_p1introrvalrefs();
int main_p2movecon();
int main_p3more();
int main_p4forward();

#include <iostream>
using namespace std;

int main()
{
   cout << "Try to follow article \"A Brief Introduction to Rvalue References\"" << endl;

   main_p1introrvalrefs();
   main_p2movecon();
   main_p3more();
   main_p4forward();

   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
