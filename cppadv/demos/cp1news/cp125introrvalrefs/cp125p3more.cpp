/**
 * file : 20200909`1633 cp12rv3more.cpp, project 20200909`1611 Cp125IntroRvalRefs (formerly project 20200909`1631)
 * summary : Follow article 'A Brief Introduction to Rvalue References' (IntroRvalRefs part 3 of 4)
 *            � Demonstrate move semantics with unique_ptr ('Movable but Non-Copyable Types')
 * authors : Hinnant/Stroustrup/Kozicki 2006-06-12 (modified by ncm)
 * license : not specified
 * reference : www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html [ref 20200911`0814]
 *     reprinted in www.artima.com/cppsource/rvalue.html [ref 20200911`0812].
 * note : The code at Artima is not 100 % that on open-std. Could not get run the
 *         move constructor example.
 * status : Starts working
 * todo : cleanup
 * note :
 */

#include <algorithm> // sort
#include <iostream>
#include <memory> // unique_ptr
#include <string>
#include <type_traits> ////
#include <vector>
using namespace std;

class A3 {
public:
   A3 () {};                           // todo: CHECK statement -- If copy constructor is deleted, also default constructor seems deleted, and we must restore it manually
   A3 (const A3&) = delete;            //// copy constructor may be deleted because not used below, only moves are used
   string name { "Anton" };
};

class B3 : public A3 {
public:
   string txB { "Berta" };
};

struct indirect_less
{
   template <class T> bool operator()(const T& x, const T& y)
   {
      if constexpr (is_pointer<T>::value) {
         return *x < *y;
      }
      else {
         return x < y;
      }
   }
};

// supplemented -- example how to replace above struct by a simple function
template <class T> bool indirect_lessFunc(const T& x, const T& y)
{
   return *x < *y;                                     // asterisk is no fun
}

int main_p3more()
{
   cout << "*** main_p3more" << endl;

   //--------------------------------------------------
   // () Move Semantics -- Movable but Non-Copyable Types

   vector<unique_ptr<A3>> v1;

   vector<unique_ptr<A3>> v2; v2.push_back(unique_ptr<A3>(new B3())); ///

   vector<shared_ptr<A3>> vShared;

   B3* b = new B3(); //// already this innocent line fails if copy constructor is deleted

   v1.push_back(unique_ptr<A3>(new B3()));             // ok, moving, not copying

   // same thing, just in two lines, so it is better observable in the debugger
   ////auto x = unique_ptr<A>(new B3());
   unique_ptr<A3> x1 = unique_ptr<A3>(new B3());
   v1.push_back(move(x1));                             // ok, moving, not copying
   //v1.push_back(x1);                                 // error, must use the move function, has no copy constructor

   // question: does the compiler automatically use the move semantics?
   ////unique_ptr<A> x2 = unique_ptr<A>(new B()); 
   shared_ptr<A3> x2 = shared_ptr<A3>(new B3());
   vShared.push_back(x2);                              // copy
   vShared.push_back(move(x2));                        // move also works

   cout << "(1) = " << v1[0]->name << endl;

   //v2 = v1;                  // compile time error 'this is not a copyable type'
   v2 = move(v1);              // Move ok. Ownership of pointers transferred to v2.
   ////vector<unique_ptr<A3>> v123 = move(v1);

   cout << "(2) = " << v2[0]->name << endl;
   //cout << "(3) = " << v1[0]->txA << endl; // runtime exception 'vector subscript out of range'

   // Many standard algorithms benefit from moving elements
   // .. of the sequence as opposed to copying them. ...
   // Note that unique_ptr is movable but not copyable.

   vector<unique_ptr<int>> v3;

   unique_ptr<int> up1 = make_unique<int>(66);
   v3.push_back(move(up1));
   v3.push_back(move(make_unique<int>(55)));
   v3.push_back(move(make_unique<int>(44)));

   cout << "(4) ";
   for (int i = 0; i < v3.size(); i++) {
      cout << " " << *(v3[i]);
   }
   cout << endl;

#if 0 //// both flavours work
   sort(v3.begin(), v3.end(), indirect_less());
#elif 1
   indirect_less il;
   sort(v3.begin(), v3.end(), il);
#else
   auto flf = indirect_lessFunc<decltype(v3[0])>;
   sort(v3.begin(), v3.end(), flf);
#endif

   cout << "(5) ";
   for (int i = 0; i < v3.size(); i++) {
      cout << " " << *(v3[i]);
   }
   cout << endl;

   //--------------------------------------------------

   vector<int> vOther {2, 1, 0, 7, 5};
   cout << "(6.1) ";
   for (int i : vOther) {
      cout << " " << i;
   }
   cout << endl;
   auto fil = indirect_less();
   sort(vOther.begin(), vOther.end(), fil); ////
   cout << "(6.2) ";
   for (int i : vOther) {
      cout << " " << i;
   }
   cout << endl;


   cout << "-------------------------------" << endl;
   return 0;
}
