
   Project Cp125IntroRvalRefs

   This projects tries to follow the article "A Brief Introduction to Rvalue
 References" by Howard E. Hinnant, Bjarne Stroustrup, Bronek Kozicki from 2006-Jun-12,
 published on http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html.

   The article is from the time before rvalue references are finally introduced
 with C++11, so it describes, what they want, which might slightly differ from
 what we got now.

   Project status: Not all code could be made run as is. Some sequences do not run
 at all (yet). The project code still needs clean up, structure and explanation.

   Files:

   • cp125main.cpp

   • cp125p1introrvalrefs.cpp

   • cp125p2movecon.cpp

   • cp125p3more.cpp

   • cp125p4forward.cpp

   —————————————————————————————————————————————
   [file 20200909°1612 project 20200909°1611] ܀Ω
