/**
 * file : 20200909`1613 cp12rv1rvals.cpp, project 20200909`1611 Cp125IntroRvalRefs
 * summary : Follow article 'A Brief Introduction to Rvalue References' (IntroRvalRefs part 1 of 4)
 * authors : Hinnant/Stroustrup/Kozicki 2006-06-12 (modified by ncm)
 * license : not specified
 * see : Article 'A Brief Introduction to Rvalue References'
 *        www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html [ref 20200911`0814]
 *        reprinted in www.artima.com/cppsource/rvalue.html [ref 20200911`0812]
 * note : The articles structure is the following
 *    (1) The rvalue reference
 *    (2) Move Sematics
 *    (2.1) Eliminating spurious copies
 *    (2.2) Overloading on lvalue / rvalue
 *    (2.3) Movable but Non-Copyable Types
 *    (3) Perfect Forwarding
 * status : Not ready
 * note :
 */

#include <iostream>
using namespace std;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// For (1)
class A1 {
public :
   int i {1};
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// For (2.1) Traditional swap function. (With 'void' supplemented.)
template <class T> void swap1(T& a, T& b)
{
   T tmp(a);                                           // now we have two copies of a
   a = b;                                              // now we have two copies of b
   b = tmp;                                            // now we have two copies of tmp (aka a)
}

// For (2.2) Swap function using move semantics
template <class T> void swap2(T& a, T& b)
{
   T tmp(move(a));                                     //// ?! Watch this in debugger. Why is a still valid after this line?
   a = move(b);
   b = move(tmp);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int main_p1introrvalrefs()
{
   cout << "*** main_p1introrvalrefs" << endl;

   //--------------------------------------------------
   // (1) The rvalue reference

   A1 a1;
   A1& lvr1 = a1;             // lvalue reference

   A1 a2;                     // though this line comes from the article, the next fails
   //A&& rvr2 = a2;           // error "an rvalue reference cannot be bound to an lvalue"

   //A& lvr3 = A1();          // Error! "Initial value of reference to non-const must be an l-value"
   A1&& rvr4 = A1();          // rvalue reference, bind to rvalue without being const, most important C++11 invention
   const A1& rvr22 = A1();    // with const, the lvalue ref can be used then ////

   const A1&& rvr5 = A1();    // rvalue reference (trivial)

   cout << "(1.1) : " << rvr4.i << endl;
   cout << "(1.2) : " << rvr5.i << endl;

   //--------------------------------------------------
   // (2) Move Semantics
   // (2.1) Eliminating spurious copies

   // traditional
   int i1 = 2;
   int i2 = 3;
   swap1(i1, i2);
   cout << "(2.1) : " << i1 << " " << i2 << endl;

   // move semantics
   int i3 = 2;
   int i4 = 3;
   swap2(i3, i4);
   cout << "(2.2) : " << i3 << " " << i4 << endl;

   // move semantics - second try
   int&& i5 = 2;
   int&& i6 = 3;
   swap2(i5, i6);
   cout << "(2.3) : " << i5 << " " << i6 << endl;

   //--------------------------------------------------
   // (2.2) Move Semantics -- Overloading on lvalue / rvalue
   // See cp12rv2movecon.cpp

   //--------------------------------------------------
   // (2.3) Move Semantics -- Movable but Non-Copyable Types
   // See cp12rv3more.cpp

   //--------------------------------------------------
   // (3)  Perfect Forwarding
   // See cp12rv4forward.cpp

   cout << "-------------------------------" << endl;
   return 0;
}
