/**
 * file : 20200909`1643 cp12rv42forward.cpp, project 20200909`1611 Cp125IntroRvalRefs (formerly project 20200909`1641)
 * summary : About Forwarding (IntroRvalRefs part 4 of 4)
 * license : not specified
 * authors : Howard E. Hinnant, Bjarne Stroustrup, Bronek Kozicki, 2006-06-12 (then modified)
 * see : www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html [ref 20200911`0814]
 *     or reprinted in www.artima.com/cppsource/rvalue.html [ref 20200911`0812]
 *     part (3) 'Perfect Forwarding'
 * status : Does not (yet) run as expected
 * note :
 */

#include <iostream>
#include <memory> // shared_ptr
#include <string>
#include <vector>
using namespace std;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// helper class

class A4 {
public:
   A4() { cout << "A4 created" << endl; }                      //// supplement output
   ~A4() { cout << "A4 destroyed" << endl; }                   //// supplement destructor
   A4(string x) : name(x) { cout << "A4 created" << endl; }    //// supplement output
   string name { "Anton" };
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// case 1 -- factory function traditional
template <class T> shared_ptr<T> factory1()   // no argument version
{
   return shared_ptr<T>(new T);
}

template <class T, class A1> shared_ptr<T> factory1(const A1& a1) // one argument version
{
   return shared_ptr<T>(new T(a1));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// case 2 -- use non-const parameters in factory function
template <class T, class A1> std::shared_ptr<T> factory2(A1& a1)
{
   return std::shared_ptr<T>(new T(a1));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// case 3 -- Rvalue references offer a simple, scalable solution to this problem
template <class T, class A1> std::shared_ptr<T> factory3(A1&& a1)
{
   return std::shared_ptr<T>(new T(std::forward<A1>(a1)));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int main_p4forward()
{
   cout << "*** main_p4forward" << endl;

   //--------------------------------------------------
   // case 1 -- use const parameters in factory function
   // The block allows watching the deletion of the shared_ptr
   {
      shared_ptr<A4> p11 = factory1<A4>("Berta");
      cout << "(1.1) " << p11->name << endl;

      // experiment
      shared_ptr<A4> p12 = p11;
      cout << "(1.2) " << p12->name << endl;
   }

   //--------------------------------------------------
   // case 2 -- use non-const parameters in factory function

   shared_ptr<A4> p2 = factory2<A4>("Caesar");           // error -- no?!
   A4* q = new A4("Dora");                               // ok

   cout << "(2.1) " << p2->name << endl;
   cout << "(2.2) " << q->name << endl;

   //--------------------------------------------------
   // case 3 -- use rvalue reference in factory function

   std::shared_ptr<A4> p3 = factory3<A4>("Emil");

   cout << "(3)   " << p3->name << endl;

   cout << "-------------------------------" << endl;
   return 0;
}
