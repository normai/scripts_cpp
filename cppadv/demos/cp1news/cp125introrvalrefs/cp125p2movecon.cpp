/**
 * file : 20200909`1623 cp12rv2movecon.cpp, project 20200909`1611 Cp125IntroRvalRefs (formerly project 20200909`1621)
 * summary : Follow article 'A Brief Introduction to Rvalue References' (IntroRvalRefs part 2 of 4)
 * authors : Hinnant/Stroustrup/Kozicki 2006-06-12 (modified by ncm)
 * license : not specified
 * see : www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html [ref 20200911`0814]
 *        reprinted in www.artima.com/cppsource/rvalue.html [ref 20200911`0812].
 * status : Starts working. Could not get run the move constructor example.
 * todo : cleanup
 * note :
 */

#include <iostream>
#include <string>
using namespace std;

class A2 {
public:
   string name{ "Anton." };
   A2* clone() const { return new A2(*this); }
};

class B2 : public A2 {
public:
   string txB{ "Berta." };
   A2* clone() const { return new B2(*this); }
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// For () Class with move semantics

template <class T> class clone_ptr
{
private:
   //T* ptr;
public:
   T* ptr; // experimentally placed here

   // construction
   explicit clone_ptr(T* p = 0) : ptr(p) {}

   // destruction
   ~clone_ptr() { delete ptr; }

   // copy semantics
   clone_ptr(const clone_ptr& p) : ptr(p.ptr ? p.ptr->clone() : 0) {}

   clone_ptr& operator=(const clone_ptr& p)
   {
      if (this != &p)
      {
         delete ptr;
         ptr = p.ptr ? p.ptr->clone() : 0;
      }
      return *this;
   }

   // move semantics
   // note : MS compiler warning "C26439: This kind of function may not throw. Declare it 'noexcept'"
   clone_ptr(clone_ptr&& p) : ptr(p.ptr) {
      p.ptr = 0;
   }

   clone_ptr& operator=(clone_ptr&& p)
   {
      swap(ptr, p.ptr);
      return *this;
   }

   // Other operations
   T& operator*() const { return *ptr; }
   // ...
};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int main_p2movecon()
{
   cout << "*** main_p2movecon" << endl;

   //--------------------------------------------------
   // () Move Semantics -- Overloading on lvalue / rvalue

   // first experiments
   clone_ptr<A2> p1(new B2());
   cout << "(1) " << p1.ptr->name << endl;

   clone_ptr<A2> p2 = p1;
   cout << "(2) " << p2.ptr->name << endl;
   cout << "(3) " << p1.ptr->name << endl;

   B2* p3 = new B2();
   clone_ptr<B2> p4(p3);
   cout << "(4) " << p3->name << " " << p3->txB << endl;
   cout << "(5) " << p4.ptr->name << " " << p4.ptr->txB << endl;

   // lines from paper
   clone_ptr p5 = p1;          // p2 and p1 each own their own pointer
   cout << "(6) " << p5.ptr->name << "" << p1.ptr->name << " " << &p5 << " " << &p1 << endl;

   // lines from paper
   clone_ptr<A2> p11(new B2);
   cout << "(7) " << p11.ptr->name << " " << &p11.ptr << endl;

   clone_ptr<A2> p12 = move(p11);  // p12 now owns the pointer intead of p1

   cout << "(8) " << p12.ptr->name << " " << &p12.ptr << endl;
   //cout << "(9) " << p11.ptr->txA << " " << &p11.ptr << endl; // exception 'access violation'





   cout << "-------------------------------" << endl;
   return 0;
}
