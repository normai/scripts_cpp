/**
 * file : 20200825`0321 cp15fold1demo.cpp, project 20200825`0311
 * summary : Overview on foldings
 * author : Norbert C. Maier
 * license : CC BY-SA 3.0 (after StackOverflow)
 * note : Snippets compiled from
 *    � riptutorial.com/cplusplus/topic/2676/fold-expressions [ref 20200912`1234]
 *    � baptiste-wicht.com/posts/2015/05/cpp17-fold-expressions.html [ref 20200912`1213]
 *    � stackoverflow.com/questions/3906796/how-to-fold-stl-container [ref 20200912`1226]
 */

#include <iostream>
#include <list>
#include <numeric> // accumulate
using namespace std;

// prefix examples with a non-template function
// No, no example found quickly with out template
/*
int addUnaryLeft(int... args) {
   return (... + args);
}
*/

// additional demo (supplied via chat)
template<typename ...Args>
void printer(Args&&... args) {          // works
////void printer(Args&... args) {       // fails "cannot convert .."
////void printer(const Args&... args) { // works
   (std::cout << ... << args) << '\n';
}

//-----------------------------------------------------
// (1) Unary left fold [seq/file 20200825`0331]
template<typename... Ts> int addUnaryLeft(Ts... args)
{
   return (... + args);
}

template<typename... Ts> int subUnaryLeft(Ts... args)
{
   return (... - args);
}

void unary_left()
{
   int i = addUnaryLeft(1000, 100, 10, 1);
   cout << "(1) Unary left     add: i = " << i;

   i = subUnaryLeft(1000, 100, 10, 1);       // (((1000 - 100) - 10) - 1)
   cout << "   sub: i = " << i << endl;
}

//-----------------------------------------------------
// (2) Unary right -- unary left and unary right are equivalent if operator is associative [seq/file 20200825`0341]
// For +, ((1+2)+3) (left fold) == (1+(2+3)) (right fold)
// For -, ((1-2)-3) (left fold) != (1-(2-3)) (right fold)

template<typename... Ts> int addUnaryRight(Ts... args)
{
   return (args + ...);
}

template<typename... Ts> int subUnaryRight(Ts... args)
{
   return (args - ...);
}

void unary_right()
{
   int i = addUnaryRight(1000, 100, 10, 1);
   cout << "(2) Unary right    add: i = " << i;

   i = subUnaryRight(1000, 100, 10, 1);         // (1000 - (100 - (10 - 1)))
   cout << "   sub: i = " << i << endl;
}

//-----------------------------------------------------
// (3) Binary left fold [seq/file 20200825`0351]

template<typename... Ts> int addBinaryLeft(Ts... args)
{
   return (3 + ... + args);
}

template<typename... Ts> int subBinaryLeft(Ts... args)
{
   return (3 - ... - args);
   ////return (... - args - 3); // error "unexpected token ..."
}

void binary_left()
{
   int i = addBinaryLeft(1000, 100, 10, 1);
   cout << "(3) Binary left    add: i = " << i;

   i = subBinaryLeft(1000, 100, 10, 1);              // ((((3 - 1000) - 100) - 10) - 1)
   cout << "   sub: i = " << i << endl;
}

//-----------------------------------------------------
// (4) Binary right fold [seq/file 20200825`0401]

template<typename... Ts> int addBinaryRight(Ts... args)
{
   return (args + ... + 3);
}

template<typename... Ts> int subBinaryRight(Ts... args)
{
   return (args - ... - 3);
   ////return (3 - args - ...); // error "unexpected token ..."
}

void binary_right()
{
   int i = addBinaryRight(1000, 100, 10, 1);
   cout << "(4) Binary right   add: i = " << i;

   i = subBinaryRight(1000, 100, 10, 1);                      // (1000 - (100 - (10 - (1 - 3))))
   cout << "   sub: i = " << i << endl;
}

//-----------------------------------------------------
// (5) Folding over comma [seq/file 20200825`0411]

// C++11
template <class... Ts>
void print_all_1(ostream& os, Ts const&... args) {
   using expander = int[];
   (void)expander {              // not possible to exchange by "int[]"
      0,
         (void(os << args), 0) ...
   };
}

// C++14
template <class... Ts>
void print_all_2(ostream& os, Ts const& ... args) {
   (os << ... << args) << '\n';                        // This suffices
}

template <class... Ts>
void print_all_3(ostream& os, Ts const & ... args) {
   ((os << args), ...);                                //// This is too much, but works
}

void foldOverComma()
{
   print_all_1(cout, "(5.1) ", "C++11 parapack : ", "Holla", "ria", 6, "ho.", '\n');
   print_all_2(cout, "(5.2) ", "C++17 folding  : ", "Holla", "ria", 7, "ho.");
   print_all_3(cout, "(5.3) ", "               : ", "Holla", "ria", 8, "ho.", '\n');
}

//-----------------------------------------------------
// (6) Using accumulate [seq/file 20200825`0421]
// This is not a fold expression but belongs to the broarder theme

template<typename collection, typename operation>
typename collection::value_type reduce(collection col, operation op)
{
   return accumulate(col.begin(), col.end(), typename collection::value_type(), op);
}

///int myFunc(int x, int y) { return x * y; }
int myFunc(int x, int &y) {
 y++;
   cout << " myFunc : x = " << x << " y = " << y << endl;
   x = (x > 0) ? x : 1;
   return x * y;
} //

void accumulat()
{
   list<int> l = { 2, 4, 30, 1, 5 };
   list <int> &x = l;                                  // use alias just for fun
   int i = reduce(x, myFunc);
   cout << "(6) Use accumulate: i = " << i << endl;

   cout << "(7) quicktest :";
   for (int i : l) { cout << " " << i; }
}

int main()
{
   cout << "Hello Foldings" << endl;

   unary_left();
   unary_right();
   binary_left();
   binary_right();
   foldOverComma();
   accumulat();

   // bonus example
   printer("A", "B", 2, 3, 'C');

   cout << "Press ENTER to exit.";
   [[maybe_unused]] int i = getchar();
   return 0;
}
