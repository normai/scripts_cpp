/**
 * file : 20200824`0321 cp15fold3bptst.cpp, proj 20200824`0311
 * summary : Folding example code
 * note  : Code after baptiste-wicht.com/posts/2015/05/cpp17-fold-expressions.html [ref 20200912`1214]
 * license : MIT
 */

#include <iostream>
using namespace std;

auto old_suma() {
   return 0;
}

template<typename T1, typename... T2>
auto old_suma(T1 x, T2... ts) {
   return x + old_suma(ts...);
}

void showVariadic()
{
   int i = old_suma(1, 2, 3, 4, 5, 6, 7);
   cout << "showVariadic sum = " << i << endl;
}

int main()
{
    cout << "This is Cp15Fold3Bptst .." << endl;

    showVariadic();

    cout << "Press ENTER to exit.";
    (void) getchar();
}
