/**
 * file : 20200913`0613 cp124forward.cpp, project 20200913`0611
 * summary : This demonstrates forwarding
 * author : Rainer Grimm, modified by ncm
 * license : GNU General Public License v3.0
 * original : github.com/RainerGrimm/ModernesCppSource/blob/master/source/perfectForwarding3.cpp
 * see : github.com/RainerGrimm/ModernesCppSource [ref 20200913`0553]
 * see : Below we saw forward<> exchangable by move(). So what?
 *        stackoverflow.com/questions/9671749/whats-the-difference-between-stdmove-and-stdforward [ref 20201013`1112]
 * todo : Supplement Stackoverflow link in script below quadrant diagram.
 * note :
 */

#include <iostream>
using namespace std;

// for additional experiment
class A {
public:
   A() { cout << " default-ctor " << endl; };
   A(const A&) { cout << " copy-ctor " << endl; }
   A(A&&) { cout << " move-ctor " << endl; }
   ~A () { cout << " dtor " << endl;  };
   string name{ "Anton" };
};

template <typename T, typename Arg> T create(Arg&& a) {
   cout << "     rv : " << is_rvalue_reference<Arg&&>::value << endl; // just for fun
   return T(forward<Arg>(a));
   //return T(move(a)); //// This line works as well. Why? (see reference above)
   //return T(a);          // this works, but goes by copy constructor
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int main()
{
   cout << "Demonstrate forwarding .." << endl;

   // Lvalues
   int five = 5;
   int myFive1 = create<int>(five);
   cout << "myFive1 : " << myFive1 << endl;

   // Rvalues
   int myFive2 = create<int>(5);
   cout << "myFive2 : " << myFive2 << endl;

   // Additional experiments
   A* myA1 = create<A*>(new A());
   cout << "myA1.name : " << myA1->name << endl;

   A myA2 = create<A>(A());
   cout << "myA2.name : " << myA2.name << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
