/**
 * file : 20200825`0213 cp15fold6stacko.cpp, project 20200825`0211
 * summary : This are some further folding usages ...
 * authors : various
 * license : CC BY-SA 3.0
 * ref : riptutorial.com/cplusplus/topic/2676/fold-expressions [ref 20200912`1234] after StackOverflow
 * ref : riptutorial.com/cplusplus/example/8932/binary-folds [ref 20200912`1236] after StackOverflow
 * ref : riptutorial.com/cplusplus/example/14773/folding-over-a-comma [ref 20200912`1237] after StackOverflow
 * ref : riptutorial.com/cplusplus/example/8931/unary-folds [ref 20200912`1238] after StackOverflow
 */

#include <iostream>
using namespace std;

//-----------------------------------------------------
// Binary Folds

template<typename... Ts>
int removeFrom(int num, Ts... args)
{
   return (num - ... - args); // Binary left fold
   // Note that a binary right fold cannot be used
   // due to the lack of associativity of operator-
}

//-----------------------------------------------------
// Folding over a comma

template <class... Ts>
void print_all_1(std::ostream& os, Ts const&... args) {
   using expander = int[];
   (void) expander {
      0, (void(os << args), 0)...
   };
}

template <class... Ts>
void print_all_2(std::ostream& os, Ts const&... args) {
   (void(os << args), ...);
}

//-----------------------------------------------------
// Unary Folds

template<typename... Ts>
int sum(Ts... args)
{
   return (... + args); //Unary left fold
   //return (args + ...); //Unary right fold

   // The two are equivalent if the operator is associative.
   // For +, ((1+2)+3) (left fold) == (1+(2+3)) (right fold)
   // For -, ((1-2)-3) (left fold) != (1-(2-3)) (right fold)
}

//-----------------------------------------------------

int main()
{
   cout << "This is Cp15Fold6Stacko, demonstrating further folding" << endl;
   //----------------------------------------------------------

   int iResult = removeFrom(1000, 5, 10, 15);                  //'result' is 1000 - 5 - 10 - 15 = 970
   cout << "(1) Binary fold result : " << iResult << endl;

   cout << "(2) print_all_1        : ";
   print_all_1(cout, "Oha", "aha", 2, 3, "oho", '\n');

   cout << "(3) print_all_2        : ";
   print_all_2(cout, "Holla", "ha", 4, 5, "huch", '\n');

   int result = sum(1, 2, 3); //  6
   cout << "(4) Unary left fold    : " << result << endl;

   //----------------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
