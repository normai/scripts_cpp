/**
 * file : 20200909`1713 cp13inits.cpp, project 20200909`1711
 * summary : Demonstrate various flavours of initialization lists
 * authors : cppreference.com (then modified)
 * license : CC BY-SA 4.0
 * see : en.cppreference.com/w/cpp/language/list_initialization [ref 20200911`0832]
 */

#include <iostream>
#include <vector>
#include <map>
#include <string>
using namespace std;

struct Foo {
   vector<int> mem1 = { 1,2,3 };               // list-initialization of a non-static member
   vector<int> mem2;
   Foo() : mem2 { -1, -2, -3 } {}              // list-initialization of a member in constructor
};

pair<string, string> f(pair<string, string> p)
{
   return { p.second, p.first };               // list-initialization in return statement
}

int main()
{
   cout << "This is Cp13Inits, demonstrating initialization lists .." << endl;
   //--------------------------------------------------

   int n0{};                                   // value-initialization (to zero)
   int n42 = {};                               //// also possible [[but using copy constructor ?]]
   int n1{ 1 };                                // direct-list-initialization

   string s1{ 'a', 'b', 'c', 'd' };            // initializer-list constructor call
   string s2{ s1, 2, 2 };                      // regular constructor call //// using substring
   string s3{ 0x61, 'a' };                     // initializer-list ctor is preferred to (int, char)

   int n2 = { 1 };                             // copy-list-initialization
   double d = double{ 1.2 };                   // list-initialization of a prvalue, then copy-init
   double d2 = { 1.2 };                        ////
   double d3 = { 1.2f };                       ////
   auto s4 = string{ "HelloWorld" };           // same as above, no temporary created since C++17

   map<int, string> m = {                      // nested list-initialization
          {1, "a"},

          {2, {'a', 'b', 'c'} },
          {3, s1}
   };

   cout << f({ "hello", "world" }).first       // list-initialization in function call
      << '\n';

   const int(&ar)[2] = { 1, 2 };               // binds a lvalue reference to a temporary array
   const int(&ar3)[] = { 1, 2 }; // fishy      //// works as well but not as expected -- what happens with the elements?
   const int ar2 [] = { 1, 2 };                //// experiment
   int&& r1 = { 1 };                           // binds a rvalue reference to a temporary int
   int&& r42 { 1 };                            //// omit assignment operator // more efficient(?)
   //int& r2 = {2};                            // error: cannot bind rvalue to a non-const lvalue ref

   //int bad { 1.0 };                          // error: narrowing conversion
   unsigned char uc1{ 10 };                    // okay
   //unsigned char uc2{-1};                    // error: narrowing conversion

   Foo f;

   cout << "(1) : " << n0 << ' ' << n1 << ' ' << n2 << endl;
   cout << "(2) : " << s1 << ' ' << s2 << ' ' << s3 << endl;

   cout << "(3) :";
   for (auto p : m) {
      cout << " " << p.first << '/' << p.second;
   }
   cout << endl;

   cout << "(4) : ";
   for (auto n : f.mem1) {
      cout << n << ' ';
   }
   cout << endl;

   cout << "(5) : ";
   for (auto n : f.mem2) {
      cout << n << ' ';
   }
   cout << endl;

   cout << "(6) : " << "d2 " << d2 << endl;
   cout << "(7) : " << "d3 " << d3 << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
