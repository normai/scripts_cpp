/**
 * file : 20200825`0632 cp11range3map.cpp, project 20200825`0631, solution 20200825`0601
 * summary : Demonstrate a range based for loop over a map
 * license : CC-BY-SA 4.0
 * authors : stackoverflow contributors
 * ref : stackoverflow.com/questions/6963894/how-to-use-range-based-for-loop-with-stdmap [ref 20200909`1416]
 */

#include <iostream>
#include <map>
#include <string>
using namespace std;

int main() {
   cout << "This is Cp11Range3Map demonstrating range-based for loop over a map .." << endl;
   //--------------------------------------------------

   map<const int, string> m1; // = new map<int, string>();
   m1.emplace(1, "Anton");
   m1.emplace(3, "Berta");
   m1.emplace(4, "Cleo");
   m1.emplace(42, "Doris");
   m1.emplace(-7, "Emma");

   map<int, string> m2 = { {1, "Anton"},  {3, "Berta"}
                        ,  {4, "Cleo"},  {42, "Doris"},  {-7, "Emma"} }
                         ;

   cout << "(1) ";
   for (const auto & [key, value] : m1) {
      cout << "  " << key << " " << value;
   }
   cout << endl;

   cout << "(2) ";
   for (auto & [key, value] : m2) {
      cout << "  " << key << " " << value;
   }
   cout << endl;

   cout << "(3) ";
   for (const auto& kv : m1) {
      cout << "  " << kv.first << " " << kv.second;
   }
   cout << endl;

   cout << "(4) ";
   for (auto & kv : m1) {
      cout << "  " << kv.first << " " << kv.second;
      //cout << "  " << typeid(kv).name();
   }
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
