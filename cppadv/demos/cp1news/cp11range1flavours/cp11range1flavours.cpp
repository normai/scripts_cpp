/**
 * file : 20200825`0612 cp11range1flavours.cpp, project 20200825`0611
 * summary : Demonstrate the usage flavours of a range based for loop
 * license : CC-BY-SA 3.0
 * authors : cppreference.com (then modified)
 * see : en.cppreference.com/w/cpp/language/range-for [ref 20200909`1412]
 */

#include <iostream>
#include <vector>
using namespace std;

int main() {
   cout << "Cp11Range1Flavours demonstrating range-based-for loop variable types .." << endl;
   //--------------------------------------------------

   vector<int> v = { 0, 1, 2, 3, 4, 5 };

   // (1) access by const reference
   cout << "(1)    ";
   for (const int& i : v) {
      cout << i << ' ';
   }
   cout << endl;

   // (1.1) const without reference? Yes
   cout << "(1.1)  ";
   for (const int i : v)
      cout << i << ' ';
   cout << endl;

   // // (1.2) const with rval ref? No! ////
   // cout << "(1.2)  ";
   // for (const int&& i : v) // error "cannot convert from 'int' to 'const int &&'"
   //   cout << i << ' ';
   // cout << endl;

   // (1.3) nonconst with rval ref? No! ////
   // cout << "(1.3)  ";
   // for (int&& i : v)
   // cout << i << ' ';
   // cout << endl;

   // (1.4) access by reference (not listed on cppreference page) ////
   cout << "(1.4) ";
   for (int& i : v) {
      cout << " " << i << ' ';
      cout << " '" << typeid(i).name() << "'"; // experiment
   }
   cout << endl;

   // (2) access by value, the type of i is int
   cout << "(2)    ";
   for (auto i : v)
      cout << i << ' ';
   cout << endl;

   // (3) access by forwarding reference, the type of i is int&
   // i is no more mutable?
   cout << "(3)   ";
   for (auto&& i : v) {
      cout << " " << i << ' ';
      cout << " '" << typeid(i).name() << "'"; //// experiment
   }
   cout << endl;

   // (4) access by f-d reference, the type of i is const int&
   cout << "(4)    ";
   const auto& cv = v;
   for (auto&& i : cv)
      cout << i << ' ';
   cout << endl;

   // (5) the initializer may be a braced-init-list
   cout << "(5)    ";
   for (int n : {0, 1, 2, 3, 4, 5}) {
      cout << n << ' ';
   }
   cout << endl;

   // (6) the initializer may be an array
   cout << "(6)    ";
   int a[] = { 0, 1, 2, 3, 4, 5 };
   for (int n : a)
      cout << n << ' ';
   cout << endl;

   // (7) the loop variable need not be used
   cout << "(7)    ";
   for ([[maybe_unused]] int n : a)  //// curiously, also without '[[maybe_unused]]' MS compiler shows no warning
      cout << 1 << ' ';
   cout << endl;

   // (8) use the init-statement (C++20)
   #if 0
      cout << "(8.1)  ";
      for (auto n = v.size(); auto i : v) {
         cout << --n + i << ' ';
      }
      cout << endl;

      /*
      cout << "(8.2)  ";
      for (int x = 1, auto n = v.size(); auto i : v) {    // FAILS
         cout << --n + i << ' ';
      }
      cout << endl;
      */

      cout << "(8.3)  ";
      for (int n = 123, x = 1; auto i : v) {
         cout << --n + i << ' ';
      }
      cout << endl;

   #endif

   //--------------------------------------------------
   cout << "Press Enter to exit.";
   (void) getchar();
   return 0;
}
