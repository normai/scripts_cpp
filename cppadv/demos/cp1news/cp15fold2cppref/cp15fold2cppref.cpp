/**
 * file : 20200823`0131 cp15fold2cppref.cpp, project 20200823`0121
 * origin : Code after CppReference page "fold expression(since C++17)"
 *           en.cppreference.com/w/cpp/language/fold [ref 20200912`1217]
 * license : CC BY-SA 4.0 en.cppreference.com/w/Cppreference:Copyright/CC-BY-SA [ref 20201013`1114]
 * note : VS2017 requries compiler flag '/std:c++17' [screenshot 20200823`0141]
 */

#include <iostream>
#include <climits>
#include <cstdint>
#include <type_traits>
#include <utility>
#include <vector>
using namespace std;

template<typename ...Args>
void printer(Args&&... args) {
   (cout << ... << args) << endl;                      // VS2017 compiler error "fold expresson require at least '/std:c++17'
}

template<typename T, typename... Args>
void push_back_vec(vector<T>& v, Args&&... args)
{
   static_assert((is_constructible_v<T, Args&&> && ...));
   (v.push_back(forward<Args>(args)), ...);
}

// compile-time endianness swap based on stackoverflow.com/a/36937049 
template<class T, size_t... N>
constexpr T bswap_impl(T i, index_sequence<N...>) {
   return (((i >> N * CHAR_BIT & uint8_t(-1)) << (sizeof(T) - 1 - N)*CHAR_BIT) | ...);
}
template<class T, class U = make_unsigned_t<T>>
constexpr U bswap(T i) {
   return bswap_impl<U>(i, make_index_sequence<sizeof(T)>{});
}

int main()
{
   cout << "This is Cp15Fold2CppRef .." << endl;

   printer(1, 2, 3, "abc");

   vector<int> v;
   push_back_vec(v, 6, 2, 45, 12);
   push_back_vec(v, 1, 2, 9);
   for (int i : v) cout << i << ' ';
   cout << endl;

   static_assert(bswap<uint16_t>(0x1234u) == 0x3412u); // VS2017 compiler error "language feature 'terse static assert' reuqires compiler flag '/std:c++17'
   static_assert(bswap<uint64_t>(0x0123456789abcdefULL) == 0xefcdab8967452301ULL);

   cout << "Press ENTER to exit.";
   (void) getchar();
}
