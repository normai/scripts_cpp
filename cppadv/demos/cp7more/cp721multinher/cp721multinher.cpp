/**
 * file : 20200912�0213 cp73multinher.cpp, project 20200912�0211
 * summary : Demonstrate multiple inheritance
 * authors : wikipedia.org, modified by ncm
 * license : CC BY-SA 3.0
 * encoding : ANSI
 * see : en.wikipedia.org/wiki/Virtual_inheritance [ref 20200913�0413]
 * see : stackoverflow.com/questions/6747089/programmatically-getting-the-name-of-a-derived-class [ref 20201105�0913]
 * see : isocpp.org/wiki/faq/virtual-functions [ref 20201109�0942]
 * note : This also demonstrates how to get the derrived class name from code inside
 *         the base class, which is done by using 'typeid(*this).name()'.
 * see : isocpp.org/wiki/faq/virtual-functions [ref 20201109`0942]
 * note :
 */

#include <iostream>
using namespace std;

struct Animal {
   virtual ~Animal() = default;                        // declare virtual destructor for any class with a virtual function (CPPPL4 p. 488)
   virtual void Eat() {
      cout << "eat" << " " << typeid(*this).name() << " (" << typeid(this).name() << ")" << endl;
   }
};

// Two classes virtually inheriting Animal:
struct Mammal : virtual Animal {
   virtual void Breathe() {
      cout << "breath " << typeid(*this).name() << endl;
   }
};

struct WingedAnimal : virtual Animal {
   virtual void Flap() {
      cout << "flap " << typeid(*this).name() << endl;
   }
};

// A bat is still a winged mammal
struct Bat : Mammal, WingedAnimal {
};

int main()
{
   cout << "This demonstrates multiple inheritance .." << endl;
   //--------------------------------------------------

   Animal a;
   a.Eat();                                            //// not always sensible

   Mammal m;
   m.Eat();                                            //// not always sensible

   Bat b;
   b.Eat();
   b.Breathe();
   b.Flap();

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
