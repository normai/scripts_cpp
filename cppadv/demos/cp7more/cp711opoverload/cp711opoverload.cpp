/**
 * file : 20200911`0713 cp711opoverload.cpp, project 20200911`0711
 * summary : This demonstrates operator overloading
 * authors : cppreference.com
 * license : CC BY-SA 4.0
 * see : en.cppreference.com/w/cpp/language/operators [ref 20200913`0312]
 * note :
 */

#include <iostream>
using namespace std;

class Fraction
{
   int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
   int n, d;
 public:
   Fraction(int n, int d = 1) : n(n / gcd(n, d)), d(d / gcd(n, d)) { } // normalisation
   int num() const { return n; }
   int den() const { return d; }

   /*
   Fraction& operator*=(const Fraction& rhs)
   {
      int new_n = n * rhs.n / gcd(n * rhs.n, d * rhs.d);
      d = d * rhs.d / gcd(n * rhs.n, d * rhs.d);
      n = new_n;
      return *this;
   }
   */
};

ostream& operator <<(ostream& out, const Fraction& f)
{
   return out << f.num() << '/' << f.den();
}
bool operator ==(const Fraction& lhs, const Fraction& rhs)
{
   return lhs.num() == rhs.num() && lhs.den() == rhs.den();
}
bool operator !=(const Fraction& lhs, const Fraction& rhs)
{
   return !(lhs == rhs);
}
Fraction operator *(Fraction lhs, const Fraction& rhs)
{
   ////return lhs *= rhs;
   int iNum = lhs.num() * rhs.num();
   int iDen = lhs.den() * rhs.den();
   return Fraction(iNum, iDen); // normalization again
}

int main()
{
   cout << "Demonstrate operator overloading .." << endl;
   //--------------------------------------------------

   Fraction f1(3, 8), f2(1, 2), f3(10, 2);

   cout << f1 << " * " << f2 << " = " << f1 * f2 << endl;
   cout << f2 << " * " << f3 << " = " << f2 * f3 << endl;
   cout << 2 << " * " << f1 << " = " << 2 * f1 << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
