/**
 * file : 20201105�1013 cp732multinher.cpp, project 20201105�1011 Cp732MultInher
 * summary : Demonstrate multiple inheritance with abstract base classes.
 *     Like project 20200912�0211, only with a pure virtual Eat function.
 * authors : wikipedia.org, modified by ncm
 * license : CC BY-SA 3.0
 * encoding : ANSI
 * chain : projects 20200912�0211 20201105�1011
 * see : isocpp.org/wiki/faq/virtual-functions [ref 20201109`0942]
 * note :
 */

#include <iostream>
using namespace std;

struct Animal {
   virtual ~Animal() = default;                        // declare virtual destructor for any class with a virtual function (CPPPL4 p. 488)
   virtual void Eat() = 0;
};

// Two classes virtually inheriting Animal:
struct Mammal : virtual Animal {
   virtual void Breathe() {
      cout << "breath " << typeid(*this).name() << endl;
   }
};

struct WingedAnimal : virtual Animal {
   virtual void Flap() {
      cout << "flap " << typeid(*this).name() << endl;
   }
};

// A bat is still a winged mammal
struct Bat : Mammal, WingedAnimal {
   void Eat() {
      cout << "eat" << " " << typeid(*this).name() << " (" << typeid(this).name() << ")" << endl;
   }
   ~Bat() = default;                                   // override Animal::~Animal() (see CPPP4 p.488)
};

int main()
{
   cout << "This demonstrates multiple inheritance with abstract base classes" << endl;
   //--------------------------------------------------

   Bat b;
   b.Eat();
   b.Breathe();
   b.Flap();

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
