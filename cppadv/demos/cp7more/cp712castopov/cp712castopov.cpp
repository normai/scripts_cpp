/**
 * file : 20200923`1513 cp712castopov.cpp, project 20200923`1511 Cp712CastOpOv
 * summary : Demonstrate cast operator overload, means user-defined conversion function.
 * license : MIT
 * authors : ncm
 * see : en.cppreference.com/w/cpp/language/cast_operator [ref 20200202`1427]
 * todo : Cleanup [todo 20201015`1531]
 */

#include <iostream>
#include <vector>
using namespace std;

struct X {
   // (1) overload implicit conversion operator
   operator int() const {
      cout << "cast 1" << endl;
      return 7;
   }

   // (2) overload explicit conversion operator
   explicit operator int* () const {
      cout << "cast 2" << endl;
      return nullptr;
   }

   // (3) overload operator int(*)[3]() const { return nullptr; }
   // Error: array operator not allowed in conversion-type-id
   using arr_t = int[3];
   operator arr_t* () const {                          // OK if done through typedef
      cout << "cast 3" << endl;
      return nullptr;
   }
   //operator arr_t () const;                          // Error: conversion to array not allowed in any case
};

int main() {
   cout << "This demonstrates some cast operator overloads" << endl;
   //--------------------------------------------------

   X x;

   int i1 = x;                                         // OK: implicitly sets m to 7
   int i2 = static_cast<int>(x);                       // OK: explicitly sets n to 7
   cout << "i1 = " << i2 << " i2 = " << i2 << endl;

   //int* p11 = x;                                     // Error: no implicit conversion
   int* p12 = static_cast<int*>(x);                    // OK: explicitly sets p to null
   cout << "p12 = " << p12 << endl;

   //int(*p21)[2] = x;                                 // Error "no suitable conversion function .."
   int(*p22)[3] = x;                                   // OK
   cout << "p22 = " << p22 << endl;

   //--------------------------------------------------
   cout << "Press Enter to exit.";
   (void) getchar();
   return 0;
}
