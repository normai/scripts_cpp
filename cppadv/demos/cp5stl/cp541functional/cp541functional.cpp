/**
 * file : 20200911`0113 cp541functional, project 20200911`0111
 * summary : Demonstrate various definitions for the function template class
 * authors : www.cplusplus.com
 * license : unspecified
 * see : www.cplusplus.com/reference/functional/function/function/ [ref 20200912`1415]
 * note :
 */

#include <functional> // std::function, std::negate
#include <iostream>
using namespace std;

// a function:
int half(int x) { return x / 2; }

// a function object class:
struct third_t {
   int operator()(int x) { return x / 3; }
};

// a class with data members:
struct MyValue {
   int value;
   int fifth() { return value / 5; }
};

int main()
{
   cout << "Demonstrate various definitions for the function template class .." << endl;
   //--------------------------------------------------
   // (1) Examples from www.cplusplus.com

   std::function<int(int)> fn1 = half;                         // function
   std::function<int(int)> fn2 = &half;                        // function pointer
   std::function<int(int)> fn3 = third_t();                    // function object
   std::function<int(int)> fn4 = [](int x) {return x / 4; };   // lambda expression
   std::function<int(int)> fn5 = std::negate<int>();           // standard function object

   std::cout << "fn1(60)      : " << fn1(60) << '\n';
   std::cout << "fn2(60)      : " << fn2(60) << '\n';
   std::cout << "fn3(60)      : " << fn3(60) << '\n';
   std::cout << "fn4(60)      : " << fn4(60) << '\n';
   std::cout << "fn5(60)      : " << fn5(60) << '\n';

   // stuff with members:
   std::function<int(MyValue&)> value = &MyValue::value;      // pointer to data member
   std::function<int(MyValue&)> execfifth = &MyValue::fifth;  // pointer to member function

   MyValue sixty{ 60 };

   std::cout << "value(sixty) : " << value(sixty) << '\n';
   std::cout << "fifth(sixty) : " << execfifth(sixty) << '\n';

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
