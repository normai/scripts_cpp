﻿
   Solution cp5stl

   • cp521iterators    — Demo after CppReference.com

   • cp522customits    — Just the beginning of a sketch, nothing working yet

   • cp523iterators3   — Empty frame for planned demo

   • cp531containers   — Demonstrate simple use of a simple STL container, a vector

   • cp532containers2  — Empty frame for planned demo ..

   • cp541functional   — Demonstrate various definitions for the function template class

   • cp551algos1       — (Empty frame for planned demo) Demonstrate simple algorithm function

   • cp552algos2       — Demonstrate a function from the algorithm header (one in not yet used so far)

   • cp56parallel      — Demonstrate some parallel working algorithm


   ——————————————————————————————————————————————
   [file 20200910°2202 solution 20200910°2201] ܀Ω
