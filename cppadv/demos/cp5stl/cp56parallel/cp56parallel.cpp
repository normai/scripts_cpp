/**
 * file : 20200911`0213 cp56parallel.cpp, project 20200911`0211
 * summary : Demonstrate some parallel working algorithm
 * authors : cppreference.com
 * license : CC BY-SA 4.0
 * see : (1) en.cppreference.com/w/cpp/experimental/reduce -- the version which does not run [ref 20201015`1312]
 * see : (2) en.cppreference.com/w/cpp/algorithm/reduce -- *** the version which runs [ref 20201015`1322]
 * see : (3) devblogs.microsoft.com/cppblog/using-c17-parallel-algorithms-for-better-performance/ -- nice article [ref 20201015�1332]
 * see : (4) cplusplus.com/forum/general/219200/ -- solution for error in (ref1) [ref 20201015`1342]
 * see : (5) en.cppreference.com/w/cpp/compiler_support -- tables about which compiler knows what [ref 20200102`0213]
 * see : (6) stackoverflow.com/questions/51031060/are-c17-parallel-algorithms-implemented-already [ref 20201015`1352]
 * see : (7) stackoverflow.com/questions/42567998/how-do-i-use-the-new-c17-execution-policies [ref 20201015`1412]
 * todo : Supplement start times to know when exactly which function started like
 *         "time_t today_time = chrono::system_clock::to_time_t(t1);"  [todo 20201004`1031]
 * status : This runs with VS2019, but not with GCC/Clang (as of 2020-Oct-08)
 */

#include <chrono>
#include <execution> // GCC in CodeBlocks 20.03 does not (yet) know this file
#include <numeric>
#include <iostream>
#include <ctime>
#include <vector>
using namespace std;

void funcAccu(vector<double> v)
{
   const auto t1 = chrono::high_resolution_clock::now();
   const double result = accumulate(v.cbegin(), v.cend(), 0.0);
   const auto t2 = chrono::high_resolution_clock::now();
   const chrono::duration<double, milli> ms = t2 - t1;

   cout << fixed << "accumulate result " << result
         << " took " << ms.count() << " ms" << endl
          ;
}

void funcReduce(vector<double> v)
{
   const auto t1 = chrono::high_resolution_clock::now();
   const double result = reduce(execution::par, v.cbegin(), v.cend());
   const auto t2 = chrono::high_resolution_clock::now();
   const chrono::duration<double, milli> ms = t2 - t1;
   cout << "reduce     result " << result
         << " took " << ms.count() << " ms" << endl
          ;
}

int main()
{
   cout << "Demonstrate a parallel working algorithm .." << endl;
   //--------------------------------------------------

   // set empirical factor for tuning execution durations
   double d = 2.1;

   // provide load test object
   const vector<double> v((d * 10'000'007l), 0.5);
   cout << "Created vector with " << v.size() << " elements" << endl;

   funcAccu(v);
   funcReduce(v);

   //--------------------------------------------------
   cout << "Press Enter to exit.";
   (void) getchar();
   return 0;
}
