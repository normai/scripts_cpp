/**
 * file : 20200913`0123 cp552algos2.cpp, project 20200913`0121
 * summary : Demonstrate a function from the algorithm header (one in not yet used so far)
 * authors : cppreference.com, modified by ncm
 * license : CC BY-SA 4.0
 * see : code -- en.cppreference.com/w/cpp/algorithm/replace [ref 20201015`1222]
 * see : bind -- en.cppreference.com/w/cpp/utility/functional/bind [ref 20201015`1232]
 * see : less -- en.cppreference.com/w/cpp/utility/functional/less [ref 20201015`1242]
 * note :
 */

#include <algorithm> // replace, replace_if
#include <array>
#include <functional> // bind, less
#include <iostream>
using namespace std;

int main()
{
   cout << "This is cp531algorithm.cpp .." << endl;
   //--------------------------------------------------

   array<int, 10> ar { 5, 7, 4, 2, 8, 6, 1, 9, 0, 3 };

   // (1) use case simple
   replace(ar.begin(), ar.end(), 8, 88);

   cout << "(1)   :";
   for (int a : ar) {
      cout << " " << a;
   }
   cout << endl;

   // (2.1) use case not so simple, using bind/less
   replace_if(ar.begin(), ar.end(), bind(less<int>(), placeholders::_1, 4), 44);
   cout << "(2.1) :";
   for (int a : ar) {
      cout << " " << a;
   }
   cout << endl;

   // (2.2) use case not so simple, using a plain lambda
   replace_if(ar.begin(), ar.end(), [](int i) { return (i < 6); }, 66);
   cout << "(2.2) :";
   for (int a : ar) {
      cout << " " << a;
   }
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
