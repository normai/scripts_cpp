/**
 * file : 20200910`2213 cp521iterators.cpp, project 20200910`2211
 * summary : Demonstrate how to implement a custom input iterator
 * authors : cppreference.com and Amir Kirsh on stackoverflow 2016-Jun-29'15:02
 * license : CC-BY-SA 3.0
 * see : en.cppreference.com/w/cpp/iterator/iterator [ref 20200912`1422]
 *        Inheriting from 'std::iterator' is deprecated. It could be made run in VS2019
 *        with compiler option '_SILENCE_CXX17_ITERATOR_BASE_CLASS_DEPRECATION_WARNING'.
 * see : stackoverflow.com/questions/37031805/preparation-for-stditerator-being-deprecated/38103394 [ref 20201007`0911]
 *        Amir Kirsh on 2016-Jun-29'15:02 posted the version without inheriting from 'std::iterator'
 * note :
 */

#include <algorithm>
#include <iostream>
using namespace std;

template<long FROM, long TO> class Range {
 public:
   class iterator {
      long num = FROM;
   public:
      iterator(long _num = 0) : num(_num) {}
      iterator& operator++() {
         num = TO >= FROM ? num + 1 : num - 1;
         //cout << " (" << num << ") ";
         return *this;
      }

      iterator operator++(int) { iterator retval = *this; ++(*this); return retval; }
      bool operator==(iterator other) const { return num == other.num; }
      bool operator!=(iterator other) const { return !(*this == other); }
      long operator*() { return num; }

      // iterator traits
      using difference_type = long;
      using value_type = long;
      using pointer = const long*;
      using reference = const long&;
      using iterator_category = forward_iterator_tag;
   };

   iterator begin() {
      //cout << " F" << FROM << " ";
      return FROM;
   }

   iterator end() {
      //cout << " E" << FROM << "/" << TO << " ";
      return TO >= FROM ? TO + 1 : TO - 1;
    }
};

int main()
{
   cout << "Demonstrate a custom input iterator .." << endl;
   //--------------------------------------------------

   // find requires an input iterator
   auto range = Range<15, 25>();
   auto itr = find(range.begin(), range.end(), 18);
   cout << "Test 1 : " << *itr << '\n';                // 18

   cout << "Test 2: ";
   for (auto x : range) {
      cout << " " << x;
   }
   cout << endl;

   // Range::iterator also satisfies range-based for requirements
   cout << "Test 3 : ";
   for (long l : Range<3, 5>()) {
      cout << l << ' ';                                // 3 4 5
   }
   cout << '\n';

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
