/**
 * file        : 20200912`0313 cp532container.cpp, project 20200912`0311
 * summary     : Empty frame for planned demo ..
 * authors     :
 * license     :
 * copyright   :
 * reference   :
 * note        :
 */

#include <iostream>
using namespace std;

int main()
{
   cout << "This shall demonstrate .." << endl;
   //--------------------------------------------------

   cout << ". . ." << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
