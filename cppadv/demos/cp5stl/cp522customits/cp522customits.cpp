/**
 * file : 20201008`0833 cp522customits.cpp, project 20201008`0831 (former file 20201110`0833 TestCust1IterClass.cpp)
 * summary : Demonstrate a custom iterator class ..
 * authors : cppreference.com, Amir Kirsh, ...
 * license : CC-BY-SA 3.0
 * status : Just the beginning of a sketch, nothing working yet
 * todo : Complete demo [todo 20201015`1422]
 * note :
 */

#include <algorithm>
#include <iostream>
using namespace std;

template<long FROM, long TO> class Range {
public:
   class iterator {
      long num = FROM;
   public:
      iterator(long _num = 0) : num(_num) {}
      iterator& operator++() {
         num = TO >= FROM ? num + 1 : num - 1;
         cout << " (" << num << ") ";
         return *this;
      }
      iterator operator++(int) { iterator retval = *this; ++(*this); return retval; }
      bool operator==(iterator other) const { return num == other.num; }
      bool operator!=(iterator other) const { return !(*this == other); }
      long operator*() { return num; }
      // iterator traits
      using difference_type = long;
      using value_type = long;
      using pointer = const long*;
      using reference = const long&;
      using iterator_category = forward_iterator_tag;
   };
   iterator begin() {
      //cout << " F" << FROM << " ";
      return FROM;
   }
   iterator end() {
      //cout << " E" << FROM << "/" << TO << " ";
      return TO >= FROM ? TO + 1 : TO - 1;
    }
};

int main()
{
   cout << "Demonstrate a custom iterator class .." << endl;
   //--------------------------------------------------


   cout << "todo . . ." << endl;


   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
