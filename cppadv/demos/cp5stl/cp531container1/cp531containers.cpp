/**
 * file : 20200910`2313 cp531containers.cpp, project 20200910`2311
 * summary : Demonstrate simple use of a simple STL container
 * authors : cppreference.com, modified by ncm
 * license : CC-BY-SA 3.0
 * see : en.cppreference.com/w/cpp/container/vector [ref 20200912`1313]
 * note :
 */

#include <iostream>
#include <vector>
using namespace std;

int main()
{
   cout << "Demonstrate simple use of simple STL container .." << endl;
   //--------------------------------------------------

   // Create a vector containing integers
   vector<int> v = { 7, 5, 16, 8 };

   // Add two more integers to vector
   v.push_back(25);
   v.push_back(13);

   // Iterate and print values of vector
   cout << "(1) :";
   for (int n : v) {
      cout << " " <<n ;
   }
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
