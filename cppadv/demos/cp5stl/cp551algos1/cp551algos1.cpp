/**
 * file : 20200912`0513 cp551algos1.cpp, project 20200912`0511
 * summary : (Empty frame for planned demo) Demonstrate simple algorithm function
 * authors :
 * license :
 * see :
 * note :
 * todo : Implement demo. Use less complictated example as in Cp552Algos2, one without
 *         bind and less. Shift here the less complicated parts from Cp552Algos2.
 */

#include <algorithm> // ..
#include <iostream>
using namespace std;

int main()
{
   cout << "This shall demonstrate the use of a simple algorithm function .." << endl;
   //--------------------------------------------------

   cout << ". . ." << endl;

   //--------------------------------------------------
   cout << "Press Enter to exit.";
   (void) getchar();
   return 0;
}
