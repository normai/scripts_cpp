@echo off
color 1f
goto skip
rem file : 20200913`0123 [after 20151019`1811]
echo **************************************************
echo *** thisfile = %~n0%~x0
echo *** thisdrive = %~d0
echo *** thisdir = %~dp0
echo *** currdir = %cd%
echo **************************************************
:skip

%~d0
cd %~dp0

call .\..\~!vcvarsall.bat

@echo on

cl.exe /std:c++17 .\..\cp1news\cp11range1flavours\cp11range1flavours.cpp

@echo off

echo.
echo ****** Finished %~n0%~x0 ******
pause
