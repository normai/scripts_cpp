rem @echo off
rem file   : 20200913`0537
rem usage  : Set here to your actual path, where vcvarsall.bat resides
rem note   : See article 'Never Put Quotation Marks in the Path Var' file 20201104`1611
rem           http://localhost/trekta/demomatrix/trunk/pages/win/more.html

set VSPATH=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build

call "%VSPATH%\vcvarsall.bat" x86

rem pause
