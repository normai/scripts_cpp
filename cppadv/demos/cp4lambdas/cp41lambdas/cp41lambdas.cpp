/**
 * file : 20200910`1913 cp41lambdas.cpp, project 20200910`1911
 * summary : Demonstrate various lambda flavours
 * authors : cppreference.com, modified by ncm
 * license : CC BY-SA 4.0
 * see : en.cppreference.com/w/cpp/language/lambda [ref 20200912`1412]
 * note :
 */

#include <algorithm>
#include <functional> // function
#include <iostream>
#include <vector>
using namespace std;

int main()
{
   cout << "This is cp41lambdas.cpp .." << endl;
   //--------------------------------------------------

   // (1)
   vector<int> v1 = { 3, 4, 1, 2, 5, 6, 7 };
   vector<int> v2 = { 3, 4, 1, 2, 5, 6, 7 };
   int x = 5;
   v1.erase(remove_if(v1.begin(), v1.end(), [x](int n) { return n < x; }), v1.end());
   std::remove_if(v2.begin(), v2.end(), [x](int n) { return n < x; }), v2.end();

   // (1.2) additonal experiment
   vector<string> v3 = { "A", "Ccc", "Bb", "Dddd", "Eeeee" };
   remove_if(v3.begin(), v3.end(), [](string s) { return s < "Ccc"; }), v3.end();

   // (2)
   cout << "v: ";
   for_each(v1.begin(), v1.end(), [](int i) { cout << i << ' '; });
   cout << endl;

   // (3)
   // the type of a closure cannot be named, but can be inferred
   // with auto since C++14, lambda could own default arguments
   auto func1 = [](int i = 6) { return i + 4; };
   cout << "func1: " << func1() << " / " << typeid(func1).name() << endl;

   // (4)
   // like all callable objects, closures can be captured in std::function
   // (this may incur unnecessary overhead)
   auto func9 = [](int i) { return i + 4; }; //
   function<int(int)> func2 = [](int i) { return i + 4; }; // if we must not use auto
   cout << "func2: " << func2(6) << " " << typeid(func2).name() << endl;
   cout << "func9: " << func9(6) << " " << typeid(func9).name() << endl;

   // (5) lambda functor with capture
   auto func3 = [x](int i = 6) { return i + 4 + x; };
   cout << "func3: " << func3(6) << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
