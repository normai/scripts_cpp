﻿/**
 * file : 20200910°2113 cp43captures.cpp, project 20200910°2111
 * summary : Demonstrate various capture flavours
 * authors: Bartłomiej Filipeks
 * license: unspecified
 * see : www.bfilipek.com/2020/08/lambda-capturing.html [ref 20200912°0722]
 * encoding : Unicode (UTF-8-with-BOM)
 * note :
 */

#include <iostream>
using namespace std;

int main()
{
   cout << "This is cp43captures demonstrating capture flavours (see the debugger)" << endl;
   //--------------------------------------------------

   int x = 2, y = 3;

   const auto l1 = []() { return 1; };                 // No capture
   const auto l4 = [x]() { return x; };                // Only x by value (copy)
   const auto l2 = [=]() { return x; };                // All by value (copy)
   const auto l3 = [&]() { return y; };                // All by ref
   // const auto lx = [=x]() { return x; };            // Wrong syntax, no need for = to copy x explicitly
   const auto l5 = [&y]() { return y; };               // Only y by ref
   const auto l6 = [x, &y]() { return x * y; };        // x by value and y by ref
   const auto l7 = [=, &x]() { return x + y; };        // All by value except x  which is by ref
   const auto l8 = [&, y]() { return x - y; };         // All by ref except y which is by value

   // // Error: "'this' can only be used as a lambda capture within a non-static member function"
   //const auto l9 = [this]() {};                      // capture this pointer
   //const auto la = [*this]() {};                     // capture a copy of *this (since C++17)

   // extra -- test a pointer
   int* p = &x;
   const auto lp = [p]() { return p; };                // curiously does not work as indicated by '*this'

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
