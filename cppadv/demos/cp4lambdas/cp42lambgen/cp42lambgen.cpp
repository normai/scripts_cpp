﻿/**
 * file: 20200910°2013 cp42lambgen.cpp, project 20200910°2011
 * summary: Demonstrate a generic lambda
 * authors: Bartłomiej Filipeks
 * license: unspecified
 * see : www.bfilipek.com/2020/08/lambda-generic.html [ref 20200912°0704]
 * encoding : Unicode (UTF-8-with-BOM)
 * note :
 */

#include <iostream>
using namespace std;

int main()
{
   cout << "This is cp42lambgen.cpp demonstrating a generic lambda" << endl;
   //--------------------------------------------------

   const auto foo2 = [](auto x) { cout << x << '\n'; };
   foo2(10);
   foo2(10.1234);
   foo2("Hello lambda");

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
