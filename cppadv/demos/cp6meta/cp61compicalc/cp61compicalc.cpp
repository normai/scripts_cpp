/**
 * file : 20200911`0313 cp61compicalc.cpp, project 20200911`0311
 * summary : This demonstrates compiletime calculation
 * authors : wikipedia.org, Rainer Grimm, learn-cpp.org, ncm
 * license : ~~ Public domain (after it is found at so many places)
 * see : en.wikipedia.org/wiki/Template_metaprogramming [ref 20200824`0515]
 * see : www.modernescpp.com/index.php/c-core-guidelines-rules-for-template-metaprogramming [ref 20200913`0132]
 * see : www.learn-cpp.org/en/Template_Metaprogramming [ref 20200913`0126]
 * note : Depending on the values, with VS2019 appears error C1202 "recursive type or function
 *   dependency context too complex ..". E.g the one algo accepts the 0, the other does not.
 */

#include <iostream>
using namespace std;

// (1) runtime calculation
unsigned int Factorial1(unsigned int n) {
   return n == 0 ? 1 : n * Factorial1(n - 1);
}

// (2) compiletime calculation
template <int N> struct Factorial2 {
   static int const value = N * Factorial2<N - 1>::value;
};

// (2.1) specialization to end the recursion
template <> struct Factorial2<0> {
   static int const value = 1;
};

// (3) compiletime calculation alternative
template <unsigned int N> struct Factorial3 {
   enum { value = N * Factorial3<N - 1>::value };
};
template <> struct Factorial3<0> {
   enum { value = 1 };
};

int main()
{
   cout << "This demonstrates compiletime calculation .." << endl;
   //--------------------------------------------------

   // (1) use runtime version
   cout << "(1.1) f(0)  = " << Factorial1(0) << endl;
   cout << "(1.2) f(1)  = " << Factorial1(1) << endl;
   cout << "(1.3) f(3)  = " << Factorial1(3) << endl;
   cout << "(1.4) f(5)  = " << Factorial1(5) << endl;
   cout << "(1.5) f(10) = " << Factorial1(10) << endl << endl;

   // (2) use compiletime version one
   cout << "(3.1) f(0)  = " << Factorial2<0>::value << endl;
   cout << "(3.2) f(1)  = " << Factorial2<1>::value << endl;
   cout << "(3.3) f(3)  = " << Factorial2<3>::value << endl;
   cout << "(3.4) f(5)  = " << Factorial2<5>::value << endl;
   cout << "(3.5) f(11) = " << Factorial2<11>::value << endl << endl;

   // (3) use compiletime version two
   cout << "(3.1) f(0)  = " << Factorial3<0>::value << endl;
   cout << "(3.2) f(1)  = " << Factorial3<1>::value << endl;
   cout << "(3.3) f(3)  = " << Factorial3<3>::value << endl;
   cout << "(3.4) f(5)  = " << Factorial3<5>::value << endl;
   cout << "(3.5) f(12) = " << Factorial3<12>::value << endl;

/*
   int iFak = 11;
   cout << "(3.5) f(12) = " << Factorial3<iFak>::value << endl;
*/

   //--------------------------------------------------
   cout << "Press Enter to exit.";
   (void) getchar();
   return 0;
}
