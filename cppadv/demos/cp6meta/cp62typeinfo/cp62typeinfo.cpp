/**
 * file : 20200911`0413 cp62typeinfo.cpp, project 20200911`0411
 * summary :
 * authors :
 * license :
 * see :
 * note :
 */

#include <iostream>
using namespace std;

int main()
{
   cout << "This is cp62typeinfo .." << endl;
   //--------------------------------------------------

   cout << endl;
   cout << "See chapter 3 'Type Determination in Detail' .." << endl;
   cout << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
