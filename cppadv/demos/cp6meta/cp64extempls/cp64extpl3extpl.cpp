/**
 * file : 20200911`0541 cp64extpl3extpl.cpp, project 20200911`0511
 * summary : Demonstrate expression templates
 * authors : Rainer Grimm
 * license : GNU GPL v3
 * see : Explanations are found with the second code on
 *        www.modernescpp.com/index.php/expression-templates [ref 20200913`0223]
 * see : The original code comes from
 *        github.com/RainerGrimm/ModernesCppSource/blob/master/source/vectorArithmeticExpressionTemplates.cpp [ref 20200913`0553]
 * note :
 */

#include <cassert>
#include <iostream>
#include <vector>

template<typename T, typename Cont= std::vector<T> >
class MyVec2 {
   Cont cont; 

public:
   // MyVec4 with initial size
   MyVec2(const std::size_t n) : cont(n){}

   // MyVec4 with initial size and value
   MyVec2(const std::size_t n, const double initialValue) : cont(n, initialValue){}

   // Constructor for underlying container
   MyVec2(const Cont& other) : cont(other){}

   // assignment operator for MyVec4 of different type
   template<typename T2, typename R2>
   MyVec2& operator=(const MyVec2<T2, R2>& other){
      assert(size() == other.size());
      for (std::size_t i = 0; i < cont.size(); ++i) cont[i] = other[i];
      return *this;
   }

   // size of underlying container
   std::size_t size() const{ 
      return cont.size(); 
   }

   // index operators
   T operator[](const std::size_t i) const{ 
      return cont[i]; 
   }

   T& operator[](const std::size_t i){ 
      return cont[i]; 
   }

   // returns the underlying data
   const Cont& data() const{
      return cont; 
   }

   Cont& data(){ 
      return cont; 
   }
};

// MyVec4 + MyVec4
template<typename T, typename Op1 , typename Op2>
class MyVec4Add{
   const Op1& op1;
   const Op2& op2;

public:
   MyVec4Add(const Op1& a, const Op2& b): op1(a), op2(b){}

   T operator[](const std::size_t i) const{ 
      return op1[i] + op2[i]; 
   }

   std::size_t size() const{ 
      return op1.size(); 
   }
};

// elementwise MyVec4 * MyVec4
template< typename T, typename Op1 , typename Op2 >
class MyVec4Mul {
   const Op1& op1;
   const Op2& op2;

public:
   MyVec4Mul(const Op1& a, const Op2& b ): op1(a), op2(b){}

   T operator[](const std::size_t i) const{ 
      return op1[i] * op2[i]; 
   }

   std::size_t size() const{ 
      return op1.size(); 
   }
};

// function template for the + operator
template<typename T, typename R1, typename R2>
MyVec2<T, MyVec4Add<T, R1, R2> >
operator+ (const MyVec2<T, R1>& a, const MyVec2<T, R2>& b){
   return MyVec2<T, MyVec4Add<T, R1, R2> > (MyVec4Add<T, R1, R2 >(a.data(), b.data()));
}

// function template for the * operator
template<typename T, typename R1, typename R2>
MyVec2<T, MyVec4Mul< T, R1, R2> >
operator* (const MyVec2<T, R1>& a, const MyVec2<T, R2>& b){
   return MyVec2<T, MyVec4Mul<T, R1, R2> >(MyVec4Mul<T, R1, R2 >(a.data(), b.data()));
}

// function template for < operator
template<typename T>
std::ostream& operator<<(std::ostream& os, const MyVec2<T>& cont){
   std::cout << std::endl;
   for (int i=0; i<cont.size(); ++i) {
      os << cont[i] << ' ';
   }
   os << std::endl;
   return os;
} 

/*
int main(){
   MyVec4<double> x(10,5.4);
   MyVec4<double> y(10,10.3);
   MyVec4<double> result(10);
   result= x+x + y*y;
   std::cout << result << std::endl;
}
*/
