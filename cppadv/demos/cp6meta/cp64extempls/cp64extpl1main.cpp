/**
 * file : 20200911`0521 cp64extempls.cpp, project 20200911`0511
 * summary : This provides the framework for the two expression templates demo classes
 * authors : Rainer Grimm, modified by ncm
 * license : GNU GPL v3
 * note :
 */

template<typename T, typename Cont = vector<T> > class MyVector1;
template<typename T, typename Cont = vector<T> > class MyVector2;

#include <iostream>
#include <vector>
#include "cp64extpl2naiv.cpp"
#include "cp64extpl3extpl.cpp"
using namespace std;

int main()
{
   cout << "This demonstrates Expression Templates" << endl;

   //--------------------------------------------------
   MyVec1<double> x1(10, 5.4);
   MyVec1<double> y1(10, 10.3);
   MyVec1<double> r1(10);

   // test write access on traditional class
   x1[1] = 1.2;
   double & a1 = x1[2];
   a1 = 2.3;

   r1 = x1 + x1 + y1 * y1;
   cout << r1;

   //--------------------------------------------------

   MyVec2<double> x2(10, 5.4);
   MyVec2<double> y2(10, 10.3);
   MyVec2<double> r2(10);
   MyVec2<double> r111(10);

   // test write access on template class
   x2[1] = 1.2;
   double& a2 = x2[2];
   a2 = 2.3;

   // this is mathematically equivalent to above lines, but bypassing the expression template advantage
   MyVec2<double> x112(10, 5.4);
   MyVec2<double> y112(10, 10.3);
   MyVec2<double> r112(10);
   MyVec2<double> r113(10);
   MyVec2<double> r114(10);
   r112 = x112 + x112;
   r113 = y112 * y112;
   r114 = r112 + r113;

   r2 = x2 + x2 + y2 * y2;

   r111 = x2 + y2;

   cout << r2 << endl;

   //--------------------------------------------------

   // Question: Above we saw the vector class only initialized with constants.
   //    Will the vector class work with wild variables instead constants as well?
   // Answer: Yes.

   int iArSiz = 10;
   MyVec2<double> x7(iArSiz, 5.4);
   MyVec2<double> y7(iArSiz, 0);
   MyVec2<double> r7(iArSiz);
   y7[0] = 0.1;
   y7[1] = 1.2;
   y7[2] = 2.3;
   y7[3] = 3.4;
   y7[4] = 4.5;
   y7[5] = 5.6;
   r7 = ((x7 + x7) + (y7 * y7)) + y7 ;
   cout << r7 << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
