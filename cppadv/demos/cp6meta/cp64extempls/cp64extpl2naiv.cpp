/**
 * file : 20200911`0531 cp64extpl2naiv.cpp, project 20200911`0511
 * summary : Demonstrate expression functionality without using expression templates
 * authors : Rainer Grimm
 * license : GNU GPL v3
 * see : www.modernescpp.com/index.php/expression-templates [ref 20200913`0223]
 *        from here the code was copied and pasted
 * note :
 */

#include <iostream>
#include <vector>

template<typename T> class MyVec1 {
   std::vector<T> cont;

public:
   // MyVec3 with initial size
   MyVec1(const std::size_t n) : cont(n){}

   // MyVec3 with initial size and value
   MyVec1(const std::size_t n, const double initialValue) : cont(n, initialValue){}

   // size of underlying container
   std::size_t size() const{ 
      return cont.size(); 
   }

   // index operators
   T operator[](const std::size_t i) const{ 
      return cont[i]; ////
   }

   T& operator[](const std::size_t i){ 
      return cont[i]; 
   }
};

// function template for the + operator
template<typename T> 
MyVec1<T> operator+ (const MyVec1<T>& a, const MyVec1<T>& b){
   MyVec1<T> result(a.size());
   for (std::size_t s = 0; s < a.size(); ++s){                 //// orginal line had error "s <= a.size()"
      result[s]= a[s]+b[s];
   }
   return result;
}

// function template for the * operator
template<typename T>
MyVec1<T> operator* (const MyVec1<T>& a, const MyVec1<T>& b){
   MyVec1<T> result(a.size());
   for (std::size_t s = 0; s < a.size(); ++s) {                //// orginal line had error "s <= a.size()"
      result[s]= a[s]*b[s];
   }
   return result;
}

// function template for << operator
template<typename T>
std::ostream& operator<<(std::ostream& os, const MyVec1<T>& cont){
   std::cout << std::endl;
   for (int i=0; i<cont.size(); ++i) {
      os << cont[i] << ' ';
   }
   os << std::endl;
   return os;
} 

/*
int main(){
   MyVec3<double> x(10,5.4);
   MyVec3<double> y(10,10.3);
   MyVec3<double> result(10);
   result= x+x + y*y;
   std::cout << result << std::endl;
}
*/
