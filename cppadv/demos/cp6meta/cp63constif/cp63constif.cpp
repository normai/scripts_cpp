/**
 * file : 20200911`0613 cp63constif.cpp, project 20200911`0611
 * summary : This demonstrates the use of constexpr-if
 * authors : cppreference.com, modified by ncm
 * license : CC BY-SA 4.0
 * see : en.cppreference.com/w/cpp/language/if [ref 20200913`0235]
 * see : en.cppreference.com/w/cpp/types/is_pointer [ref 20201015`1432]
 * todo : Cleanup [todo 20201015`1442]
 * note :
 */

#include <iostream>
using namespace std;

// () provide class for experiments (only using string is boring)
class A {
public:
   // A(const A&) = delete;                            // copy constructor must not be deleted for use with get_value1()
   A() {}
   string toString() const { return "Bla"; }
};

// (1) case from CppReference -- the original solid function
template <typename T> auto get_value1(T t) {           // writing 'auto&' were fine as well
   if constexpr (is_pointer_v<T>) {
      return *t;
   }
   else {
      return t;
   }
}

// (2) try returning the value with move semantics -- not sure this makes sense
template <typename T> auto get_value2(T t) {
   if constexpr (is_pointer_v<T>) {
      return *t;
   }
   else {
      cout << ".. using move semantics" << endl;
      return forward<T>(t);                            // seems not to work as expected
   }
}

//template <typename * T> auto get_value1(T t) {
//template <typename & T> auto get_value1(T t) {

int main()
{
   cout << "This demonstrates the use of constexpr-if .." << endl;
   //--------------------------------------------------

   string s1 = "Holla";
   string* sp = &s1;

   auto x1 = get_value1(s1);
   auto x2 = get_value1(sp);

   cout << "s1  = " << s1 << endl;                     // 'Holla'
   cout << "sp  = " << sp << endl;                     // e.g. 00000013452FFAF8
   cout << "x1  = " << x1 << endl;                     // 'Holla'
   cout << "x2  = " << x2 << endl;                     // 'Holla'

   A a1;
   auto x31 = get_value1(a1);                          // needs the copy constructor
   cout << "x31 = " << x31.toString() << endl;

   A a2;
   auto x32 = get_value2(a2);
   cout << "a2  = " << a2.toString() << endl;
   cout << "x32 = " << x32.toString() << endl;

   //--------------------------------------------------
   cout << "Press ENTER to exit.";
   (void) getchar();
   return 0;
}
