
   Demos

   cp1news        sln  20200825°0601
                  proj 20200825°0611 cp11range1flavours            Range-basierte for-Schleife
                  proj 20200825°0621 cp11range2foreach
                  proj 20200825°0631 cp11range3map
                  proj 20200913°0611 Cp124Forward
                  proj 20200909°1611 Cp125IntroRvalRefs
                  proj 20200909°1711 cp13inits.cpp
                  proj 20200909°1811 cp14braces.cpp
                  proj 20200825°0311 cp15fold1demo.cpp
                  proj 20200823°0121 cp15fold2cppref.cpp
                  proj 20200824°0311 cp15fold3bptst.cpp
                  proj 20200824°0411 cp15fold4wicht.cpp
                  proj 20200825°0111 cp15fold5coliru.cpp
                  proj 20200825°0211 cp15fold6stacko.cpp
   cp2tplats      sln  20200909°1901                               Templates
                  proj 20200920°1731 cp211minifunctpt.cpp
                  proj 20200909°1911 cp212functplats.cpp
                  proj 20200909°2011 cp221classtplats
                  proj 20200912°0111 cp222tplclass.cpp
                  proj 20200909°2111 cp23vartplats.cpp
                  proj 20200909°2211 cp24specs.cpp
                  proj 20200913°0651 cp251nontypes.cpp
                  proj 20200913°0641 cp252nontypes.cpp
                  proj 20200909°2311 cp253nontypes.cpp
                  proj 20200913°0631 cp254nontypes.cpp
                  proj 20200910°0111 cp26functors.cpp
                  proj 20200910°0211 cp27crtp.cpp
   cp31tempargs   sln  20200910°0301                               Template-Argumente
                  proj 20200910°0311 tempargs.cpp
   cp3types       sln  20200910°0401                               auto (C++11)
                  proj 20200910°0411 autos.cpp
                  proj 20200910°0511 structbind.cpp
                  proj 20200910°0611 decltype11.cpp
                  proj 20200910°0711 decltype14.cpp
                  proj 20200910°0811 autoretval.cpp
                  proj 20200910°1811 autoctypes.cpp
   cp4lambdas     sln  20200910°1901                               Grundlagen
                  proj 20200910°1911 lambdas.cpp
                  proj 20200910°2011 lambgen.cpp
                  proj 20200910°2111 capturs.cpp
   cp5stl         sln  20200910°2201                               Iteratorkonzept
                  proj 20200910°2211 iterators.cpp
                  proj 20200912°0411 cp513iterators
                  proj 20200910°2311 cp521container
                  proj 20200912°0311 cp522container
                  proj 20200911°0111 funcs.cpp
                  proj 20200912°0511 cpl3algos.cpp
                  proj 20200911°0211 parallel.cpp

                  proj 20201008`0831 TestCust1IterClass
                  file 20201008`0833 TestCust1IterClass.cpp

   cp6meta        sln  20200911°0301                               Den Compiler rechnen lassen
                  proj 20200911°0311 compicalc.cpp
                  proj 20200911°0411 typeinfos.cpp
                  proj 20200911°0511
                  proj 20200911°0611 constif.cpp
                  file 20200911°0521 cp63extpl1main.cpp
                  file 20200911°0531 cp63extpl2naiv.cpp
                  file 20200911°0541 cp63extpl3extpl.cpp
   cp7more        sln  20200911°0701                               Operatorüberladung
                  proj 20200911°0711 opoverload.cpp
                  proj 20200912°0211 multinher.cpp

   ———————————————————————
   Discarded

   file 20200909°1622 project 20200909°1621 Cp12Rv2MoveCon
   file 20200909°1632 project 20200909°1631 Cp12Rv3More
   file 20200909°1622 project 20200909°1621 Cp12Rv2MoveCon
// cp12rvals         sln  20200909°1601                            RValues und Move-Semantik
// cp13inits         sln  20200909°1701                            Initialisierungslisten
// cp14braces        sln  20200909°1801                            {} oder ()? (C++11)
// cp15folds         sln  20200823°0111                            Faltungen (C++17)
// cp22ctemplats     sln  20200909°2001                            Klassentemplates
// cp23vartempls     sln  20200909°2101                            Variadische Templates
// cp24specs         sln  20200909°2201                            Spezialisierung
// cp25nontype       sln  20200909°2301                            Nichttyp-Templateargumente
// cp26functors      sln  20200910°0101                            Funktoren
// cp27crtp          sln  20200910°0201                            CRTP (*)
// cp32auto          sln  20200910°0401                            auto (C++11)
// cp33structbind    sln  20200910°0501                            Strukturierte Bindung
// cp34decltype11    sln  20200910°0601                            decltype (C++11)
// cp35decltype14    sln  20200910°0701                            decltype(auto) (C++14)
// cp36autoretval    sln  20200910°0801                            Automatischer Rückgabetyp
// cp37autoctypes    sln  20200910°1801                            Automatische Klassentypen
// cp41lambdas       sln  20200910°1901                            Grundlagen
// cp42lambgen       sln  20200910°2001                            Generische Lambdas
// cp43captures      sln  20200910°2101                            Erweiterte Captures
// cp51iters         sln  20200910°2201                            Iteratorkonzept
// cp52containers    sln  20200910°2301                            Container (z.T. C++11)
// cp53funcs         sln  20200911°0101                            Funktionen (z.T. C++11)
// cp54parallel      sln  20200911°0201                            Parallele Algorithmen
// cp61compicalc     sln  20200911°0301                            Den Compiler rechnen ..
// cp62typeinfo      sln  20200911°0401                            Typinformationen ..
// cp63extempls      sln  20200911°0501                            Expression Templates
// cp64constif       sln  20200911°0601                            constexpr if (C++17)
// cp71opover        sln  20200911°0701                            Operatorüberladung
// cp72tplclass      sln  20200912°0101                            Template-Klassen
// cp73multinher     sln  20200912°0201                            Mehrfachvererbung ..
// cp74stlib         sln  20200912°0301                            Standard-20200912°0311

   ———————————————————————
   [file 20200909°1501] ܀Ω
