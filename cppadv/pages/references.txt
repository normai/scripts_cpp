﻿

   ref 2020xxxx°xxxx →
   objtype     :
   url         :
   title       :
   authors     :
   releasedate :
   usage       :
   tags        : cpp
   note        :
   ܀

   ref 20201109°0942 isocpp → virtual functions
   objtype     : wikipage
   url         : https://isocpp.org/wiki/faq/virtual-functions
   title       : Inheritance — virtual functions
   authors     : isocpp members
   usage       : project 20200912°0211 Cp731MultInher, project 20201105°1011 Cp732MultInher
   tags        : cpp
   note        :
   ܀

   ref 20201105°0922 alanwood →
   objtype     : lookup
   url         : http://www.alanwood.net/demos/ansi.html
   title       : ANSI character set and equivalent Unicode and HTML characters
   authors     :
   releasedate :
   usage       : Project 20200912°0211 ~Cp73MultInher
   tags        : cpp cpp.encoding
   note        :
   ܀

   ref 20201105°0913 stackoverflow → get name of derived class
   objtype     : thread
   url         : https://stackoverflow.com/questions/6747089/programmatically-getting-the-name-of-a-derived-class
   title       : Programmatically getting the name of a derived class
   authors     :
   releasedate : 2011-Jul-19 .. 2020-Aug-24
   usage       : Project 20200912°0211 ~Cp73MultInher
   tags        : cpp, cpp.types
   note        :
   ܀

   ref 20201105°0912 cplusplus →
   objtype     : article
   url         : http://www.cplusplus.com/doc/tutorial/polymorphism/
   title       : Polymorphism
   authors     :
   releasedate :
   usage       : Project 20200912°0211 ~Cp73MultInher
   tags        : cpp
   note        :
   ܀

   ref 20201104°1417 stackoverflow → calling vcvarsall.bat
   objtype     : thread
   url         : https://stackoverflow.com/questions/8756828/visual-studio-command-prompt-gives-common-was-unexpected-at-this-time
   title       : Visual Studio Command Prompt gives “\Common was unexpected at this time”
   releasedate : 2012-Jan-06 .. 2020-Jun-21
   chain       : file 20200913`0537 !vcvarsall.bat
   tags        : cpp, visualstudio
   note        : Path C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\"
      containing parenthesis and blanks make problems when calling vcvarsall.bat, which
      result in unintuitive error message "\Microsoft was unexpected at this time."
   ܀

   ———————————————————————
   [file 20201104°1414] ܀Ω
