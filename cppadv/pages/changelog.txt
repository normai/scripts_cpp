﻿

   log 20201110°1745 Version 0.1.8 release
   • Streamline demo project names

   log 20201028°1731 Version 0.1.7 — 2020-10-28`17:31 renumbered
   • Streamline chapter structure, renumber some chapters

   log 20201013°0911 — Version 0.1.5 — 2020-10-13`09:11 working

   log 20201010°0851 — Version 0.1.4 — 2020-10-10`08:51 final

   log 20201004°1511 — Version 0.1.1 — 2020-10-04`15:11 release

   log 20200922°0751 — Version 0.0.4 — 2020-09-22`07:51 final

   log 20200921°0731 — Version 0.0.2 — 2020-09-21`07:31 initial release

   ———————————————————————
   [file 20201004°0915] ⬞Ω
