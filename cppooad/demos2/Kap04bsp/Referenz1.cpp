#include <iostream>
using namespace std;
int main()
{
   //// int i; //// error 20200109.0131`01 error "C4700 uninitialized variable 'i' use"
   int iVal = 123; //// fix 20200109.0131`02
   int jVal = iVal;
   int& ri = iVal;      // & is referrence oparator
   int *pi = &iVal;     // & is address operator
   iVal = 1;
   jVal = 3;
   cout << "i = " << iVal << ", j = " << jVal << endl;
   cout << "&i = " << &iVal << ", &j = " << &jVal << endl << endl;
   ri = 5;
   cout << "i = " << iVal << ", ri = " << ri << endl;
   cout << "&i = " << &iVal << ", &ri = " << &ri << endl;
   cout << endl;

   cout << " i = " <<  iVal << ",  pi = " << pi << endl;
   cout << "*i = " <<  iVal << ", *pi = " << *pi << endl;
   cin.peek();
   return 0;
}
