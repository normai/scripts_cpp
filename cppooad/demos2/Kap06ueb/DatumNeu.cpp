#include <iostream>
#include <ctime>
#include <string>
using namespace std;
class Datum
{
protected:
   int Tag, Monat, Jahr;
public:
   Datum();
   Datum(int t, int m, int j);
   void SetDatum(int t, int m, int j);
   void DruckDatum();
};
Datum::Datum()                         // Standardkonstruktor
{
   Tag = 0;
   Monat = 0;
   Jahr = 0;
}
Datum::Datum(int t, int m, int j)      // Zweiter Konstruktor
{
   Tag = t;
   Monat = m;
   Jahr = j;
}
void Datum::DruckDatum()
{
   cout << Tag << ".";
   cout << Monat << ".";
   cout << Jahr << endl;
}
void Datum::SetDatum(int t, int m, int j)
{
   Tag = t;
   Monat = m;
   Jahr = j;
}
// ============================================================
class DatumAusgabe : public Datum
{
private:
   int WTag;
   void GetTag();
public:
   DatumAusgabe() : Datum() {}
   DatumAusgabe(int t, int m, int j) : Datum(t, m, j) {}
   void AusgabeA();
   void AusgabeB();
   void AusgabeC() { DruckDatum(); }
   void AusgabeD();
};
void DatumAusgabe::AusgabeA()
{
   GetTag();
   switch (WTag)
   {
   case 1: cout << "Mo"; break;
   case 2: cout << "Di"; break;
   case 3: cout << "Mi"; break;
   case 4: cout << "Do"; break;
   case 5: cout << "Fr"; break;
   case 6: cout << "Sa"; break;
   case 0: cout << "So"; break;
   default: cout << "unbekannt";
   };
   cout << ", der ";
   AusgabeB();
}
void DatumAusgabe::AusgabeB()
{
   string s;
   switch (Monat)
   {
   case 1: s = "Januar"; break;
   case 2: s = "Februar"; break;
   case 3: s = "M�rz"; break;
   case 4: s = "April"; break;
   case 5: s = "Mai"; break;
   case 6: s = "Juni"; break;
   case 7: s = "Juli"; break;
   case 8: s = "August"; break;
   case 9: s = "September"; break;
   case 10: s = "Oktober"; break;
   case 11: s = "November"; break;
   case 12: s = "Dezember"; break;
   default: s = "unbekannt";
   }
   cout << Tag << ".";
   cout << s << " ";
   cout << Jahr << endl;
}
void DatumAusgabe::AusgabeD()
{
   int s;
   s = Jahr / 100;
   s = s * 100;
   s = Jahr - s;
   cout << Tag << ".";
   cout << Monat << ".";
   if (s < 10)
      cout << 0;
   cout << s;
   cout << endl;
}
void DatumAusgabe::GetTag()
{
   // 01.01.2007 war Montag
   switch (Jahr)
   {
   case 2006: WTag = 0; break;
   case 2007: WTag = 1; break;
   case 2008: WTag = 2; break;
   case 2009: WTag = 4; break;
   case 2010: WTag = 5; break;
   case 2011: WTag = 5; break;
   case 2012: WTag = 7; break;
   case 2013: WTag = 9; break;
   case 2014: WTag = 10; break;
   case 2015: WTag = 11; break;
   case 2016: WTag = 5; break;
   case 2017: WTag = -1; break;
   };
   switch (Monat)
   {
   case 2: WTag = WTag + 3; break;
   case 3: WTag = WTag + 3; break;
   case 4: WTag = WTag + 6; break;
   case 5: WTag = WTag + 8; break;
   case 6: WTag = WTag + 11; break;
   case 7: WTag = WTag + 13; break;
   case 8: WTag = WTag + 16; break;
   case 9: WTag = WTag + 19; break;
   case 10: WTag = WTag + 21; break;
   case 11: WTag = WTag + 24; break;
   case 12: WTag = WTag + 26; break;
   };
   WTag = WTag + Tag;
   WTag = WTag % 7;
}
//=============================================================
int main()
{
   DatumAusgabe Heute(25, 10, 2017);
   Heute.AusgabeA();
   Heute.AusgabeB();
   Heute.AusgabeC();
   Heute.AusgabeD();
   cin.get();
   return 0;
}
//-------------------------------------------------------------
