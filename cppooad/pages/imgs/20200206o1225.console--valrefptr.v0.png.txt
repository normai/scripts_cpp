﻿*** ValRefPtr -- play with value, reference and pointer
Plain value          : fVal  = 123.4
Address operator     : &fVal = 00EFF890
Plain reference      : fRef  = 123.4
Pointer itself       : fPtr  = 00EFF890
Pointer dereferenced : *fPtr = 123.4
Arbitrary ptr addr   : ipX   = 00EFFA4C
Arbitrary ptr val    : *ipX  = 0
*** Press Enter to exit.
