﻿#include <iostream>
#include <string>
#include <thread> // sleep_for

class Potato
{
public:
   Potato();
   void bloom() { std::cout << "Potato blooms.              [4 - Method defined inline]" << std::endl; };
   void mature();
   float Size() { return _size; }
private:
   float _size{ 1.0 };
};

void buySeed() { std::cout << "Buy potato seed.            [1 - Free function above usage]" << std::endl; }

void plant(); // try outcommenting this

int main()
{
   std::cout << "*** Potato ***" << std::endl;
   std::cout << "This demonstrates function placements." << std::endl;

   buySeed();

   Potato* p = new Potato();

   std::cout << "Potato size = " << p->Size() << "mm" << std::endl;
   plant();
   p->bloom();
   p->mature();

   if (false) {  // toggle flag
      std::cout << "*** Bye .." << std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(2222));
   }
   else {
      std::cout << "*** Press Enter to exit .." << std::endl;
      std::cin.peek();
   }
}

Potato::Potato() { std::cout << "Plant potato.               [2 - Constructor defined free standing]" << std::endl; }

void Potato::mature() { std::cout << "Harvest potato.             [5 - Method defined free standing]" << std::endl; }

void plant() { std::cout << "Water it.                   [3 - Free function defined below usage]" << std::endl; }

/*
 * file : /potato/potato1.cpp [file 20200106°0221]
 * summary : This demonstrates
 *  - Function defintion placements (3 flavours)
 * encoding : UTF-8-with-BOM •Äöüß۞
 */
