﻿
   Project 'potato1' demonstrates function and method calling flavours.

   The flavours are :

   (1) Free function defined above usage

   (2) Constructor defined free standing

   (3) Free function defined below usage

   (4) Method defined inline

   (5) Methode defined free standing

   ————————————————————————————————————————————————————————————

   Items :
   • solution 20200106°0201 'potato1'
   • project 20200106°0211 'potato1'
   • file 20200106°0221 'potato1.cpp'

   • solution 20200106°0301 'potato2'
   • project 20200203°0311 'potato2'
   • file 20200203°0321 'potato2.cpp'
   • file 20200203°0331 'potato2.h'

   ———————————————————————
   [file 20200106°0202] ܀Ω
