﻿/**
 *  file       : 20200118°2021 https://normai.gitlab.io/scripts_cpp/cppooad/demos1/tuttifrutti/tuttifrutti.cpp
 *  summary    : Demonstrate various memory management behaviour and misbehaviour
 *  version    : v0.2.0 — 20220106°0727 — Cleanup
 *  license    : MIT License
 *  copyright  : © 2020 - 2022 Norbert C. Maier
 *  authors    : ncm
 *  encoding   : UTF-8-with-BOM
 *  compiled   : With • VS2022 (cl.exe v19)
 */
#include <conio.h>                                     // Windows specific _getch()
#include <iostream>
using namespace std;

void plainComparison()
{
   cout << "Plain stack/heap comparison :" << endl;

   const char* c1 = "Hello";                           // Allocate on stack

   char* c2 = new char[6];                             // Allocate on heap
   c2[0] = 'H';
   c2[1] = 'o';
   c2[2] = 'l';
   c2[3] = 'l';
   c2[4] = 'a';
   c2[5] = '\0';

   const char* c3 = new char[] { 'H', 'o', 'p', 'l', 'a', '\0' }; // Allocate on heap [line 20200219°1117]

   cout << " - Stack : c1 = " << c1 << endl;
   cout << " - Heap  : c2 = " << c2 << endl;
   cout << " - Heap  : c3 = " << c3 << endl;

   delete[] c2;                                        // Cleanup heap
   delete[] c3;
}

void depleteMemory()
{
   static int iCount = { 0 };

   if (iCount++ % 1000) {
      cout << ".";
   }

   char* c2 = new char[10000000];                     // For one machine
   //char* c2 = new char[60000000];                   // For the other machine

   // The point: delete[] is missing here
}

void manageMemory()
{
   static int iCount = { 0 };

   if (iCount++ % 1000) {
      cout << ".";
   }

   char* c2 = new char[10000000];                      // For one machine, a desktop with e.g. 8 GB. See todo 20200119°1234 'allocate more elegantly'
   //char* c2 = new char[60000000];                    // For other machine, e.g. 60 GB in a VM on a big computer, possibly cached to disk, that's why the dots come slower

   // Do things here with c2 ..

   delete[] c2;
}

void stackOverflow()
{
   static int iCnt {0};
   iCnt++;

   cout << ".";

   char c[10000] = { 'x', '\0' };                      // First element is 'x', others are '\0'
   c[123] = 'y';                                       // Avoid warning 'unreferenced local variable' [chg 20220106°0732]

   stackOverflow();
}                                                      // Warning 'will cause runtime stack overflow' [ss 20220106°0724]

void saveStack()
{
   bool bContinue = true;
   static int iCnt{ 0 };
   iCnt++;

   if (iCnt < 10) {
      cout << ".";
   }
   else {
      cout << " Stack is used up, time to escape." << endl;
      bContinue = false;
   }

   char c[10000] = { 'x', '\0' };                      // First element is 'x', others are '\0'
   c[123] = 'y';                                       // Avoid warning 'unreferenced local variable' [chg 20220106°0732`02]

   if (bContinue) {
      saveStack();
   }
}

void other() {
   double d = 3.14;
   int i = static_cast<int>(d);
   cout << "static_cast from double to int: " << "d = " << d << ", i = " << i << endl;
}

int main()
{
   cout << "*** Welcome to Tuttifrutti ***" << endl;

   bool bLoop = true;
   char c = ' ';
   while (bLoop)
   {
      std::cout << std::endl;
      std::cout << "Menu: 1 = Stack/heap, 2 = Save stk, 3 = Stk overflow, 4 = Save heap, 5 = Deplete heap, 6 = Static cast, Other = Exit" << std::endl;

      c = _getch();
      std::cout << "Key pressed = '" << c << "'" << std::endl;
      switch (c)
      {
         case '1' :
            plainComparison();
            break;

         case '2':
            cout << "Save stack :" << endl;
            saveStack();
            break;

         case '3':
            cout << "Stackoverflow :" << endl;
            stackOverflow();
            break;

         case '4':
            cout << "Treat heap correctly :" << endl;
            for (int i = 0; i < 1000; i++) {
               manageMemory();
            }
            cout << " Heap is used up. Finish looping." << endl;
            break;

         case '5':
            cout << "Deplete heap with 'std::bad_alloc at memory location ~0x010FF8F8':" << endl;
            for (int i = 0; i < 1000; i++) {
               depleteMemory();
            }
            cout << " Finish looping." << endl;         // Possibly never reached
            break;

         case '6':
            other();
            break;

         default :
            cout << "Unknown key '" << c << "' -- exit" << std::endl;
            bLoop = false;
            break;
      }
   }
}
