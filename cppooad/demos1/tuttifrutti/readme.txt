﻿
   *********************
   Project 'TuttiFrutti'
   *********************

   This demonstrates:

   • 1 Plain demo      — Compare stack and heap allocation

   • 2 Save stack      — Deplete stack but exit gracefully

   • 3 Stack overflow  — Provoke stack overflow

   • 4 Save heap       — Use heap correctly

   • 4 Deplete heap    — Provoke heap depletion

   • 5 Other           — Show static_cast double to int


   ———————————————————— Changelog —————————————————————

   version v0.2.0 — 20220106°0727 — Cleanup
   • Retarget with VS2022 [screenshot 20220106°0722]
   • Reduce compiler warnings [screenshots 20220106°0722/°0723]
   • Cleanup a bit

   version v0.1.0 — 20200118°2021 — Initial version

   —————————————————————— Notes ———————————————————————

   todo 20200119°1234 'allocate more elegantly'
   matter : Do not use just a dumb allocating constant, but more elegantly
             test the pointer for being nullptr to stop allocating.
   status : Open

   // todo 20200219°1110 'compact heap array assignment'
   // matter : I would rather like a more compact assignment, than a
   //          single byte assignment, just this may be not possible.
   // ref : Compare stackoverflow.com/questions/14114686/create-an-array-of-structure-using-new [ref 20200219°1111]
   // wrongline : char* c3 = new[] { 'H', 'o', 'p', 'l', 'a', '\n' };
   // location : line 20200219°1117
   // status : Done 20200219°1117

   ————————————————————— Dataset ——————————————————————

   • solution 20200118°2001 TuttiFrutti

   • project 20200118°2011 TuttiFrutti

   • file 20200118°2021 tuttifrutti.cpp

   ———————————————————————
   [file 20200118°2002] ܀Ω
