#include <iostream>

class Rocket2 {
public:
   Rocket2() { std::cout << "New Rocket, fuel = " << GetFuel() << std::endl; }
   float GetFuel() const { return fuel; }
   void SetFuel(float f) { fuel = f; }
private:
   float fuel = 0.0; // tons kerose and liquid hydrogen/oxygen for Saturn V
};


/*
 * project : 20200201�0611 rocket2
 * file :  /rocket/rocket2.h [file 20200201�0231]
 * encoding : ANSI
 */
