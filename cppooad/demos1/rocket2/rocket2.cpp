#include "rocket2.h"

//int main(int argc, char argv[][]) // compiler: "an array may not have elements of this type"
int main(int argc, char * argv[])
{
   std::cout << "Rocket2 is demonstrating ... args" << std::endl;

   for (int i = 0; i < argc; i++)
   {
      std::cout << " - " << i << " : " << argv[i] << std::endl;
   }

   Rocket2 *r = new Rocket2();

   r->SetFuel(2345.6F);

   std::cout << "Filled up, fuel = " << r->GetFuel() << " tons" << std::endl;

   std::cout << "Press Enter to exit." << std::endl;
   std::cin.peek();
}

/*
 * project : 20200201�0611 rocket2
 * file :  /rocket/rocket2.cpp [file 20200201�0221]
 * encoding : ANSI
 */
