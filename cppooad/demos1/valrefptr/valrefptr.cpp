﻿/*
 * file : /valrefptr/valrefptr.cpp [file 20200202°0721] [proj 20200202°0711 valrefprt]
 * version : 20220120°1005
 * encoding : UTF-8-with-BOM
 * ref : computer-programming-forum.com/47-c-language/33ede7a64638abe9.htm [ref 20200206°1241]
 * todo 20220120°1005 : (1) ip1Arr has wrong name.
 *         (2) Implement diff bewtwwn "(*ip1Arr)++" and "*(ip1Arr++)"
 *         (3) Cleanup in general
 */

#include <iostream>
using namespace std;

void increment(int v, int &r, int ir, int* ip1Arr, int* ip2Arr) {
   v++;
   r++;
   ir++;
   (*ip1Arr)++; //// *ip1Arr++;
   //*(ip1Arr++); //// *ip1Arr++;

   // This let point the pointer to next int, possibly not existing but
   // some accidential memory  but this does not matter, because the pointer
   // itself it lost on return anyway
   ip1Arr++;

   ip2Arr++; // behaves nice because has an well formed array behind it 
}

int main()
{
   std::cout << "*** ValRefPtr -- play with value, reference and pointer" << std::endl;

   ////int v = r = 5;
   int v = 5;
   int r = 5;

   int &ir = v;                  // 'Alias'
   int* ip1Arr = &v;             // Makes garbage in test call

   int ip2Arr[] = {42, 43};
   int* ip2 = ip2Arr;            // Behaves nice in test call

   increment(v, r, ir, ip1Arr, ip2Arr);
   std::cout << "Now v = " << v << ", r = " << r << endl           // v = 5, r = 6
             << " ir = " << ir << ", *ip1Arr = " << *ip1Arr << ", *ip2Arr = " << *ip2Arr << endl;
   //return 0;

   if (false)  {

      /*
      int main() {
         int v = r = 5;
         increment(v, r);
         std::Cout << "Now v = " << v << ", r = " << r << endl; // v = 5, r = 6
         return 0;
      }
      */
   }
   else {

   float fVal = 123.4;                         // value initialization
   float & fRef = fVal;                        // get reference to value
   float * fPtr = &fVal;                       // make pointer by value address
   int *ipX1 = reinterpret_cast<int*>(1000);   // possible 'read access violation' when using
   int *ipX2 = reinterpret_cast<int*>(&fVal + 111); // less probable 'read access violation'

   cout << "Plain value          : fVal  = " << fVal << std::endl;
   cout << "Address operator     : &fVal = " << &fVal << std::endl;
   cout << "Plain reference      : fRef  = " << fRef << std::endl;
   cout << "Pointer itself       : fPtr  = " << fPtr << std::endl;
   cout << "Pointer dereferenced : *fPtr = " << *fPtr << std::endl;
   cout << "Arbitrary ptr addr   : ipX   = " << ipX2 << std::endl;
   cout << "Arbitrary ptr val    : *ipX  = " << *ipX2 << std::endl;

   }

   cout << "*** Press Enter to exit." << std::endl;
   cin.get();
}
