﻿#include <iostream>    // cout
#include <string>      // string
#include <typeinfo>    // typeid()

#define sDefine "Hello define"
const auto sConst = "Hello const";
constexpr auto sConstexpr = "Hello constexpr";

int main()
{
   std::cout << "*** Show string flavours ***" << std::endl;

   char sArr[99] = { 'H', 'e', 'l', 'l', 'o', ' ', 'a', 'r', 'r', '\0' };
   const char* sChar = "Hello char";  // without const, the (modern) compiler refuses
   std::string sClass = "Hello class";

   std::cout << "sDefine    = " << sDefine    << "      " << typeid(sDefine).name() << std::endl;
   std::cout << "sConst     = " << sConst     << "       " << typeid(sConst).name() << std::endl;
   std::cout << "sConstexpr = " << sConstexpr << "   " << typeid(sConstexpr).name() << std::endl;
   std::cout << "sArr       = " << sArr       << "         " << typeid(sArr).name() << std::endl;
   std::cout << "sChar      = " << sChar      << "        " << typeid(sChar).name() << std::endl;
   std::cout << "sClass     = " << sClass     << "       " << typeid(sClass).name() << std::endl;
   std::cout << std::endl;

   std::cout << "*** Press Enter to exit." << std::endl;
   std::cin.get();
}

/*
 * project : datatyps 20200202°0811
 * file : /datatyps/datatyps.cpp 20200202°0821
 * encoding : UTF-8-with-BOM
 * ref : www.prismnet.com/~mcmahon/Notes/strings.html [ref 20200202°0712]
 */
