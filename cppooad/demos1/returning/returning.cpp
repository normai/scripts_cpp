﻿#include <iostream>    // cout
#include <string>      // string

int retval() {
   return 42;
}

int retref() {
   int i = 42;
   int &r = i;
   return r;      // Not good, value will vanish, then reference points to nirwana
}

char * retptr1() {
   static char s [] = "Holla";
   return s;
}

// added 20220122°1405
char* retptr12() {
   ////static char c[99] = { 'H', 'o', 'l', 'l', 'o', '\0' };
   static char c[6] = { 'H', 'o', 'l', 'l', 'o', '\0' };

   // Experiment
   c[1] = 'e';
   c[3] = 'a';
   c[4] = 'u';
   //c[5] = 'x'; // Fatal
   //c[6] = 'x'; // Schreibt über das Ende des Arrays hinaus

   return c;
}

std::string * retptr2() {
   std::string s = "Hello";           // C-style string
   std::string * sp = &s;
   return sp;
}

std::string * retptr3() {
   std::string * sp = new std::string("Hello");
   return sp;
}

std::shared_ptr<std::string> retptr4() {
   std::shared_ptr<std::string> sp = std::make_shared<std::string>("Hi there!");
   return sp;
}

int main()
{
   std::cout << "*** Returning ***" << std::endl << std::endl;

   int iV = retval();
   int iR = retref();
   char* sP1 = retptr1();
   char* sP12 = retptr12(); // new
   std::string * sP2 = retptr2();
   std::string * sP3 = retptr3();
   delete sP3;
   std::shared_ptr<std::string> sP4 = retptr4();

   std::cout << " - iV   = " << iV << std::endl;
   std::cout << " - iR   = " << iR << std::endl;
   std::cout << " - sP1  = " << sP1 << std::endl;
   std::cout << " - sP12 = " << sP12 << std::endl;
   std::cout << " - sP2  = " << sP2 << std::endl;
   // std::cout << " - *sP2  = " << *sP2 << std::endl; // access violation
   std::cout << " - sP3  = " << sP3 << std::endl;
   // std::cout << " - *sP3  = " << *sP3 << std::endl; // access violation
   std::cout << " - sP4  = " << sP4 << std::endl;
   std::cout << " - *sP4 = " << *sP4 << std::endl;
   std::cout << std::endl;

   std::cout << "*** Press Enter to exit." << std::endl;
   std::cin.get();
}

/*
 * project : returning 20200203°0521
 * file : /returning/returning.cpp 20200203°0531
 * encoding : UTF-8-with-BOM
 */
