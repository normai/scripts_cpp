/**
 * file : getexepath.cpp [file 20200109`0311]
 */

#include <iostream>
#include <filesystem>  // filesystem
#include <sstream>     // std::istringstream
#include <vector>

//namespace fs = std::filesystem;

// split string by given delimiter [func 20200109`0331]
std::vector<std::string> split(const std::string& s, char cDelim)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, cDelim))
   {
      tokens.push_back(token);
   }
   return tokens;
}

// [func 20200109`0341]
std::string getPathFromArgs(int argc, char* args[])
{
   for (int i = 0; i < argc; ++i)
      std::cout << " - " << i << " : " << args[i] << "\n";

   std::vector<std::string> v = split(args[0], '\\');

   for (std::string s : v) {
      std::cout << " - " << s << "\n";
   }

   std::string sPath = "";
   for (int i = 0; i < (int) v.size() - 1; i++) {
      sPath = sPath + v[i] + '\\';
   }

   return sPath;
}

// [func 20200109`0321]
int main(int argc, char* args[]) // equivalent (int argc, char** args)
{
   std::cout << "*** GetExePath *** \n";

   std::string sPath = getPathFromArgs(argc, args);

   std::cout << "Wanted folder : " << sPath << "\n";

   // () try using  C++17 current_path() [seq 20200109`0351]
   // Function current_path seems not available in VS2017
   //std::cout << "CWD = " << fs::current_path() << '\n';
   //std::cout << "CWD = " << std::filesystem::current_path() << '\n';

   std::cout << "*** Press Enter to exit .. *** \n";
   std::cin.peek();
}
