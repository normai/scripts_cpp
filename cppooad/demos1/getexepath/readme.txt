﻿
   This project demonstrates

   • How to retrieve the path of the executable

   • Retrieve the CWD current working directory (in VS2017 n/a)

   ————————————————————————————————————————————————————————————
   References :

   ref 20200109°0247 'cppreference → cwd'
   url : https://en.cppreference.com/w/cpp/filesystem/current_path
   title : std::filesystem::current_path
   authors : 
   copyright :
   license :
   releasedate :
   note :

   ref 20200109°0245 'martinbroadhurst → string split'
   url : http://www.martinbroadhurst.com/how-to-split-a-string-in-c.html
   title : How to split a string in C++
   authors : 
   copyright :
   license :
   releasedate :
   note :

   ref 20200109°0243 'fluentcpp → string split'
   url : https://www.fluentcpp.com/2017/01/12/ranges-stl-to-the-next-level/
   title : Ranges: the STL to the Next Level
   authors : Jonathan Boccara
   copyright : © Text 2018 by Fluent C++
   license : n/a?
   releasedate : 2017-Jan-12
   chain : http://arne-mertz.de/2017/01/ranges-stl-next-level/
   note :

   ref 20200109°0241 'fluentcpp → string split'
   url : https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
   title : How to split a string in C++
   authors : Jonathan Boccara
   copyright : © Text 2018 by Fluent C++
   license : n/a?
   releasedate : 2017-Apr-21
   usage : From here, algorithm .. is used in func 20200109`0331 split()
   note :

   ————————————————————————————————————————————————————————————
   Changelog :

   log 20200111°1721 create folder

   log 20200109°0301 create project 'GetExePath'

   ———————————————————————
   [file 20200109°0315] ܀Ω
