﻿#include <iostream>    // cout
#include <limits>      // std::numeric_limits

using namespace std;

int main()
{
   std::cout << "*** DataTyps ***" << std::endl;

   int int1 = 42;                              //
   long long1 = 2147483647;                    // max             2 147 483 647
   long long2 = 2147483648;                    // max             2 147 483 647
   long long llong1 = 9223372036854775807;     // max 9 223 372 036 854 775 807

   if (2147483648 > LONG_MAX) {
      cout << "Attention, long2 will have wrong value." << endl << endl;
   }

   cout << "int1 = " << int1 << endl;                    // 42
   cout << "long1 = " << long1 << endl;                  // 2147483647
   cout << "long2 = " << long2 << endl;                  // -2147483648 ↯
   cout << "llong1 = " << llong1 << endl;                // 9223372036854775807
   cout << endl;

   cout << "int       min/max : " << std::numeric_limits<int>::min() << " / " << INT_MAX << endl;
   cout << "long      min/max : " << LONG_MIN << " / " << LONG_MAX << endl;
   cout << "long long min/max : " << LLONG_MIN << " / " << LLONG_MAX << endl;
   cout << endl;

   // What about runtime calculations plain and checked?
   long longBig = long1 + 1;
   cout << "longBig = " << longBig << " (silent overflow)" << endl;

   // checked
   if ((LONG_MAX - long1) < 1)
   {
      cout << " - longBig will be wrong" << endl;
   }
   cout << "longBig = " << longBig << " (known overflow)" << endl;
   cout << endl;

   cout << "*** Press Enter to exit .." << std::endl;
   cin.get();
}

/*
 * project : datatyps 20200202`0511
 * file : /datatyps/datatyps.cpp 20200202`0521
 * encoding : UTF-8-with-BOM
 * ref : www.cplusplus.com/reference/limits/numeric_limits/ []
 * ref : stackoverflow.com/questions/199333/how-do-i-detect-unsigned-integer-multiply-overflow []
 * ref : docs.microsoft.com/en-us/cpp/preprocessor/runtime-checks?view=vs-2019 []
 */
