﻿using System;

namespace CsDataTypes
{
   class Programm
   {
      static int Main()
      {
         Console.WriteLine("*** CsDataTypes ..");

         short short1 = 42;                            //
         int int1 = 2147483647;                        // max             2 147 483 647
         int int2 = unchecked ((int) 2147483648);      // max             2 147 483 647
         long long1 = 9223372036854775807;             // max 9 223 372 036 854 775 807

         if (2147483648 > int.MaxValue)
         {
            Console.WriteLine("Attention, int2 will have wrong value.");
         }
         Console.WriteLine();

         Console.WriteLine("short1 = " + short1);      // 42
         Console.WriteLine("int1 = " + int1);          // 2147483647
         Console.WriteLine("int2 = " + int2);          // -2147483648 ↯
         Console.WriteLine("long1 = " + long1);        // 9223372036854775807
         Console.WriteLine();

         Console.WriteLine("short  min/max : " + short.MinValue + " / " + short.MaxValue);
         Console.WriteLine("int    min/max : " + int.MinValue + " / " + int.MaxValue);
         Console.WriteLine("long   min/max : " + long.MinValue + " / " + long.MaxValue);
         Console.WriteLine();

         // What about runtime calculations plain and checked?
         int iBig1 = int1 + 1;
         Console.WriteLine("int1 + 1 = " + iBig1 + " ('plain mode' results in silent fail)");

         // use the checked keyword
         int iBig2 = 0;
         checked // causes System.OverflowException 'Arithmetic operation resulted in an overflow.'
         {
            try
            {
               iBig2 = int1 + 1;
            }
            catch (Exception ex)
            {
               Console.WriteLine(" - Exception " + ex.Message);
            }
            Console.WriteLine("int1 + 1 = " + iBig2 + " ('checked mode' results in exception)");
         }
         Console.WriteLine();

         Console.WriteLine("Press anykey to exit ..");
         Console.ReadKey();
         return 0;
      }
   }
}

/*
 * project : datatyps 20200202°0611
 * file :
 * summary :
 * encoding : UTF-8-with-BOM (default VS2017 for CS)
 * ref : docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/integral-numeric-types []
 * ref : docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/unchecked []
 */
