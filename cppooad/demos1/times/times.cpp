﻿#define _CRT_SECURE_NO_WARNINGS // use localtime anyway

#include <stdio.h>
#include <time.h>
#include <locale.h>

#include <chrono>       // for std::chrono::system_clock::now // since C++11
#include <iostream>
#include <stdio.h>      // printf
#include <time.h>       // time_t, struct tm, time, localtime, asctime
#include <ctime>        // std::time_t

int main()
{
   std::cout << "*** Times." << std::endl;

   // (1) use time() localtime() asctime()
   // See www.cplusplus.com/reference/ctime/asctime/ [ref 20200206°1342]
   // See en.cppreference.com/w/c/chrono/localtime [ref 20200206°1343]
   time_t rawtime;
   struct tm * timeinfo;
   time(&rawtime);
   timeinfo = localtime(&rawtime);
   printf("(1)   Datetime time()     = %s", asctime(timeinfo));

   // (2)
   // (2.1) use std::chrono::system_clock::now
   auto timstmp = std::chrono::system_clock::now;              // en.cppreference.com/w/cpp/chrono/system_clock/now
   std::cout << "(2.1) system_clock::now   = " << timstmp << std::endl;

   // (2.2)
   // See stackoverflow.com/questions/34857119/how-to-convert-stdchronotime-point-to-string [ref 20200206°1352]
   // See en.cppreference.com/w/c/chrono/strftime [ref 20200206°1353]
   std::chrono::system_clock::time_point tp = std::chrono::system_clock::now(); // en.cppreference.com/w/cpp/chrono/time_point
   std::time_t now_c = std::chrono::system_clock::to_time_t(tp);
   std::tm now_tm = *std::localtime(&now_c);
   char buff[70];
   strftime(buff, sizeof buff, "%A %c", &now_tm); // needs paranoia, see ref 20200206°1352
   std::cout << "(2.2) system_clock::now() = " << buff << std::endl; // has no milli

   // (3) want timestamp with milliseconds -- not yet succeeded
   // See en.cppreference.com/w/cpp/chrono/duration/duration_cast [ref 20200206°1354]
   // See stackoverflow.com/questions/15957805/extract-year-month-day-etc-from-stdchronotime-point-in-c?rq=1 [ref 20200206°1355]
   // See stackoverflow.com/questions/12835577/how-to-convert-stdchronotime-point-to-calendar-datetime-string-with-fraction [ref 20200206°1356]
   std::chrono::system_clock::duration tp2 = std::chrono::system_clock::now().time_since_epoch();
   std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(tp2);
   unsigned int iMs = ms.count();
   std::cout << "(3)   std::chrono::millis = " << iMs << " (ms since epcoch)" << std::endl;

   // (4) use std::time()
   // See www.cplusplus.com/forum/beginner/170947/ [ref 20200206°1346]
   std::time_t now = std::time(0);
   const char * dt = std::ctime(&now);
   std::cout << "(4)   Todays Date         = " << dt;
   struct tm * timeinfo2; // not used so far

   // (5) use std::time -- std::localtime
   // See en.cppreference.com/w/c/chrono/localtime [ref 20200206°1343]
   // See stackoverflow.com/questions/6012663/get-unix-timestamp-with-c [ref 20200206°1344]
   // See en.cppreference.com/w/cpp/chrono/c/time [ref 20200206°1345]
   std::time_t result = std::time(nullptr);
   std::cout << "(5.1) Timestamp now       = " << std::asctime(std::localtime(&result));
   std::cout << "(5.2) Epoch seconds       = " << result << " (compare with above)" << std::endl;
   std::cout << std::endl;


   std::cout << "*** Press Enter to exit." << std::endl;
   std::cin.get();
}

/*
 * project : dynamicast [proj 20200203°0111]
 * file : /typecasting/dynamicast.cpp [file 20200203°0121]
 * status : Not yet satisfactory
 * todo : Make timestamp with milliseconds
 * encoding : UTF-8-with-BOM
 */
