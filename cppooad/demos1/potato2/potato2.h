// include guards
#ifndef POTATO2_H
#define POTATO2_H

#include <iostream>
#include <string>

class Potato
{
public:
   Potato();
   void bloom() { std::cout << "Potato blooms.              [4 - Method defined inline]" << std::endl; };
   void mature();
   float Size() { return _size; }
private:
   float _size {1.0};
};

#endif // POTATO2_H

/**
 * file : /potato/potato.h [file 20200203�0331]
 * encoding : ANSI
 */
