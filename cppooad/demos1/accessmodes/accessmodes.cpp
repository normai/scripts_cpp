﻿#include <iostream>    // cout
#include <limits>      // std::numeric_limits
using namespace std;

int main()
{
   std::cout << "*** DataTyps ***" << std::endl;

   int i = 11;
   signed long sl   = 223372036854775808;              //                  2 147 483 647
   unsigned long ul = 18446744073709551615;            // max              4 294 967 295
   unsigned long long ll = 18446744073709551615;       // max 18 446 744 073 709 551 615

   cout << "i = " << i << endl;                        //   11
   cout << "sl = " << sl << endl;                      // ↯ 494665728
   cout << "ul = " << ul << endl;                      // ↯ 4294967295
   cout << "ll = " << ll << endl;                      //   18446744073709551615

   cout << "" << endl;
   std::cout << "int       min/max : " << std::numeric_limits<int>::min() << " / " << std::numeric_limits<int>::max() << endl;
   std::cout << "long      min/max : " << std::numeric_limits<long>::min() << " / " << std::numeric_limits<long>::max() << endl;
   std::cout << "ulong     min/max : " << std::numeric_limits<unsigned long>::min() << " / " << std::numeric_limits<unsigned long>::max() << endl;
   std::cout << "ulonglong min/max : " << std::numeric_limits<unsigned long long>::min() << " / " << std::numeric_limits<unsigned long long>::max() << endl;

   cout << "*** Press Enter to exit .." << std::endl;
   cin.get();
}

/*
 * project : datatyps 20200202`0511
 * file : /datatyps/datatyps.cpp 20200202`0521
 * encoding : UTF-8-with-BOM
 * ref : www.cplusplus.com/reference/limits/numeric_limits/ []
 */
