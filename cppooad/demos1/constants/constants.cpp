﻿#include <conio.h> // _getch() Windows specific
#include <iostream>

// C++11 constexpr functions use recursion rather than iteration
// (C++14 constexpr functions may use local variables and loops)
constexpr int factorial(int n)
{
   return n <= 1 ? 1 : (n * factorial(n - 1));
}

int main(int argc, char* args[])
{
   std::cout << "*** Constants *** \n";

   std::cout << 4 << "! = " << factorial(4) << std::endl; // computed at run time

   std::cout << "Press Anykey to exit ..";
   _getch();
}

/**
 * file : constants.cpp [file 20200118°2121]
 * encoding : UTF-8-with-BOM
 * see : en.cppreference.com/w/cpp/language/constexpr [ref 20200203°0736]
 */
