﻿/**
 * file : /vehicles231/Vehicle.cpp [file 20151024°0731]
 * summary : This file holds the Vehicle class definition.
 * encoding : UTF-8-with-BOM
 * see : stackoverflow.com/questions/7405740/how-can-i-initialize-base-class-member-variables-in-derived-class-constructor [ref 20200203°0712]
 */

#include "Vehicle.h"

Vehicle::Vehicle() { }
Vehicle::Vehicle(string sNam) : vehiclename(sNam = "X") { } // not yet used

Vehicle::~Vehicle() { }

void Vehicle::justForFun() { cout << "Holla." << endl; }

bool Vehicle::getProperty(vehicleFlags flag)
{
   unsigned int iMask = 0;
   unsigned int iTmpResult = 0;
   bool bResult = 0;

   if (flag == vehicleFlags::Leased) // bit no 7 // GNU CPP complains: 'vehicleFlags' is not a class or namespace
   {
      iMask = 0x80; // 0b10000000;
   }
   else if (flag == vehicleFlags::ReadyToGo) // bit no 5
   {
      iMask = 0x20; // 0b00100000;
   }
   else if (flag == vehicleFlags::Registered) // bit no 0
   {
      iMask = 1; // 0b00000001;
   }
   else // Registered
   {
      iMask = 0; // TODO: replace by corret error handling
   }

   iTmpResult = this->flagStorage & iMask; // the bitwise and

   bResult = (iTmpResult > 0) ? true : false;

   return bResult;
}

void Vehicle::setProperty(vehicleFlags flag, bool val)
{
   if (val)
   {
      /////this->flagStorage = 0xffff; // just set all 16 lower bits
      unsigned int i = this->flagStorage | 0x80; // 0b10000000; for Leased
      this->flagStorage = i;

   }
   else
   {
      ////this->flagStorage = 0; // clear all 16 lower bits
      unsigned int i = this->flagStorage | 0;
      this->flagStorage = i;
   }
}

bool Vehicle::toggleProperty(vehicleFlags flag)
{
   return true; // dummy
}
