﻿#include <algorithm>
#include <conio.h> // Windows _getch()
#include <iostream>
#include <string>
#include <typeinfo> // GCC : typeid()
#include <vector>

#include "Bicycle.h"
#include "Car.h"
#include "Vehicle.h"

#define PROGNAME "Vehicles"
#define PROGVERSION "0.1.2"
#define ARRAYMAX 10

using namespace std;

void printParam(int argc, char* argv[]);

class S_VEHICLE
{
public:
   string classname;
   Vehicle* obj;
};

void retString(S_VEHICLE veh);

int autorun();

/*const*/ char* outputArray[ARRAYMAX];   // workaround for missing return at for_each
int arrayCount = 0;

int main(int argc, char* argv[])
{
   bool bLoop = true;
   char c = ' ';
   std::cout << "*** This is " << PROGNAME << " version: " << PROGVERSION << std::endl;

   while (bLoop)
   {
      std::cout << "Keyboardmenu: a = auto-run p = print params, q = exit" << std::endl;

      c = _getch();
      switch (c)
      {
      case 'a':
         autorun();
         break;
      case 'p':
         printParam(argc, argv);
         break;
      case 'q':
         bLoop = false;
         break;
      default:
         std::cout << "Unknown key '" << c << "'" << std::endl;
         break;
      }
   }
}

int autorun()
{
   vector<S_VEHICLE> myVehicles;

   Car* carOne = new Car();
   S_VEHICLE vehStruct;
   vehStruct.classname = "Car";
   vehStruct.obj = (Vehicle*)carOne;
   myVehicles.push_back(vehStruct);

   carOne = new Car();
   vehStruct.classname = "Car";
   vehStruct.obj = (Vehicle*)carOne;
   myVehicles.push_back(vehStruct);

   carOne = new Car();
   vehStruct.classname = "Car";
   vehStruct.obj = (Vehicle*)carOne;
   myVehicles.push_back(vehStruct);

   Bicycle* bicycleOne = new Bicycle();
   vehStruct.classname = "Bicycle";
   vehStruct.obj = (Vehicle*)bicycleOne;
   myVehicles.push_back(vehStruct);

   for_each(myVehicles.begin(), myVehicles.end(), retString);

   S_VEHICLE sv = myVehicles.at(1);
   Car * c = (Car *) sv.obj;

   // test about problems calling derrived member
   Car * c2 = new Car();
   c2->justForFun(); // did not work either
   c2->unboard(); // this works

   c->setProperty(Vehicle::vehicleFlags::Leased, true);
   bool bLeased = c->getProperty(Vehicle::vehicleFlags::Leased);
   cout << "(1) Car is leased : " << bLeased << " ." << endl;

   c->setProperty(Vehicle::vehicleFlags::Leased, false);
   bLeased = c->getProperty(Vehicle::vehicleFlags::Leased);
   cout << "(2) Car is leased : " << bLeased << " ." << endl;

   return 0;
}

/**
 * summary : This outputs the commandlines arguments
 */
void printParam(int argc, char* argv[])
{
   cout << "The commandline parameters:" << endl;
   for (int i = 0; i < argc; i++)
   {
      cout << " " << i << ": " << argv[i] << endl;
   }
}

/**
 * summary :
 */
void retString(S_VEHICLE vehContainer)
{
   string typeName = vehContainer.classname;

   if (typeName == "Car")
   {
      Car* c = (Car*)vehContainer.obj;
      cout << c->returnAsString() << endl;
   }
   else if (typeName == "Bicycle")
   {
      Bicycle* b = (Bicycle*)vehContainer.obj;
      cout << b->returnAsString() << endl;
   }
   else
   {
      cout << "ERROR: Could not determine type of object!" << endl;
   }

   if (false)
   {
      string s = typeid(vehContainer).name(); // GCC : include <typeinfo>
      cout << "DEBUG " << s << endl;
   }
}

/**
 * file : /vehicles231/main.cpp [file 20151024°0721]
 * summary : This module provides a keyboard menu to call functionalities.
 *   This project demonstrates polymorphy, interface, commandline arguments
 * authors : Norbert C. Maier
 * license : MIT
 * encoding : UTF-8-with-BOM
 * see : www.cplusplus.com/reference/algorithm/for_each/ [ref 20200203°0714]
 */


/**
 * TODO
 * - Provide a vector of type Vehicles and store some
 *    polymorphic objects of types Car and Bicycles in it.
 * - Iterate over the vector and handle the objects. E.g:
 *  - apply toString()
 *  - accelerate
 *  - steer
 *  - brake
 * - To exercise bit wise logic, provide three flags, implemented as three bits
 *    in one int variable.
 */
