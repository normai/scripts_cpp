﻿/**
 * file : /vehicles231/Bicycle.cpp [file 20151024°0811]
 * summary : This file holds the Bicycle class definition
 * encoding : UTF-8-with-BOM
 */

#include "Bicycle.h"

Bicycle::Bicycle() : mounted(false) { }

Bicycle::~Bicycle() { }

void Bicycle::mount()
{
   if (this->mounted)
   {
      cout << "Cannot mount, is already mounted." << endl;
   }
   else
   {
      this->mounted = true;
   }
}

void Bicycle::unmount()
{
   if (this->mounted)
   {
      this->mounted = false;
   }
   else
   {
      cout << "Cannot unmount, not yet mounted." << endl;
   }
}

bool Bicycle::getMountStatus()
{
   return this->mounted;
}

string Bicycle::returnAsString()
{
   string testString = "This is a bicycle.";
   return testString;
}
