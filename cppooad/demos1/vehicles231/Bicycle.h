﻿/**
 * file : /vehicles231/Bicycle.h [file 20151024°0821]
 * summary : This file holds the Bicycle class declaration
 * encoding : UTF-8-with-BOM
 */

#ifndef BICYCLE_H      // include guard
#define BICYCLE_H

#include "Vehicle.h"

class Bicycle :
   public Vehicle
{
public:
   Bicycle();
   ~Bicycle();

   void mount();
   void unmount();
   bool getMountStatus();

   string returnAsString();

private:
   bool mounted;
};

#endif // BICYCLE_H
