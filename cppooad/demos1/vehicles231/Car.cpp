﻿/**
 * file : /vehicles231/Car.cpp [file 20151024°0831]
 * summary : This file holds the Car class definition
 * encoding : UTF-8-with-BOM
 */

#include "Car.h"

Car::Car() : boarded(false)
{
   //this->justForFun();
   //Vehicle::justForFun();
}

Car::~Car()
{
}

void Car::board()
{
   if (this->boarded)
   {
      cout << "Cannot board, is already done." << endl;
   }
   else
   {
      this->boarded = true;
   }
}

void Car::unboard()
{
   if (this->boarded)
   {
      this->boarded = false;
   }
   else
   {
      cout << "Cannot unboard, not yet boarded." << endl;
   }
}

bool Car::getBoarded()
{
   return this->boarded;
}

string Car::returnAsString()
{
   string testString = "This is a Car.";
   return testString;
}
