﻿/**
 * file : /vehicles231/IOutput.h [file 20151024°0751]
 * summary : This file demonstrates an interface definition
 * encoding : UTF-8-with-BOM
 */

#ifndef IOUTPUT_H
#define IOUTPUT_H

#include <string> // C++ Standard Library

using namespace std;

class IOutput
{
 public:
   IOutput(){}
   virtual ~IOutput(){}

   virtual string returnAsString() = 0;
};

#endif // IOUTPUT_H
