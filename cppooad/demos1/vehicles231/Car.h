/**
 * file : /vehicles231/Car.h [file 20151024°0841]
 * summary : This file holds the Car class declaration
 * encoding : UTF-8-with-BOM
 */

#ifndef CAR_H
#define CAR_H

#include "IOutput.h"
#include "Vehicle.h"

class Car :
   public Vehicle, public IOutput
{
public:
   Car();
   ~Car();

   void board();
   void unboard();
   bool getBoarded();

   string returnAsString();

private:
   bool boarded;
};

#endif // CAR_H
