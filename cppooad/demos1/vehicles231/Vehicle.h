﻿/**
 * file : /vehicles231/Vehicle.h [file 20151024°0741]
 * summary : This file holds the Vehicle class declaration.
 * encoding : UTF-8-with-BOM
 * note : This enum definition works only with the compiler set for C++11
 * ref : msdn.microsoft.com/en-us/library/2dzy4k6e.aspx [ref 20160219o0613]
 * ref : stackoverflow.com/questions/2503807/declaring-an-enum-within-a-class [ref 20160219o0612]
 */

// include guards
#ifndef VEHICLE_H
#define VEHICLE_H

#include <iostream>
#include <string>

using namespace std;

class Vehicle
{
protected:
   Vehicle();
   Vehicle(string);
   virtual ~Vehicle();

public:
   // void accelerate(int km);
   // int getSpeed() const;

   enum vehicleFlags // C++11 as opposed to 'enum class vehicleFlags'
   {
      Registered = 0,
      ReadyToGo = 5,
      Leased = 7
   };

   bool getProperty(vehicleFlags flag);
   void setProperty(vehicleFlags flag, bool val);
   bool toggleProperty(vehicleFlags flag);

   void justForFun();

   string vehiclename {"x"};

private:

   int speed;

   /**
    * summary : flagStorage is used to store some boolean properties
    *            inside one int to execise bit wise logic.
    *
    * Bit 0: Registered
    * Bit 5: ReadyToGo
    * Bit 10: Leased
    */
   unsigned int flagStorage;

};

#endif // VEHICLE_H
