﻿

   This is demonstrating the PIMPL idiom after code example
   by Joseph Mansfield on cpppatterns.com/patterns/pimpl.html

   The original example is so terse it provides no main function to see
   a use case. Here, a main function is supplemented and a method to get
   the internal value from the outside.

   The supplemented lines are recognized by '////'.

   References :
   • https://cpppatterns.com/patterns/pimpl.html [ref 20200113°1441]
   • https://github.com/sftrabbit/CppPatterns-Patterns/blob/master/common-tasks/classes/pimpl.cpp [ref 20200113°1451]

   ————————————————————————————————————————————————————————————

   project 20200109°0511 'pimpl'
   title       : Pimpl Demo
   summary     :
   status      : draft
   ܀

   ———————————————————————
   [file 20200109°0512] ܀Ω
