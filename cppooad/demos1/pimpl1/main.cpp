#include <iostream>
#include "foo.h"

int main()
{
    std::cout << "*** This is PIMPL demo.\n";

    foo f1;

    foo* f2 = new foo();

    int i1 = f1.impl_get_internal_data();
    int i2 = f2->impl_get_internal_data();
    std::cout << "Main : internal_data i1 = " << i1 << std::endl;
    std::cout << "Main : internal_data i2 = " << i2 << std::endl;

    std::cout << "*** Press Enter to exit .." << std::endl;
    std::cin.peek();
}

/**
 * file : /pimpl/pimpl.cpp [file 20200109�0521]
 */
