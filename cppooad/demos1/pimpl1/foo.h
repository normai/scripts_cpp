#include <memory>

class foo
{
public:
   foo();
   ~foo();
   foo(foo&&);
   foo& operator=(foo&&);
   int impl_get_internal_data(); ////
private:
   class impl;
   std::unique_ptr<impl> pimpl;
};

/**
 * file : /pimpl/pimpl.h [file 20200109�0541]
 */
