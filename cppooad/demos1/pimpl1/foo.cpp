// foo.cpp - implementation file

#include <iostream>
#include "foo.h"

class foo::impl
{
public:
   void do_internal_work()
   {
      internal_data = 5;
      std::cout << "Ctor foo internal_data = " << internal_data << std::endl; ////
   }
   int tell_internal_data() { return internal_data; }; //// can be changed without changes in foo.h
private:
   int internal_data = 0;
};

foo::foo()
   : pimpl { std::make_unique<impl>() }  // using C++14 std::unique_ptr
{
   pimpl->do_internal_work();
}

foo::~foo() = default;

foo::foo(foo&&) = default;  // C++11 '&&' r-value reference

foo& foo::operator=(foo&&) = default;

int foo::impl_get_internal_data() { return this->pimpl->tell_internal_data() ; }; ////

/**
 * file : /pimpl/foo.cpp [file 20200109�0531]
 * summary : Implementation
 * See en.cppreference.com/w/cpp/memory/unique_ptr [ref 20200206�1416]
 * See stackoverflow.com/questions/4549151/c-double-address-operator [ref 20200206�1412]
 * See stackoverflow.com/questions/5481539/what-does-t-double-ampersand-mean-in-c11 [ref 20200206�1414]
 */
