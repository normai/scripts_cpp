﻿#define _CRT_SECURE_NO_WARNINGS // use localtime anyway
#include <chrono>       // C++11
#include <ctime>        // std::time_t
#include <iomanip>      // std::setw()
#include <iostream>
#include <string>
#include <vector>

struct Entry {
   std::tm timepoint;
   unsigned int iDeposit;
   unsigned int iPayout;
   unsigned int iBalance;
   std::string sText;
};

std::tm getNow() {
   // (compare project 20200203°0111 'times')
   std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
   std::time_t now_c = std::chrono::system_clock::to_time_t(tp);
   std::tm now_tm = *std::localtime(&now_c);
   return now_tm;
}

std::string tm2s(std::tm tim) {
   char buff[99];
   strftime(buff, sizeof buff, "%A %c", &tim); // needs paranoia, see ref 20200206°1352
   std::string s(buff);
   return s;
}

class Account {
 protected :
   std::vector<Entry> entries;
   long int balance = {0}; // cent
 public :
   virtual void deposit(unsigned int iAmount) {
      balance += iAmount;
      std::tm now = getNow();
      Entry e = { now, iAmount, 0, balance, "Pay in" };
      entries.push_back(e);
   };
   virtual void payout(unsigned int iAmount) {
      balance -= iAmount;
      std::tm now = getNow();
      Entry e = { now, 0, iAmount, balance , "Pay out" };
      entries.push_back(e);
   };
   void printAccountStatement() {
      std::cout << "Account statement for " << std::endl;
      for (auto const& e : entries) {
         std::cout
            << " | " << std::setw(11) << std::right << double(e.iDeposit)/100
            << " | " << std::setw(11) << double(e.iPayout) / 100
            << " | " << std::setw(11) << double(e.iBalance) / 100
            << " | " << std::setw(16) << std::left << e.sText
            << " | " << tm2s(e.timepoint)
            << " |"  << std::endl;
      }
   }
};

class BankAccount : virtual public Account {
};

class WireAccount : virtual public Account {
   void deposit(int iAmount) {
      balance += balance - 20;
   }
   void payout(int iAmount) {
      balance -= balance - 20;
   }
};

class CheckingAccount : public BankAccount, public WireAccount {
};

class SavingsAccount : public BankAccount, public WireAccount {
   void deposit(int iAmount) {
      balance += balance;
   }
   void payout(int iAmount) {
      balance -= balance;
   }
};

class CashAccount : virtual public Account {
};

int main()
{
   std::cout << "*** Dynamicast." << std::endl;

   std::cout << "Time = " << tm2s(getNow()) << std::endl;

   Account* acc = nullptr;                     // virtual
   BankAccount* accBank = nullptr;             // virtual
   WireAccount* accWire = nullptr;             // virtual
   CashAccount* accCash = new CashAccount;     // concrete
   SavingsAccount* accSaving = nullptr;        // concrete
   CheckingAccount accChecking;                // concrete

   accCash->deposit(12345);
   accCash->payout(2345);
   accCash->deposit(1100);

   // upcast
   acc = dynamic_cast<Account*>(&accChecking); // not really necessary
   acc = &accChecking;                         //

   // downcast
   accBank = dynamic_cast<BankAccount*>(acc);

   // crosscast -- will not work
   //accCash = dynamic_cast<WireAccount*>(accBank);

   // results in nullptr
   accSaving = dynamic_cast<SavingsAccount*>(acc);

   accCash->printAccountStatement();

   std::cout << "*** Press Enter to exit." << std::endl;
   std::cin.get();
}

/*
 * project : dynamicast [proj 20200202°1211]
 * file : /typecasting/dynamicast.cpp [file 20200202°1221]
 * encoding : UTF-8-with-BOM
 * ref : Loudon and Grimm 2018, subchapter 'dynamic_cast', p.82-84
 * see diagram 20200202o1311.accounting.uxf
 */
