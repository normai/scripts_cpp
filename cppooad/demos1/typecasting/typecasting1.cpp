﻿#include <iostream>
using namespace std;

int redouble(int i) { return i + i; }

int main()
{
   std::cout << "*** Some type casting ***" << std::endl;

   std::cout << "(1) Implicit conversions :" << std::endl;
   short int si = 1111;
   char c = si;                        // compiler warning 'possible loss of data'
   long int li1 = c;
   long int li2 = si;
   double d = si;
   int iX1 = 1.23;                     // compiler warning 'possible loss of data'
   //int iX2 = { 1.23 };               // compiler error 'narrowing conversion'

   cout << "si   = " << si << "      " << sizeof(si) << endl;
   cout << "c    = " << c << "         " << sizeof(c) << endl;
   cout << "li1  = " << li1 << "        " << sizeof(li1) << endl;
   cout << "li2  = " << li2 << "      " << sizeof(li2) << endl;
   cout << "d    = " << d << "      " << sizeof(d) << endl;
   cout << "iX1  = " << iX1 << "         " << sizeof(iX1) << endl;
   cout << endl;

   std::cout << "(2) Implicit function parameter conversion" << std::endl;
   double d2 = redouble(2.3);
   cout << "redouble(2.3) = " << d2 << endl;
   cout << endl;

   std::cout << "(3) Classic explicit cast operator and functional notation" << std::endl;
   int iX3 = { (int) 1.23 };           // explicit
   int iX4 = { int(1.23) };
   cout << "iX3    = " << iX3 << "       " << sizeof(iX3) << endl;
   cout << "iX4    = " << iX4 << "       " << sizeof(iX4) << endl;
   cout << endl;

   std::cout << "(4) Explicit typecast functions" << std::endl;

   // (4.1) static_cast
   double d41 = 3.14159265;
   int i41= static_cast<int>(d41);
   double i42 = static_cast<double>(i41);
   cout << " - d41 =" << d41 << "; i41 = " << i41 << "; i42 = " << i42 << std::endl;
   cout << std::endl;

   // (4.2) dynamic_cast
   // (4.3) const_cast
   // (4.4) reinterpret_cast

   // (5) std::chrono::duration_cast

   // (6) userdefined by cast operator overload

   // (7) userdefined by constructor parameter


   cout << "*** Press Enter to exit." << std::endl;
   cin.get();
}

/*
 * project : typecasting1 [proj 20200202°1121]
 * file : /typecasting/typecasting1.cpp [file 20200202°1131]
 * encoding : UTF-8-with-BOM
 * ref : https://stackoverflow.com/questions/12873919/why-compiler-allows-narrowing-conversions []
 */
