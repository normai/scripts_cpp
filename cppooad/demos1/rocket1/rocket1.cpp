#include <iostream>

class Rocket {
public:
   Rocket() { std::cout << "New Rocket, fuel = " << GetFuel() << std::endl; }
   float GetFuel() const { return fuel; }
   void SetFuel(float f) { fuel = f; }
private:
   float fuel = 0.0; // tons kerose and liquid hydrogen/oxygen for Saturn V
};


int main()
{
    std::cout << "Rocket is demonstrating Accessors." << std::endl;

    Rocket *r = new Rocket();

    r->SetFuel(2345.6F);

    std::cout << "Filled up, fuel = " << r->GetFuel() << " tons" << std::endl;

    std::cout << "Press Enter to exit." << std::endl;
    std::cin.peek();
}

/*
 * project : 20200201�0511
 * file : /rocket/rocket1.cpp [file 20200201�0521]
 * encoding : ANSI
 * see : https://en.wikipedia.org/wiki/Saturn_V [ref 20200201`0416]
 * see : https://www.sciencealert.com/here-s-the-massive-amount-of-fuel-it-takes-to-launch-a-rocket-into-space-measured-in-elephants [ref 20200201`0412]
 */
